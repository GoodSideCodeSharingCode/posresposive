﻿using GoodSide.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace GoodSide.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var context = new HttpContextWrapper(Context);

            if (HttpContext.Current.Items["AjaxPermissionDenied"] is bool)
            {
                HttpContext.Current.Response.StatusCode = 501;
                HttpContext.Current.Response.End();

            }

        }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            POSLogger.Error(exception);
        }
    }
}
