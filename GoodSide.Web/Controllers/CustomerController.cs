﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GoodSide.Domain.Services;
using GoodSide.Web.Models.Customer;
using AutoMapper;
using GoodSide.Web.Helpers;
using GoodSide.Web.Models.POS;
using System.Web.Script.Serialization;
using GoodSide.Domain.Helpers;
using System.Configuration;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace GoodSide.Web.Controllers
{
    public class CustomerController : BaseController
    {
        private ICustomerService _customerService;
        private IOrderMasterService _orderMasterService;
        private IProductService _productService;
        private IPOSService _posService;
        private IPaymentInfoService _paymentInfoService;
        private IVoucherService _voucherService;
        private IAccountUserService _actUserService;
        private IKeyValueItemService _keyValueItemService;
        private static bool isRun = false;
        private static readonly object getInfoLock = new object();

        public CustomerController(ICustomerService customerService, IOrderMasterService orderMasterService, IKeyValueItemService keyValueItemService, IPOSService posService, IPaymentInfoService paymentInfoService, IAccountUserService actUserService, IProductService productService, IVoucherService voucherService)
        {
            _customerService = customerService;
            _orderMasterService = orderMasterService;

            _posService = posService;
            _paymentInfoService = paymentInfoService;

            _actUserService = actUserService;
            _keyValueItemService = keyValueItemService;

            _productService = productService;
            _voucherService = voucherService;
        }

        // GET: StockDetail
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTableData(int draw, int start, int length, string CustActive)
        {
            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }


            int sortColumn = -1;
            string sortDirection = "DESC";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);
            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(CustomerViewItem).GetProperties().ToList();


            //if (string.IsNullOrEmpty(search))
            //{
            //    search = "%%";
            //}
            //else
            //{
            //    search = "%" + search + "%";
            //}

            //search = HttpUtility.UrlEncode(search);
            var result = _customerService.GetAll(search.Trim(), start, length, lstProperties[sortColumn].Name, sortDirection, "customerapi", CustActive);

            DataTableDataCustomer dataTableData = new DataTableDataCustomer();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().total;
            dataTableData.data = result.customerSearchItemInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().total;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetViewOrderTableData(int draw, int start, int length)
        {
            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }

            string customerId = "";
            string Orderstatus = "";
            if (HttpContext.Request.QueryString["customerid"] != null)
            {
                customerId = HttpContext.Request.QueryString["customerid"];
            }
            if (HttpContext.Request.QueryString["Ordstatus"] != null)
            {
                Orderstatus = HttpContext.Request.QueryString["Ordstatus"];
            }


            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);

            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(CustomerViewOrderModel).GetProperties().ToList();

            var sort = string.Empty;
            if (lstProperties[sortColumn].Name == "orderno")
                sort = "o.orderno";
            else if (lstProperties[sortColumn].Name == "oodate")
                sort = "o.orderdate";
            else if (lstProperties[sortColumn].Name == "ordstatus")
                sort = "o.orderstatus";
            else
                sort = lstProperties[sortColumn].Name;


            if (string.IsNullOrEmpty(search))
            {
                search = "%%";
            }
            else
            {
                search = "%" + search + "%";
            }

            //search = HttpUtility.UrlEncode(search);
            var result = _customerService.GetOrdersByCustomer(search, start, length, sort, sortDirection, customerId, Orderstatus);

            DataTableDataCustomervieworder dataTableData = new DataTableDataCustomervieworder();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().total;
            dataTableData.data = result.CustomerVieworderInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().total;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetViewAllOrderTableData(int draw, int start, int length)
        {
            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }

            string customerId = "";
            string Orderstatus = "";
            if (HttpContext.Request.QueryString["customerid"] != null)
            {
                customerId = HttpContext.Request.QueryString["customerid"];
            }
            if (HttpContext.Request.QueryString["Ordstatus"] != null)
            {
                Orderstatus = HttpContext.Request.QueryString["Ordstatus"];
            }


            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);

            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(CustomerViewOrderModel).GetProperties().ToList();

            var sort = string.Empty;
            if (lstProperties[sortColumn].Name == "orderno")
                sort = "o.orderno";
            else if (lstProperties[sortColumn].Name == "oodate")
                sort = "o.orderdate";
            else if (lstProperties[sortColumn].Name == "ordstatus")
                sort = "o.orderstatus";
            else
                sort = lstProperties[sortColumn].Name;


            if (string.IsNullOrEmpty(search))
            {
                search = "%%";
            }
            else
            {
                search = "%" + search + "%";
            }

            //search = HttpUtility.UrlEncode(search);
            var result = _customerService.GetAllOrdersByCustomer(search, start, length, sort, sortDirection, customerId, Orderstatus);

            DataTableDataCustomervieworder dataTableData = new DataTableDataCustomervieworder();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().total;
            dataTableData.data = result.CustomerVieworderInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().total;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetSalesPersons()
        {
            var listItems = _keyValueItemService.GetAllSalesPerson().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }
        public List<SelectListItem> GetCountryList()
        {
            var listItems = _keyValueItemService.GetAllCountryList().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }

        public List<SelectListItem> GetCountyList()
        {
            var listItems = _keyValueItemService.GetAllCountyList().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }

        public List<SelectListItem> GetAboutUsList()
        {
            var listItems = _keyValueItemService.GetAboutUsList().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }

        public List<SelectListItem> GetFinancialList()
        {

            var listItems = _keyValueItemService.GetFinancialList().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }

        public List<SelectListItem> GetDeliveryTypeList()
        {
            var listItems = _keyValueItemService.GetDeliveryTypeList().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }

        //private List<SelectListItem> returnList(GoodSide.Domain.Entities.NameValueItem listItems)
        //{
        //    var selectListItemList = new List<SelectListItem>();
        //    foreach (GoodSide.Domain.Entities.NameValueItemInfo obj in listItems.nameValueItemInfo)
        //    {
        //        var item = new SelectListItem();
        //        item.Value = obj.id;
        //        item.Text = obj.name;
        //        selectListItemList.Add(item);
        //    }
        //    return selectListItemList;
        //}

        public CustomerViewModel GetCustomerEdit(string customerId)
        {
            var result = _customerService.GetCustomerEdit(customerId);
            CustomerViewModel custInfoModel = new CustomerViewModel();
            Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
            custInfoModel = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(result);
            return custInfoModel;
        }

        public GoodSide.Domain.Entities.CustomerItem GetCustomerById(string customerId)
        {
            var result = _customerService.GetCustomerById(customerId);
            return result;
        }

        public string GetNewCustomerCode()
        {
            var code = _customerService.GetNewCustomerCode();
            return code;
        }

        public JsonResult PlaceOrder(string inputpassword, string customeremail, bool isOrderChecked, string ordercomment, string salesperson)
        {
            StringBuilder errorDetail = new StringBuilder(500);
            if (validateOrder(ordercomment, out errorDetail))
            {

                OrderMaster ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                ordMaster.salespersonid = salesperson;
                ClientSession.SetSession(SessionConstants.POSOrderMaster, ordMaster);
                string UserId = string.Empty;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["isAuthenticateRequire"]))
                {
                    if (string.IsNullOrEmpty(inputpassword))
                    {
                        return Json(new { success = false, message = "Please enter password for save order" });
                    }
                    var companyID = Convert.ToInt32(ClientSession.GetSession(SessionConstants.companyid));

                    var sysUsr = _actUserService.GetLoggedInUser(inputpassword, companyID);
                    if (sysUsr == null)
                    {
                        return Json(new { success = false, message = "Invalid Password" });
                    }
                    UserId = sysUsr.UserId.ToString();
                }
                else
                {
                    UserId = ClientSession.GetSession(SessionConstants.userid).ToString();
                }

                OrderMaster orderMain = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);

                if (String.IsNullOrEmpty(orderMain.salespersonid) || orderMain.salespersonid == "0")
                {
                    return Json(new { success = false, message = "Please select sales person for order" });
                }

                if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) == null || ((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts)).Count == 0)
                    return Json(new { success = false, message = "Please select products" });


                Domain.Entities.GeneralRuleResult placeorderresult = PlaceOrderDetails((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts), orderMain, orderMain.orderstatus == 6 ? "6" : "4");

                if (placeorderresult.status == "success")
                {
                    string generatedOrderId = placeorderresult.outputvalue;
                    if (!String.IsNullOrEmpty(generatedOrderId)) //orderId
                    {
                        OrderMaster posorderMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                        posorderMaster.comments = ordercomment;
                        GoodSide.Domain.Entities.OrderPayItem orderPaymentItem = new Domain.Entities.OrderPayItem();
                        orderPaymentItem.amount = String.Format("{0:0.00}", posorderMaster.totalpaid);
                        orderPaymentItem.authorized = "1";
                        orderPaymentItem.cntotal = Math.Round(posorderMaster.creditnote, 2).ToString();
                        orderPaymentItem.date = DateTime.Today.Date.ToString("dd-MM-yyyy");

                        orderPaymentItem.description = posorderMaster.comments;
                        orderPaymentItem.orderId = generatedOrderId;
                        if (!String.IsNullOrEmpty(posorderMaster.paymentmode))
                            orderPaymentItem.pmode = posorderMaster.paymentmode;
                        else
                            orderPaymentItem.pmode = "5"; //need to verify for other cases

                        orderPaymentItem.reference = posorderMaster.comments;
                        orderPaymentItem.showmaponreceipt = "1";
                        orderPaymentItem.voucheramount = posorderMaster.giftvoucher.ToString();
                        orderPaymentItem.voucherlists = posorderMaster.vid.ToString();
                        orderPaymentItem.useridnew = UserId;

                        orderPaymentItem.cash = String.Format("{0:0.00}", posorderMaster.cash);
                        orderPaymentItem.visa = String.Format("{0:0.00}", posorderMaster.visa);
                        orderPaymentItem.mastercard = String.Format("{0:0.00}", posorderMaster.mastercard);
                        orderPaymentItem.laser = String.Format("{0:0.00}", posorderMaster.laser);
                        orderPaymentItem.cheque = String.Format("{0:0.00}", posorderMaster.cheque);
                        orderPaymentItem.paypal = String.Format("{0:0.00}", posorderMaster.paypal);

                        orderPaymentItem.visadetail = posorderMaster.visadetail;
                        orderPaymentItem.cashdetail = posorderMaster.cashdetail;
                        orderPaymentItem.chequedetail = posorderMaster.chequedetail;
                        orderPaymentItem.mastercarddetail = posorderMaster.mcdetail;
                        orderPaymentItem.gvdetail = posorderMaster.gvdetail;
                        orderPaymentItem.cndetails = posorderMaster.creditnotedetail;
                        orderPaymentItem.paypaldetail = posorderMaster.paypaldetail;
                        orderPaymentItem.amount_tendered = posorderMaster.amount_tendered;

                        string cndetailtable = string.Empty;
                        if (posorderMaster.creditnote > 0)
                        {
                            if (ClientSession.GetSession(SessionConstants.POSCreditNoteItem) != null)
                            {
                                List<CreditNoteListViewModel> tempcreditNoteList = (List<CreditNoteListViewModel>)ClientSession.GetSession(SessionConstants.POSCreditNoteItem);
                                foreach (CreditNoteListViewModel creditenoteitem in tempcreditNoteList)
                                {
                                    cndetailtable = cndetailtable + creditenoteitem.cnno + ":" + creditenoteitem.amountpaid + "|";
                                }
                            }
                        }
                        orderPaymentItem.cndetailtable = cndetailtable;

                        POSLogger.Info("Call order Pay save");
                        GoodSide.Domain.Entities.GeneralRuleResult ruleResult = _orderMasterService.OrderPaySave(orderPaymentItem);
                        POSLogger.Info("finish order Pay save with ruleResult:" + ruleResult.status);
                        if (ruleResult != null && ruleResult.status == "success")
                        {
                            POSLogger.Info(String.Format("finish order Pay order ID:{0}: OrderNumber:{1}", posorderMaster.orderid, posorderMaster.temporderid));
                            SendOrderEmail(orderMain.temporderid, customeremail, isOrderChecked);
                            //Once Complete Order Clear Session Info
                            ClientSession.SetSession(SessionConstants.customerId, null);
                            ClientSession.SetSession(SessionConstants.POSOrderMaster, null);
                            ClientSession.SetSession(SessionConstants.POSSelectedProducts, null);
                            POSLogger.Info("clear sessions");
                            return Json(new { success = true, orderId = generatedOrderId, message = "Order created successfully." }, JsonRequestBehavior.AllowGet);
                        }
                        else if (ruleResult != null && (ruleResult.status == "warning" || ruleResult.status == "error"))
                        {
                            return Json(new { success = false, orderId = generatedOrderId, message = ruleResult.outputvalue }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, orderId = generatedOrderId, message = "error in processing payment" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                return Json(new { success = false, orderId = "", message = "Confirm Order Error !! Detail:-" + placeorderresult.outputvalue }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false, orderId = "", message = "Confirm Order Error !! " + Environment.NewLine + errorDetail.ToString() }, JsonRequestBehavior.AllowGet);
        }

        private bool validateOrder(string comment, out StringBuilder errorDetail)
        {
            var isSuccess = true;
            errorDetail = new StringBuilder();
            bool IsDelivery = false;
            bool IsDeliveryType = false;
            bool ConfigDeliveryType = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsCheckDeliveryTypeComment"] != null ? (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["IsCheckDeliveryTypeComment"])) : true);
            var OrderdetailSession = (List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts);

            foreach (var item in OrderdetailSession)
            {
                if (item.DestinationPoint == 0)
                {
                    IsDelivery = true;
                    break;
                }
                if(item.deliverytype==1)
                {
                    IsDeliveryType = true;
                    break;
                }
            }
            if (IsDelivery)
            {
                if (comment == string.Empty)
                {
                    errorDetail.Append("Error : One Of the Product in Order has a missing Delivery/Collection Point, Please Set Delivery/Collection Point before save order " + Environment.NewLine);
                    isSuccess = false;
                }
            }
            if (ConfigDeliveryType)
            {
                if (IsDeliveryType)
                {
                    if (comment == string.Empty)
                    {
                        errorDetail.Append("One Of the Product in Order has a Type as Delivery, Please Set Delivery Comments before save order" + Environment.NewLine);
                        isSuccess = false;
                    }
                }
            }
            return isSuccess;
        }
        public void SendOrderEmail(string orderid, string emailto, bool isChecked)
        {
            try
            {

                if (!String.IsNullOrEmpty(emailto) && !String.IsNullOrEmpty(orderid) && isChecked)
                {
                    POSLogger.Info("Order Email Start:" + orderid + " To:" + emailto);
                    string smtpAddress = ConfigurationManager.AppSettings["smtpAddress"].ToString();// "smtp.elive.net";
                    int portNumber = Convert.ToInt32(ConfigurationManager.AppSettings["portNumber"].ToString()); //587;
                    bool enableSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["enableSSL"].ToString()); //true
                    string smtpUsername = ConfigurationManager.AppSettings["smtpUsername"].ToString();//"smtp@ezliving.ie";
                    string smtpPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();// "0sdIw13y5H";
                    string emailFrom = ConfigurationManager.AppSettings["emailFrom"].ToString();//"Horizon@ezliving.ie";
                    string emailsubjectCompanyName;
                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["emailsubjectcompanyname"].ToString()))
                        emailsubjectCompanyName = ConfigurationManager.AppSettings["emailsubjectcompanyname"].ToString() + " - " + ClientSession.GetSession(SessionConstants.storename).ToString(); //ConfigurationManager.AppSettings["emailsubjectcompanyname"].ToString();//"Horizon@ezliving.ie";
                    else
                        emailsubjectCompanyName = ClientSession.GetSession(SessionConstants.storename).ToString(); //ConfigurationManager.AppSettings["emailsubjectcompanyname"].ToString();//"Horizon@ezliving.ie";

                    StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/OrderTemplate.htm"));

                    string readFile = reader.ReadToEnd();
                    string bodyMessage = string.Empty;
                    _customerService.SendOrderEmail(orderid, string.Empty, readFile, emailsubjectCompanyName, emailFrom, emailto, enableSSL, smtpAddress, portNumber, smtpUsername, smtpPassword);
                }
            }
            catch (Exception ex)
            {
                POSLogger.Error("Error:-SendOrderEmail" + ex.ToString());
            }
        }

        public Domain.Entities.GeneralRuleResult PlaceOrderDetails(List<OrderDetail> lstord, OrderMaster posorderMaster, string orderStatus)
        {
            POSLogger.Info(String.Format("call PlaceOrderDetails ID:{0}: OrderNumber:{1}", posorderMaster.orderid, posorderMaster.temporderid));
            Mapper.CreateMap<OrderMaster, GoodSide.Domain.Entities.OrderMaster>();
            var poMasterEntity = Mapper.Map<OrderMaster, GoodSide.Domain.Entities.OrderMaster>(posorderMaster);
            poMasterEntity.discgiven_base = lstord.Sum(x => x.discgiven_base);
            poMasterEntity.netpayment = Math.Round(lstord.Sum(x => (x.slprice_base * x.qty) - x.discgiven_base), 2);
            var result = _orderMasterService.Create(poMasterEntity, orderStatus);

            if (result.status == "success")
            {
                string orderid = result.outputvalue;// generated orderId
                string ordernumber = result.outputvalue2;// generated ordernumber if get changed.
                POSLogger.Info(String.Format("result generated order ID:{0}: OrderNumber:{1}", result.outputvalue, result.outputvalue2));

                if (!string.IsNullOrEmpty(orderid))
                {
                    posorderMaster.orderid = orderid;
                    posorderMaster.temporderid = ordernumber;
                    ClientSession.SetSession(SessionConstants.POSOrderMaster, posorderMaster);
                    var lst = lstord.Select(x => new { x.pid, x.uniqueid }).Distinct();
                    foreach (var item in lst)
                    {
                        //iis = new System.Collections.Specialized.NameValueCollection();
                        string table = string.Empty;
                        decimal totaldiscgiven_base = 0;
                        OrderDetail orderDetInfo = new OrderDetail();
                        List<OrderDetail> lstOrderDetailsProcess = new List<OrderDetail>();
                        lstOrderDetailsProcess = lstord.Where(p => p.pid == item.pid && p.uniqueid == item.uniqueid).ToList();
                        foreach (OrderDetail ord in lstOrderDetailsProcess)
                        {
                            if (table == string.Empty)
                            {
                                orderDetInfo = (OrderDetail)ord;
                            }
                            table = table + ord.id + ":" + ord.qty.ToString() + "_" + ord.oversaleFlag.ToString() + "_" + ord.orderitemdetailid + "|";
                            totaldiscgiven_base = totaldiscgiven_base + Convert.ToDecimal(ord.discgiven_base.ToString());
                        }

                        //orderDetInfo.discgiven_base = totaldiscgiven_base;
                        orderDetInfo.table = table;

                        Mapper.CreateMap<OrderDetail, GoodSide.Domain.Entities.OrderDetail>();
                        var oDetailEntity = Mapper.Map<OrderDetail, GoodSide.Domain.Entities.OrderDetail>(orderDetInfo);
                        oDetailEntity.discgiven_base = totaldiscgiven_base;
                        var resultOrderItem = _orderMasterService.CreateOrderitem(oDetailEntity, orderid);
                        if (resultOrderItem.status != "success")
                        {
                            //throw new Exception("Status:" + resultOrderItem.status + "Detail:"+ resultOrderItem.outputvalue);
                            return resultOrderItem;
                        }
                        //item.OrderDetailId = Int32.Parse(resultOrderItem.outputvalue);
                        List<OrderDetail> lstOrderDetails = lstord.Where(x => x.pid == item.pid && x.uniqueid == item.uniqueid).ToList();


                        foreach (OrderDetail itemdetail in lstOrderDetails)
                        {
                            itemdetail.OrderDetailId = Int32.Parse(resultOrderItem.outputvalue);
                            var OrderitemdetailIds = resultOrderItem.outputvalue2.Split('|').ToList<string>();
                            foreach (var orderitemdetailid in OrderitemdetailIds)
                            {

                                string[] orderitemdetailresult = orderitemdetailid.Split(':');
                                if (itemdetail.id == orderitemdetailresult[1] && itemdetail.OrderDetailId == Int32.Parse(resultOrderItem.outputvalue))
                                    itemdetail.orderitemdetailid = Convert.ToInt32(orderitemdetailresult[0]);
                            }
                        }
                    }
                    ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstord);
                }
            }
            return result;
        }

        public JsonResult CreateNewOrder()
        {
            POSLogger.Info("Create New Order");
            //Session Clear
            ClientSession.SetSession(SessionConstants.customerId, null);
            ClientSession.SetSession(SessionConstants.POSOrderMaster, null);
            ClientSession.SetSession(SessionConstants.POSSelectedProducts, null);
            ClientSession.SetSession(SessionConstants.orderstatus, null);

            return this.Json(new { IsSuccess = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult CustomerSearch()
        {
            return PartialView();
        }

        public PartialViewResult PaymentInfo()
        {

            PaymentInfoViewModel model = new PaymentInfoViewModel();
            OrderCalculation(0, model);
            var ordermstr = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            model.isOrderChecked = Regex.Replace((ordermstr.customer).ToLower(), @"\s+", "") == "cashsale" ? false : true;
            model.showpasswordoption = false;
            var EntityId = ClientSession.GetSession(SessionConstants.entityid);
            model.SalesPersonList = new SelectList(_keyValueItemService.GetAllSalesPerson(), "id", "name");
            model.Entityid = EntityId == null ? model.Entityid : EntityId.ToString();
            string ChangeSalesPErsonCheck = (System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] != null ? System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] : string.Empty);

            if (string.IsNullOrEmpty(ChangeSalesPErsonCheck))
                model.IsSalesPersonChange = 0;
            else
                model.IsSalesPersonChange = Convert.ToInt32(ChangeSalesPErsonCheck);

            if (ordermstr.orderstatus == 6)
            {
                lock (getInfoLock)
                {
                    if (!isRun)
                    {
                        model.remainingpayment = ordermstr.amountdue.ToString();
                        model.changedue = ordermstr.amountdue;
                        model.discount = (ordermstr.discount * ordermstr.exchangerate);
                        model.subtotal = (ordermstr.netpayment + model.discount);
                        model.discount = (ordermstr.discount * ordermstr.exchangerate);
                        model.invoiceamount = (ordermstr.netpayment + ordermstr.misc).ToString();
                        model.paidamount = ordermstr.totalpaid == 0 ? (ordermstr.allreadypaidamount).ToString() : ordermstr.totalpaid.ToString();
                        isRun = true;
                    }
                }
            }
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["isAuthenticateRequire"]))
                model.showpasswordoption = true;

            model.BIRTReportURL = CommonMethods.BIRTReportUrl();
            return PartialView("_PaymentInfo", model);
        }

        public PartialViewResult VoucherSearch()
        {
            VoucherSearchInfo model = new VoucherSearchInfo();

            //  return Json(dataTableData, JsonRequestBehavior.AllowGet);
            return PartialView("_VoucherSearch", model);
        }

        public ActionResult GetTableDataVoucherDisplay(int draw, int start, int length)
        {
            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }


            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);
            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(PaymentVoucherListViewModel).GetProperties().ToList();


            //if (string.IsNullOrEmpty(search))
            //{
            //    search = "%%";
            //}
            //else
            //{
            //    search = "%" + search + "%";
            //}

            //search = HttpUtility.UrlEncode(search);
            var result = _paymentInfoService.GetAllVoucher(search, start, length, "", "");

            DataTableDataVoucher dataTableData = new DataTableDataVoucher();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().t;
            dataTableData.data = result.voucherSearchItemInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().t;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult VoucherDisplay()
        {
            PaymentVoucherViewModel model = new PaymentVoucherViewModel();
            return PartialView("_VoucherInfo", model);
        }

        public ActionResult AddGiftVoucher(String code, decimal remainingamount, decimal amount, int voucherid)
        {
            //Button tbnadd = (Button)sender;
            //HiddenField hdncode = (HiddenField)tbnadd.FindControl("hdncode");
            //HiddenField hdnremainingamount = (HiddenField)tbnadd.FindControl("hdnremainingamount");
            //HiddenField hdnid = (HiddenField)tbnadd.FindControl("hdnid");
            //HiddenField hdnexpirydate = (HiddenField)tbnadd.FindControl("hdnexpirydate");
            //TextBox txtAmount = (TextBox)tbnadd.FindControl("txtAmount");
            // string val = code;
            // string val1 = remainingamount;


            // decimal amount = 0;// model.amount;
            // string 
            //remainingamount = model.remainingamount;
            var hdnidValue = voucherid;
            var hdncodeValue = code;//hdncode.Value;
            if (Convert.ToDecimal(amount) > Convert.ToDecimal(remainingamount))
            {
                return this.Json(new { IsSuccess = false, Message = "Voucher has not enough amount to pay" }, JsonRequestBehavior.AllowGet);
            }

            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);

            if (sessionitemsOm != null)
            {
                if (Convert.ToDecimal(amount) > Convert.ToDecimal(sessionitemsOm.netpayment.ToString()))
                {

                    return this.Json(new { IsSuccess = false, Message = "The amount paid is bigger than the amount due!" }, JsonRequestBehavior.AllowGet);
                }

                sessionitemsOm.vid = int.Parse(hdnidValue.ToString());
                sessionitemsOm.giftvoucher = Convert.ToDecimal(amount);
                sessionitemsOm.gvdetail = "Voucher-" + hdncodeValue;
            }


            // GetTableDataVoucherDisplay(0,0,10);

            return this.Json(new { IsSuccess = true, Message = "Voucher " + hdncodeValue + " successfully added into order" }, JsonRequestBehavior.AllowGet);


        }

        //public PartialViewResult VoucherDisplay()
        //{
        //    PaymentVoucherViewModel model = new PaymentVoucherViewModel();


        //    string search = " ";
        //    if (HttpContext.Request.QueryString["search[value]"] != null)
        //    {
        //        search = HttpContext.Request.QueryString["search[value]"]; ;
        //    }

        //    var lstProperties = typeof(CustomerViewItem).GetProperties().ToList();


        //    if (string.IsNullOrEmpty(search))
        //    {
        //        search = "%%";
        //    }
        //    else
        //    {
        //        search = "%" + search + "%";
        //    }

        //    search = HttpUtility.UrlEncode(search);

        //    List<GoodSide.Domain.Entities.VoucherSearchItem> result = _voucherService.GetAll(search, 0, 13, "", "");

        //    var lstAvaibaleQuantities = new List<PaymentVoucherListViewModel>();

        //    Mapper.CreateMap<GoodSide.Domain.Entities.VoucherSearchItem, PaymentVoucherListViewModel>();

        //    lstAvaibaleQuantities = Mapper.Map<List<GoodSide.Domain.Entities.VoucherSearchItem>, List<PaymentVoucherListViewModel>>(result);

        //    model.Items = lstAvaibaleQuantities;


        //    return PartialView("_Voucher", model);
        //}

        public PartialViewResult CreditNoteSearch()
        {
            CreditNoteListViewModel model = new CreditNoteListViewModel();

            return PartialView("_CreditNote", model);
        }

        public JsonResult OrderCalculation(PaymentType ptype, PaymentInfoViewModel model)
        {
            var sessionObjOd = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            if (sessionObjOd == null)
            {
                return Json(new { success = false, message = "Please select items" });
            }

            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            List<OrderDetail> sessionitemsOd = ((List<OrderDetail>)sessionObjOd);


            if (sessionitemsOd != null & sessionitemsOd.Any())
            {
                decimal vatamount = 0, netpayment = 0, discount = 0,
                    cash = 0, subtotal = 0, visa = 0, giftvoucher = 0, remaining = 0, paid = 0,
                    mastercard = 0, cheque = 0, laser = 0, misc = 0, creditnote = 0, paypal = 0, priviouspaidamount = 0;

                string novoucherdetails = "";
                string customeremail = "";
                string comments = "";
                string salespersonid = "0";

                discount = sessionitemsOd.Sum(item => item.discgiven); // Convert.ToDecimal(tb.Compute("SUM(discgiven)", "discgiven IS NOT NULL"));
                vatamount = sessionitemsOd.Sum(item => item.vatamount);   // Convert.ToDecimal(tb.Compute("SUM(vatamount)", "vatamount IS NOT NULL"));
                netpayment = sessionitemsOd.Sum(item => item.netprice); //Convert.ToDecimal(tb.Compute("SUM(netprice)", "netprice IS NOT NULL"));
                if (sessionitemsOm != null)
                    misc = sessionitemsOm.misc;// Convert.ToDecimal(tbom.Rows[0]["misc"]);

                if (ptype == 0 && (sessionitemsOm != null))
                {
                    cash = Convert.ToDecimal(sessionitemsOm.cash);
                    visa = Convert.ToDecimal(sessionitemsOm.visa);
                    cheque = Convert.ToDecimal(sessionitemsOm.cheque);
                    mastercard = Convert.ToDecimal(sessionitemsOm.mastercard);
                    giftvoucher = Convert.ToDecimal(sessionitemsOm.giftvoucher);
                    laser = Convert.ToDecimal(sessionitemsOm.laser);
                    creditnote = Convert.ToDecimal(sessionitemsOm.creditnote);
                    paypal = Convert.ToDecimal(sessionitemsOm.paypal);
                    if (sessionitemsOm.orderstatus == 6)
                        priviouspaidamount = Convert.ToDecimal(sessionitemsOm.allreadypaidamount);
                    if (giftvoucher > 0)
                        novoucherdetails = sessionitemsOm.giftvoucher.ToString();// tbom.Rows[0]["gvdetail"].ToString();
                    else
                        novoucherdetails = "No voucher selected";
                    customeremail = sessionitemsOm.customeremail;
                    comments = sessionitemsOm.comments;
                    salespersonid = sessionitemsOm.salespersonid;
                }
                else
                {
                    cash = model.cash;
                    visa = model.visa;
                    giftvoucher = model.giftvoucher;
                    creditnote = model.creditnote;
                    mastercard = model.mastercard;
                    cheque = model.cheque;
                    laser = model.laser;
                    customeremail = model.customeremail;
                    paypal = model.paypal;
                    comments = model.description;
                    salespersonid = model.Entityid;
                }
                subtotal = Math.Round(discount + netpayment + misc, 2);
                paid = cash + visa + giftvoucher + creditnote + cheque + mastercard + laser + paypal + priviouspaidamount;
                remaining = netpayment + misc - paid;


                if (sessionitemsOm == null)
                {
                    var item = new OrderMaster();
                    item.customerid = "0";
                    item.salespersonid = "0";
                    item.misc = 0;
                    item.deliverycharge = 0;
                    item.vatamount = vatamount;
                    item.netpayment = netpayment;
                    item.discount = discount;
                    item.comments = model.description;
                    item.deliveryaddressid = 0;
                    item.cash = cash;
                    item.visa = visa;
                    item.cheque = cheque;
                    item.mastercard = mastercard;
                    item.giftvoucher = giftvoucher;
                    item.laser = laser;
                    item.creditnote = creditnote;
                    item.visadetail = model.description;
                    item.cashdetail = model.description;
                    item.gvdetail = model.voucherdetail;
                    item.creditnotedetail = model.description;
                    item.chequedetail = model.description;
                    item.mcdetail = model.description;
                    item.laserdetail = model.description;
                    item.totalpaid = Math.Round(paid, 2);
                    Domain.Entities.TemopraryOrderNumber orderdata = GetTemporaryOrderNumber();
                    item.temporderid = orderdata.ordernumber;
                    item.orderid = orderdata.orderid;
                    item.paypal = paypal;
                    item.paypaldetail = model.description;
                    if (ptype != 0)
                    {
                        item.paymentmode = ((int)(ptype)).ToString();
                    }

                    ClientSession.SetSession(SessionConstants.POSOrderMaster, item);
                    sessionitemsOm = item;
                }
                else
                {
                    // modify certain values into the DataTable
                    sessionitemsOm.netpayment = netpayment;
                    sessionitemsOm.discount = discount;
                    sessionitemsOm.salespersonid = salespersonid;
                    if (remaining < 0)
                    {
                        switch (ptype)
                        {
                            case PaymentType.visa:
                                sessionitemsOm.visa = (visa + remaining);
                                sessionitemsOm.amount_tendered = visa + model.cash + model.cheque + model.laser + model.mastercard + model.giftvoucher + model.creditnote + model.paypal;
                                break;
                            case PaymentType.mastercard:
                                sessionitemsOm.mastercard = (mastercard + remaining);
                                sessionitemsOm.amount_tendered = mastercard + model.cash + model.visa + model.cheque + model.laser + model.giftvoucher + model.creditnote + model.paypal;
                                break;
                            case PaymentType.laser:
                                sessionitemsOm.laser = (laser + remaining);
                                sessionitemsOm.amount_tendered = laser + model.cash + model.visa + model.cheque + model.mastercard + model.giftvoucher + model.creditnote + model.paypal;
                                break;
                            case PaymentType.cheque:
                                sessionitemsOm.cheque = (cheque + remaining);
                                sessionitemsOm.amount_tendered = cheque + model.cash + model.visa + model.laser + model.mastercard + model.giftvoucher + model.creditnote + model.paypal;
                                break;
                            case PaymentType.cash:
                                sessionitemsOm.cash = (cash + remaining);
                                sessionitemsOm.amount_tendered = cash + model.visa + model.cheque + model.laser + model.mastercard + model.giftvoucher + model.creditnote + model.paypal;
                                break;
                            case PaymentType.giftvoucher:
                                sessionitemsOm.giftvoucher = (giftvoucher + remaining);
                                sessionitemsOm.amount_tendered = giftvoucher + model.cash + model.visa + model.cheque + model.laser + model.mastercard + model.creditnote + model.paypal;
                                break;
                            case PaymentType.paypal:
                                sessionitemsOm.paypal = (paypal + remaining);
                                sessionitemsOm.amount_tendered = paypal + model.cash + model.visa + model.cheque + model.laser + model.mastercard + model.giftvoucher + model.creditnote;
                                break;
                            default: break;
                        }
                        sessionitemsOm.totalpaid = Math.Round(netpayment, 2);
                    }
                    else
                    {
                        sessionitemsOm.cash = cash;
                        sessionitemsOm.visa = visa;
                        sessionitemsOm.cheque = cheque;
                        sessionitemsOm.mastercard = mastercard;
                        sessionitemsOm.giftvoucher = giftvoucher;
                        sessionitemsOm.laser = laser;
                        sessionitemsOm.creditnote = creditnote;
                        sessionitemsOm.paypal = paypal;
                        sessionitemsOm.totalpaid = Math.Round(paid, 2);
                    }

                    switch (ptype)
                    {
                        case PaymentType.visa:
                            sessionitemsOm.visadetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.mastercard:
                            sessionitemsOm.mcdetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.laser:
                            sessionitemsOm.laserdetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.cheque:
                            sessionitemsOm.chequedetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.cash:
                            sessionitemsOm.cashdetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.giftvoucher:
                            sessionitemsOm.gvdetail = model.voucherdetail;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.creaditnote:
                            sessionitemsOm.creditnotedetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        case PaymentType.paypal:
                            sessionitemsOm.paypaldetail = model.description;
                            sessionitemsOm.comments = model.description;
                            break;
                        default: break;
                    }
                    //tbom.AcceptChanges();
                    if (ptype != 0)
                    {
                        sessionitemsOm.paymentmode = ((int)(ptype)).ToString();
                    }

                }


                if (remaining < 0)
                {
                    model.changedue = Math.Round(-1 * remaining, 2); //to do // String.Format("{0:0.00}", (-1 * remaining));
                }
                else
                    model.changedue = Math.Round(remaining, 2);//to do // txtChange.Text = String.Format("{0:0.00}", (remaining));


                ClientSession.SetSession(SessionConstants.POSOrderMaster, sessionitemsOm);

                model.cash = cash;// to do // String.Format("{0:0.00}", cash);
                model.visa = visa;// to do  String.Format("{0:0.00}", visa);
                model.cheque = cheque;// to do  String.Format("{0:0.00}", cheque);
                model.laser = laser;// to do  String.Format("{0:0.00}", laser);
                model.mastercard = mastercard;// to do  String.Format("{0:0.00}", mastercard);
                model.discount = discount;// to do  String.Format("{0:0.00}", discount);
                model.giftvoucher = giftvoucher;// to do  String.Format("{0:0.00}", giftvoucher);
                model.creditnote = creditnote;// to do  String.Format("{0:0.00}", creditnote);
                model.paypal = paypal;// to do // String.Format("{0:0.00}", cash);
                model.subtotal = subtotal;// to do String.Format("{0:0.00}", subtotal);
                model.amount = 0;// to do  "0.00";
                model.priviouspaidamount = priviouspaidamount;
                //dhara: do not need to set empty
                /// model.description = "";



                model.subtotal = subtotal;// String.Format("{0:0.00}", subtotal);
                model.discount = discount;// String.Format("{0:0.00}", discount);
                //lblInvoiceAmt.Text = String.Format("{0:0.00}", netpayment + misc);// to do  
                // lblPaid.Text = String.Format("{0:0.00}", paid);// to do  
                // lblRemaining.Text = String.Format("{0:0.00}", remaining);// to do  

                model.misc = misc;//   lblMisc.Text = String.Format("{0:0.00}", misc);// to do  
                model.invoiceamount = String.Format("{0:0.00}", netpayment + misc);
                model.paidamount = String.Format("{0:0.00}", paid);
                model.remainingpayment = String.Format("{0:0.00}", remaining);
                model.vatamount = vatamount;

                model.changedueInfo = Math.Round(sessionitemsOm.amount_tendered - paid, 2);

                if (sessionitemsOm.vid > 0)
                    model.voucherdetail = sessionitemsOm.gvdetail;
                // btnPlaceOrder.Attributes.Remove("style");// to do 
                // btnPlaceOrder.Enabled = true;// to do 
                List<string> summaryList = new List<string>();

                //String.Join(",", TargetList.ToArray())

                if (model.priviouspaidamount > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.priviouspaidamount) + " priviouspaidamount");
                if (model.cash > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.cash) + " cash");
                if (model.visa > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.visa) + " visa");
                if (model.cheque > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.cheque) + " cheque");
                if (model.mastercard > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.mastercard) + " mastercard");
                //if (model.discount > 0)
                //    summaryList.Add(model.discount + " discount");
                if (model.giftvoucher > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.giftvoucher) + " giftvoucher");
                if (model.creditnote > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.creditnote) + " creditnote");
                if (model.paypal > 0)
                    summaryList.Add(String.Format("{0:0.00}", model.paypal) + " paypal");

                model.displaypaidamount = String.Join(",", summaryList.ToArray());
                if (model.displaypaidamount.Length > 0)
                    model.displaypaidamount = "   ( " + model.displaypaidamount + " )";

                model.customeremail = customeremail;
                model.description = comments;
                model.Entityid = salespersonid;

            }
            return this.Json(new { IsSuccess = true, Message = "Save Payment Summary" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ClearPayment()
        {
            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);

            if (sessionitemsOm != null)
            {
                sessionitemsOm.cash = 0;
                sessionitemsOm.cheque = 0;
                sessionitemsOm.visa = 0;
                sessionitemsOm.mastercard = 0;
                sessionitemsOm.giftvoucher = 0;
                sessionitemsOm.creditnote = 0;
                sessionitemsOm.paypal = 0;
                sessionitemsOm.totalpaid = 0;
                sessionitemsOm.visadetail = string.Empty;
                sessionitemsOm.mcdetail = string.Empty;
                sessionitemsOm.chequedetail = string.Empty;
                sessionitemsOm.chequedetail = string.Empty;
                sessionitemsOm.cashdetail = string.Empty;
                sessionitemsOm.gvdetail = string.Empty;
                sessionitemsOm.paymentmode = string.Empty;
                sessionitemsOm.amount_tendered = 0;
                sessionitemsOm.paypaldetail = string.Empty;
                ClientSession.SetSession(SessionConstants.POSOrderMaster, sessionitemsOm);

            }
            return Json(new { success = false, message = "Please select items" });
        }
        private Domain.Entities.TemopraryOrderNumber GetTemporaryOrderNumber()
        {
            string companyId = ClientSession.GetSession(SessionConstants.companyid).ToString();
            var result = _posService.GetTemporaryOrderNumber(companyId);
            return result;
        }

        public ActionResult CustomerAdd()
        {
            CustomerSearchInfo custInfoModel = new CustomerSearchInfo();
            return View(custInfoModel);
        }

        [ChildActionOnly]
        public PartialViewResult CustomerAddToOrder(string customerId)
        {
            CustomerViewModel custInfo = new CustomerViewModel();

            try
            {

                POSLogger.Info("Reload POS Index Page with CustomerId: " + Convert.ToString(customerId));
                if (string.IsNullOrEmpty(customerId) && ClientSession.GetSession(SessionConstants.customerId) != null)
                {
                    customerId = ClientSession.GetSession(SessionConstants.customerId).ToString();
                    POSLogger.Info("Set with CustomerId: " + Convert.ToString(customerId));
                }
                else
                    ClientSession.SetSession(SessionConstants.customerId, customerId);

                var result = _customerService.GetCustomerById(customerId);


                //if (dtData.data.Count > 0)
                if (result != null)
                {
                    Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                    custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(result);
                }
                else
                {
                    custInfo.name = "CASH SALE";
                    custInfo.code = "CASH SALE";

                    var cashSaleCustomer = _customerService.GetCashSaleCustomer();
                    POSLogger.Info("Set Cash Sale CustomerId: " + Convert.ToString(cashSaleCustomer.custid));
                    if (cashSaleCustomer != null)
                    {
                        Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                        custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(cashSaleCustomer);
                        var custInfoFromDB = _customerService.GetCustomerById(custInfo.id);
                        custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(custInfoFromDB);
                        customerId = custInfo.id;
                        ClientSession.SetSession(SessionConstants.customerId, custInfo.id);
                    }
                }
                if (customerId != null)
                {
                    var cashSaleCustomer = _customerService.GetCashSaleCustomer();
                    OrderMaster ordermstr;
                    // Check OrderMaster Available
                    if (ClientSession.GetSession(SessionConstants.POSOrderMaster) == null)
                    {
                        //Set OrderMaster entries into Session.
                        ordermstr = new OrderMaster();
                        Domain.Entities.TemopraryOrderNumber orderdata = GetTemporaryOrderNumber();
                        ordermstr.temporderid = orderdata.ordernumber;
                        ordermstr.orderid = orderdata.orderid;
                        ordermstr.customerActive = cashSaleCustomer.active == true ? 1 : 0;

                    }
                    else
                    {
                        ordermstr = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                    }
                    var salespersonId = ClientSession.GetSession(SessionConstants.entityid).ToString();
                    ordermstr.customerid = customerId;
                    ordermstr.customer = custInfo.name;
                    //ordermstr.salespersonid = custInfo.salespersonid;
                    ordermstr.salespersonid = salespersonId == "0" ? custInfo.salespersonid : salespersonId;
                    ordermstr.salesperson = custInfo.salesperson;
                    ordermstr.VatRegNo = custInfo.VatRegNo;
                    ordermstr.customerActive = custInfo.active == true ? 1 : 0;
                    if (custInfo.defaultcurrencyid > 0)
                    {
                        ordermstr.currency = custInfo.defaultcurrencyid;
                        ordermstr.exchangerate = custInfo.exchange_rate;
                        ordermstr.currency_symbol = custInfo.currencysymbol;
                    }
                    else
                    {
                        Domain.Entities.Currency defaultcurrency = _posService.GetDefaultCurrency();
                        ordermstr.currency = defaultcurrency.id;
                        ordermstr.exchangerate = defaultcurrency.exchange_rate;
                        ordermstr.currency_symbol = defaultcurrency.symbol;
                    }


                    ordermstr.customeremail = custInfo.email;
                    ClientSession.SetSession(SessionConstants.POSOrderMaster, ordermstr);

                    var productSessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    if (productSessionObj != null)
                    {
                        var lstItems = (List<OrderDetail>)productSessionObj;
                        foreach (var item in lstItems)
                        {
                            item.discgiven = item.discgiven_base * ordermstr.exchangerate;
                            item.slprice = item.slprice_base * ordermstr.exchangerate;
                            item.netprice = (item.qty * item.slprice) - item.discgiven;
                        }

                        ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                    }


                }
            }
            catch (Exception ex)
            {

                POSLogger.Info("error to add to customer : " + ex.ToString());
            }
            return PartialView("_CustomerInfo", custInfo);
        }

        public PartialViewResult AddCustomerInfoToOrder(string customerId)
        {
            ClientSession.SetSession(SessionConstants.customerId, customerId);
            CustomerViewModel custInfo = new CustomerViewModel();
            var result = _customerService.GetCustomerById(customerId);


            //if (dtData.data.Count > 0)
            if (result != null)
            {
                Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(result);
            }
            else
            {
                var cashSaleCustomer = _customerService.GetCashSaleCustomer();
                if (cashSaleCustomer != null)
                {
                    Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                    custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(cashSaleCustomer);
                    var custInfoFromDB = _customerService.GetCustomerById(custInfo.id);
                    custInfo = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(custInfoFromDB);
                    customerId = custInfo.id;
                    ClientSession.SetSession(SessionConstants.customerId, custInfo.id);
                }
            }
            if (customerId != null)
            {
                OrderMaster ordermstr;
                // Check OrderMaster Available
                if (ClientSession.GetSession(SessionConstants.POSOrderMaster) == null)
                {
                    //Set OrderMaster entries into Session.
                    ordermstr = new OrderMaster();
                    Domain.Entities.TemopraryOrderNumber orderdata = GetTemporaryOrderNumber();
                    ordermstr.temporderid = orderdata.ordernumber;
                    ordermstr.orderid = orderdata.orderid;
                }
                else
                {
                    ordermstr = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                }

                var salespersonId = ClientSession.GetSession(SessionConstants.entityid).ToString();
                ordermstr.customerid = customerId;
                ordermstr.customer = custInfo.name;
                //ordermstr.salespersonid = custInfo.salespersonid;
                ordermstr.salespersonid = salespersonId == "0" ? custInfo.salespersonid : salespersonId;
                ordermstr.salesperson = custInfo.salesperson;
                ordermstr.VatRegNo = custInfo.VatRegNo;
                ordermstr.customerActive = custInfo.active == true ? 1 : 0;
                var sourcetype = ClientSession.GetSession(SessionConstants.sourcetype);
                if (sourcetype == null)
                {
                    if (custInfo.defaultcurrencyid > 0)
                    {
                        ordermstr.currency = custInfo.defaultcurrencyid;
                        ordermstr.exchangerate = custInfo.exchange_rate;
                        ordermstr.currency_symbol = custInfo.currencysymbol;
                    }
                    else
                    {
                        Domain.Entities.Currency defaultcurrency = _posService.GetDefaultCurrency();
                        ordermstr.currency = defaultcurrency.id;
                        ordermstr.exchangerate = defaultcurrency.exchange_rate;
                        ordermstr.currency_symbol = defaultcurrency.symbol;
                    }
                }

                ordermstr.customeremail = custInfo.email;
                ClientSession.SetSession(SessionConstants.POSOrderMaster, ordermstr);

                var productSessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                if (productSessionObj != null)
                {
                    var lstItems = (List<OrderDetail>)productSessionObj;
                    foreach (var item in lstItems)
                    {
                        item.discgiven = item.discgiven_base * ordermstr.exchangerate;
                        item.slprice = item.slprice_base * ordermstr.exchangerate;
                        item.netprice = (item.qty * item.slprice) - item.discgiven;
                    }

                    ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                }
            }
            return PartialView("_CustomerInfo", custInfo);
        }

        public PartialViewResult CustomerAddEdit(string customerId)
        {
            string custId = "0";

            if (!string.IsNullOrEmpty(customerId))
            {
                custId = customerId;
            }

            var model = new Models.Customer.CustomerViewModel();
            var custItem = new GoodSide.Domain.Entities.CustomerItem();
            model.iscashsale = false;

            if (!string.IsNullOrEmpty(custId) && int.Parse(custId) > 0)
            {
                //model = custController.GetCustomerEdit(custId);
                custItem = _customerService.GetCustomerEdit(customerId);
                Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                model = Mapper.Map<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>(custItem);
                model.postalcode = custItem.zip;
                model.salespersonid = custItem.salesId;
                model.phonenumber = custItem.phone;
                model.mobilenumber = custItem.mobile;
                model.defaultcurrency = custItem.defaultcurrencyid.ToString();
                if (model.name.Trim().ToLower() == "cash sale")
                {
                    model.iscashsale = true;
                }

            }
            else
            {
                model.id = custId;
                model.code = _customerService.GetNewCustomerCode();//custController.GetNewCustomerCode();
                model.countryid = "0";
                model.defaultcurrencyid = _posService.GetDefaultCurrency().id;
                model.defaultcurrency = model.defaultcurrencyid.ToString();
                model.active = true;
            }

            model.SalesPersons = new SelectList(GetSalesPersons(), "Value", "Text");
            model.CountyList = new SelectList(GetCountyList(), "Value", "Text");

            model.AboutUsList = new SelectList(GetAboutUsList(), "Value", "Text");
            model.FianacialList = new SelectList(GetFinancialList(), "Value", "Text");
            model.DeliveryTypeList = new SelectList(GetDeliveryTypeList(), "Value", "Text");

            model.CurrencyList = new SelectList(_keyValueItemService.GetCurrencies(), "id", "name");

            return PartialView("_CustomerAddEdit", model);
        }

        public JsonResult SaveCustomer(CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {

                var custItem = new GoodSide.Domain.Entities.CustomerItem();
                Mapper.CreateMap<CustomerViewModel, GoodSide.Domain.Entities.CustomerItem>();
                //Replace with "\n" with encode character values
                model.address = model.address.Replace("\r\n", "\n").Replace("\n", "%0A");
                custItem = Mapper.Map<CustomerViewModel, GoodSide.Domain.Entities.CustomerItem>(model);
                custItem.defaultcurrencyid = Convert.ToInt32(model.defaultcurrency);
                //custItem.salespersonid = ClientSession.GetSession(SessionConstants.userid).ToString();
                string custid = _customerService.Update(custItem);// to do how model 
                if (custid != "0")
                    return this.Json(new { IsSuccess = true, Message = "", ID = custid }, JsonRequestBehavior.AllowGet);
                else
                    return this.Json(new { IsSuccess = false, Message = "Error on save customer", ID = "0" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new { IsSuccess = false, Message = "Error on save customer" }, JsonRequestBehavior.AllowGet);
            }
        }
        public PartialViewResult ChangeDueInfo(string changeval, string isPrint)
        {
            var model = new PaymentInfoViewModel();
            model.changedueInfo = Convert.ToDecimal(changeval);
            model.isPrint = isPrint;
            return PartialView("_ChangeDueInfo", model);
        }


        public JsonResult _ChangeDueOk(PaymentInfoViewModel model)
        {

            return Json(new { success = true });
        }

        public ActionResult SavePOSPaymentSummary(PaymentInfoViewModel model)
        {
            PaymentType paymentType = (PaymentType)Enum.Parse(typeof(PaymentType), model.paymentaction, true);
            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            POSLogger.Info("PaymentType:" + paymentType + "Amount:" + model.amount.ToString());
            model.salespersonid = model.Entityid;
            switch (paymentType)
            {
                case PaymentType.visa:
                    model.visa = model.amount;
                    break;
                case PaymentType.cash:
                    model.cash = model.amount;
                    break;
                case PaymentType.cheque:
                    model.cheque = model.amount;
                    if (String.IsNullOrEmpty(model.description))
                        return Json(new { success = false, message = "Please write cheque details" });
                    else
                        break;
                case PaymentType.mastercard:
                    model.mastercard = model.amount;
                    break;
                case PaymentType.laser:
                    model.laser = model.amount;
                    break;
                case PaymentType.giftvoucher:
                    model.giftvoucher = model.giftvoucher;
                    break;
                case PaymentType.creaditnote:
                    model.creditnote = model.creditnote;
                    break;
                case PaymentType.paypal:
                    model.paypal = model.amount;
                    break;
                default:
                    break;
            }

            return OrderCalculation(paymentType, model);


        }

        public JsonResult ApplyDiscount(DiscountModel model)
        {
            POSLogger.Info("ApplyDiscount Type:" + model.DiscountTypeID.ToString() + "Amount:" + model.Discount.ToString() + "For SelectedProducts:" + model.SelectedPrductIds);
            if (string.IsNullOrEmpty(model.Password))
            {
                return Json(new { success = false, message = "Please enter password to apply discount" });
            }
            if (string.IsNullOrEmpty(model.Discount_Comment))
            {
                return Json(new { success = false, message = "Please enter reason for apply discount" });
            }
            var companyID = Convert.ToInt32(ClientSession.GetSession(SessionConstants.companyid));

            var sysUsr = _actUserService.GetLoggedInUser(model.Password, companyID);
            if (sysUsr == null)
            {
                return Json(new { success = false, message = "Invalid Password" });
            }

            var productIds = model.SelectedPrductIds.Split(',').ToList();

            var sessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            decimal totalPrice = 0;
            decimal disgiven = 0;
            string discountComment = string.Empty;
            if (sessionObj != null)
            {
                var sessionitems = ((List<OrderDetail>)sessionObj);
                foreach (var item in sessionitems)
                {
                    if (productIds.Contains(item.pid))
                    {
                        totalPrice = totalPrice + (item.slprice_base * item.qty);
                    }
                }

                if (model.Discount > 0)
                {
                    if (model.DiscountTypeID == (int)DiscountType.FixedAmount)
                    {
                        if (model.Discount > totalPrice)
                        {
                            return Json(new { success = false, message = "You cannot give 100% or more than 100% discount" });
                        }
                        disgiven = model.Discount;
                        discountComment = model.Discount_Comment;
                    }
                    else if (model.DiscountTypeID == (int)DiscountType.InPercentage)
                    {
                        if (model.Discount >= 100)
                        {
                            return Json(new { success = false, message = "You cannot give 100% or more than 100% discount" });
                        }
                        else
                        {
                            disgiven = (model.Discount * totalPrice) / 100;
                            discountComment = model.Discount_Comment;
                        }
                    }
                    else
                    {
                        return Json(new { success = false, message = "Please select atleast one discount type" });
                    }
                }
                else
                {
                    //return Json(new { success = false, message = "Enter discount amount" });
                    disgiven = 0;
                }

                decimal exchangeRate = 1;
                if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
                {
                    var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                    exchangeRate = ordMaster.exchangerate;
                }


                foreach (var item in sessionitems)
                {
                    if (productIds.Contains(item.pid))
                    {
                        decimal qty = item.qty, sellingprice = item.slprice_base;
                        decimal currentgpprice = qty * sellingprice, netprice = item.netprice;
                        decimal discpercent = Math.Round((((qty * sellingprice) / totalPrice) * 100), 2);
                        decimal Newdiscounamt = (currentgpprice * disgiven) / totalPrice;// (((discgiven - curdiscount) / 100) * (((qty * sellingprice) / totalprice) * 100));
                        item.discgiven_base = Math.Round(Newdiscounamt, 2);
                        item.discgiven = Math.Round(item.discgiven_base * exchangeRate, 2);
                        item.netprice = (item.qty * item.slprice) - item.discgiven;
                        item.vatamount = Math.Round((item.netprice - ((item.netprice * 100) / (100 + item.vatrate))), 2);
                        item.Discount_Comment = discountComment;
 
                    }
                }
                ClientSession.SetSession(SessionConstants.POSSelectedProducts, sessionitems);
            }
            return Json(new { success = true });
        }


        public JsonResult OrderAvailableCheck()
        {
            string SalespersonId = string.Empty;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                OrderMaster orderMain = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                SalespersonId = orderMain.salespersonid;
            }
            if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null && ((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts)).Count > 0)
                return this.Json(new { IsSuccess = true, Message = "sucess", Saleperson = SalespersonId }, JsonRequestBehavior.AllowGet);
            else
                return this.Json(new { IsSuccess = false, Message = "not available", Saleperson = SalespersonId }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SalesPersonCheck()
        {
            string SalespersonId = string.Empty;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                OrderMaster orderMain = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                SalespersonId = orderMain.salespersonid;
            }
            if (SalespersonId != string.Empty)
                return this.Json(new { IsSuccess = true, Message = "sucess", Saleperson = SalespersonId }, JsonRequestBehavior.AllowGet);
            else
                return this.Json(new { IsSuccess = false, Message = "not available", Saleperson = SalespersonId }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveOrderAsQuote()
        {
            OrderMaster orderMain = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            int CheckIsSalesPersonChange = 0;
            int CustomerActive = 0;

            if (orderMain != null)
                CustomerActive = orderMain.customerActive;

            if (CustomerActive == 1)
            {

                if (String.IsNullOrEmpty(orderMain.salespersonid) || orderMain.salespersonid == "0")
                {
                    return Json(new { success = false, message = "Please select sales person for order" }, JsonRequestBehavior.AllowGet);
                }

                string ChangeSalesPErsonCheck = (System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] != null ? System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] : string.Empty);
                if (string.IsNullOrEmpty(ChangeSalesPErsonCheck))
                    CheckIsSalesPersonChange = 0;
                else
                    CheckIsSalesPersonChange = Convert.ToInt32(ChangeSalesPErsonCheck);

                Domain.Entities.GeneralRuleResult result = PlaceOrderDetails((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts), orderMain, orderMain.orderstatus == 6 ? "6" : "4");

                POSLogger.Info("Order SaveOrderAsQuote status:" + result.status);
                POSLogger.Info("Order SaveOrderAsQuote orderId:" + result.outputvalue);
                POSLogger.Info("Order SaveOrderAsQuote orderno:" + (string.IsNullOrEmpty(result.outputvalue2) ? "" : result.outputvalue2));

                if (result.status == "success")
                    return Json(new { success = true, orderId = result.outputvalue, message = "Quote Saved Successfully", IsSalesPersonChange = CheckIsSalesPersonChange }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, orderId = "", message = result.outputvalue }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, orderId = "", message = "Cannot Create Quotes for Inactive Customer" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CustomerPhoneSearch(string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                term = "%%";
            }
            else
            {
                term = "%" + term + "%";
            }

            term = HttpUtility.UrlEncode(term);

            var lstProperties = typeof(CustomerPhoneSearch).GetProperties().ToList();

            var customerList = _customerService.CustomerPhoneSearch(term);

            return Json(customerList, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _OrderSummary(string orderId)
        {
            var ordersummary = new OrderDetailSummaryModel();
            var result = _customerService.OrderSummaryByOrderId(orderId);
            var lstItems = new List<OrderDetail>();
            //var sessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            //if (sessionObj != null)
            //{
            //    var sessionitems = ((List<OrderDetail>)sessionObj).ToList();
            //    lstItems = sessionitems.GroupBy(x => x.pid).Select(x => new OrderDetail()
            //    {
            //        pid = x.First().pid,
            //        SKU = x.First().SKU,
            //        productname = x.First().productname,
            //        qty = x.Sum(y => y.qty),
            //        slprice = x.First().slprice,
            //        discgiven = x.Sum(y => y.discgiven),
            //        netprice = ((x.Sum(y => y.qty) * x.First().slprice) - x.Sum(y => y.discgiven)),
            //        vatamount = x.Sum(y => y.vatamount)
            //    }).ToList();
            //}
            ordersummary.orderdetailList = result.Orderdetails;
            ordersummary.orderMaster = result.OrderMasterSummary;
            ordersummary.orderPaydetails = result.OrderPaydetails;
            string birtURL = CommonMethods.BIRTReportUrl();
            ordersummary.BIRTReportURL = birtURL.Replace("{orderid}", orderId);
            return PartialView(ordersummary);
        }

        public PartialViewResult _CustomerOrderSummary(string CustomerId, int OrderStatus)
        {
            CustomerViewOrderModel objviewOrdmaster = new CustomerViewOrderModel();
            objviewOrdmaster.cisentityId = Convert.ToInt32(CustomerId);
            objviewOrdmaster.OrderStatus = Convert.ToString(OrderStatus);
            return PartialView(objviewOrdmaster);
        }

        public PartialViewResult _AllCustomerOrderSummary()
        {
            CustomerViewOrderModel objviewOrdmaster = new CustomerViewOrderModel();
            //objviewOrdmaster.cisentityId = Convert.ToInt32(OrderId);
            //objviewOrdmaster.OrderStatus = Convert.ToString(OrderStatus);
            return PartialView(objviewOrdmaster);
        }
        public PartialViewResult CreditNoteDisplay()
        {
            CreditNoteViewModel model = new CreditNoteViewModel();
            return PartialView("_CreditNote", model);
        }

        public ActionResult GetTableDataCreditNoteDisplay(int draw, int start, int length)
        {
            length = 100;
            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);


            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }


            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);
            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(CreditNoteListViewModel).GetProperties().ToList();


            if (string.IsNullOrEmpty(search))
            {
                search = "%%";
            }
            else
            {
                search = "%" + search + "%";
            }

            //search = HttpUtility.UrlEncode(search);
            var result = _paymentInfoService.GetAllCreditNote(sessionitemsOm.customerid, search, start, length, "", "");

            DataTableDataCreditNote dataTableData = new DataTableDataCreditNote();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().t;
            dataTableData.data = result.creaditNoteSearchItemInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().t;

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddCreditNote(CreditNoteViewModel model)
        {
            string message = string.Format("Credit note added to order successfully");
            OrderMaster sessionitemsOm = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            decimal creditnote = 0;
            creditnote = model.Items.Sum(item => item.amountpaid);
            ClientSession.SetSession(SessionConstants.POSCreditNoteItem, model.Items);
            if (sessionitemsOm != null)
            {
                sessionitemsOm.creditnote = creditnote;
            }
            return this.Json(new { IsSuccess = true, Message = message }, JsonRequestBehavior.AllowGet);
            //if (ClientSession.GetSession(SessionConstants.POSCreditNoteItem) != null)
            //{
            //    List<CreditNoteListViewModel> CreditItems = (List<CreditNoteListViewModel>)ClientSession.GetSession(SessionConstants.POSCreditNoteItem);
            //    //Update if match
            //    foreach (var lineitem in model.Items)
            //    {
            //        CreditNoteListViewModel credititem = CreditItems.FirstOrDefault(x => x.id == lineitem.id);
            //        if (credititem != null)
            //        {
            //            credititem.amountpaid = credititem.amountpaid + lineitem.amountpaid;
            //        }
            //        else
            //        {
            //            CreditItems.Add(lineitem);
            //        }
            //    }

            //    creditnote = CreditItems.Sum(item => item.amountpaid);
            //    ClientSession.SetSession(SessionConstants.POSCreditNoteItem, CreditItems);

            //}
            //else
            //{
            //    creditnote = model.Items.Sum(item => item.amountpaid);
            //    ClientSession.SetSession(SessionConstants.POSCreditNoteItem, model.Items);
            //}

        }
        public PartialViewResult HeaderInfo()
        {
            GoodSide.Web.Models.Home.HeaderInfoModel header = new Models.Home.HeaderInfoModel();
            header.UserName = ClientSession.GetSession(SessionConstants.username).ToString();
            header.StoreName = ClientSession.GetSession(SessionConstants.storename).ToString();
            return PartialView("_Header", header);
        }

        public ActionResult LogOut()
        {
            return RedirectToAction("LogOff", "Account");
        }

        public PartialViewResult VoucherAddEdit(string VId)
        {
            string VoucherId = "0";

            if (!string.IsNullOrEmpty(VoucherId))
            {
                VoucherId = VId;
            }

            var model = new Models.Customer.VoucherViewModel();
            var VoucherItem = new GoodSide.Domain.Entities.VoucherItemInfo();


            if (!string.IsNullOrEmpty(VoucherId) && int.Parse(VoucherId) > 0)
            {
                //model = custController.GetCustomerEdit(custId);
                // custItem = _customerService.GetCustomerEdit(customerId);
                Mapper.CreateMap<GoodSide.Domain.Entities.CustomerItem, CustomerViewModel>();
                model = Mapper.Map<GoodSide.Domain.Entities.VoucherItemInfo, VoucherViewModel>(VoucherItem);
                model.amount = VoucherItem.amount;
                model.code = VoucherItem.code;
                model.customername = "CASH SALE";
                model.Description = VoucherItem.Description;
                model.ExpiryDate = VoucherItem.ExpiryDate;
                model.IssueDate = VoucherItem.IssueDate;
                model.PaymentType = VoucherItem.PaymentType;
                model.RefNo = VoucherItem.RefNo;
                model.SalePersonId = VoucherItem.SalePersonId;
            }
            else
            {
                model.voucherid = VoucherId;
                model.code = "Not Generated";
                model.customername = "CASH SALE";
            }

            model.SalesPersons = new SelectList(GetSalesPersons(), "Value", "Text");
            model.PayTypes = new SelectList(GetPaymentTypes(), "Value", "Text");
            return PartialView("_VoucherAddEdit", model);
        }

        public List<SelectListItem> GetPaymentTypes()
        {
            var listItems = _keyValueItemService.GetPaymentTypes().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }


        public JsonResult SaveVoucher(VoucherViewModel model)
        {
            if (ModelState.IsValid)
            {

                var voucherItem = new GoodSide.Domain.Entities.VoucherItemInfo();
                Mapper.CreateMap<VoucherViewModel, GoodSide.Domain.Entities.VoucherItemInfo>();
                //Replace with "\n" with encode character values              
                voucherItem = Mapper.Map<VoucherViewModel, GoodSide.Domain.Entities.VoucherItemInfo>(model);

                var result = _voucherService.Update(voucherItem);// to do how model 
                if (result.status == "success")
                    return this.Json(new { IsSuccess = true, VoucherCode = result.outputvalue }, JsonRequestBehavior.AllowGet);
                else
                    return this.Json(new { IsSuccess = false, Message = result.outputvalue, ID = "0" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(new { IsSuccess = false, Message = "Error on save Voucher" }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult _ChangeSalesPerson()
        {
            var model = new OtheInfoViewModel();
            int CheckIsSalesPersonChange = 0;
            string OptionToShow = string.Empty;
            string ChangeSalesPErsonCheck = (System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] != null ? System.Configuration.ConfigurationManager.AppSettings["IsSalespersonCheck"] : string.Empty);

            if (string.IsNullOrEmpty(ChangeSalesPErsonCheck))
                CheckIsSalesPersonChange = 0;
            else
                CheckIsSalesPersonChange = Convert.ToInt32(ChangeSalesPErsonCheck);

            if (CheckIsSalesPersonChange != 0)
                model.OptiontoShow = ((ChangeSalespersonOption)CheckIsSalesPersonChange).ToString();

            //var EntityId = ClientSession.GetSession(SessionConstants.entityid);
            //model.salespersonid = EntityId == null ? model.salespersonid : EntityId.ToString();
            //model.SalesPersonList = new SelectList(_keyValueItemService.GetAllSalesPerson(), "id", "name");
            return PartialView(model);
        }

        public JsonResult ChangeSalesPerson(OtheInfoViewModel model)
        {
            if (string.IsNullOrEmpty(model.PassWord))
            {
                return Json(new { success = false, message = "Please enter password" });
            }
            var companyID = Convert.ToInt32(ClientSession.GetSession(SessionConstants.companyid));

            var sysUsr = _actUserService.GetLoggedInUser(model.PassWord, companyID);
            if (sysUsr == null)
            {
                return Json(new { success = false, message = "Invalid Password" });
            }

            ClientSession.SetSession(SessionConstants.userid, sysUsr.UserId);
            ClientSession.SetSession(SessionConstants.entityid, sysUsr.EntityId);
            ClientSession.SetSession(SessionConstants.username, sysUsr.UserName);
            ClientSession.SetSession(SessionConstants.userrole, sysUsr.UserRole);
            return Json(new { success = true });
        }

        public JsonResult CustomerActiveCheck()
        {
            OrderMaster orderMain = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            int CustomerActive = 0;

            if (orderMain != null)
                CustomerActive = orderMain.customerActive;

            if (CustomerActive == 1)
                return this.Json(new { IsSuccess = true, Message = "sucess", CustomerActive = CustomerActive }, JsonRequestBehavior.AllowGet);
            else
                return this.Json(new { IsSuccess = false, Message = "not available", CustomerActive = CustomerActive }, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult OrderInfoCheck()
        //{
        //    //First check how many items into order and if anyone having delivery type as delivery and collection
        //    var itemdetailObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
        //    var customerId = ClientSession.GetSession(SessionConstants.customerId).ToString();
        //    var result = _customerService.GetCustomerById(customerId);
        //    bool isdeltypecondition = false;
        //    if (itemdetailObj != null)
        //    {
        //        var orderitems = ((List<OrderDetail>)itemdetailObj).ToList();

        //        var items = orderitems.Where(x => (x.deliverytype == 0 || x.deliverytype==1));
        //        if (items.Any() && (result.name.ToLower().IndexOf("cash sale")>=0))
        //        {
        //            isdeltypecondition = true;
        //        }

        //        var itemsdelivery = orderitems.Where(x => x.deliverytype == 0 );
        //        if (itemsdelivery.Any())
        //        {
        //            isdeltypecondition = true;
        //        }

        //    }

        //    return Json(new { success = isdeltypecondition, orderId = isdeltypecondition, message = "Order created successfully" }, JsonRequestBehavior.AllowGet);
        //}
        //public PartialViewResult GetOrderInfoCheckList()
        //{
        //    GoodSide.Web.Models.Customer.OrderInfoCheckList orderchecklist = new OrderInfoCheckList();
        //    return PartialView("__OrderInfoCheckList", orderchecklist);
        //}
    }
}