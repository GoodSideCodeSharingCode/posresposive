﻿using GoodSide.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodSide.Domain.Services;
using GoodSide.Web.Models.Common;
using GoodSide.Web.Models.POS;
using System.Net.Http;
using System.Net;
using AutoMapper;
using Newtonsoft.Json;
using GoodSide.Web.Models.Customer;
using GoodSide.Domain.Helpers;
using System.ComponentModel;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;


namespace GoodSide.Web.Controllers
{
    public class POSController : BaseController
    {
        // GET: POS
        private IProductSearchService _productSearchService;
        private IOrderDetailService _orderDetailService;
        private IKeyValueItemService _keyValueItemService;
        private IAvailableQuantityService _availableQuantityService;
        private IProductService _productService;
        private IPOSService _posService;
        private IOrderMasterService _OrderMasterService;

        public POSController(IProductSearchService productSearchService, IOrderDetailService orderDetailService, IKeyValueItemService keyValueItemService, IAvailableQuantityService availableQuantityService, IProductService productService, IPOSService posService, IOrderMasterService OrderMasterService)
        {
            _productSearchService = productSearchService;
            _orderDetailService = orderDetailService;
            _keyValueItemService = keyValueItemService;
            _availableQuantityService = availableQuantityService;
            _productService = productService;
            _posService = posService;
            _OrderMasterService = OrderMasterService;
        }

        public ActionResult Index()
        {
            var datev = DateTime.Now.ToString("yyyyMMdd");

            var OrdStatus = 0;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                OrdStatus = ordMaster.orderstatus;
            }
            ClientSession.SetSession(SessionConstants.orderstatus, OrdStatus);
            ViewBag.Ordstatus = OrdStatus;
            return View();
        }

        public PartialViewResult ProductDisplay(string items)
        {
            var ordersummary = new ProductDetailSummaryModel();
            var lstItems = new List<OrderDetail>();
            var sessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            if (sessionObj != null)
            {
                var sessionitems = ((List<OrderDetail>)sessionObj).ToList();
                lstItems = sessionitems.GroupBy(x => new { x.pid, x.uniqueid }).Select(x => new OrderDetail()
                {
                    pid = x.First().pid,
                    SKU = x.First().SKU,
                    productname = x.First().productname,
                    qty = x.Sum(y => y.qty),
                    slprice = x.First().slprice,
                    slprice_base = x.First().slprice_base,
                    discgiven_base = x.Sum(y => y.discgiven_base),
                    discgiven = x.Sum(y => y.discgiven),
                    netprice = ((x.Sum(y => y.qty) * x.First().slprice) - x.Sum(y => y.discgiven)),
                    vatamount = x.Sum(y => y.vatamount),
                    productdesc = x.First().productdesc,
                    deliverytype = x.First().deliverytype,
                    deliverydate = x.First().deliverydate,
                    promotion = x.First().promotion,
                    uniqueid = x.First().uniqueid

                }).ToList();
            }
            ordersummary.orderdetailList = lstItems;
            var ordermasterSession = ClientSession.GetSession(SessionConstants.POSOrderMaster);
            if (ordermasterSession != null)
            {
                ordersummary.orderMaster = (OrderMaster)ordermasterSession;
            }
            else
            {
                ordersummary.orderMaster = (new OrderMaster());
            }
            ordersummary.BIRTReportURL = CommonMethods.BIRTReportUrl();
            return PartialView(ordersummary);
        }

        public ActionResult ProductDisplayPopup()
        {
            return PartialView();
        }

        public JsonResult GetProductSearchData(int draw, int start, int length)
        {
            //var Searchhistory = ClientSession.GetSession(SessionConstants.Searchhistory);
            //if (Searchhistory != null)
            //{
            //    string[] searchP = Convert.ToString(Searchhistory).Split(',');
            //    draw = (Convert.ToInt32(searchP[0]) > draw) ? Convert.ToInt32(searchP[0]) : draw;
            //    start = (Convert.ToInt32(searchP[1]) > start) ? Convert.ToInt32(searchP[1]) : start;
            //    length = (Convert.ToInt32(searchP[2]) > length) ? Convert.ToInt32(searchP[2]) : length;
            //}            
            var pname = HttpContext.Request.QueryString["pname"];
            var description = HttpContext.Request.QueryString["description"];
            var pno = HttpContext.Request.QueryString["pno"];
            var barcode_no = HttpContext.Request.QueryString["barcode_no"];
            var supplierid = HttpContext.Request.QueryString["supplierid"];
            var typeid = HttpContext.Request.QueryString["typeid"];
            var categoryid = HttpContext.Request.QueryString["categoryid"];
            var stockTypeid = HttpContext.Request.QueryString["stockTypeid"];

            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);
            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(ProductSearchViewModel).GetProperties().ToList();

            var data = _productSearchService.GetAll(pname, description, pno, barcode_no, supplierid, typeid, categoryid, stockTypeid, start, length, lstProperties[sortColumn].Name, sortDirection);

            ProductSearchDataTableData dataTableData = new ProductSearchDataTableData();
            dataTableData.recordsTotal = data.pagingInfo.FirstOrDefault().total;
            dataTableData.data = data.productSearchItemInfo;
            dataTableData.recordsFiltered = data.pagingInfo.FirstOrDefault().total;
            var history = string.Concat(Convert.ToString(draw), ',', Convert.ToString(start), ',', Convert.ToString(length));
            ClientSession.SetSession(SessionConstants.Searchhistory, history);
            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _ProductInfo(string pid, string uniqueId)
        {
            if (pid == "0")
            {
                return PartialView(new OrderDetail());
            }
            var sessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            if (sessionObj != null)
            {
                //GroupBy(x => new { x.pid, x.uniqueid })
                var product = ((List<OrderDetail>)sessionObj).Where(x => x.pid == pid && x.uniqueid == int.Parse(uniqueId)).GroupBy(x => new { x.pid, x.uniqueid }).Select(x => new OrderDetail()
                {
                    pid = x.Key.pid,
                    productname = x.First().productname,
                    productdesc = x.First().productdesc,
                    slprice = x.First().slprice,
                    qty = x.Sum(y => y.qty),
                    discgiven = x.Sum(y => y.discgiven),
                    colors = x.First().colors,
                    vatamount = x.Sum(y => y.vatamount),
                    uniqueid = x.Key.uniqueid


                }).FirstOrDefault();

                if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
                {
                    var orderMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                    if (orderMaster != null)
                    {
                        product.currency_symbol = orderMaster.currency_symbol;
                    }
                }

                return PartialView(product);
            }
            return PartialView(new OrderDetail());
        }

        public JsonResult _EditProductInfo(string pid, string uniqueId)
        {
            var sessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            var product = ((List<OrderDetail>)sessionObj).Where(x => x.pid == pid && x.uniqueid == int.Parse(uniqueId)).GroupBy(x => new { x.pid, x.uniqueid }).Select(x => new OrderDetail()
            {
                pid = x.Key.pid,
                productname = x.First().productname,
                productdesc = x.First().productdesc,
                slprice = x.First().slprice,
                qty = x.Sum(y => y.qty),
                discgiven = x.Sum(y => y.discgiven),
                colors = x.First().colors,
                vatamount = x.Sum(y => y.vatamount),
                uniqueid = x.Key.uniqueid,
                stype = x.First().stype,
                sale_type = x.First().sale_type,
                cust_leadtime = x.First().cust_leadtime,
                Discount_Comment = x.First().Discount_Comment
            }).FirstOrDefault();

            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var orderMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                if (orderMaster != null)
                {
                    product.currency_symbol = orderMaster.currency_symbol;
                }
            }
            return this.Json(new { IsSuccess = true, pid = product.pid, productname = product.productname, slsprice = product.slprice, stype = product.stype, sale_type = product.sale_type, uniqueId = product.uniqueid, description = product.productdesc, cust_leadtime = product.cust_leadtime, discount = product.discgiven, discountComment = product.Discount_Comment }, JsonRequestBehavior.AllowGet);


        }
        public PartialViewResult _SimilarProductInfo(string pid)
        {
            //  ViewBag.ImageRootPath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"];

            var OrdStatus = 0;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                OrdStatus = ordMaster.orderstatus;
            }
            ViewBag.Ordstatus = OrdStatus;
            string ImagePath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"];
            var model = new Models.StockDetail.StockDetailSimilarProductsModel();
            var result = _posService.GetSimilarProductInfo(pid);
            foreach (var image in result)
            {
                image.image_filepath = image.image_filepath.Replace("productimages", ImagePath);
            }
            Mapper.CreateMap<Domain.Entities.StockDetailSimilarImage, Models.StockDetail.StockProductInfo>();
            model.lststockImages = Mapper.Map<List<Domain.Entities.StockDetailSimilarImage>, List<Models.StockDetail.StockProductInfo>>(result);

            return PartialView(model);
        }
        public PartialViewResult _OtherInfo(string pid)
        {
            var model = new OtheInfoViewModel();

            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var sessionObj = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                model.defaultcurrency = Convert.ToString(sessionObj.currency);
                var currency = _posService.GetCurrencyById(sessionObj.currency);
                model.currencysymbol = currency.symbol;
                model.currencyname = currency.name;
                model.salesperson = sessionObj.salesperson;
            }
            else
            {
                var defaultcurrency = _posService.GetDefaultCurrency();
                model.defaultcurrency = Convert.ToString(defaultcurrency.id);
                model.currencysymbol = defaultcurrency.symbol;
                model.currencyname = defaultcurrency.name;
            }
            return PartialView(model);
        }

        public PartialViewResult _OtherInfoUpdate(string requestedsourcetype)
        {
            string sourcetype = requestedsourcetype;
            ClientSession.SetSession(SessionConstants.sourcetype, sourcetype);
            var model = new OtheInfoViewModel();
            model.CurrencyList = new SelectList(_keyValueItemService.GetCurrencies(), "id", "name");
            model.SalesPersonList = new SelectList(_keyValueItemService.GetAllSalesPerson(), "id", "name");
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var sessionObj = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                model.defaultcurrency = Convert.ToString(sessionObj.currency);
                model.salespersonid = sessionObj.salespersonid;

            }
            else
            {
                //var defaultcurrency = _posService.GetDefaultCurrency();
                //model.defaultcurrency = Convert.ToString(defaultcurrency.id);
            }
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult _OtherInfoUpdate(OtheInfoViewModel model)
        {
            //if (string.IsNullOrEmpty(model.defaultcurrency))
            //{
            //    var defaultCurrency = _posService.GetDefaultCurrency();
            //    model.defaultcurrency = Convert.ToString(defaultCurrency.id);
            //}

            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var sessionObj = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);

                if (sessionObj != null)
                {
                    //sessionObj.currency = Convert.ToInt32(model.defaultcurrency);
                    //var currency = _posService.GetCurrencyById(sessionObj.currency);
                    //sessionObj.exchangerate = currency.exchange_rate;
                    //sessionObj.currency_symbol = currency.symbol;
                    sessionObj.salespersonid = model.salespersonid;
                    var salesperson = _keyValueItemService.GetAllSalesPerson().ToList().Where(x => x.id == model.salespersonid).Select(x => x.name).FirstOrDefault();
                    sessionObj.salesperson = salesperson;
                    ClientSession.SetSession(SessionConstants.POSOrderMaster, sessionObj);

                    var productSessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    if (productSessionObj != null)
                    {
                        var lstItems = (List<OrderDetail>)productSessionObj;
                        foreach (var item in lstItems)
                        {
                            item.discgiven = item.discgiven_base * sessionObj.exchangerate;
                            item.slprice = item.slprice_base * sessionObj.exchangerate;
                            item.netprice = (item.qty * item.slprice) - item.discgiven;
                        }
                        ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                    }
                }
                else
                {
                    var ordMaster = new OrderMaster();
                    //ordMaster.currency = Convert.ToInt32(model.defaultcurrency);
                    //var currency = _posService.GetCurrencyById(sessionObj.currency);
                    //ordMaster.exchangerate = currency.exchange_rate;
                    //ordMaster.currency_symbol = currency.symbol;
                    //ClientSession.SetSession(SessionConstants.POSOrderMaster, ordMaster);

                    var productSessionObj = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    if (productSessionObj != null)
                    {
                        var lstItems = (List<OrderDetail>)productSessionObj;
                        foreach (var item in lstItems)
                        {
                            item.discgiven = item.discgiven_base * sessionObj.exchangerate;
                            item.slprice = item.slprice_base * sessionObj.exchangerate;
                            item.netprice = (item.qty * item.slprice) - item.discgiven;

                        }

                        ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                    }

                }
            }
            var sourcetype = ClientSession.GetSession(SessionConstants.sourcetype);
            return Json(new { success = true, requestedsourcetype = sourcetype });
        }

        public PartialViewResult ProductSearch()
        {
            var model = new ProductSearchViewModel();

            var lstSuppliers = _keyValueItemService.GetSuppliers().Select(x => new SelectListItem()
            {
                Text = x.supplier,
                Value = x.id

            }).ToList();
            model.Suppliers = new SelectList(lstSuppliers, "Value", "Text");

            var lstCategories = _keyValueItemService.GetCategories().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();
            model.Categories = new SelectList(lstCategories, "Value", "Text");

            //var lstTypes = _keyValueItemService.GetCategories().Select(x => new SelectListItem()
            //{
            //    Text = x.name,
            //    Value = x.id

            //}).ToList();

            //model.ProductTypes = new SelectList(lstTypes, "Value", "Text");

            var lstStockTypes = _keyValueItemService.GetStockTypes().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();
            model.StockTypes = new SelectList(lstStockTypes, "Value", "Text");

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult GetProductTypeList(string categoryid)
        {
            List<SelectListItem> producttypeList = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(categoryid))
            {
                producttypeList = _keyValueItemService.GetProductType(categoryid).Select(x => new SelectListItem()
                {
                    Text = x.name,
                    Value = x.id

                }).ToList();
            }
            return Json(producttypeList, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AvailableQuantity(string str, bool fromSeachProduct = true)
        {
            var lstItems = new List<OrderDetail>();
            var args = str.Split(';').ToList();
            var productFromDB = _productService.GetById(Convert.ToInt32(args[0] != null ? Convert.ToInt32(args[0]) : 0));

            var model = new AvailableQuantityModel();
            model.pid = !string.IsNullOrWhiteSpace(args[0]) ? Convert.ToInt32(args[0]) : 0;
            model.ProductName = !string.IsNullOrWhiteSpace(args[1]) ? Convert.ToString(args[1]) : string.Empty;
            model.SellingPrice = productFromDB.Promosellingprice == 0 ? !string.IsNullOrWhiteSpace(args[2]) ? Math.Round(Convert.ToDecimal(args[2]), 2) : 0 : productFromDB.Promosellingprice;
            model.stype = !string.IsNullOrWhiteSpace(args[4]) ? Convert.ToInt32(args[4]) : 0;
            model.unallocatedS = !string.IsNullOrWhiteSpace(args[3]) ? Convert.ToString(args[3]) : string.Empty;
            model.uniqueid = !string.IsNullOrWhiteSpace(args[5]) ? Convert.ToInt32(args[5]) : 0;
            model.ProductDescription = !string.IsNullOrWhiteSpace(args[6]) ? Convert.ToString(args[6]) : string.Empty;
            if (args.Count > 8)
                model.sale_type = !string.IsNullOrWhiteSpace(args[7]) ? Convert.ToInt32(args[7]) : 0;
            if (args.Count > 9)
                model.CustomerLeadtime = !string.IsNullOrWhiteSpace(args[8]) ? Convert.ToString(args[8]) : string.Empty;
            if (args.Count >= 10)
            {
                model.discount = !string.IsNullOrWhiteSpace(args[9]) ? Convert.ToDecimal(args[9]) : 0;
                model.Discount_Comment = !string.IsNullOrWhiteSpace(args[10]) ? Convert.ToString(args[10]) : string.Empty;
            }
         
            if (model.stype == 1)
            {

                List<GoodSide.Domain.Entities.AvailableQuantity> result = _availableQuantityService.GetAvailableQuantityByBarcodeMisc(string.Empty, args[0]);

                var lstAvaibaleQuantities = new List<AvailableQuantityItemViewModel>();

                Mapper.CreateMap<GoodSide.Domain.Entities.AvailableQuantity, AvailableQuantityItemViewModel>();

                lstAvaibaleQuantities = Mapper.Map<List<GoodSide.Domain.Entities.AvailableQuantity>, List<AvailableQuantityItemViewModel>>(result);

                ViewBag.DeliveryTypes = DeliveryTypeEnum.DELIVERY.ToSelectListValue();
                ViewBag.SaleType = SaleType.Standard_Item.ToSelectListValue();
                model.DeliveryTypeID = (int)DeliveryTypeEnum.DELIVERY;
                model.sale_type = (int)SaleType.Standard_Item;
                ViewBag.DestinationPointList = new SelectList(GetDestinationLoc(), "Value", "Text");
                var existingOrderProducts = ClientSession.GetSession(SessionConstants.POSSelectedProducts);

                if (existingOrderProducts != null && fromSeachProduct == false)
                {
                    var sessionitems = (List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    if (sessionitems.Count > 0)
                    {
                        model.IsAnyProductAdded = true;

                        var objUpdateSlPrice = sessionitems.Where(x => x.pid == Convert.ToString(model.pid) && x.uniqueid == model.uniqueid).FirstOrDefault();
                        if (objUpdateSlPrice != null)
                        {
                            model.SellingPrice = objUpdateSlPrice.slprice;
                            model.DeliveryTypeID = objUpdateSlPrice.deliverytype;
                            model.Reference = objUpdateSlPrice.colors;
                            model.ProductDescription = objUpdateSlPrice.productdesc;
                            model.DestinationPoint = objUpdateSlPrice.DestinationPoint;
                            model.deliverydate = Convert.ToDateTime(objUpdateSlPrice.deliverydate).ToShortDateString();
                            model.uniqueid = objUpdateSlPrice.uniqueid;
                            model.CustomerLeadtime = objUpdateSlPrice.cust_leadtime;
                            model.Discount_Comment = objUpdateSlPrice.Discount_Comment;
                        }
                        else
                            model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        //session items list

                        foreach (var sessionItem in sessionitems.Where(x => x.pid == Convert.ToString(model.pid) && x.uniqueid == model.uniqueid))
                        {
                            var product = lstAvaibaleQuantities.Where(x => x.pid == sessionItem.pid && x.id == sessionItem.id).FirstOrDefault();
                            if (product != null)
                            {
                                product.qty = sessionItem.qty;
                            }
                        }

                        //just added dummy item which is related to scan item oversell which is not exists into above list.
                        //first find if any item is added via scan quick sell.
                        List<OrderDetail> quicksellitems = sessionitems.Where(x => x.isscanitem == true && x.pid == Convert.ToString(model.pid) && x.uniqueid == model.uniqueid).ToList();
                        List<SelectListItem> lstlocations = GetDestinationLoc();
                        foreach (OrderDetail item in quicksellitems)
                        {
                            var product = lstAvaibaleQuantities.Where(x => x.pid == item.pid && x.id == item.id).FirstOrDefault();
                            if (product == null)
                            {
                                AvailableQuantityItemViewModel quicksellitem = new AvailableQuantityItemViewModel();
                                quicksellitem.id = item.id;
                                quicksellitem.pid = item.pid;
                                quicksellitem.poid = item.poid;
                                quicksellitem.qty = item.qty;
                                quicksellitem.reference = item.colors;
                                quicksellitem.discount = item.discgiven;
                                quicksellitem.stocklocationid = item.locationid.ToString();
                                quicksellitem.totalQty = item.qty;
                                var stocklocationname = lstlocations.Where(x => x.Value == item.locationid.ToString()).FirstOrDefault();
                                quicksellitem.name = stocklocationname.Text + "-OVERSELL";
                                lstAvaibaleQuantities.Add(quicksellitem);
                            }
                        }
                    }
                    else
                        model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                    model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                model.Items = lstAvaibaleQuantities;
                model.pid = Convert.ToInt32(args[0]);

                ViewBag.FromSeachProduct = fromSeachProduct;

                var Ordmaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                ViewBag.orderstatus = Ordmaster.orderstatus;

                return PartialView(model);
            }
            else
            {
                //  var lstItems = new List<OrderDetail>();

                //var args = str.Split(';').ToList();
                //var model = new AvailableQuantityModel();
                //model.pid = args[0] != null ? Convert.ToInt32(args[0]) : 0;
                //model.ProductName = args[1] != null ? Convert.ToString(args[1]) : string.Empty;
                //model.SellingPrice = args[2] != null ? Math.Round(Convert.ToDecimal(args[2]), 2) : 0;
                //model.stype = args[4] != null ? Convert.ToInt32(args[4]) : 0;
                //model.unallocatedS = args[3] != null ? Convert.ToString(args[3]) : string.Empty;

                List<GoodSide.Domain.Entities.AvailableQuantity> result = _availableQuantityService.GetAvailableQuantityByBarcodeMisc(string.Empty, args[0]);

                var lstAvaibaleQuantities = new List<AvailableQuantityItemViewModel>();

                Mapper.CreateMap<GoodSide.Domain.Entities.AvailableQuantity, AvailableQuantityItemViewModel>();

                lstAvaibaleQuantities = Mapper.Map<List<GoodSide.Domain.Entities.AvailableQuantity>, List<AvailableQuantityItemViewModel>>(result);


                ViewBag.DeliveryTypes = DeliveryTypeEnum.DELIVERY.ToSelectListValue();
                ViewBag.SaleType = SaleType.Standard_Item.ToSelectListValue();
                model.DeliveryTypeID = (int)DeliveryTypeEnum.DELIVERY;
                ViewBag.DestinationPointList = new SelectList(GetDestinationLoc(), "Value", "Text");
                var existingOrderProducts = ClientSession.GetSession(SessionConstants.POSSelectedProducts);

                if (existingOrderProducts != null)
                {
                    var sessionitems = (List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    if (sessionitems.Count > 0)
                    {
                        model.IsAnyProductAdded = true;

                        var objUpdateSlPrice = sessionitems.Where(x => x.pid == Convert.ToString(model.pid)).FirstOrDefault();
                        if (objUpdateSlPrice != null)
                        {
                            model.SellingPrice = objUpdateSlPrice.slprice;
                            model.DeliveryTypeID = objUpdateSlPrice.deliverytype;
                            model.Reference = objUpdateSlPrice.colors;
                            model.ProductDescription = objUpdateSlPrice.productdesc;
                            model.DestinationPoint = objUpdateSlPrice.DestinationPoint;
                            model.deliverydate = Convert.ToDateTime(objUpdateSlPrice.deliverydate).ToShortDateString();
                            model.uniqueid = objUpdateSlPrice.uniqueid;
                            model.CustomerLeadtime = objUpdateSlPrice.cust_leadtime;
                            model.Discount_Comment = objUpdateSlPrice.Discount_Comment;
                        }
                        else
                            model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        //session items list
                        foreach (var sessionItem in sessionitems)
                        {
                            var product = lstAvaibaleQuantities.Where(x => x.pid == sessionItem.pid && x.id == sessionItem.id).FirstOrDefault();
                            if (product != null)
                            {
                                product.qty = sessionItem.qty;
                            }
                        }

                        //just added dummy item which is related to scan item oversell which is not exists into above list.
                        //first find if any item is added via scan quick sell.
                        List<OrderDetail> quicksellitems = sessionitems.Where(x => x.isscanitem == true && x.pid == Convert.ToString(model.pid)).ToList();
                        List<SelectListItem> lstlocations = GetDestinationLoc();
                        foreach (OrderDetail item in quicksellitems)
                        {
                            var product = lstAvaibaleQuantities.Where(x => x.pid == item.pid && x.id == item.id).FirstOrDefault();
                            if (product == null)
                            {
                                AvailableQuantityItemViewModel quicksellitem = new AvailableQuantityItemViewModel();
                                quicksellitem.id = item.id;
                                quicksellitem.pid = item.pid;
                                quicksellitem.poid = item.poid;
                                quicksellitem.qty = item.qty;
                                quicksellitem.reference = item.colors;
                                quicksellitem.discount = item.discgiven;
                                quicksellitem.stocklocationid = item.locationid.ToString();
                                quicksellitem.totalQty = item.qty;
                                var stocklocationname = lstlocations.Where(x => x.Value == item.locationid.ToString()).FirstOrDefault();
                                quicksellitem.name = stocklocationname.Text + "-OVERSELL";
                                lstAvaibaleQuantities.Add(quicksellitem);
                            }
                        }
                    }
                    else
                        model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                    model.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                model.Items = lstAvaibaleQuantities;
                model.pid = Convert.ToInt32(args[0]);

                ViewBag.FromSeachProduct = fromSeachProduct;

                var Ordmaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                ViewBag.orderstatus = Ordmaster.orderstatus;

                return PartialView(model);
            }

        }

        public HttpStatusCodeResult SearchProduct(string barcode)
        {
            try
            {

                var lstItems = new List<OrderDetail>();
                POSLogger.Info("GetDefaultLocationForBarCode:" + barcode);
                // var result = _availableQuantityService.GetAvailableQuantityByBarcode(barcode, "0");

                var result = _availableQuantityService.CheckDefaultLocationForBarcode(barcode);
                //string[] values = result.Split('|');

                if (result != null && result.Count() > 0)
                {
                    string table = "0";
                    //var productInfo = result.FirstOrDefault();
                    string[] values = result.Split('|');

                    table = _availableQuantityService.GetDefaultLocationForBarCode(values[2]);
                    POSLogger.Info("GetDefaultLocationForBarCode" + table);
                    POSLogger.Info("productinfo.id" + values[2]);

                    string[] DefaultLocation = table.Split('/');

                    decimal exchange_rate = 1;

                    if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
                    {
                        var orderMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                        exchange_rate = orderMaster.exchangerate;
                    }


                    var cachedProductList = ClientSession.GetSession(SessionConstants.POSSelectedProducts);
                    bool isItemExists = false;
                    if (cachedProductList != null)
                    {
                        lstItems = (List<OrderDetail>)cachedProductList;
                        var matchingProduct = lstItems.Where(p => p.id == table && p.pid == values[2]).FirstOrDefault();
                        if (matchingProduct != null)
                        {
                            matchingProduct.qty = matchingProduct.qty + 1;
                            matchingProduct.discgiven = (((matchingProduct.slprice_base * matchingProduct.qty) * (matchingProduct.Promodiscount)) / 100) * exchange_rate;
                            matchingProduct.discgiven_base = (((matchingProduct.slprice_base * matchingProduct.qty) * (matchingProduct.Promodiscount)) / 100);
                            matchingProduct.netprice = Math.Round((matchingProduct.qty * matchingProduct.slprice) - matchingProduct.discgiven, 2);
                            var salePrice = matchingProduct.netprice;
                            matchingProduct.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + matchingProduct.vatrate))), 2);   
                            isItemExists = true;
                        }
                    }


                    if (!isItemExists)
                    {

                        var productFromDB = _productService.GetById(Convert.ToInt32(values[2]));
                        if (productFromDB != null)
                        {

                            var orderDetail = new OrderDetail();
                            orderDetail.pid = Convert.ToString(productFromDB.id);
                            orderDetail.id = table;
                            orderDetail.productname = productFromDB.product_name;
                            orderDetail.productdesc = productFromDB.description;
                            orderDetail.SKU = productFromDB.Part_No;
                            orderDetail.costprice = productFromDB.Cost_Price;
                            orderDetail.qty = 1;
                            orderDetail.slprice = productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price * exchange_rate : productFromDB.Promosellingprice * exchange_rate;
                            orderDetail.slprice_base = productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice;
                            if (productFromDB.Promodiscount != 0)
                            {
                                orderDetail.discgiven = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100) * exchange_rate;
                                orderDetail.discgiven_base = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100);

                            }
                            else
                            {
                                orderDetail.discgiven = 0;
                                orderDetail.discgiven_base = 0;
                            }
                            orderDetail.vatrate = Convert.ToDecimal(productFromDB.vatrate);
                            var salePrice = ((Convert.ToDecimal(productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice)) - orderDetail.discgiven_base);
                            orderDetail.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + (Convert.ToDecimal(productFromDB.vatrate))))), 2);                               
                                //Convert.ToDecimal(1 * (Convert.ToDecimal(productFromDB.vatrate) * (Convert.ToDecimal(productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice) / 100))) * exchange_rate;

                            orderDetail.locationid = Convert.ToInt32(DefaultLocation[1]);
                            orderDetail.DestinationPoint = Convert.ToInt32(DefaultLocation[1]);

                            orderDetail.poid = Convert.ToInt32(DefaultLocation[0]);
                            orderDetail.porid = -1;
                            orderDetail.product_image = Convert.ToString(productFromDB.product_image);
                            orderDetail.colors = Convert.ToString(productFromDB.id);
                            orderDetail.stype = Convert.ToInt32(productFromDB.sType);
                            orderDetail.deliverytype = (int)DeliveryTypeEnum.TAKEAWAY;
                            orderDetail.deliverydate = DateTime.Now.Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                            //orderDetail.DestinationPoint = Convert.ToInt32(destinationPoint);
                            //DateTime.Today.Date.ToString("dd-MM-yyyy");
                            orderDetail.netprice = (orderDetail.qty * orderDetail.slprice) - orderDetail.discgiven;
                            orderDetail.promotion = productFromDB.promotion;
                            orderDetail.Promodiscount = productFromDB.Promodiscount;
                            orderDetail.PromoId = productFromDB.PromoId;
                            orderDetail.Promosellingprice = productFromDB.Promosellingprice;
                            orderDetail.isscanitem = true;
                            lstItems.Add(orderDetail);
                        }
                    }
                    ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }

            }
            catch (Exception ex)
            {
                POSLogger.Error(ex);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        public JsonResult AddProductsToSession(string items, bool addaction = true)
        {
            var lstItems = new List<OrderDetail>();
            var lstItemsToRemove = new List<OrderDetail>();
            var order = JsonConvert.DeserializeObject<AvailableQuantityModel>(items);
            var sessionitems = new List<OrderDetail>();
            if (order.stype == 1)
            {


                List<string> itemIds = new List<string>();
                itemIds = order.Items.Select(x => x.id).ToList();

                decimal exchangeRate = 1;
                if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
                {
                    var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                    exchangeRate = ordMaster.exchangerate;
                }

                if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null)
                {
                    sessionitems = ((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts)).ToList();
                }

                if (addaction)
                {
                    lstItems = sessionitems;

                    var itemsToAdd = order.Items.Where(x => itemIds.Contains(x.id) && x.qty > 0).ToList();
                    if (itemsToAdd.Any())
                    {
                        var pid = itemsToAdd.First().pid;

                        var productFromDB = _productService.GetById(Convert.ToInt32(pid.ToString()));

                        if (productFromDB != null)
                        {
                            int uniqueid = lstItems.Count + 1;

                            foreach (var item in itemsToAdd)
                            {
                                if (!item.id.Trim().Contains("-1"))
                                {
                                    if (item.qty > item.totalQty && item.name != "OVERSELL")
                                    {
                                        item.qty = item.totalQty;
                                    }
                                }

                                if (item.qty > 0)
                                {
                                    var orderDetail = new OrderDetail();
                                    orderDetail.uniqueid = uniqueid;
                                    orderDetail.pid = item.pid;
                                    orderDetail.id = item.id;
                                    orderDetail.productname = order.ProductName;
                                    orderDetail.productdesc = order.ProductDescription;
                                    orderDetail.SKU = Convert.ToString(productFromDB.Part_No);
                                    orderDetail.costprice = productFromDB.Cost_Price;
                                    orderDetail.qty = item.qty;
                                    orderDetail.sale_type = order.sale_type;
                                    orderDetail.cust_leadtime = order.CustomerLeadtime;
                                    orderDetail.Discount_Comment = order.Discount_Comment;

                                    orderDetail.slprice = order.SellingPrice * exchangeRate;
                                    orderDetail.slprice_base = order.SellingPrice;
                                    if (productFromDB.Promodiscount != 0)
                                    {
                                        orderDetail.discgiven = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100) * exchangeRate;
                                        orderDetail.discgiven_base = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100);

                                    }
                                    else
                                    {
                                        orderDetail.discgiven = 0;
                                        orderDetail.discgiven_base = 0;
                                    }
                                    orderDetail.vatrate = Convert.ToDecimal(productFromDB.vatrate);
                                   //orderDetail.vatamount = Convert.ToDecimal(item.qty * (Convert.ToDecimal(productFromDB.vatrate) * (Convert.ToDecimal(productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice) / 100)));
                                    orderDetail.locationid = Convert.ToInt32(item.stocklocationid);
                                    orderDetail.poid = item.poid;
                                    orderDetail.porid = -1;
                                    orderDetail.product_image = productFromDB.product_image;
                                    orderDetail.colors = order.Reference;
                                    orderDetail.stype = order.stype;
                                    orderDetail.netprice = (orderDetail.qty * orderDetail.slprice) - (orderDetail.discgiven);
                                    var salePrice = orderDetail.netprice;
                                    orderDetail.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + (Convert.ToDecimal(productFromDB.vatrate))))), 2);                                          
                                    orderDetail.deliverytype = order.DeliveryTypeID;
                                    orderDetail.deliverydate = order.deliverydate;
                                    //DateTime.Today.Date.ToString("dd-MM-yyyy");                            
                                    orderDetail.promotion = productFromDB.promotion;
                                    orderDetail.Promodiscount = productFromDB.Promodiscount; // Here we set a Discount from DB for current Product
                                    orderDetail.PromoId = productFromDB.PromoId;
                                    orderDetail.Promosellingprice = productFromDB.Promosellingprice;
                                    orderDetail.oversaleFlag = item.name == "OVERSELL" ? -2 : 0;
                                    if (order.DestinationPoint != 0)
                                        orderDetail.DestinationPoint = order.DestinationPoint;
                                    else
                                        orderDetail.DestinationPoint = Convert.ToInt32(item.stocklocationid);
                                    lstItems.Add(orderDetail);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null)
                    {
                        //var sessionitemsfilterlist = sessionitems.Where(x=> x.pid==order.pid && x.uniqueid= order.uniqueid)

                        foreach (var sessionItem in sessionitems)
                        {
                            //First check if Orders 
                            var item = order.Items.FirstOrDefault(x => x.id == sessionItem.id && x.pid == sessionItem.pid);
                            if (item != null && order.uniqueid == sessionItem.uniqueid)
                            {
                                if (item.qty == 0)
                                {
                                    lstItemsToRemove.Add(sessionItem);
                                }
                                else
                                {
                                    if (!item.id.Trim().Contains("-1"))
                                    {
                                        if (item.qty > item.totalQty && item.name != "OVERSELL")
                                        {
                                            item.qty = item.totalQty;
                                        }
                                    }
                                    if (item.qty > 0)
                                    {
                                        sessionItem.discgiven = ((sessionItem.discgiven / sessionItem.qty) * item.qty);
                                        sessionItem.discgiven_base = ((sessionItem.discgiven_base / sessionItem.qty) * item.qty);
                                        sessionItem.qty = item.qty;
                                        sessionItem.productdesc = order.ProductDescription;
                                        sessionItem.colors = order.Reference;
                                        sessionItem.slprice = Math.Round(order.SellingPrice * exchangeRate, 2);
                                        sessionItem.slprice_base = Math.Round(order.SellingPrice, 2);
                                        sessionItem.deliverytype = order.DeliveryTypeID;
                                        sessionItem.netprice = Math.Round((item.qty * sessionItem.slprice) - sessionItem.discgiven, 2);
                                        sessionItem.DestinationPoint = order.DestinationPoint;
                                        sessionItem.deliverydate = order.deliverydate;
                                        sessionItem.stype = order.stype;
                                        sessionItem.sale_type = order.sale_type;
                                        sessionItem.cust_leadtime = order.CustomerLeadtime;
                                        sessionItem.Discount_Comment = order.Discount_Comment;
                                        var salePrice = sessionItem.netprice;
                                        sessionItem.vatamount = Math.Round((salePrice - (salePrice * 100) / (100 + sessionItem.vatrate)), 2); 
                                        sessionItem.oversaleFlag = item.name == "OVERSELL" ? -2 : 0;                                        
                                        ClientSession.SetSession(SessionConstants.POSSelectedProducts, sessionitems);
                                        itemIds.Remove(sessionItem.id);
                                    }
                                }
                            }
                        }

                        var itemsToAdd = order.Items.Where(x => itemIds.Contains(x.id) && x.qty > 0).ToList();
                        if (itemsToAdd.Any())
                        {
                            int uniqueid = sessionitems.Count + 1;
                            var pid = itemsToAdd.First().pid;

                            foreach (var item in itemsToAdd)
                            {
                                if (item.qty > 0)
                                {
                                    var productFromDB = _productService.GetById(Convert.ToInt32(pid.ToString()));

                                    var orderDetail = new OrderDetail();
                                    orderDetail.uniqueid = order.uniqueid;
                                    orderDetail.pid = item.pid;
                                    orderDetail.id = item.id;
                                    orderDetail.productname = order.ProductName;
                                    orderDetail.productdesc = order.ProductDescription;
                                    orderDetail.SKU = Convert.ToString(productFromDB.Part_No);
                                    orderDetail.costprice = productFromDB.Cost_Price;
                                    orderDetail.qty = item.qty;
                                    orderDetail.sale_type = order.sale_type;
                                    orderDetail.cust_leadtime = order.CustomerLeadtime;
                                    orderDetail.Discount_Comment = order.Discount_Comment;
                                    orderDetail.slprice = order.SellingPrice * exchangeRate;
                                    orderDetail.slprice_base = order.SellingPrice;
                                    if (productFromDB.Promodiscount != 0)
                                    {
                                        orderDetail.discgiven = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100) * exchangeRate;
                                        orderDetail.discgiven_base = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100);

                                    }
                                    else
                                    {
                                        orderDetail.discgiven = 0;
                                        orderDetail.discgiven_base = 0;
                                    }
                                    orderDetail.vatrate = Convert.ToDecimal(productFromDB.vatrate);
                                     //orderDetail.vatamount = Convert.ToDecimal(item.qty * (Convert.ToDecimal(productFromDB.vatrate) * (Convert.ToDecimal(productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice) / 100)));
                                    orderDetail.locationid = Convert.ToInt32(item.stocklocationid);
                                    orderDetail.poid = item.poid;
                                    orderDetail.porid = -1;
                                    orderDetail.product_image = productFromDB.product_image;
                                    orderDetail.colors = order.Reference;
                                    orderDetail.stype = order.stype;
                                    orderDetail.netprice = (orderDetail.qty * orderDetail.slprice) - (orderDetail.discgiven);
                                    var salePrice = orderDetail.netprice;
                                    orderDetail.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + (Convert.ToDecimal(productFromDB.vatrate))))), 2);                                                      
                                    orderDetail.deliverytype = order.DeliveryTypeID;
                                    orderDetail.deliverydate = order.deliverydate;
                                    //DateTime.Today.Date.ToString("dd-MM-yyyy");                            
                                    orderDetail.promotion = productFromDB.promotion;
                                    orderDetail.Promodiscount = productFromDB.Promodiscount; // Here we set a Discount from DB for current Product
                                    orderDetail.PromoId = productFromDB.PromoId;
                                    orderDetail.Promosellingprice = productFromDB.Promosellingprice;
                                    if (order.DestinationPoint != 0)
                                        orderDetail.DestinationPoint = order.DestinationPoint;
                                    else
                                        orderDetail.DestinationPoint = Convert.ToInt32(item.stocklocationid);
                                    orderDetail.oversaleFlag = item.name == "OVERSELL" ? -2 : 0;

                                    sessionitems.Add(orderDetail);
                                }

                            }
                        }
                        for (int i = 0; i < lstItemsToRemove.Count; i++)
                        {
                            sessionitems.Remove(lstItemsToRemove[i]);
                        }
                        //sessionitems.Except(lstItemsToRemove).ToList();
                        lstItems = sessionitems;
                    }
                }
                ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                return Json(JsonRequestBehavior.AllowGet);
            }
            else
            {


                List<string> itemIds = new List<string>();
                itemIds = order.Items.Select(x => x.id).ToList();

                decimal exchangeRate = 1;
                if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
                {
                    var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                    exchangeRate = ordMaster.exchangerate;
                }
                if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null)
                {
                    sessionitems = ((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts)).ToList();
                    foreach (var sessionItem in sessionitems)
                    {
                        var item = order.Items.FirstOrDefault(x => x.id == sessionItem.id && x.pid == sessionItem.pid);
                        if (item != null)
                        {
                            if (item.qty == 0)
                            {
                                lstItemsToRemove.Add(sessionItem);
                            }
                            else
                            {
                                if (!item.id.Trim().Contains("-1"))
                                {
                                    if (item.qty > item.totalQty && item.name != "OVERSELL")
                                    {
                                        item.qty = item.totalQty;
                                    }
                                }
                                if (item.qty > 0)
                                {
                                    sessionItem.discgiven = ((sessionItem.discgiven / sessionItem.qty) * item.qty);
                                    sessionItem.discgiven_base = ((sessionItem.discgiven_base / sessionItem.qty) * item.qty);
                                    sessionItem.qty = item.qty;
                                    sessionItem.productdesc = order.ProductDescription;
                                    sessionItem.colors = order.Reference;
                                    sessionItem.slprice = Math.Round(order.SellingPrice * exchangeRate, 2);
                                    sessionItem.slprice_base = Math.Round(order.SellingPrice, 2);
                                    sessionItem.deliverytype = order.DeliveryTypeID;
                                    sessionItem.netprice = Math.Round((item.qty * sessionItem.slprice) - sessionItem.discgiven, 2);
                                    sessionItem.DestinationPoint = order.DestinationPoint;
                                    sessionItem.deliverydate = order.deliverydate;
                                    sessionItem.sale_type = order.sale_type;
                                    var salePrice = sessionItem.netprice;
                                    sessionItem.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + sessionItem.vatrate))), 2); 
                                    sessionItem.cust_leadtime = order.CustomerLeadtime;
                                    sessionItem.Discount_Comment = order.Discount_Comment;
                                    sessionItem.oversaleFlag = item.name == "OVERSELL" ? -2 : 0;
                                    ClientSession.SetSession(SessionConstants.POSSelectedProducts, sessionitems);
                                    itemIds.Remove(sessionItem.id);
                                }
                            }
                        }
                    }
                    for (int i = 0; i < lstItemsToRemove.Count; i++)
                    {
                        sessionitems.Remove(lstItemsToRemove[i]);
                    }
                    //sessionitems.Except(lstItemsToRemove).ToList();
                    lstItems = sessionitems;
                }


                var itemsToAdd = order.Items.Where(x => itemIds.Contains(x.id) && x.qty > 0).ToList();
                if (itemsToAdd.Any())
                {
                    var pid = itemsToAdd.First().pid;

                    var productFromDB = _productService.GetById(Convert.ToInt32(pid.ToString()));

                    if (productFromDB != null)
                    {

                        foreach (var item in itemsToAdd)
                        {
                            if (!item.id.Trim().Contains("-1"))
                            {
                                if (item.qty > item.totalQty && item.name != "OVERSELL")
                                {
                                    item.qty = item.totalQty;
                                }
                            }

                            if (item.qty > 0)
                            {
                                var orderDetail = new OrderDetail();
                                orderDetail.pid = item.pid;
                                orderDetail.id = item.id;
                                orderDetail.uniqueid = order.uniqueid;
                                orderDetail.productname = order.ProductName;
                                orderDetail.productdesc = order.ProductDescription;
                                orderDetail.SKU = Convert.ToString(productFromDB.Part_No);
                                orderDetail.costprice = productFromDB.Cost_Price;
                                orderDetail.qty = item.qty;
                                orderDetail.sale_type = order.sale_type;
                                orderDetail.cust_leadtime = order.CustomerLeadtime;
                                orderDetail.Discount_Comment = order.Discount_Comment;
                                orderDetail.slprice = order.SellingPrice * exchangeRate;
                                orderDetail.slprice_base = order.SellingPrice;
                                if (productFromDB.Promodiscount != 0)
                                {
                                    orderDetail.discgiven = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100) * exchangeRate;
                                    orderDetail.discgiven_base = (((orderDetail.slprice_base * orderDetail.qty) * (productFromDB.Promodiscount)) / 100);

                                }
                                else
                                {
                                    orderDetail.discgiven = 0;
                                    orderDetail.discgiven_base = 0;
                                }
                                orderDetail.vatrate = Convert.ToDecimal(productFromDB.vatrate);
                                // Convert.ToDecimal(item.qty * (Convert.ToDecimal(productFromDB.vatrate) * (Convert.ToDecimal(productFromDB.Promosellingprice == 0 ? productFromDB.Selling_Price : productFromDB.Promosellingprice) / 100)));
                                orderDetail.locationid = Convert.ToInt32(item.stocklocationid);
                                orderDetail.poid = item.poid;
                                orderDetail.porid = -1;
                                orderDetail.product_image = productFromDB.product_image;
                                orderDetail.colors = order.Reference;
                                orderDetail.stype = order.stype;
                                orderDetail.netprice = (orderDetail.qty * orderDetail.slprice) - (orderDetail.discgiven);
                                var salePrice = orderDetail.netprice;
                                orderDetail.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + (Convert.ToDecimal(productFromDB.vatrate))))), 2);                               
                                orderDetail.deliverytype = order.DeliveryTypeID;
                                orderDetail.deliverydate = order.deliverydate;
                                //DateTime.Today.Date.ToString("dd-MM-yyyy");                            
                                orderDetail.promotion = productFromDB.promotion;
                                orderDetail.Promodiscount = productFromDB.Promodiscount; // Here we set a Discount from DB for current Product
                                orderDetail.PromoId = productFromDB.PromoId;
                                orderDetail.Promosellingprice = productFromDB.Promosellingprice;
                                if (order.DestinationPoint != 0)
                                    orderDetail.DestinationPoint = order.DestinationPoint;
                                else
                                    orderDetail.DestinationPoint = Convert.ToInt32(item.stocklocationid);
                                orderDetail.oversaleFlag = item.name == "OVERSELL" ? -2 : 0;
                                lstItems.Add(orderDetail);
                            }
                        }
                    }
                }

                ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
                return Json(JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult OrderAvailableCheck()
        {
            if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null)
                return this.Json(new { IsSuccess = true, Message = "sucess" }, JsonRequestBehavior.AllowGet);
            else
                return this.Json(new { IsSuccess = false, Message = "not available" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult chkQuicksaleforlocation(string barcode)
        {
            var OrdStatus = 0;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                OrdStatus = ordMaster.orderstatus;
            }

            if (OrdStatus != 6)
            {
                var result = _availableQuantityService.CheckDefaultLocationForBarcode(barcode);

                string[] values = result.Split('|');

                Boolean IsScanLocationByCategory = true; //default true;

                if (System.Configuration.ConfigurationManager.AppSettings["ScanLocationByCategory"] != null)
                {
                    IsScanLocationByCategory = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ScanLocationByCategory"]);
                }

                if (IsScanLocationByCategory)
                    return this.Json(new { IsSuccess = true, ChooseFromloc = values[0], productinfo = values[1] }, JsonRequestBehavior.AllowGet);
                else
                    return this.Json(new { IsSuccess = true, ChooseFromloc = "1", productinfo = values[1] }, JsonRequestBehavior.AllowGet);
            }
            else
                return this.Json(new { IsSuccess = false, Message = "Cannot Add new Item To Confirmed Order" }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteItem(string selectedProductIds)
        {
            var result = new Domain.Entities.GeneralRuleResult();
            string OrderId = string.Empty;

            try
            {
                var OrderMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                if (OrderMaster != null)
                    OrderId = OrderMaster.orderid;
                POSLogger.Info("Items deleted:" + selectedProductIds);
                string[] Ids = selectedProductIds.Split(',');
                var sessionitems = ((List<OrderDetail>)ClientSession.GetSession(SessionConstants.POSSelectedProducts)).ToList();
                string IdstoDelete = string.Empty;

                foreach (string Id in Ids)
                {
                    int[] array_uid_pid = Array.ConvertAll(Id.Split('|'), int.Parse);
                    List<OrderDetail> lstItems = sessionitems.Where(s => s.uniqueid == array_uid_pid[0] && s.pid == array_uid_pid[1].ToString()).ToList();
                    for (int i = 0; i < lstItems.Count; i++)
                    {
                        if (lstItems[i].OrderDetailId == 0)
                            sessionitems.Remove(lstItems[i]);
                        else
                        {
                            sessionitems.Remove(lstItems[i]);
                            IdstoDelete = IdstoDelete + "|" + lstItems[i].OrderDetailId;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(IdstoDelete))
                    result = _OrderMasterService.DeleteOrderItem(IdstoDelete.Substring(1), OrderId);

                ClientSession.SetSession(SessionConstants.POSSelectedProducts, sessionitems);

                if (result.status == "success")
                    return Json(new { success = true, message = result.outputvalue }); 
                else
                    return Json(new { success = true, message = "Items deleted successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "Error On Delete Item" });
            }

        }

        public PartialViewResult Discount(string selectedProductIds)
        {
            var OrdStatus = 0;
            var disc_Comment = string.Empty;
            if (ClientSession.GetSession(SessionConstants.POSOrderMaster) != null)
            {
                var ordMaster = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
                OrdStatus = ordMaster.orderstatus;
            }
            //if (ClientSession.GetSession(SessionConstants.POSSelectedProducts) != null)
            //{
            //    var ordDetails = (OrderDetail)ClientSession.GetSession(SessionConstants.POSSelectedProducts);
            //    disc_Comment = ordDetails.Discount_Comment;
            //}
            ViewBag.Ordstatus = OrdStatus;
            var model = new DiscountModel();
            model.SelectedPrductIds = selectedProductIds;
            //model.Discount_Comment = disc_Comment;
            return PartialView(model);
        }

        public List<SelectListItem> GetDestinationLoc()
        {
            var listItems = _keyValueItemService.GetDestinationLocation().Select(x => new SelectListItem()
            {
                Text = x.name,
                Value = x.id

            }).ToList();

            return listItems;
        }
        [HttpPost]
        public JsonResult Openselectedorder(string orderid)
        {
            var lstItems = new List<OrderDetail>();
            //var lstordermasteritem = new List<OrderMaster>();
            int ordstatus = 0;
            string result = string.Empty;

            var productFromDB = _productService.GetByOrderid(orderid);
            if (productFromDB != null)
            {
                foreach (var item in productFromDB.orderMaster)
                {
                    ordstatus = Convert.ToInt32(item.orderstatus);
                }
            }

            if (productFromDB != null)
            {
                //if (ordstatus == 4)
                //{
                foreach (var item in productFromDB.orderMaster)
                {
                    var orderMaster = new OrderMaster();
                    orderMaster.customerid = item.customerid;
                    orderMaster.salespersonid = item.salespersonid;
                    orderMaster.deliverycharge = item.deliverycharge;
                    orderMaster.vatamount = item.vatamount;
                    orderMaster.netpayment = item.netpayment;
                    orderMaster.comments = item.comments;
                    orderMaster.deliveryaddressid = item.deliveryaddressid;
                    orderMaster.customer = item.customer;
                    orderMaster.salesperson = item.salesperson;
                    orderMaster.temporderid = item.temporderid;
                    orderMaster.orderid = item.orderid;
                    orderMaster.exchangerate = item.exchangerate;
                    orderMaster.discount = item.discgiven_base;
                    orderMaster.currency = item.currency;
                    orderMaster.orderstatus = item.orderstatus;
                    orderMaster.totalpaid = item.paid;
                    orderMaster.amountdue = item.amountdue;
                    orderMaster.misc = item.MiscCharges;
                    orderMaster.allreadypaidamount = item.paid;
                    ClientSession.SetSession(SessionConstants.orderstatus, item.orderstatus);
                    ClientSession.SetSession(SessionConstants.customerId, item.customerid);
                    ClientSession.SetSession(SessionConstants.POSOrderMaster, orderMaster);
                }
                int uniqueid = 0;
                foreach (var item in productFromDB.orderDetails)
                {
                    if (item.qty > 0)
                    {
                        var orderDetail = new OrderDetail();
                        orderDetail.qty = item.qty;
                        if (((List<OrderDetail>)lstItems).Where(x => x.OrderDetailId == item.OrderDetailId).Count() == 0)
                        {
                            uniqueid = lstItems.Count + 1;
                            orderDetail.discgiven = item.discountgiven / item.qty;
                            orderDetail.discgiven_base = item.discountgiven / item.exchangerate;
                        }
                        orderDetail.isscanitem = item.isscanitem == 1 ? true : false;
                        orderDetail.slprice = item.SellingPrice;
                        orderDetail.slprice_base = item.SellingPrice / item.exchangerate;
                        orderDetail.vatrate = Convert.ToDecimal(item.vatrate) * 100;
                        orderDetail.costprice = item.costprice;
                          
                        //orderDetail.vatamount = Convert.ToDecimal(item.qty * (Convert.ToDecimal(item.vatrate * 100) * (Convert.ToDecimal(item.SellingPrice) / 100)));
                        orderDetail.netprice = (orderDetail.qty * orderDetail.slprice) - (orderDetail.discgiven);
                        var salePrice = orderDetail.netprice;
                        orderDetail.vatamount = Math.Round((salePrice - ((salePrice * 100) / (100 + orderDetail.vatrate))), 2);                         
                        orderDetail.uniqueid = uniqueid;
                        orderDetail.OrderDetailId = item.OrderDetailId;
                        orderDetail.pid = item.ProductId;
                        orderDetail.id = item.poid + "/" + item.stocklocationid;
                        orderDetail.productname = item.product_name;
                        orderDetail.productdesc = item.productdesc;
                        orderDetail.colors = item.colors;
                        orderDetail.cust_leadtime = item.cust_leadtime;
                        orderDetail.SKU = Convert.ToString(item.part_no);


                        orderDetail.locationid = Convert.ToInt32(item.stocklocationid);
                        orderDetail.poid = item.poid;
                        orderDetail.porid = -1;
                        orderDetail.product_image = item.product_image;
                        orderDetail.colors = item.colors;
                        orderDetail.sale_type = item.sale_type;
                        orderDetail.deliverytype = item.deliverytype;
                        orderDetail.deliverydate = Convert.ToDateTime(item.DeliveryDate).ToShortDateString();//item.DeliveryDate;                           
                        //DateTime.Today.Date.ToString("dd-MM-yyyy");                            
                        //orderDetail.promotion = productFromDB.promotion;
                        //orderDetail.Promodiscount = productFromDB.Promodiscount; // Here we set a Discount from DB for current Product
                        //orderDetail.PromoId = productFromDB.PromoId;
                        //orderDetail.Promosellingprice = productFromDB.Promosellingprice;
                        orderDetail.DestinationPoint = item.destinationpoint;
                        orderDetail.orderitemdetailid = item.orderitemdetailid;
                        orderDetail.stype = item.stype;
                        orderDetail.Discount_Comment = item.Discount_Comment;

                        if (orderDetail.id == item.oversaleid)
                            orderDetail.oversaleFlag = -2;
                        else
                            orderDetail.oversaleFlag = 0;


                        lstItems.Add(orderDetail);
                    }
                }
                result = "success";
                //}
                //else
                //{
                //    result = "Cannot re-open confirmed Order";
                //}
            }




            ClientSession.SetSession(SessionConstants.POSSelectedProducts, lstItems);
            OrderMaster ord = (OrderMaster)ClientSession.GetSession(SessionConstants.POSOrderMaster);
            return Json(new { success = true, openstatus = result });
            //return Json(JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetSaletypeInfo(string param)
        {
            string[] Parameter = param.Split(',');
            var result = _productService.GetStypeInfo(Parameter[0], Parameter[1]);

            return Json(new { success = true, description = result.description, saletype = result.sale_type });
        }

        public PartialViewResult _GetReport()
        {
            string defaultreportvalue = (System.Configuration.ConfigurationManager.AppSettings["defaultreport"] != null ? System.Configuration.ConfigurationManager.AppSettings["defaultreport"].ToString() : "salesacknowledgementQPOSreprintreport");

            List<ReportItem> model = new List<ReportItem>();

            string fileName = Server.MapPath("~/ReportsConfiguration.xml");
            string xmlString = System.IO.File.ReadAllText(fileName);

            ReportConfig.Reports reconnectResonse = new ReportConfig.Reports();
            XmlSerializer serializer = new XmlSerializer(typeof(ReportConfig.Reports));
            using (TextReader reader = new StringReader(xmlString))
            {
                reconnectResonse = (ReportConfig.Reports)serializer.Deserialize(reader);
            }
            foreach (var item in reconnectResonse.Report)
            {
                if (item.name == defaultreportvalue)
                    model.Add(new ReportItem { DefaultValue = item.@default, Displayname = item.displayname, Name = item.name, Selected = true });
                else
                    model.Add(new ReportItem { DefaultValue = item.@default, Displayname = item.displayname, Name = item.name, Selected = false });
            }
            return PartialView(model);
        }

        public JsonResult SelectReport(List<ReportItem> model)
        {
            string Reportlist = string.Empty;
            try
            {
                foreach (var item in model)
                {
                    if (item.Selected)
                        Reportlist = Reportlist + item.Name + "|";
                }
                if (Reportlist != string.Empty)
                {
                    int Place = Reportlist.LastIndexOf("|");
                    string result = Reportlist.Remove(Place);
                    return Json(new { success = true, reportnames = result });
                }
                else
                    return Json(new { success = true, reportnames = Reportlist });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, reportnames = ex.Message });
            }

        }
        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        [HttpPost]
        public JsonResult GetProductData(string ProductId)
        {
            var result = _availableQuantityService.GetProductData(ProductId);


            if (result.result != null)
                return this.Json(new { IsSuccess = true, productinfo = result.result }, JsonRequestBehavior.AllowGet);
            else
                return this.Json(new { IsSuccess = false }, JsonRequestBehavior.AllowGet);
        }
    }
}