﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;
using GoodSide.Web.Helpers;
using GoodSide.Web.Models.Home;
using GoodSide.Web.Attributes;

namespace GoodSide.Web.Controllers
{
    [SessionExpireFilterAttribute]
    public class HomeController : BaseController
    {
        private IProductService _productService;

        public HomeController(IProductService productService)
        {
            _productService = productService;
        }

        public ActionResult Index()
        {

            //HttpContext.Session["userid"] = "16";
            //HttpContext.Session["companyid"] = "40";
            //HttpContext.Session["username"] = "admin";
            //HttpContext.Session["storename"] = "EZ Living Galway,";

            //RuleParameters ruleParameters = new RuleParameters();

            //// ensure there are products for the example
            //if (!_productService.GetAll(ruleParameters).Any())
            //{
            //    //_productService.Create(new Product { Name = "Product 1" });
            //    //_productService.Create(new Product { Name = "Product 2" });
            //    //_productService.Create(new Product { Name = "Product 3" });
            //}

            var viewModel = new IndexViewModel();
            //viewModel.Products = _productService.GetAll(ruleParameters);

            return View(viewModel);
        }
    }
}
