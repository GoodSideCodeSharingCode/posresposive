﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
//using GoodSide.Domain.Helpers;
using Ninject;
using GoodSide.Web.Helpers;
using System.Web.Routing;

namespace GoodSide.Web.Controllers
{
    public class BaseController : Controller
    {
        public BaseController()
        {

        }

        //// base controller for the project 
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (ClientSession.GetSession(SessionConstants.userid)==null &&  Convert.ToInt32(ClientSession.GetSession(SessionConstants.userid)) == 0)
            {
                #region CHECK SESSION TIMEOUT
                // Check if Ajax Request
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {

                    #region FOR AJAX REQUEST
                    // Set HttpContext Item  AjaxPermissionDenied 
                    // It will check on Global.asax->  Application_EndRequest with HttpContext item.
                    filterContext.HttpContext.Items["AjaxPermissionDenied"] = true;
                    HttpContext.Response.StatusCode = 501;
                    #endregion
                }
                else
                {
                    #region FOR GENERAL REQUEST
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                         {
                                { "controller", "Account" },
                                { "action", "LogOff" },
                                { "area", "Account" }
                         });
                    #endregion
                }
                #endregion
            }

        }

    }
}
