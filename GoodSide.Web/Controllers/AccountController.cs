﻿using GoodSide.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodSide.Web.Helpers;
using GoodSide.Web.Models.Account;
using GoodSide.Domain.Services;
using GoodSide.Domain.Entities;
using System.Reflection;

namespace GoodSide.Web.Controllers
{
    public class AccountController : Controller
    {
        private IAccountCompanyService _actCompanyService;
        private IAccountUserService _actUserService;

        public AccountController(IAccountCompanyService actCompanyService, IAccountUserService actUserService)
        {
            _actCompanyService = actCompanyService;
            _actUserService = actUserService;
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        // GET: Account/Account
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;           

            // Get companies
            IList<AccountCompany> lstCompanies = _actCompanyService.GetCompanies();
          
            var result = lstCompanies
     .Select((r, index) => new SelectListItem { Text = r.Name, Value = r.CompanyId.ToString() });
            ViewBag.lstCompanies = new SelectList(result, "Value", "Text", "SelectedValue"); ;
            

            Assembly assembly = Assembly.LoadFrom(Server.MapPath("~/bin/GoodSide.Web.dll"));
            Version ver = assembly.GetName().Version;
            ViewBag.Version = string.Format("{0}.{1}.{2}", ver.Major, ver.Minor, ver.Build);
            ViewBag.PublishedDate = System.IO.File.GetLastWriteTime(assembly.Location).ToShortDateString();
            
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel dataForms, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;            
        
            // Try and log the user in
            AccountUser sysUsr = _actUserService.GetLoggedInUser(dataForms.Password, dataForms.CompanyID);

            // Get companies
            IList<AccountCompany> lstCompanies = _actCompanyService.GetCompanies();

            //Get Selected Company Name 
            string CompaniesName = (lstCompanies.Where(item => item.CompanyId == dataForms.CompanyID)).Select(c => c.Name).SingleOrDefault();


            if (sysUsr != null)
            {
                TempData["ErrorMessage"] = null;
                //Add the user to the application session                
                addUserDetailsToSession(sysUsr, dataForms, CompaniesName);
                return RedirectToLocal(returnUrl);
            }
            else
            {                
                TempData["ErrorMessage"] = "The user name or password provided is incorrect.";
                return RedirectToAction("Login", new { url = returnUrl });                
            }
        }

        public ActionResult LogOff()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login");
        }
        /// <summary>
        /// Add user to current session for navigation
        /// </summary>
        /// <param name="user">Current User</param>
        private void addUserDetailsToSession(AccountUser user, LoginModel dataForms, string StoreName)
        {
            if (user != null)
            {
                ClientSession.SetSession(SessionConstants.userid, user.UserId);
                ClientSession.SetSession(SessionConstants.companyid, dataForms.CompanyID);
                ClientSession.SetSession(SessionConstants.username, user.UserName);
                ClientSession.SetSession(SessionConstants.userrole, user.UserRole);
                ClientSession.SetSession(SessionConstants.passwordOnly, true);
                ClientSession.SetSession(SessionConstants.storename, StoreName);
                ClientSession.SetSession(SessionConstants.entityid, user.EntityId);

            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "POS");
        }      
    }
}