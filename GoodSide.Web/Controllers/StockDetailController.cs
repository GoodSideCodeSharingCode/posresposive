﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;

using System.Net;
using System.Net.Http;

using System.Web.Http;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;
using GoodSide.Web.Models.StockDetail;
using AutoMapper;
namespace GoodSide.Web.Controllers
{
    public class StockDetailController : Controller
    {
        private IStockDetailService _stockDetailService;
        private IStockItemService _stockItemService;

        public StockDetailController(IStockDetailService stockDetailService, IStockItemService stockItemService)
        {
            _stockDetailService = stockDetailService;
            _stockItemService = stockItemService;
        }

        // GET: StockDetail
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTableData(int draw, int start, int length)
        {
            var test = string.Empty;

            string search = " ";
            if (HttpContext.Request.QueryString["search[value]"] != null)
            {
                search = HttpContext.Request.QueryString["search[value]"]; ;
            }

            int sortColumn = -1;
            string sortDirection = "asc";

            if (HttpContext.Request.QueryString["order[0][column]"] != null)
            {
                sortColumn = int.Parse(HttpContext.Request.QueryString["order[0][column]"]);
            }
            if (HttpContext.Request.QueryString["order[0][dir]"] != null)
            {
                sortDirection = HttpContext.Request.QueryString["order[0][dir]"];
            }

            var lstProperties = typeof(StockItemInfo).GetProperties().ToList();

            //if (string.IsNullOrEmpty(search))
            //{
            //    search = "%%";
            //}
            //else
            //{
            //    search = "%" + search + "%";
            //}

            // search = HttpUtility.UrlEncode(search);
            var result = _stockItemService.GetAll(search.Trim(), start, length, lstProperties[sortColumn].Name, sortDirection);

            DataTableData dataTableData = new DataTableData();
            dataTableData.draw = draw;
            dataTableData.recordsTotal = result.pagingInfo.FirstOrDefault().t;
            dataTableData.data = result.stockItemInfo;
            dataTableData.recordsFiltered = result.pagingInfo.FirstOrDefault().t;
            foreach (var row in dataTableData.data)
            {
                if (row.onpromo == "x")
                    row.onpromo = row.onpromo.Replace("x", "YES");
                else
                    row.onpromo = row.onpromo.Replace("blank", "NO");
            }

            return Json(dataTableData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StockDetailedInfo(int stockId)
        {
            var result = _stockDetailService.GetStockDetailedInfo(stockId);

            StockDetailViewModel objModel = new StockDetailViewModel();

            Mapper.CreateMap<StockDetail, StockDetailViewModel>();

            objModel = Mapper.Map<StockDetail, StockDetailViewModel>(result.stockDetail[0]);

            Mapper.CreateMap<StockAvailabilityDetail, StockAvailabilityInfo>();

            objModel.lstStockAvailability = Mapper.Map<List<StockAvailabilityDetail>, List<StockAvailabilityInfo>>(result.stockAvailability);

            return PartialView("_StockDetailedInfo", objModel);
        }
        public ActionResult StockDetailedImageInfo(int stockId)
        {
            var result = _stockDetailService.GetStockImageInfo(stockId);

            StockDetailImageViewModel objModel = new StockDetailImageViewModel();

            Mapper.CreateMap<StockDetailImage, StockDetailImageViewModel>();

            if(result.stockDetail.Count>0)
                objModel = Mapper.Map<StockDetailImage, StockDetailImageViewModel>(result.stockDetail[0]);

            Mapper.CreateMap<StockDetailImageList, StockImageInfo>();

            objModel.lststockImages = Mapper.Map<List<StockDetailImageList>, List<StockImageInfo>>(result.stockImages);

            return PartialView("_StockDetailsImageInfo", objModel);
        }
        public PartialViewResult _StockDetails()
        {

            return PartialView();
        }




    }
}