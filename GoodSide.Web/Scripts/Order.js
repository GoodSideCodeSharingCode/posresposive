﻿var orderService = (function () {

    $(".nav-justified").on("click", "a", function (e) {
        $(".nav-justified").find("a").removeClass('active')
        $(this).addClass('active').tab('show');

    })

    // Stores a reference of the messageBoxService
    var instance;
    var dialogCustomerInstance;
   

    ///General function to open DialogueBox
    function LoadDialogueBox(contentURL, divID, dialogtitle,data) {
        AjaxRequestStart();
        var dialogInstance;
        $.ajax({
            url: contentURL,
            cache: false,
            async: false,
            data: data,
            success: function (result) {
                dialogInstance = BootstrapDialog.show({
                    id: divID,
                    size: BootstrapDialog.SIZE_WIDE,
                    title: dialogtitle,
                    type: BootstrapDialog.TYPE_SUCCESS,
                    closeByBackdrop: false,
                    closeByKeyboard: false,
                    draggable: true,
                    message: $('<div id="modelWindow"></div>').html(result)
                });
                
            },
            complete: function () {
                AjaxRequestFinish();
            }
        });
        return dialogInstance;
    }


    function LoadContentInDialog(contentURL, dialogID, headerText, data, needToReload) {
        $.ajax({
            url: contentURL,
            cache: false,
            data: data,
            success: function (result) {
                var dlgID = '#' + dialogID;
                if (headerText != undefined) {
                    $(dlgID).igDialog("option", "headerText", headerText);
                }
                $(dlgID).igDialog('open');
                $(dlgID).unbind('igdialogstatechanged');
                $(dlgID).bind('igdialogstatechanged', function (evt, ui) {
                    if (ui.action == "close") {
                        // clear the html of the dialog
                        $(dlgID).setHtml('');
                    }
                });
                $(dlgID).setHtml(result);
            },
            complete: function () {
            }
        });
    }


    function SelectCustomer(custId, actionUrl, customerdiv) {
        AjaxRequestStart();
        $.ajax({
            url: actionUrl,
            data: { customerId: custId },
            type: "GET",
            dataType: "html",
            success: function (data) {
                $("#" + customerdiv).html(data);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            complete: function () {
                AjaxRequestFinish();
            }
        });
    }
        
    function ReloadCustomerEdit(actionUrl, customerdata) {

        var result = false;
        AjaxRequestStart();
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            data: customerdata,
            datatype: "html",
            success: function (data) {
                result = data;
            },
            error: function (xhr, status, error) {
                result = '';
            },
            complete: function () {
                AjaxRequestFinish();
            }
        });

        return result;
    }

    

    // Initialize object of the messageBoxService
    function init() {
        // Public methods and variables
        return {
            LoadDialogueBox: function (contentURL, dialogID, dialogtitle,data) {
                return LoadDialogueBox(contentURL, dialogID, dialogtitle,data);
            },
            LoadContentInDialog: function (contentURL, dialogID, headerText, data, needToReload) {
                LoadContentInDialog(contentURL, dialogID, headerText, data, needToReload);
            },
            SelectCustomer: function (customerId, contentURL, contentDiv) {
                SelectCustomer(customerId, contentURL, contentDiv);
            },
            ReloadCustomerEdit: function (contentURL, data) {
                return ReloadCustomerEdit(contentURL, data)
            }
        }
    };


    return {
        // Get the Singleton instance if one exists or create one if it doesn't
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };

})();

var orderjs = orderService.getInstance();