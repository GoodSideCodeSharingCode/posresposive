﻿var posService = (function () {
    var instance;
    var dialogCustomerInstance;
    var baseUrl = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');

    $(".numericbox").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
           (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
           (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    ///General function to open DialogueBox
    function LoadDialogueBox(contentURL, divID, dialogtitle, data) {

        var dialogInstance;
        $.ajax({
            url: contentURL,
            cache: false,
            async: false,
            data: data,
            success: function (result) {
                dialogInstance = BootstrapDialog.show({
                    id: divID,
                    size: BootstrapDialog.SIZE_WIDE,
                    title: dialogtitle,
                    type: BootstrapDialog.TYPE_SUCCESS,
                    closeByBackdrop: false,
                    closeByKeyboard: false,
                    draggable: true,
                    message: $('<div id="modelWindow"></div>').html(result)
                });

            },
            complete: function () {

            }
        });
        return dialogInstance;
    }
    function LoadDialogueBoxSmall(contentURL, divID, dialogtitle, data) {

        var dialogInstance;
        $.ajax({
            url: contentURL,
            cache: false,
            async: false,
            data: data,
            success: function (result) {
                dialogInstance = BootstrapDialog.show({
                    id: divID,
                    size: BootstrapDialog.SIZE_SMALL,
                    title: dialogtitle,
                    type: BootstrapDialog.TYPE_SUCCESS,
                    closeByBackdrop: false,
                    closeByKeyboard: false,
                    draggable: true,
                    message: $('<div id="modelWindow"></div>').html(result)
                });

            },
            complete: function () {

            }
        });
        return dialogInstance;
    }


    function SearchProductByBarcode(barcodeNo) {
        if (barcodeNo == null || (barcodeNo != null || barcodeNo == "")) {
            LoadProductSearchPopup();
        }
        $.ajax({
            "url": baseUrl + "POS/SearchProduct",
            data: { barcode: barcodeNo },
            cache: false,
            async: false,
            type: "GET",
            success: function (resultData) {
                loadOrderMainGrid();
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }


    function LoadOrderMainGrid() {
        $.ajax({
            url: baseUrl + "POS/productdisplay",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#productSearchBody").html(data);
            }
        })
    }

    function LoadProductSearchPopup() {
        var url = baseUrl + "pos/ProductSearch";
        BootstrapDialog.show({
            id: 'dialogProductSearch',
            size: BootstrapDialog.SIZE_WIDE,
            title: 'Item Search',
            type: BootstrapDialog.TYPE_SUCCESS,
            closeByBackdrop: false,
            closeByKeyboard: false,
            draggable: true,
            message: $('<div id="modelWindow"></div>').load(url)

        });
    }

    function LoadDiscountPopup() {
        var selectedProducts = [];
        var selectedCheckBoxCount = $('.chkProducts:checkbox:checked').length;
        if (selectedCheckBoxCount > 0) {
            var selectedProducts = [];
            var checkboxes = $('.chkProducts:checkbox:checked');
            $.each(checkboxes, function () {
                var id = $(this).attr("data-id");
                selectedProducts.push(id);
            });
            var str = selectedProducts.join(',');
            var url = baseUrl + "order/Discount";
            BootstrapDialog.show({
                id: 'dialogDiscount',
                size: BootstrapDialog.SIZE_SMALL,
                title: 'Discount',
                type: BootstrapDialog.TYPE_SUCCESS,
                closeByBackdrop: false,
                closeByKeyboard: false,
                draggable: true,
                message: $('<div id="modelWindow"></div>').load(partialViewUrl)
            });
        }
        else {
            BootstrapDialog.alert("Please select atleast one product.");
        }
    }


    function LoadContentInDialog(contentURL, dialogID, headerText, data, needToReload) {
        $.ajax({
            url: contentURL,
            cache: false,
            data: data,
            success: function (result) {
                var dlgID = '#' + dialogID;
                if (headerText != undefined) {
                    $(dlgID).igDialog("option", "headerText", headerText);
                }
                $(dlgID).igDialog('open');
                $(dlgID).unbind('igdialogstatechanged');
                $(dlgID).bind('igdialogstatechanged', function (evt, ui) {
                    if (ui.action == "close") {
                        // clear the html of the dialog
                        $(dlgID).setHtml('');
                    }
                });
                $(dlgID).setHtml(result);
            },
            complete: function () {
            }
        });
    }


   
    function SelectCustomer(custId, actionUrl, customerdiv) {

        $.ajax({
            url: actionUrl,
            data: { customerId: custId },
            type: "GET",
            dataType: "html",
            success: function (data) {
                $("#" + customerdiv).html(data);
                if (dialogCustomerInstance != null) {
                    dialogCustomerInstance.close();
                    dialogCustomerInstance = null;
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            complete: function () {

            }
        });
    }
    function SelectOrder(OrderId, actionUrl, customerdiv) {

        $.ajax({
            url: actionUrl,
            data: { orderId: OrderId },
            type: "GET",
            dataType: "html",
            success: function (data) {
                $("#" + customerdiv).html(data);
                if (dialogCustomerInstance != null) {
                    dialogCustomerInstance.close();
                    dialogCustomerInstance = null;
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            complete: function () {

            }
        });
    }

    function OrderAvailableCheck(actionUrl) {
        var result = false;
        var salespersonid = null;
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "json",
            success: function (data) {
                result = data.IsSuccess;              
            },
            error: function (xhr, status, error) {
                result = false;
            },
            complete: function () {

            }
        });

        return result;
    }
    function OpenReport(actionUrl) {
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "json",
            success: function (data) {
                result = data.IsSuccess;
            },
            error: function (xhr, status, error) {
                result = false;
            },
            complete: function () {

            }
        });

        return result;
    }
    function SalesPersonCheck(actionUrl) {
        var result = false;
        var salespersonid = null;
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "json",
            success: function (data) {               
                salespersonid = data.Saleperson;               
            },
            error: function (xhr, status, error) {
                result = false;
            },
            complete: function () {

            }
        });

        return salespersonid;
    }


    function SelectProduct(prodId, actionUrl, productDiv, Uid) {

        if (Uid == null)
            formdata ={ pid: prodId}
        else
            formdata ={ pid: prodId,uniqueId:Uid}

        $.ajax({
            url: actionUrl,
            data: formdata,
            type: "GET",
            dataType: "html",
            success: function (data) {
                $("#" + productDiv).html(data);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            complete: function () {
            }
        });
    }

    function LoadVoucherSearchPopup() {

        var contentURL = baseUrl + '/Customer/VoucherSearch';
        var customerdialog = LoadDialogueBox(contentURL, 'dialogGiftVoucher', "Customer Search", null)
    }


    function LoadCustomerSearchPopup() {

        var contentURL = baseUrl + '/Customer/CustomerSearch';
        var customerdialog = LoadDialogueBox(contentURL, 'dialogCustomerSearch', "Customer Search", null)
    }

    function LoadCustomerAddEditForm(custId) {

        var data = { customerId: custId }

        var contentURL = baseUrl + 'Customer/CustomerAddEdit';

        var customerdialog = LoadDialogueBox(contentURL, 'dialogCustomerAddEdit', "Customer Add/Edit", data)
    }

    function CreateNewOrder(actionUrl) {
        var result = false;
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "json",
            success: function (data) {
                result = data.IsSuccess;
            },
            error: function (xhr, status, error) {
                result = false;
            },
            complete: function () {
            }
        });

        return result;
    }
    function ReloadCustomerEdit(actionUrl, customerdata) {

        var result = false;
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            data: customerdata,
            datatype: "html",
            success: function (data) {
                result = data;
            },
            error: function (xhr, status, error) {
                result = '';
            },
            complete: function () {

            }
        });

        return result;
    }

    function SaveOrderAsQuote(actionUrl) {
        
        var resultData = false;
        AjaxRequestStart();
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "json",
            success: function (data) {
                resultData = data;
            },
            error: function (xhr, status, error) {
                alert("error during create an order.");
                return false;
            },
            complete: function () {
                AjaxRequestFinish();
            }
        });

        return resultData;
    }

    function UpdateOtherInfo(actionUrl, otherInfodiv) {
        $.ajax({
            url: actionUrl,
            type: "GET",
            dataType: "html",
            success: function (data) {
                $("#" + otherInfodiv).html(data);
                loadOrderMainGrid();
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            },
            complete: function () {
            }
        });
    }

    function ReloadPaymentInfo(actionUrl) {
        var result = false;
        AjaxRequestStart();
        $.ajax({
            url: actionUrl,
            type: "GET",
            async: false,
            datatype: "html",
            success: function (data) {
                result = data;
            },
            error: function (xhr, status, error) {
                result = '';
            },
            complete: function () {
                AjaxRequestFinish();
            }
        });

        return result;
    }



    function CloseDialog(dialogid) {
        $.each(BootstrapDialog.dialogs, function (id, dialog) {
            if (id === dialogid) {
                dialog.close();
            }
        });
    }
    // Initialize object of the messageBoxService
    function init() {
        // Public methods and variables
        return {
            LoadDialogueBox: function (contentURL, dialogID, dialogtitle, data) {
                return LoadDialogueBox(contentURL, dialogID, dialogtitle, data);
            },
            LoadDialogueBoxSmall: function (contentURL, dialogID, dialogtitle, data) {
                return LoadDialogueBoxSmall(contentURL, dialogID, dialogtitle, data);
            },
            LoadContentInDialog: function (contentURL, dialogID, headerText, data, needToReload) {
                LoadContentInDialog(contentURL, dialogID, headerText, data, needToReload);
            },
            SelectCustomer: function (customerId, contentURL, contentDiv) {
                SelectCustomer(customerId, contentURL, contentDiv);
            },
            SelectOrder: function (OrderId, contentURL, contentDiv) {
                SelectCustomer(OrderId, contentURL, contentDiv);
            },
            SelectProduct: function (productId, contentURL, contentDiv, uniqueId) {
                SelectProduct(productId, contentURL, contentDiv, uniqueId);
            },
            UpdateOtherInfo: function (contentURL, contentDiv) {
                UpdateOtherInfo(contentURL, contentDiv);
            },
            OrderAvailableCheck: function (contentURL) {
                return OrderAvailableCheck(contentURL);
            },
            SalesPersonCheck: function (contentURL) {
                return SalesPersonCheck(contentURL);
            },
            OpenReport: function (contentURL) {
                return OpenReport(contentURL);
            },
            ReloadCustomerEdit: function (contentURL, data) {
                return ReloadCustomerEdit(contentURL, data)
            },
            SearchProductByBarcode: function (barcodeNo) {
                return SearchProductByBarcode(barcodeNo);
            },
            LoadOrderMainGrid: function () {
                return LoadOrderMainGrid();
            },
            LoadProductSearchPopup: function () {
                return LoadProductSearchPopup();
            },
            LoadDiscountPopup: function () {
                return LoadDiscountPopup();
            },
            LoadCustomerSearchPopup: function () {
                return LoadCustomerSearchPopup();
            },
            LoadCustomerAddEditForm: function (custId) {
                return LoadCustomerAddEditForm(custId);
            },
            CreateNewOrder: function (contentURL) {
                return CreateNewOrder(contentURL);
            },
            SaveOrderAsQuote: function (contentURL) {
                return SaveOrderAsQuote(contentURL);
            },
            ReloadPaymentInfo: function (contentURL) {
                return ReloadPaymentInfo(contentURL);
            },
            LoadVoucherSearchPopup: function () {
                return LoadVoucherSearchPopup();
            },
            LoadChangeDueForm: function (changeDue) {
                return LoadChangeDueForm(changeDue);
            },

            CloseDialog: function (dialogid) {
                return CloseDialog(dialogid);
            }
        }
    };


    return {
        // Get the Singleton instance if one exists or create one if it doesn't
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };

})();

var POSJS = posService.getInstance();