﻿var ajaxRequestCount = 0;

var messageType = {
    success: 1,
    error: 2,
    warning: 3,
    information: 4,
    broadcast: 5
};

function AjaxRequestStart() {
    ajaxRequestCount++;
    $("#ProgressBar").show();
}

function AjaxRequestFinish() {
    if (ajaxRequestCount > 0)
        ajaxRequestCount--;
    if (ajaxRequestCount == 0)
        $("#ProgressBar").hide();
}

$(document).ajaxStart(function () {  
    $("#ProgressBar").show();




});

$(document).ajaxComplete(function () {   
    $("#ProgressBar").hide();
});


//setup ajax error handling
$.ajaxSetup({
    error: function (jqXHR, textStatus, errorThrown) {
        RedirectToLoginOnSessionTimeout(jqXHR);
    }
});


// Function to take action on error in response
function RedirectToLoginOnSessionTimeout(xhr) {
   
    // If error is 401 or 501 then redirect user to login page
    if (xhr.status == "401" || xhr.status == "501") {
        if ($(this).parent.length > 0) {
            window.parent.location.reload();
        }
        else
            window.location.reload();
    }
}


function DisplayMessage(divID, messageType, messages, isConcateWithPreviousMessage, isShowClose) {

    if (isConcateWithPreviousMessage == undefined) {
        isConcateWithPreviousMessage = false;
    }
    if (isShowClose == undefined) {
        isShowClose = true;
    }

    if (!isConcateWithPreviousMessage) {
        // first remove all existing messages
        $("#" + divID + " .success").remove();
        $("#" + divID + " .error").remove()
        $("#" + divID + " .warning").remove()
        $("#" + divID + " .information").remove();
        $("#" + divID + " .broadcast").remove();
    }

    var messageclass = '';
    switch (messageType) {
        case 1:
            messageclass = 'success';
            break;
        case 2:
            messageclass = 'error';
            break;
        case 3:
            messageclass = 'warning';
            break;
        case 4:
            messageclass = 'information';
            break;
        case 5:
            messageclass = 'broadcast';
            break;
        default:
            break;
    }

    // append the newly created message to specified div
    var messageHTML = "";
    if (isShowClose)
        messageHTML = "<div class='" + messageclass + "'><a href='javascript:void(0)' onclick='HideMessages(this);' class='ic-close'></a>" + messages.join("<br/>") + "</div>";
    else
        messageHTML = "<div class='" + messageclass + "'><a href='javascript:void(0)' onclick='HideMessages(this);'></a>" + messages.join("<br/>") + "</div>";
    $("#" + divID).prepend(messageHTML);
}

function HideMessages(elem) {
    $(elem).parent().hide();
}