﻿using GoodSide.Domain.Entities;
using GoodSide.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodSide.Web.Models.POS
{



    public class ProductSearchViewModel
    {
        public string id { get; set; }
        public string pno { get; set; }
        public string pname { get; set; }
        public string description { get; set; }
        public string barcode_no { get; set; }
        public string supplier { get; set; }
        public string supplierid { get; set; }
        public string typeid { get; set; }
        public string categoryid { get; set; }
        public string stockTypeid { get; set; }
        public int companyid { get; set; }
        public string price { get; set; }
        public string quantity { get; set; }
        public string discount { get; set; }
        public string stype { get; set; }
        public string allocatedS { get; set; }
        public string unallocatedS { get; set; }
        public string totaS { get; set; }
        public int locationid { get; set; }

        public SelectList Suppliers { get; set; }
        public SelectList Categories { get; set; }
        public SelectList StockTypes { get; set; }
        public SelectList ProductTypes { get; set; }

       

    }

    public class ProductSearchDataTableData : DataTableModel
    {
        public List<ProductSearchItemInfo> data { get; set; }
    }




}