﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodSide.Web.Models.POS
{
    public class OtheInfoViewModel
    {
        [Required]
        public string defaultcurrency { get; set; }
        public string currencyname { get; set; }
        public string currencysymbol { get; set; }

        [Required]
        [Display(Name="Sales Person")]
        public string salespersonid { get; set; }

        public string salesperson { get; set; }

        public string DestinationPoint { get; set; }

        public string PassWord { get; set; }

        public string OptiontoShow { get; set; }

        public SelectList CurrencyList { get; set; }

        public SelectList SalesPersonList { get; set; }

        public SelectList DestinationPointList { get; set; }

        public string changeSalesPersonMessage { get; set; }

    }
}