﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodSide.Web.Models.POS
{
    public class AvailableQuantityModel
    {
        public int pid { get; set; }

        public string ProductName { get; set; }

        public string ProductDescription { get; set; }

        public string Reference { get; set; }

        public decimal SellingPrice { get; set; }

        public string hiddenId { get; set; }

        public int stype { get; set; }
        public string allocatedS { get; set; }
        public string unallocatedS { get; set; }

        public bool IsDelivery { get; set; }

        public bool IsAnyProductAdded { get; set; }

        public int DeliveryTypeID { get; set; }

        public string deliverydate { get; set; }

        public int DestinationPoint { get; set; }     

        public List<AvailableQuantityItemViewModel> Items { get; set; }

        public int uniqueid { get; set; }

        public decimal DeliveryCharge { get; set; }

        public string Notes { get; set; }

        public decimal discount { get; set; }

        public int sale_type { get; set; }

        public string itemdescription { get; set; }

        public string CustomerLeadtime { get; set; }

        public string cssType { get; set; }

        public string view_only { get; set; }

        public string ordercol { get; set; }

        public string Discount_Comment { get; set; }

    }

    public class AvailableQuantityItemViewModel
    {
        public string id { get; set; }

        public string name { get; set; }

        public string reference { get; set; }

        public string edate { get; set; }

        public int totalQty { get; set; }

        public string inorder { get; set; }

        public int poid { get; set; }

        public string stocklocationid { get; set; }

        public string pid { get; set; }

        public int qty { get; set; }

        public decimal discount { get; set; }

        public string cssType { get; set; }

        public string view_only { get; set; }

        public string ordercol { get; set; }


    }

    public class POSViewModel
    {
        public string id { get; set; }
        public string code { get; set; }

        [Required(ErrorMessage = "customer name is required.")]
        public string name { get; set; }

        public string countyid { get; set; }
        public string countryid { get; set; }
        public string postalcode { get; set; }

        [Required(ErrorMessage = "salesperson is required.")]
        public string salespersonid { get; set; }

        [Required(ErrorMessage = "phone number is required.")]
        public string phonenumber { get; set; }
        public string mobilenumber { get; set; }

        [Required(ErrorMessage = "address is required.")]
        public string address { get; set; }

        public string email { get; set; }

        public SelectList SalesPersons { get; set; }
        public SelectList CountyList { get; set; }
        public SelectList CountryList { get; set; }
    }

    [Serializable()]
    public class OrderDetail
    {
        public int uniqueid { get; set; }
        public string pid { get; set; }
        public string id { get; set; }
        public string SKU { get; set; }
        public decimal costprice { get; set; }
        public decimal slprice { get; set; }
        public decimal discgiven { get; set; }
        public decimal vatrate { get; set; }
        public decimal vatamount { get; set; }
        public decimal netprice { get; set; }
        public int locationid { get; set; }
        public int porid { get; set; }
        public int poid { get; set; }
        public int qty { get; set; }
        public string productdesc { get; set; }
        public string productname { get; set; }
        public string product_image { get; set; }
        public string colors { get; set; }
        public int deliverytype { get; set; }
        public string deliverydate { get; set; }
        public string table { get; set; }
        public int sumqty { get; set; }
        public int stype { get; set; }
        public int promotion { get; set; }
        public Decimal Promodiscount { get; set; }
        public Decimal Promosellingprice { get; set; }
        public int PromoId { get; set; }
        public int DestinationPoint { get; set; }
        public SelectList DestinationPointList { get; set; }



        public decimal discgiven_base { get; set; }     
        public decimal slprice_base { get; set; }
        public string currency_symbol { get; set; }
        public bool isscanitem { get; set; }
        public int sale_type { get; set; }
        public string cust_leadtime { get; set; }
        public int OrderDetailId { get; set; }
        public int orderitemdetailid { get; set; }
        public int oversaleFlag { get; set; }
        public string Discount_Comment { get; set; }
    }

    [Serializable()]
    public class OrderMaster
    {
        public string orderid { get; set; } 
        public string customerid { get; set; }
        public string salespersonid { get; set; }
        public decimal misc { get; set; }
        public decimal deliverycharge { get; set; }
        public decimal vatamount { get; set; }
        public decimal netpayment { get; set; }
        public decimal discount { get; set; }
        public string comments { get; set; }
        public int deliveryaddressid { get; set; }
        public string customer { get; set; }
        public string salesperson { get; set; }
        public decimal cash { get; set; }
        public string cashdetail { get; set; }
        public decimal visa { get; set; }
        public string visadetail { get; set; }
        public decimal giftvoucher { get; set; }
        public string gvdetail { get; set; }
        public decimal cheque { get; set; }
        public string chequedetail { get; set; }
        public decimal mastercard { get; set; }
        public string mcdetail { get; set; }
        public decimal laser { get; set; }
        public string laserdetail { get; set; }
        public decimal totalpaid { get; set; }
        public string deliveryaddress { get; set; }
        public decimal creditnote { get; set; }
        public string creditnotedetail { get; set; }
        public int vid { get; set; }
        public string temporderid { get; set; }
        public decimal changedue { get; set; }
        public string VatRegNo { get; set; }

        public decimal exchangerate { get; set; }
        public int currency { get; set; }
        public string currency_symbol { get; set; }
        public string paymentmode { get; set; }
        public string customeremail { get; set; }
        public decimal amount_tendered { get; set; }
        public int orderstatus { get; set; }
        public decimal paypal { get; set; }
        public string paypaldetail { get; set; }
        public decimal amountdue { get; set; }
        public decimal allreadypaidamount { get; set; }
        public int customerActive { get; set; }
    }

    [Serializable()]
    public class OrderItemDetails
    {
        public int id { get; set; }
        public int companyid { get; set; }
        public int groupid { get; set; }
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public string orderitemdescription { get; set; }
        public string Colors { get; set; }
        public int ProductType { get; set; }
        public int Type { get; set; }
        public int Qty { get; set; }
        public int Qty_original { get; set; }
        public int ApproxDelvQty { get; set; }
        public int ActualDelvQty { get; set; }
        public DateTime deliverydate { get; set; }
        public int DeliveryType { get; set; }
        public int Deliverd { get; set; }
        public int IsDeleted { get; set; }
        public int isReplaced { get; set; }
        public int stocklocationid { get; set; }
        public int porid { get; set; }
        public int poid { get; set; }
        public Decimal costprice { get; set; }
        public int zoneid { get; set; }
        public string dispatchStatus { get; set; }

    }

    public class DiscountModel
    {
        public int DiscountTypeID { get; set; }
        public decimal Discount { get; set; }
        public string Password { get; set; }
        public string SelectedPrductIds { get; set; }
        public string Discount_Comment { get; set; }
    }

    public class ProductDetailSummaryModel
    {
        public List<OrderDetail> orderdetailList { get; set; }
        public OrderMaster orderMaster { get; set; }
        public string BIRTReportURL { get; set; }
    }
    public class ReportModel
    {
        public string Report { get; set; }
      
    }
}