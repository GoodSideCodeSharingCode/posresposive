﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Models.POS
{

    public class GetReportModel
    {
        public List<ReportItem> reportsList { get; set; }
    }

    public class ReportItem
    {
        public string Name { get; set; }
        public string Displayname { get; set; }
        public string DefaultValue { get; set; }
        public bool Selected { get; set; }
    }
}