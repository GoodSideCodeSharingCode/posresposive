﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Models.Home
{
    public class HeaderInfoModel
    {
        public string UserName { get; set; }
        public string StoreName { get; set; }

    }
}