﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Models.StockDetail
{
    public class StockDetailSimilarProductsModel
    {
        public List<StockProductInfo> lststockImages;
    }

    public class StockProductInfo 
    {
        public string productid { get; set; }
        public string product_name { get; set; }
        public string image_filepath { get; set; }
        public string image_alt_text { get; set; }
        public string total { get; set; }
    }

   
}