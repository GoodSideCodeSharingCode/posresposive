﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Models.StockDetail
{
    public class StockDetailViewModel
    {
        public string id { get; set; }
        public string part_no { get; set; }
        public string product_name { get; set; }
        public string description { get; set; }
        public string Selling_Price { get; set; }
        public string barcode_no { get; set; }
        public string barcodefile { get; set; }
        public string url { get; set; }
        public string product_image { get; set; }

        public List<StockAvailabilityInfo> lstStockAvailability;
    }

    public class StockAvailabilityInfo 
    {
        public string id { get; set; }
        public string name { get; set; }
        public string reference { get; set; }
        public string edate { get; set; }
        public string totalQty { get; set; }
        public string inorder { get; set; }
    }

    public class DataTableData
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<StockItemInfo> data { get; set; }
    }   
}