﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Models.Account
{
    public class LoginModel
    {
        public int CompanyID { get; set; }
        public string Password { get; set; }
    }
}