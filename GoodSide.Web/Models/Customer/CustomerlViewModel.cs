﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GoodSide.Web.Models.Common;

namespace GoodSide.Web.Models.Customer
{
    public class CustomerSearchInfo
    {
        public string SearchType { get; set; }
    }
    public class CustomerViewItem
    {
        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string creditlimit { get; set; }
        public string mindeposit { get; set; }
        public string phone { get; set; }
        public string salesperson { get; set; }
        public string tdate { get; set; }
        public string balance { get; set; }
        public string address { get; set; }
        public string totalorders { get; set; }
        public string salespersonid { get; set; }
        public string VatRegNo { get; set; }
    }
    public class CustomerViewModel
    {
        public string id { get; set; }
        public string code { get; set; }

        //[RegularExpression(@"^[0-9a-zA-Z''-'\s]{1,40}$",ErrorMessage = "special characters are not  allowed.")]
        [Required(ErrorMessage = "customer name is required.")]
        public string name { get; set; }

        public string countryid { get; set; }
        public string postalcode { get; set; }

        public string defaultcurrency { get; set; }

        [Required(ErrorMessage = "salesperson is required.")]
        public string salespersonid { get; set; }

        [Required(ErrorMessage = "phone number is required.")]
        public string phonenumber { get; set; }

        public string mobilenumber { get; set; }

        [Required(ErrorMessage = "address is required.")]
        public string address { get; set; }

        [Required(ErrorMessage = "email is required.")]
        public string email { get; set; }


        public string primaryphonecontact { get; set; }
        public string secondaryphonecontact { get; set; }

        public string howdidyouhearaboutusid { get; set; }
        public string financialinstitutionid { get; set; }
        public string deliveryType { get; set; }
        public bool showmaponreceipt { get; set; }

        public SelectList SalesPersons { get; set; }
        public SelectList CountyList { get; set; }
        public SelectList AboutUsList { get; set; }
        public SelectList FianacialList { get; set; }
        public SelectList DeliveryTypeList { get; set; }
        public SelectList CurrencyList { get; set; }

        public string creditlimit { get; set; }
        public string mindeposit { get; set; }
        public string phone { get; set; }
        public string salesperson { get; set; }
        public string tdate { get; set; }
        public string balance { get; set; }
        public string totalorders { get; set; }

        public string zip { get; set; }
        public string salesId { get; set; }
        public string mobile { get; set; }
        public string VatRegNo { get; set; }

        public int defaultcurrencyid { get; set; }
        public decimal exchange_rate { get; set; }
        public string currencyname { get; set; }
        public string currencysymbol { get; set; }
        public bool iscashsale { get; set; }
        public bool active { get; set; }
    }

    public class DataTableDataCustomer
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CustomerSearchItemInfo> data { get; set; }
        public List<CustomerSearchItemInfo> dataItem { get; set; }
    }
    public class CustomerPhoneSearch
    {
        public string id { get; set; }
        public string lable { get; set; }
        public string value { get; set; }
    }
    public class PhoneSearchDataTableData : DataTableModel
    {
        public List<CustomerPhoneSearchItem> data { get; set; }
    }

    public class CustomerViewOrderModel
    {
        public int id { get; set; }
        public int cisentityId { get; set; }
        public string orderno { get; set; }
        public string refno { get; set; }
        public string customername { get; set; }
        public string phone { get; set; }
        public string oodate { get; set; }
        public string status { get; set; }
        public string ordstatus { get; set; }
        public string spersonid { get; set; }
        public decimal ordtotal { get; set; }
        public decimal discgiven { get; set; }
        public decimal mcharges { get; set; }
        public decimal dcharges { get; set; }
        public decimal payment { get; set; }
        public decimal paid { get; set; }
        public decimal dueamount { get; set; }
        public string odate { get; set; }
        public string OrderStatus { get; set; } 
    }
    public class DataTableDataCustomervieworder
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CustomerViewOrdersInfo> data { get; set; }
        public List<CustomerViewOrdersInfo> dataItem { get; set; }
    }

    public class OrderDetailSummaryModel
    {
        public List<OrderMasterDetail> orderMaster { get; set; }
        public List<OrderDetail> orderdetailList  { get; set; }
        public List<OrderPaytypeDetail> orderPaydetails { get; set; }
        public string BIRTReportURL { get; set; }
    }
}
