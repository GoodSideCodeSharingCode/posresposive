﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GoodSide.Domain.Entities;

namespace GoodSide.Web.Models.Customer
{
    public class VoucherSearchInfo
    {
        public string SearchType { get; set; }
    }


    public class PaymentVoucherViewModel
    {
        public string SearchType { get; set; }
        
        public List<PaymentVoucherListViewModel> Items { get; set; }

        public string code { get; set; }

        public string remainingamount { get; set; }

        public decimal amount { get; set; }

        public string voucherid { get; set; }

    }
    public class PaymentVoucherListViewModel
    {

        public string voucherid { get; set; }

        public string customername { get; set; }

        public string code { get; set; }

        public string remainingamount { get; set; }

        public decimal amount { get; set; }

        public string ExpiryDate { get; set; }

    }

    public class VoucherViewModel
    {       

        public string code { get; set; }

        public string remainingamount { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Amount Should be greater than 0")]
        public decimal amount { get; set; }

        public string voucherid { get; set; }

        public string customername { get; set; }

        public string ExpiryDate { get; set; }

        public string RefNo { get; set; }

        [Required(ErrorMessage = "salesperson is required.")]
        public string SalePersonId { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "PaymentType is required.")]
        public string PaymentType { get; set; }

        public string IssueDate { get; set; }

        public string Password { get; set; } 

        public SelectList SalesPersons { get; set; }

        public SelectList PayTypes { get; set; }


    }

    public class DataTableDataVoucher
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<VoucherSearchItemInfo> data { get; set; }
        public List<VoucherSearchItemInfo> dataItem { get; set; }
    }
}