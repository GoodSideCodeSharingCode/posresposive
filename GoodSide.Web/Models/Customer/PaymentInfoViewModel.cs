﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace GoodSide.Web.Models.Customer
{
    public class PaymentInfoViewModel
    {

        public decimal vatamount { get; set; }
        public decimal netpayment { get; set; }
        public decimal discount { get; set; }
        public decimal cash { get; set; }
        public decimal subtotal { get; set; }
        public decimal visa { get; set; }
        public decimal giftvoucher { get; set; }
        public decimal remaining { get; set; }
        public decimal priviouspaidamount { get; set; }
       
        public decimal mastercard { get; set; }
        public decimal cheque { get; set; }
        public decimal laser { get; set; }
        public decimal misc { get; set; }
        public decimal creditnote { get; set; }
        public bool discounttype { get; set; }
        public decimal paypal { get; set; }

        [Required(ErrorMessage = "amount is required.")]
        [Range(1,int.MaxValue, ErrorMessage = "amount must be greater than zero")]
        public decimal amount { get; set; }

        public decimal changedue { get; set; }
        public decimal changedueInfo { get; set; }
        
        public string description { get; set; }
        public string password { get; set; }
        public string voucherdetail { get; set; }
        public string paymentaction { get; set; }

        public int discounttypeId { get; set; }
        public string novoucherdetail { get; set; }

        public string invoiceamount { get; set; }
        public string paymentreceived { get; set; }
        public string remainingpayment { get; set; }
        public string paidamount { get; set; }
        public string BIRTReportURL { get; set; }
        public string displaypaidamount { get; set; }
        public string customeremail { get; set; }
        public bool showpasswordoption { get; set; }
        public string isPrint { get; set; }
        public bool isOrderChecked { get; set; }

        [Display(Name = "Sales Person")]
        public string salespersonid { get; set; }
        public SelectList SalesPersonList { get; set; }
        public string Entityid { get; set; }
        public int IsSalesPersonChange { get; set; }    
    }
}