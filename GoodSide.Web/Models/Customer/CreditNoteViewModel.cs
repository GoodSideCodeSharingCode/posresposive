﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using GoodSide.Domain.Entities;

namespace GoodSide.Web.Models.Customer
{
    public class CreditNoteSearchInfo
    {
        public string SearchType { get; set; }
    }
      
    public class CreditNoteViewModel
    {
        public string SearchType { get; set; }

        public List<CreditNoteListViewModel> Items { get; set; }

    }

   
    public class CreditNoteListViewModel
    {

        public string id { get; set; }

        public string cnno { get; set; }

        public string tdate { get; set; }

        public decimal amount { get; set; }

        public string ono { get; set; }

        public decimal amountpaid { get; set; }

       
        

    }
    public class DataTableDataCreditNote
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CreaditNoteSearchItemInfo> data { get; set; }
       
    }
}