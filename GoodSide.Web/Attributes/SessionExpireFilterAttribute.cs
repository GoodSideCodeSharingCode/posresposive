﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoodSide.Web.Attributes
{
    public class SessionExpireFilterAttribute : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;

            // check if session is supported

            var session = System.Web.HttpContext.Current.Session;
            if (session["USERID"] == null)
            {
                // check if a new session id was generated
                filterContext.Result = new RedirectResult(VirtualPathUtility.ToAbsolute("~/account/login"));
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}