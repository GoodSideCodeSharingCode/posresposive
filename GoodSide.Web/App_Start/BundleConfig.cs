﻿using System.Web;
using System.Web.Optimization;

namespace GoodSide.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/ubold/js/jquery.min.js",
                        "~/Content/ubold/js/bootstrap.min.js",
                        "~/Content/ubold/js/detect.js",
                        "~/Content/ubold/js/jquery.core.js",
                        "~/Content/ubold/js/jquery.app.js",
                        "~/Content/ubold/js/jquery.scrollTo.min.js",
                         "~/Content/ubold/js/jquery.slimscroll.js",
                         "~/POSContent/lightbox/ekko-lightbox.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/JqueryUI").Include(
                    "~/Content/ubold/js/jquery-ui.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/JqueryValidation").Include(
                       "~/Scripts/jquery.validate.min.js",
                       "~/Scripts/jquery.validate.unobtrusive.min.js",
                       "~/Scripts/jquery.unobtrusive-ajax.min.js"
                       ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/ubold/css/bootstrap.min.css",
                      "~/Content/ubold/css/core.css",
                      "~/Content/ubold/css/components.css",
                      "~/Content/ubold/css/icons.css",
                      "~/Content/ubold/css/pages.css",
                      "~/Content/ubold/css/menu.css",
                      "~/Content/ubold/css/responsive.css"
                    ));


            bundles.Add(new StyleBundle("~/Content/DataTableCSS").Include(
                  "~/Content/ubold/plugins/datatables/jquery.dataTables.min.css",
                    "~/Content/ubold/plugins/datatables/buttons.bootstrap.min.css",
                    "~/Content/ubold/plugins/datatables/fixedHeader.bootstrap.min.css",
                    "~/Content/ubold/plugins/datatables/responsive.bootstrap.min.css",
                    "~/Content/ubold/plugins/datatables/scroller.bootstrap.min.css",
                    "~/Content/ubold/plugins/datatables/dataTables.colVis.css"
                 ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/ubold/js/modernizr.min.js"));

            bundles.Add(new StyleBundle("~/Content/BootstrapCSS").Include(
                  "~/Content/bootstrap.min.css",
                  "~/Content/bootstrap-dialog.min.css"

                ));

            bundles.Add(new ScriptBundle("~/bundles/BootstrapJS").Include(
                  "~/Scripts/bootstrap.js",
                   "~/Scripts/bootstrap-dialog.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/ubold/js/modernizr.min.js"));


            bundles.Add(new StyleBundle("~/Content/POSLayoutCSS").Include(
                "~/POSContent/css/bootstrap.css",
                       "~/POSContent/css/style.css",
                       "~/POSContent/css/jquery-ui.css",
                       "~/POSContent/lightbox/ekko-lightbox.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/CommonJS").Include(
                      "~/Scripts/Custom/Common.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/dataTablesJS").Include(
                        "~/Content/ubold/plugins/datatables/jquery.dataTables.min.js",
                        "~/Content/ubold/plugins/datatables/dataTables.bootstrap.js",
                        "~/Content/ubold/plugins/datatables/dataTables.responsive.min.js",
                        "~/Content/ubold/pages/datatables.init.js"
                       ));
        }
    }
}
