﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;


namespace GoodSide.Web.Helpers
{
    [Serializable]
    public class ClientSession
    {

        /// <summary>
        /// Set Session Variable With Enum Type-SessionConstants
        /// </summary>
        /// <param name="sessionValue"></param>
        /// <param name="sessionVarName"></param>
        
        public static void SetSession(SessionConstants sessionVarName, object sessionValue)
        {
            HttpContext.Current.Session[sessionVarName.ToString()] = sessionValue;
        }


        /// <summary>
        /// Get Session Variable Values with Enum Type-SessionConstants
        /// </summary>
        /// <param name="sessionVarName"></param>
        /// <returns></returns>
        public static object GetSession(SessionConstants sessionVarName)
        {
            if (HttpContext.Current.Session[sessionVarName.ToString()] == null)
            {
                return null;
            }

            return HttpContext.Current.Session[sessionVarName.ToString()];
        }

    }

    public enum SessionConstants
    {
        companyid,      // please Never change it. Use internally for execute Rules 
        userid,         // please Never change it. Use internally for execute Rules
        username,       // please Never change it. Use internally for execute Rules
        userrole,       // please Never change it. Use internally for execute Rules
        storename,      // please Never change it. Use internally for execute Rules
        passwordOnly,   // please Never change it. Use internally for execute Rules
        customerId,      // stored selected customer for pos order,
        POSSelectedProducts,// Use to add products to session at time of order place.
        POSOrderMaster,//use to store ordermaster to session, it will be use as customer select,payment and other screens
        VatRegNo,
        POSCreditNoteItem,
        Searchhistory,
        sourcetype,
        entityid,
        orderstatus
    }
}