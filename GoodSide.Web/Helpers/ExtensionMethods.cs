﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GoodSide.Web.Helpers
{
    public static class ExtensionMethods
    {
        public static bool AnySafe<T>(this IEnumerable<T> items)
        {
            return items != null && items.Any();
        }


    }

    public static class EnumExt
    {
        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        {
            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                          select new { Id = e, Name = e.ToString().Replace("_", " ") }).OrderBy(x => x.Name);

            return new SelectList(values, "Id", "Name", enumObj);
        }
        public static SelectList ToSelectListValue<TEnum>(this TEnum enumObj)
        {
            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                          select new { Id = Convert.ToInt32(e), Name = e.ToString().Replace("_", " ") }).OrderBy(x => x.Name);

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static SelectList ToSelectListDropDown<TEnum>(this TEnum enumObj)
        {
            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                          select new { Id = e.ToString(), Name = e.ToString().Replace("_", " ") }).OrderBy(x => x.Name);
            var val = values.Where(x => x.Id == enumObj.ToString()).FirstOrDefault();

            var list = new SelectList(values, "Id", "Name", val);
            return list;
        }
        public static SelectList ToSelectList<TEnum>(this TEnum enumObj, TEnum[] notInList)
        {
            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                          select new { Id = e, Name = e.ToString().Replace("_", " ") }).OrderBy(x => x.Name);
            var final = values.Where(x => !notInList.Contains(x.Id));

            return new SelectList(final, "Id", "Name", enumObj.ToString());
        }

        public static SelectList ToSelectListId<TEnum>(this TEnum enumObj)
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = Convert.ToInt32(e), Name = Convert.ToInt32(e) };

            return new SelectList(values, "Id", "Name", enumObj);
        }
        public static SelectList ToSelectListIdAndName<TEnum>(this TEnum enumObj)
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = Convert.ToInt32(e), Name = Convert.ToInt32(e) + " - " + e.ToString() };

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static MultiSelectList ToMultiSelectListIdAndName<TEnum>()
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = Convert.ToInt32(e), Name = e.ToString().Replace("_", " ") };
            return new MultiSelectList(values, "Id", "Name");
        }


    }

    public static class CommonMethods
    {
        public static string BIRTReportUrl()
        {
            string birtURL = System.Configuration.ConfigurationManager.AppSettings["BIRTReportURL"].ToString();
            string dbName = System.Configuration.ConfigurationManager.AppSettings["database"].ToString();
            string TID = "TID" + DateTime.Now.Month.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
            return string.Format("{0}&tid={1}&companyid={2}&storename={3}&orderid={4}&userid={5}&userrole={6}&hoapp=false&passwordonly=True&username={7}&db={8}&autoprint=true",
                birtURL, TID, ClientSession.GetSession(SessionConstants.companyid), ClientSession.GetSession(SessionConstants.storename), "{orderid}", ClientSession.GetSession(SessionConstants.userid), ClientSession.GetSession(SessionConstants.userrole), ClientSession.GetSession(SessionConstants.username), dbName);
        }
    }

    public static class HtmlHelperExtensions
    {
        public static string ImageOrDefault(this HtmlHelper helper, string filename)
        {
            //var imagePath = @"~\" + filename;
            //var imageSrc = File.Exists(HttpContext.Current.Server.MapPath(imagePath))
            //                   ? filename : defaultImage;
            var imageSrc = string.Empty;
            if (!string.IsNullOrEmpty(filename))
                imageSrc = filename ;
            else
                imageSrc = defaultImage;

            return imageSrc;
        }
        private static string defaultImage = "POSContent/Images/noimage.png";

        static public bool URLExists(string url)
        {
            bool result = false;

            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Timeout = 1200; // miliseconds
            webRequest.Method = "HEAD";

            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)webRequest.GetResponse();
                result = true;
            }
            catch (WebException webException)
            {
                result = false;
                return result;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
            return result;
        }
    }
}