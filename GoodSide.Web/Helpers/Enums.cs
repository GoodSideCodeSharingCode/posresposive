﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GoodSide.Web.Helpers
{
    public static class Enums
    {

    }

    public enum PaymentType
    {
        visa = 1,
        discount = 6,
        cash = 5,
        cheque = 4,
        mastercard = 2,
        laser = 3,
        giftvoucher = 9,
        creaditnote = 8,
        paypal = 16

    }
    public enum DiscountType
    {
        FixedAmount = 1,
        InPercentage = 2
    }

    public enum DeliveryTypeEnum
    {
        DELIVERY = 1,
        COLLECTION = 0,
        TAKEAWAY = 4
    }
    public enum SaleType
    {
        Standard_Item = 0,
        Special_Order = 1
    }
    public enum ChangeSalespersonOption
    {
        None = 0,
        DropdownCheck = 1,
        PasswordCheck = 2

    }
    //public enum Reports
    //{
    //    [Description("salesorderreport")]
    //    Sales_order_Acknowledgement = 1,       
    //    [Description("salesacknowledgementQPOSreprintreport")]
    //    POS_Receipt = 2,       
    //    [Description("paymenthistoryreport")]
    //    Customer_Payment_History = 3,      
    //    [Description("collectiontakeawayreport")]
    //    Collection_Takeaway = 4            
    //}  
}