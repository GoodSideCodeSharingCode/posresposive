﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Web.Helpers
{

    public class ReportConfig
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public partial class Reports
        {

            private ReportsReport[] reportField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Report")]
            public ReportsReport[] Report
            {
                get
                {
                    return this.reportField;
                }
                set
                {
                    this.reportField = value;
                }
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class ReportsReport
        {

            private string nameField;

            private string displaynameField;

            private string defaultField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name
            {
                get
                {
                    return this.nameField;
                }
                set
                {
                    this.nameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string displayname
            {
                get
                {
                    return this.displaynameField;
                }
                set
                {
                    this.displaynameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string @default
            {
                get
                {
                    return this.defaultField;
                }
                set
                {
                    this.defaultField = value;
                }
            }

        }
    }
}