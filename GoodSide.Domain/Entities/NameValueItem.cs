﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class NameValueItem : IEntity
    {
        public List<NameValueItemInfo> nameValueItemInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }
    [Serializable]
    public class NameValueItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public virtual string id { get; set; }
        public virtual string name { get; set; }
    }
}
