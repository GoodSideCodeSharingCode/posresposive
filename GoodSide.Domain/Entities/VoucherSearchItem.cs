﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class VoucherSearchItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }      

       
        public List<VoucherSearchItemInfo> voucherSearchItemInfo { get; set; }

        public List<PagingInfo> pagingInfo { get; set; }



    }


    [Serializable]
    public class VoucherSearchItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string voucherid { get; set; }

        public string customername { get; set; }

        public string code { get; set; }

        public string remainingamount { get; set; }

        public int amount { get; set; }

        public string ExpiryDate { get; set; }

    }

    [Serializable]
    public class VoucherItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string voucherid { get; set; }

        public string customername { get; set; }

        public string code { get; set; }

        public string remainingamount { get; set; }

        public decimal amount { get; set; }

        public string ExpiryDate { get; set; }

        public string RefNo { get; set; }

        public string SalePersonId { get; set; }

        public string Description { get; set; }

        public string PaymentType { get; set; }

        public string IssueDate { get; set; }
        public string Password { get; set; } 


    }



}

