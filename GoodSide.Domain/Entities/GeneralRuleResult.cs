﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class GeneralRuleResult:IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public string input { get; set; }
        public string result { get; set; }

        public string status { get; set; } // "success","error","warning"
        public string outputvalue { get; set; } //"output values" //orderid, orderitemid,invoiceid... or detail error
        public string outputvalue2 { get; set; } //"output value2"
    }
}
