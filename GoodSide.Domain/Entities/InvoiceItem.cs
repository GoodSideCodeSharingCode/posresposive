﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    [Serializable]
    public class OrderPayItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string orderId { get; set; }
        public string amount { get; set; }
        public string cntotal { get; set; }
        public string date { get; set; }
        public string reference { get; set; }
        public string description { get; set; }
        public string pmode { get; set; }
        public string voucherlists { get; set; }
        public string voucheramount { get; set; }
        public string showmaponreceipt { get; set; }
        public string authorized { get; set; }
        public string useridnew { get; set; }
        public string cndetailtable { get; set; }
        public string cash { get; set; }
        public string visa { get; set; }
        public string mastercard { get; set; }
        public string cheque { get; set; }
        public string laser { get; set; }
        public string paypal { get; set; }

        public string paypaldetail { get; set; }
        public string cashdetail { get; set; }
        public string visadetail { get; set; }
        public string mastercarddetail { get; set; }
        public string chequedetail { get; set; }
        public string cndetails { get; set; }
        public string gvdetail { get; set; }
        public decimal amount_tendered { get; set; }    


    }
}

