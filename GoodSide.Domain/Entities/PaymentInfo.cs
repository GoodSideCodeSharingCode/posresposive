﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class PaymentInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public decimal vatamount { get; set; }
        public decimal netpayment { get; set; }
        public decimal discount { get; set; }
        public decimal cash { get; set; }
        public decimal subtotal { get; set; }
        public decimal visa { get; set; }
        public decimal giftvoucher { get; set; }
        public decimal remaining { get; set; }
       
        public decimal mastercard { get; set; }
        public decimal cheque { get; set; }
        public decimal laser { get; set; }
        public decimal misc { get; set; }
        public decimal creditnote { get; set; }
        public bool discounttype { get; set; }

      //  [Required(ErrorMessage = "amount is required.")]
       // [Range(1,int.MaxValue, ErrorMessage = "amount must be greater than zero")]
        public decimal amount { get; set; }

        public decimal changedue { get; set; }
        
        public string description { get; set; }
        public string password { get; set; }
        public string voucherdetail { get; set; }
        public string paymentaction { get; set; }

        public int discounttypeId { get; set; }
        public string novoucherdetail { get; set; }

        public string invoiceamount { get; set; }
        public string paymentreceived { get; set; }
        public string remainingpayment { get; set; }
        public string paidamount { get; set; }
    }
    

    //public class CustomerViewModel
    //{
    //    public string id { get; set; }
    //    public string code { get; set; }

    //   // [Required(ErrorMessage = "customer name is required.")]
    //    public string name { get; set; }

    //    public string countyid { get; set; }
    //    public string countryid { get; set; }
    //    public string postalcode { get; set; }

    //   // [Required(ErrorMessage = "salesperson is required.")]
    //    public string salespersonid { get; set; }

    //   // [Required(ErrorMessage = "phone number is required.")]
    //    public string phonenumber { get; set; }
    //    public string mobilenumber { get; set; }

    //  //  [Required(ErrorMessage = "address is required.")]
    //    public string address { get; set; }

    //    public string email { get; set; }

    //    public SelectList SalesPersons { get; set; }
    //    public SelectList CountyList { get; set; }
    //    public SelectList CountryList { get; set; }
    //}



}