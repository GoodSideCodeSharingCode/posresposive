﻿using GoodSide.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class StockItem : IEntity
    {
        public List<StockItemInfo> stockItemInfo { get; set; }      
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemMagento : IEntity
    {     
        public List<StockItemInfoforMagento> stockItemInfoforMagento { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemImageMagento : IEntity
    {
        public List<StockItemImageInfoforMagento> stockItemImageInfoforMagento { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemWooCommerce : IEntity
    {
        public List<StockItemforWooCommerce> stockItemInfoforWooCommerce { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemEbay : IEntity
    {
        public List<StockItemforEbay> stockItemInfoforEbay { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemInfo : IEntity
    {
        public string part_no { get; set; }
        public string product_name { get; set; }
        public string supplier { get; set; }
        public string categoryid { get; set; }
        public string typeid { get; set; }
        public string description { get; set; }
        public string barcode_no { get; set; }
        public string stockdetails { get; set; }
        public string spnow { get; set; }
        public string sellingprice { get; set; }
        public string onpromo { get; set; }
        public string physicalstocklevel { get; set; }
        public string allocatedstocklevel { get; set; }
        public string unallocatedstocklevel { get; set; }
        public string inorder { get; set; }
        public string labelflag { get; set; }
        public int id { get; set; }
        public string checkid { get; set; }
        public string itemhistory { get; set; }
        public string locations { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }
    public class StockItemInfoforMagento : IEntity
    {
        //public string part_no { get; set; }
        //public string product_name { get; set; }
        //public string supplier { get; set; }
        //public string categoryid { get; set; }
        //public string typeid { get; set; }
        //public string description { get; set; }
        //public string barcode_no { get; set; }
        //public string stockdetails { get; set; }
        //public string spnow { get; set; }
        //public string sellingprice { get; set; }
        //public string onpromo { get; set; }
        //public string physicalstocklevel { get; set; }
        //public string allocatedstocklevel { get; set; }
        //public string unallocatedstocklevel { get; set; }
        //public string inorder { get; set; }
        //public string labelflag { get; set; }
        //public int id { get; set; }
        //public string checkid { get; set; }
        //public string itemhistory { get; set; }
        //public string locations { get; set; }
        //public string special_price { get; set; }
        //public DateTime special_price_from_date { get; set; }
        //public DateTime special_price_to_date { get; set; }
        //public string Weight { get; set; }
        //public string imagepaths { get; set; }
        //public string stockcreateddate { get; set; }
        //public string stockmodifieddate { get; set; }
        //public int stockactive { get; set; }    

        public int sku { get; set; }
        public string part_no { get; set; }
        public string visibility { get; set; }
        public string attribute_set_code { get; set; }
        public string product_type { get; set; }
        public string name { get; set; }
        public string product_online { get; set; }
        public DateTime new_from_date { get; set; }
        public DateTime new_to_date { get; set; }
        public string categories { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string short_description { get; set; }
        public string description { get; set; }
        public string price { get; set; }
        public string special_price { get; set; }
        public DateTime special_price_from_date { get; set; }
        public DateTime special_price_to_date { get; set; }
        public string tax_class_name { get; set; }
        public string cost { get; set; }
        public string url_key { get; set; }
        public string meta_title { get; set; }
        public string meta_keywords { get; set; }
        public string meta_description { get; set; }
        public string qty { get; set; }
        public string is_in_stock { get; set; }
        public string out_of_stock_qty { get; set; }
        public string allow_backorders { get; set; }
        public string imagepaths { get; set; }
        public string additional_attribute { get; set; }
        //public string web_name { get; set; }
        //public string web_product_visibility { get; set; }
        //public int web_status { get; set; }
        //public DateTime web_new_status_date_from { get; set; }
        //public DateTime web_new_status_date_to { get; set; }
        //public DateTime web_date_created { get; set; }
        //public DateTime web_date_updated { get; set; }
        //public string web_short_description { get; set; }
        //public string web_long_description { get; set; }
        //public int web_tax_class { get; set; }
        //public string web_seo_url_key { get; set; }
        //public string web_seo_meta_title { get; set; }
        //public string web_seo_keywords { get; set; }
        //public string web_meta_description { get; set; }       



        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemImageInfoforMagento : IEntity
    {
        public string sku { get; set; }
        public string base_image { get; set; }
        public string base_image_label { get; set; }
        public string small_image { get; set; }
        public string small_image_label { get; set; }
        public string thumbnail_image { get; set; }
        public string thumbnail_image_label { get; set; }
        public string additional_images { get; set; }
        public string additional_images_label { get; set; }       
    }

    public class StockItemforWooCommerce : IEntity
    {
        public string part_no { get; set; }
        public string product_name { get; set; }
        public string supplier { get; set; }
        public string categoryid { get; set; }
        public string typeid { get; set; }
        public string description { get; set; }
        public string barcode_no { get; set; }
        public string stockdetails { get; set; }
        public string spnow { get; set; }
        public string sellingprice { get; set; }
        public string regularprice { get; set; }
        public string onpromo { get; set; }
        public string physicalstocklevel { get; set; }
        public string allocatedstocklevel { get; set; }
        public string unallocatedstocklevel { get; set; }
        public string inorder { get; set; }
        public string labelflag { get; set; }
        public int id { get; set; }
        public string checkid { get; set; }
        public string itemhistory { get; set; }
        public string locations { get; set; }
        public string special_price { get; set; }
        public DateTime special_price_from_date { get; set; }
        public DateTime special_price_to_date { get; set; }
        public string Weight { get; set; }
        public string imagepaths { get; set; }

      
        public string short_description { get; set; }           
        public string tax_class_name { get; set; }       
        public string cost { get; set; }      
        public string url_key { get; set; }       
        public string meta_title { get; set; }      
        public string meta_keywords { get; set; }     
        public string meta_description { get; set; }      
        public string is_in_stock { get; set; }      
        public string out_of_stock_qty { get; set; }      
        public string allow_backorders { get; set; }
        public string OnWeb { get; set; }
        public DateTime new_from_date { get; set; }
        public DateTime new_to_date { get; set; }
        public string categories { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }       
        public string visibility { get; set; }
        public string item_length { get; set; }
        public string item_width { get; set; }
        public string item_height { get; set; }
        public string product_colour { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemforEbay : IEntity
    {
        public string product_name { get; set; }      //required Field 
        public string CategoryName { get; set; }
        public string CategoryID { get; set; } //required Field      
        public string CurrentPrice { get; set; }
        public string BuyItNowPrice { get; set; }
        public string HighBidder_UserID { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int Quantity { get; set; }
        public string QuantitySold { get; set; }
        public string BestOfferCount { get; set; }
        public string BestOfferEnabled { get; set; }
        public string PayPalEmailAddress { get; set; }
        public string ApplicationData { get; set; }
        public string PictureURL { get; set; }
        public string Site { get; set; }  //required Field     
        public string Description { get; set; } //Conditional required Field
        public string CurrencyCode { get; set; } //required Field 
        public double Listing_StartPrice_Value { get; set; } //required Field
        public string Listing_StartPrice_CurrencyCode { get; set; } //required Field
        public string ListingDuration { get; set; } //required Field 
        public string Location { get; set; }
        public string CountryCode { get; set; }  //required Field    
        public int ConditionID { get; set; }
        public int DispatchTimeMax { get; set; }
        public string ReturnsAcceptedOption { get; set; }
        public double Item_StartPrice_Value { get; set; }
        public string Item_StartPrice_CurrencyCode { get; set; }
        public string ListingType { get; set; }
        public string eBayItemId { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }


}
