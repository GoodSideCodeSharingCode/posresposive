﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.Domain.Entities
{
    public class PagingInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public int t { get; set; }

        public int total { get; set; }

    }
}