﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
   
    [Serializable]
    public class CustomerPhoneSearchItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public string id { get; set; }
        public string lable { get; set; }
        public string value { get; set; }
    }
}
