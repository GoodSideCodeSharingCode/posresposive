﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class CreaditNoteSearchItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

    
        public List<CreaditNoteSearchItemInfo> creaditNoteSearchItemInfo { get; set; }

        public List<PagingInfo> pagingInfo { get; set; }

    }


    [Serializable]
    public class CreaditNoteSearchItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string id { get; set; }
         
        public string cnno { get; set; }

        public string tdate { get; set; }

        public decimal amount { get; set; }

        public string ono { get; set; }

        public string type { get; set; }

        public decimal amountpaid { get; set; }

        public string done { get; set; }
    }
    
}

