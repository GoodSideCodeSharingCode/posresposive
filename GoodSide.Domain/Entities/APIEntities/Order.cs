﻿using GoodSide.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities.APIEntities
{
    public class Order : IEntity
    {
        [FieldMappingAttribute("orderid")]
        public int OrderId { get; set; }

        [FieldMappingAttribute("companyid")]
        public int StoreId { get; set; }

        [FieldMappingAttribute("orddate")]
        public string Created_At { get; set; }

        [FieldMappingAttribute("ordmodifieddate")]
        public string Updated_At { get; set; }

        [FieldMappingAttribute("orderstatus")]
        public int Is_Active { get; set; }

        [FieldMappingAttribute("companyname")]
        public int StoreName { get; set; }

        [FieldMappingAttribute("ordvatamount")]
        public decimal Tax_Amount { get; set; }

        [FieldMappingAttribute("ordvatamountbase")]
        public decimal Base_Tax_Amount { get; set; }

        [FieldMappingAttribute("orddiscgiven")]
        public decimal Discount_Amount { get; set; }

        [FieldMappingAttribute("orddiscgivenbase")]
        public decimal Base_Discount_Amount { get; set; }

        [FieldMappingAttribute("ordnetpayment")]
        public decimal Grand_Total { get; set; }

        [FieldMappingAttribute("ordnetpaymentbase")]
        public decimal Base_Grand_Total { get; set; }

        [FieldMappingAttribute("ordpaidamount")]
        public decimal Total_Paid { get; set; }

        [FieldMappingAttribute("qtyordered")]
        public int Total_Qty_Ordered { get; set; }

        [FieldMappingAttribute("offlinerefunded")]
        public decimal Total_Offline_Refunded { get; set; }

        [FieldMappingAttribute("ordcurrency")]
        public string Order_Currency_Code { get; set; }

        public BillingAddress billingAddress { get; set; }

        public ShippingAddress shippingAddress { get; set; }

        public Customer customer { get; set; }

        public List<OrderLineItem> orderitem { get; set; }

        public List<OrderPaymentInfo> PaymentInfo { get; set; }
    }

    public class BillingAddress
    {

        public int Billing_Addres_Id { get; set; }

        [FieldMappingAttribute("Billing_fname")]
        public string Billing_FirstName { get; set; }

        [FieldMappingAttribute("Billing_lname")]
        public string Billing_LastName { get; set; }

        [FieldMappingAttribute("Billing_createddate")]
        public string Created_At { get; set; }

        [FieldMappingAttribute("Billing_modifieddate")]
        public string Updated_At { get; set; }

        [FieldMappingAttribute("Billing_address")]
        public string StreetandCityregion { get; set; }

        [FieldMappingAttribute("Billing_postalcode")]
        public string PostCode { get; set; }

        [FieldMappingAttribute("Billing_country")]
        public int Country_id { get; set; }

        [FieldMappingAttribute("Billing_phonenumber")]
        public string Telephone { get; set; }
    }

    public class ShippingAddress
    {

        public int Shipping_Addres_Id { get; set; }

        [FieldMappingAttribute("Shipping_fname")]
        public string Shipping_FirstName { get; set; }

        [FieldMappingAttribute("Shipping_lname")]
        public string Shipping_LastName { get; set; }

        [FieldMappingAttribute("Shipping_weight")]
        public int Weight { get; set; }

        [FieldMappingAttribute("Shipping_createddate")]
        public string Created_At { get; set; }

        [FieldMappingAttribute("Shipping_modifieddate")]
        public string Updated_At { get; set; }

        [FieldMappingAttribute("Shipping_address")]
        public string StreetandCityregion { get; set; }

        [FieldMappingAttribute("Shipping_postalcode")]
        public string PostCode { get; set; }

        [FieldMappingAttribute("Shipping_country")]
        public int Country_id { get; set; }

        [FieldMappingAttribute("Shipping_phonenumber")]
        public string Telephone { get; set; }
    }

    public class Customer
    {
        [FieldMappingAttribute("customerId")]
        public int Customer_Id { get; set; }

        [FieldMappingAttribute("email")]
        public string Customer_Email { get; set; }

        [FieldMappingAttribute("fname")]
        public string Customer_FirstName { get; set; }

        [FieldMappingAttribute("lname")]
        public string Customer_LastName { get; set; }
    }

    public class OrderLineItem
    {
        [FieldMappingAttribute("productid")]
        public int Item_id { get; set; }

        [FieldMappingAttribute("createdDate")]
        public string Created_At { get; set; }

        [FieldMappingAttribute("modifieddate")]
        public string Update_At { get; set; }

        [FieldMappingAttribute("typeid")]
        public string Product_Type { get; set; }

        [FieldMappingAttribute("part_no")]
        public string SKU { get; set; }

        [FieldMappingAttribute("weight")]
        public string Weight { get; set; }

        [FieldMappingAttribute("product_name")]
        public string name { get; set; }

        [FieldMappingAttribute("qty")]
        public int qty_ordered { get; set; }

        [FieldMappingAttribute("Qty_original")]
        public int qty_shipped { get; set; }

        [FieldMappingAttribute("SellingPrice_ordercurrency")]
        public decimal price { get; set; }

        [FieldMappingAttribute("CostPrice")]
        public decimal cost { get; set; }

        [FieldMappingAttribute("vatrate")]
        public decimal tax_percent { get; set; }

        [FieldMappingAttribute("vatamount_ordercurrency")]
        public decimal tax_amount { get; set; }

        [FieldMappingAttribute("DiscountGiven_ordercurrency")]
        public decimal discount_amount { get; set; }

        [FieldMappingAttribute("netprice_ordercurrency")]
        public decimal row_total { get; set; }

        [FieldMappingAttribute("SellingPrice")]
        public decimal base_price { get; set; }

        [FieldMappingAttribute("VatAmount")]
        public decimal base_tax_amount { get; set; }

        [FieldMappingAttribute("discountgiven")]
        public decimal base_discount_amount { get; set; }

        [FieldMappingAttribute("NetPrice")]
        public decimal base_row_total { get; set; }

    }

    public class OrderPaymentInfo
    {     

        [FieldMappingAttribute("amount")]
        public string amount_ordered { get; set; }

        [FieldMappingAttribute("payment_id")]
        public string payment_id { get; set; }

        [FieldMappingAttribute("created_at")]
        public DateTime Created_At { get; set; }

        [FieldMappingAttribute("updated_at")]
        public DateTime Updated_At { get; set; }

        [FieldMappingAttribute("pmode")]
        public string method { get; set; }

        [FieldMappingAttribute("cntotal")]
        public string cntotal { get; set; }

        [FieldMappingAttribute("date")]
        public string Date { get; set; }

        [FieldMappingAttribute("reference")]
        public string reference { get; set; }

        [FieldMappingAttribute("description")]
        public string description { get; set; }

        [FieldMappingAttribute("authorized")]
        public string authorized { get; set; }

        [FieldMappingAttribute("voucherslist")]
        public string voucherslist { get; set; }

        [FieldMappingAttribute("voucheramount")]
        public string voucheramount { get; set; }       

        [FieldMappingAttribute("minimumpayment")]
        public string minimumpayment { get; set; }

        [FieldMappingAttribute("showmaponreceipt")]
        public string showmaponreceipt { get; set; }

        [FieldMappingAttribute("creditnotedetailtable")]
        public string creditnotedetailtable { get; set; }

        [FieldMappingAttribute("cash")]
        public string cash { get; set; }

        [FieldMappingAttribute("visa")]
        public string visa { get; set; }

        [FieldMappingAttribute("mastercard")]
        public string mastercard { get; set; }

        [FieldMappingAttribute("cheque")]
        public string cheque { get; set; }

        [FieldMappingAttribute("laser")]
        public string laser { get; set; }

        [FieldMappingAttribute("cashdetail")]
        public string cashdetail { get; set; }

        [FieldMappingAttribute("visadetail")]
        public string visadetail { get; set; }

        [FieldMappingAttribute("mastercarddetail")]
        public string mastercarddetail { get; set; }

        [FieldMappingAttribute("chequedetail")]
        public string chequedetail { get; set; }

        [FieldMappingAttribute("cndetails")]
        public string cndetails { get; set; }

        [FieldMappingAttribute("vdetail")]
        public string vdetail { get; set; }

        [FieldMappingAttribute("amount_tendered")]
        public string amount_tendered { get; set; }

    }

}
