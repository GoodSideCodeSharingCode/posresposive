﻿using GoodSide.Domain.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities.APIEntities
{
    public class StockManagementInfo : IEntity
    {
        [FieldMappingAttribute("id")]
        public int StockId { get; set; }

        [FieldMappingAttribute("part_no")]
        public string SKU { get; set; }

        [FieldMappingAttribute("barcode_no")]
        public string BarcodeNo { get; set; }

        public string Description { get; set; }

        [FieldMappingAttribute("product_info")]
        public string ProductInformation { get; set; }

        [FieldMappingAttribute("product_reference")]
        public string ProductReference { get; set; }

        [FieldMappingAttribute("relatedpartno")]
        public string RelatedPartNo { get; set; }

        [FieldMappingAttribute("name")]
        public string ProductName { get; set; }

        public int TypeId { get; set; }

        public int CategoryId { get; set; }

        public int DepartmentId { get; set; }

        [FieldMappingAttribute("supplier")]
        public int SupplierId { get; set; }

        public int Status { get; set; }

        [FieldMappingAttribute("Stock_Type")]
        public int StockType { get; set; }

        [FieldMappingAttribute("cost_price")]
        public int Cost { get; set; }

        [FieldMappingAttribute("sell_price")]
        public int SellingPrice { get; set; }

        [FieldMappingAttribute("stockno")]
        public string StockNumber { get; set; }

        [FieldMappingAttribute("minval")]
        public int StockMinimumValue { get; set; }

        [FieldMappingAttribute("leadtime")]
        public int LeadTime { get; set; }

        [FieldMappingAttribute("vat_price")]
        public decimal VATPrice { get; set; }

        [FieldMappingAttribute("lorder")]
        public bool LocalOrder { get; set; }

        [FieldMappingAttribute("vatrate")]
        public decimal VATRate { get; set; }

        [FieldMappingAttribute("discount")]
        public decimal Discount { get; set; }

        [FieldMappingAttribute("discount_per")]
        public decimal DiscountPercentage { get; set; }

        [FieldMappingAttribute("total_price")]
        public decimal TotalPrice { get; set; }

        [FieldMappingAttribute("distcost")]
        public decimal DistributionCost { get; set; }

        [FieldMappingAttribute("distprice")]
        public decimal DistributionPrice { get; set; }

        [FieldMappingAttribute("storecost")]
        public decimal StoreCost { get; set; }

        [FieldMappingAttribute("storeprice")]
        public decimal StorePrice { get; set; }

        [FieldMappingAttribute("generatebarcode")]
        public bool AutoGenerateBarcode { get; set; }

        [FieldMappingAttribute("active")]
        public bool Active { get; set; }

        [FieldMappingAttribute("allow_sales_from_stock")]
        public bool AllowSalesFromStock { get; set; }

        [FieldMappingAttribute("allow_POR_from_sales")]
        public bool AllowPORFromSales { get; set; }

        [FieldMappingAttribute("allow_manual_POR")]
        public bool AllowManualPOR { get; set; }

        [FieldMappingAttribute("discontinued")]
        public bool Discontinued { get; set; }

        [FieldMappingAttribute("promotionprice")]
        public decimal PromotionPrice { get; set; }

        [FieldMappingAttribute("promotable")]
        public string AppliedPromotionInfo { get; set; }

        [FieldMappingAttribute("for_production")]
        public bool IsProductForPromotion { get; set; }

        [FieldMappingAttribute("product_url_text")]
        public string ProductURL { get; set; }

        [FieldMappingAttribute("unit_id")]
        public int UnitId { get; set; }

        [FieldMappingAttribute("customer_id")]
        public int CustomerId { get; set; }

        [FieldMappingAttribute("purchasing_currencies")]
        public int PurchasingCurrency { get; set; }

        [FieldMappingAttribute("selling_currencies")]
        public int SellingCurrency { get; set; }

        [FieldMappingAttribute("selling_price_ex_vat")]
        public decimal SellingPriceExVAT { get; set; }      
    }
}
