﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    
    public class OrderMaster : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
       // public List<OrderMasterInfo> orderMasterItemInfo { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public string customerid { get; set; }
        public string salespersonid { get; set; }
        public decimal misc { get; set; }
        public decimal deliverycharge { get; set; }
        public decimal vatamount { get; set; }
        public decimal netpayment { get; set; }
        public decimal discount { get; set; }
        public string comments { get; set; }
        public int deliveryaddressid { get; set; }
        public string customer { get; set; }
        public string salesperson { get; set; }
        public decimal cash { get; set; }
        public string cashdetail { get; set; }
        public decimal visa { get; set; }
        public string visadetail { get; set; }
        public decimal giftvoucher { get; set; }
        public string gvdetail { get; set; }
        public decimal cheque { get; set; }
        public string chequedetail { get; set; }
        public decimal mastercard { get; set; }
        public string mcdetail { get; set; }
        public decimal laser { get; set; }
        public string laserdetail { get; set; }
        public decimal totalpaid { get; set; }
        public string deliveryaddress { get; set; }
        public decimal creditnote { get; set; }
        public string creditnotedetail { get; set; }
        public int vid { get; set; }
        public string temporderid { get; set; }
        public string orderid { get; set; }
        public decimal changedue { get; set; }
        public string VatRegNo { get; set; }
        public string paypal { get; set; }
        public string paypaldetail { get; set; }

        public decimal exchangerate { get; set; }
        public decimal discgiven_base { get; set; }
        public decimal slprice_base { get; set; }
        public int currency { get; set; }
        public int orderstatus { get; set; }
        
        
    }

   
   
}
