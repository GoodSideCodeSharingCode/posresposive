﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class CustomerViewOrders : IEntity
    {

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public List<CustomerViewOrdersInfo> CustomerVieworderInfo { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

    }

    [Serializable]
    public class CustomerViewOrdersInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }     

        public int id { get; set; }
        public int cisentityId { get; set; }
        public string orderno { get; set; }
        public string refno { get; set; }
        public string customername { get; set; }
        public string phone { get; set; }
        public string oodate { get; set; }
        public string status { get; set; }
        public string ordstatus { get; set; }
        public string spersonid { get; set; }
        public decimal ordtotal { get; set; }
        public decimal discgiven { get; set; }
        public decimal mcharges { get; set; }
        public decimal dcharges { get; set; }
        public decimal payment { get; set; }
        public decimal paid { get; set; }
        public decimal dueamount { get; set; }
        public string odate { get; set; }       
    }
}
