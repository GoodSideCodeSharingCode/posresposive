﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class AccountUser : IEntity
    {
        public int? EntityId { get; set; }
        public string Password { get; set; }
        public int? status { get; set; }
        public int? UserAuthorizationLevel { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int? UserRole { get; set; }
      
        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
