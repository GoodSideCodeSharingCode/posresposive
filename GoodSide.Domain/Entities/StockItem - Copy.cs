﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class StockItem : IEntity
    {       
        public List<StockItemInfo> stockItemInfo { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class StockItemInfo : IEntity
    {
        public string part_no { get; set; }
        public string product_name { get; set; }
        public string supplier { get; set; }
        public string categoryid { get; set; }
        public string typeid { get; set; }
        public string description { get; set; }
        public string barcode_no { get; set; }
        public string stockdetails { get; set; }
        public string spnow { get; set; }
        public string sellingprice { get; set; }
        public string onpromo { get; set; }
        public string physicalstocklevel { get; set; }
        public string allocatedstocklevel { get; set; }
        public string unallocatedstocklevel { get; set; }
        public string inorder { get; set; }
        public string labelflag { get; set; }
        public int id { get; set; }
        public string checkid { get; set; }
        public string itemhistory { get; set; }
        public string locations { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }

    public class PagingInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public int t { get; set; }
    }
}
