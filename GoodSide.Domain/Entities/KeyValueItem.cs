﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class KeyValueItem : IEntity
    {
        public virtual string id { get; set; }
        public virtual string name { get; set; }
        public string supplier { get; set; }


        public RuleParameters ruleParameters
        {
            get;
            set;
        }
    }
}
