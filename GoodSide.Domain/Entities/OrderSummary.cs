﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class OrderSummary  : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public List<OrderMasterDetail> OrderMasterSummary { get; set; }
        public List<OrderDetail> Orderdetails { get; set; }      
        public List<OrderPaytypeDetail> OrderPaydetails { get; set; }
       
    }
   
    public class OrderMasterDetail : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public string salespersonname { get; set; }
        public string orderno { get; set; }
        public string customername { get; set; }
        public string currencysymbol { get; set; }
        public string VatRegNo { get; set; }
        public decimal netpayment { get; set; }
    }
    [Serializable()]
    public class OrderPaytypeDetail : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public string paytype { get; set; }
        public decimal amount { get; set; }
        public string Paydate { get; set; }

    }

}
