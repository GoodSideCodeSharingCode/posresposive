﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    public class StockDetailImagesInfo : IEntity
    {
        public List<StockDetailImageList> stockImages { get; set; }
        public List<StockDetailImage> stockDetail { get; set; }
        

        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
    

    public class StockDetailImage: IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string Id { get; set; }
        public string Part_No { get; set; }
        public string Product_Name { get; set; }
        public string Description { get; set; }
        public string Selling_Price { get; set; }
        public string Barcode_No { get; set; }
        public string BarcodeFile { get; set; }
        public string URL { get; set; }
        public string Product_Image { get; set; }        
    }

    public class StockDetailImageList :IEntity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string image_filepath { get; set; }
        public string image_order { get; set; }
        public string status { get; set; }
        public string image_alt_text { get; set; }

        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
