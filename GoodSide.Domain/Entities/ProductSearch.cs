﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    public class ProductSearchItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public List<ProductSearchItemInfo> productSearchItemInfo { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }
    }


    [Serializable]
    public class ProductSearchItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string id { get; set; }
        public string pno { get; set; }
        public string pname { get; set; }
        public string description { get; set; }
        public string barcode_no { get; set; }
        public string supplier { get; set; }
        public string supplierid { get; set; }
        public string typeid { get; set; }
        public string categoryid { get; set; }
        public string stockTypeid { get; set; }
        public int companyid { get; set; }
        public string price { get; set; }
        public string quantity { get; set; }
        public string discount { get; set; }
        public string stype { get; set; }
        public string allocatedS { get; set; }
        public string unallocatedS { get; set; }
        public string totaS { get; set; }
        public int locationid { get; set; }
    }
}
