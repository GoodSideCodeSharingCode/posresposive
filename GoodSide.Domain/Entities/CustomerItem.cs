﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    [Serializable]
    public class CustomerItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        //public List<StockItemInfo> stockItemInfo { get; set; }
        public List<PagingInfo> pagingInfo { get; set; }

        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        
        public string countryid { get; set; }
        public string postalcode { get; set; }
        public string salespersonid { get; set; }
        public string phonenumber { get; set; }
        public string mobilenumber { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public string primaryphonecontact { get; set; }
        public string secondaryphonecontact { get; set; }
      

        public string howdidyouhearaboutusid { get; set; }
        public string financialinstitutionid { get; set; }
        public string deliveryType { get; set; }
        public bool showmaponreceipt { get; set; }

        public NameValueItem SalesPersons { get; set; }
        public NameValueItem CountyList { get; set; }
        public NameValueItem AboutUsList { get; set; }
        public NameValueItem FianacialList { get; set; }
        public NameValueItem DeliveryTypeList { get; set; }
        public string custid { get; set; }

        public string phone { get; set; }
        public string creditlimit { get; set; }
        public string mindeposit { get; set; }
        public string salesperson { get; set; }
        public string tdate { get; set; }
        public string balance { get; set; }
        public string totalorders { get; set; }

        public string zip { get; set; }
        public string salesId { get; set; }
        public string mobile { get; set; }
        public string VatRegNo { get; set; }
        public string default_DeliveryType { get; set; }

        public int defaultcurrencyid { get; set; }
        public decimal exchange_rate { get; set; }

        public string currencyname { get; set; }
        public string currencysymbol { get; set; }
        public bool active { get; set; }

    }
}

