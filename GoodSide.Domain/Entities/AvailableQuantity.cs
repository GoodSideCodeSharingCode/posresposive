﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class AvailableQuantity : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string id { get; set; }

        public string name { get; set; }

        public string reference { get; set; }

        public string edate { get; set; }

        public int totalQty { get; set; }

        public string inorder { get; set; }

        public int poid { get; set; }

        public string stocklocationid { get; set; }

        public string pid { get; set; }

        public int qty { get; set; }

        public string cssType { get; set; }

        public string view_only { get; set; }

        public string ordercol { get; set; }

    }
}
