﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class Currency : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public int id { get; set; }

        public string code { get; set; }

        public string name { get; set; }

        public string symbol { get; set; }

        public decimal exchange_rate { get; set; }

        public bool isBase { get; set; }
    }
}
