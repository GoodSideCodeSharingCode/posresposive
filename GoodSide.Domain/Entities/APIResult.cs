﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class Result : IEntity
    {
        public bool status { get; set; } 
        public string Id { get; set; }
        public string message { get; set; } 
    }    
}
