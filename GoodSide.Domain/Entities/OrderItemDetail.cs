﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable()]
    public class OrderItemDetail : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public int id { get; set; }
        public int companyid { get; set; }
        public int groupid { get; set; }
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public string orderitemdescription { get; set; }
        public string Colors { get; set; }
        public int ProductType { get; set; }
        public int Type { get; set; }
        public int Qty { get; set; }
        public int Qty_original { get; set; }
        public int ApproxDelvQty { get; set; }
        public int ActualDelvQty { get; set; }
        public DateTime deliverydate { get; set; }
        public int DeliveryType { get; set; }
        public int Deliverd { get; set; }
        public int IsDeleted { get; set; }
        public int isReplaced { get; set; }
        public int stocklocationid { get; set; }
        public int porid { get; set; }
        public int poid { get; set; }
        public Decimal costprice { get; set; }
        public int zoneid { get; set; }
        public string dispatchStatus { get; set; }
    }
}
