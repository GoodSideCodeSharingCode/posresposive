﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;


namespace GoodSide.Domain.Entities
{

    //public static string _SORT = "sort", _ORDER = "order", _OFFSET = "offset", _ROWS = "rows", _SUBJECT = "subject", _APPLICATION = "application", _ACTION = "action", _FILTER = "filter";

    public enum CoreParameters
    {
        _SORT,
        _ORDER,
        _OFFSET,
        _ROWS,
        _RULENAME,
        _RULEFILE,
        _ACTION,
        _FILTER
    }

    /// <summary>
    /// Rule file names
    /// </summary>
    public enum RuleFileName
    {
        ACC,
        CAS,
        CIS,
        DEV,
        GIP,
        GOL,
        ORD,
        POP,
        POS,
        PRS,
        SER,
        SHP,
        STK,
        SYS
    }

    public enum RuleAction
    {
        Get,
        Set,
        Edit
    }

    public enum SortOrder
    {
        ASC,
        DESC
    }

    public class RuleParameters
    {
        /// <summary>
        /// Without Get and Set postfix and without Rule File name as postfix. 
        /// Use string constansts only
        /// e.g. produtsearch  if you have the "POSProductSearchGet" in the xml file
        /// </summary>
        public string RuleName { get; set; }

        /// <summary>
        /// Get or Set.
        /// Always use enum <see cref="RuleAction"/>          
        /// </summary>        
        public RuleAction Action { get; set; }

        /// <summary>
        /// Rule file from which this rule needs to be called
        /// Always use enum <see cref="RuleFileName"/>  
        /// </summary>
        public RuleFileName RuleFile { get; set; }

        /// <summary>
        /// Can be empty
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Can be empty
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// Can be empty
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// Can be empty
        /// </summary>
        public SortOrder SortOrder { get; set; }

        /// <summary>
        /// Can be empty
        /// </summary>
        public string Filter { get; set; }

        /// <summary>
        /// Additions parameters specific to rule
        /// </summary>
        public NameValueCollection customParameters { get; set; }
    }

    public interface IEntity
    {
        //RuleParameters ruleParameters { get; set; }
    }
}
