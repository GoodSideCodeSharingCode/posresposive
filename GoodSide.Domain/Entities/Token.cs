﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class Token : IEntity
    {
        public int TokenId { get; set; }
        public int StoreId { get; set; }
        public string AuthToken { get; set; }
        public string IssuedOn { get; set; }
        public string ExpiresOn { get; set; }      
        public string UserName { get; set; }
        public string StoreName { get; set; }
        public string PasswordOnly { get; set; }
        public int UserRole { get; set; }
        public int CompanyId { get; set; }
        public int UserId { get; set; }

        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
