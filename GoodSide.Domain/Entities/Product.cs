﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    public class Product : IEntity
    {
        public int id { get; set; }
        public int localStock { get; set; }
        public string Part_No { get; set; }
        public string relatedPartNo { get; set; }
        public string product_name { get; set; }
        public string Supplier { get; set; }
        public string manufactureid { get; set; }
        public string typeid { get; set; }
        public string categoryid { get; set; }
        public string Item { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string Local_Order { get; set; }
        public decimal Cost_Price { get; set; }
        public decimal Discount { get; set; }
        public decimal Percent_Discount { get; set; }
        public decimal Selling_Price { get; set; }
        public decimal VAT_Amount { get; set; }
        public decimal VAT_Percentage { get; set; }
        public string sType { get; set; }
        public string type { get; set; }
        public string Last_Updated { get; set; }
        public string modifiedBy { get; set; }
        public string Last_Updated_by { get; set; }
        public string NonStandardStock { get; set; }
        public string Cost_Price_Inc_VAT { get; set; }
        public string active { get; set; }
        public string LeadTime { get; set; }
        public string StockDetailNo { get; set; }
        public decimal StockMinValue { get; set; }
        public decimal store_cost_price { get; set; }
        public decimal store_sell_price { get; set; }
        public decimal dist_cost_price { get; set; }
        public decimal dist_sell_price { get; set; }
        public string modifiedDate { get; set; }
        public decimal volume { get; set; }
        public decimal sellingpricebeforesale { get; set; }
        public string product_reference { get; set; }
        public string barcode_no { get; set; }
        public string departmentid { get; set; }
        public string flag { get; set; }
        public string product_info { get; set; }
        public string product_image { get; set; }
        public string vatrate { get; set; }
        public int promotion { get; set; }
        public Decimal Promodiscount { get; set; }
        public Decimal Promosellingprice { get; set; }
        public int PromoId { get; set; }
        public int sale_type { get; set; }

        public RuleParameters ruleParameters
        {
            get;
            set;
        }

      
    }
}
