﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{
    [Serializable]
    public class CustomerSearchItem : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public List<CustomerSearchItemInfo> customerSearchItemInfo { get; set; }

        public List<PagingInfo> pagingInfo { get; set; }

    }


    [Serializable]
    public class CustomerSearchItemInfo : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string creditlimit { get; set; }
        public string mindeposit { get; set; }
        public string phone { get; set; }
        public string salesperson { get; set; }
        public string tdate { get; set; }
        public string balance { get; set; }
        public string address { get; set; }
        public string totalorders { get; set; }
        public string salespersonid { get; set; }
        public string Zip { get; set; }

    }
    
}

