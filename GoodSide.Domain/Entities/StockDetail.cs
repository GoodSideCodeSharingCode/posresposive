﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    public class StockDetailInfo : IEntity
    {
        public List<StockAvailabilityDetail> stockAvailability { get; set; }
        public List<StockDetail> stockDetail { get; set; }
        

        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
    

    public class StockDetail: IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string Id { get; set; }
        public string Part_No { get; set; }
        public string Product_Name { get; set; }
        public string Description { get; set; }
        public string Selling_Price { get; set; }
        public string Barcode_No { get; set; }
        public string BarcodeFile { get; set; }
        public string URL { get; set; }
        public string Product_Image { get; set; }        
    }

    public class StockAvailabilityDetail :IEntity
    {
        public string id { get; set; }
        public string name { get; set; }
        public string reference { get; set; }
        public string edate { get; set; }
        public string totalQty { get; set; }
        public string inorder { get; set; }

        public RuleParameters ruleParameters
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
