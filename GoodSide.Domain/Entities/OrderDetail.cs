﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Entities
{

    public class OpenOrderItemLists : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }
        public List<OpenOrderMaster> orderMaster { get; set; }
        public List<OpenOrderDetail> orderDetails { get; set; }
    }
    [Serializable()]
    public class OpenOrderMaster : IEntity
    {

        public string customerid { get; set; }
        public string salespersonid { get; set; }
        public decimal deliverycharge { get; set; }
        public decimal vatamount { get; set; }
        public decimal netpayment { get; set; }
        public decimal discount { get; set; }
        public string comments { get; set; }
        public int deliveryaddressid { get; set; }
        public string customer { get; set; }
        public string salesperson { get; set; }
        public string deliveryaddress { get; set; }
        public string temporderid { get; set; }
        public string orderid { get; set; }
        public decimal exchangerate { get; set; }
        public decimal discgiven_base { get; set; }
        public int currency { get; set; }
        public int orderstatus { get; set; }
        public decimal paid { get; set; }
        public decimal amountdue { get; set; }
        public decimal MiscCharges { get; set; } 

    }
    [Serializable()]
    public class OpenOrderDetail : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string ProductId { get; set; }
        public string id { get; set; }
        public string SKU { get; set; }
        public decimal costprice { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal discountgiven { get; set; }
        public decimal vatrate { get; set; }
        public decimal vatamount { get; set; }
        public decimal netprice { get; set; }
        public int stocklocationid { get; set; }
        public int porid { get; set; }
        public int poid { get; set; }
        public int qty { get; set; }
        public string productdesc { get; set; }
        public string product_name { get; set; }
        public string product_image { get; set; }
        public string colors { get; set; }
        public int deliverytype { get; set; }
        public string DeliveryDate { get; set; }
        public string table { get; set; }
        public int sumqty { get; set; }
        public decimal exchangerate { get; set; }
        public decimal discgiven_base { get; set; }
        public decimal slprice_base { get; set; }
        public int destinationpoint { get; set; }
        public int isscanitem { get; set; }
        public string part_no { get; set; }
        public int sale_type { get; set; }
        public int OrderDetailId { get; set; }
        public decimal NetPrice { get; set; } 
        public int orderitemdetailid { get; set; }
        public int stype { get; set; }
        public string cust_leadtime { get; set; }
        public string oversaleid { get; set; }
        public string Discount_Comment { get; set; }
    }   

    [Serializable()]
    public class OrderDetail : IEntity
    {
        public RuleParameters ruleParameters
        {
            get;
            set;
        }

        public string pid { get; set; }
        public string id { get; set; }
        public string SKU { get; set; }
        public decimal costprice { get; set; }
        public decimal slprice { get; set; }
        public decimal discgiven { get; set; }
        public decimal vatrate { get; set; }
        public decimal vatamount { get; set; }
        public decimal netprice { get; set; }
        public int locationid { get; set; }
        public int porid { get; set; }
        public int poid { get; set; }
        public int qty { get; set; }
        public string productdesc { get; set; }
        public string productname { get; set; }
        public string product_image { get; set; }
        public string colors { get; set; }
        public int deliverytype { get; set; }
        public string deliverydate { get; set; }
        public string table { get; set; }
        public int sumqty { get; set; }

        public decimal discgiven_base { get; set; }
        public decimal slprice_base { get; set; }
        public int DestinationPoint { get; set; }
        public bool isscanitem { get; set; }
        public int sale_type { get; set; }
        public string cust_leadtime { get; set; }
        public int OrderDetailId { get; set; }
        public int orderitemdetailid { get; set; }
        public int oversaleFlag { get; set; }
        public string Discount_Comment { get; set; } 
    }
}
