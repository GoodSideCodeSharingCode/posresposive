﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IProductSearchService
    {        
        ProductSearchItem GetAll(string pname, string description, string pno, string barcode_no, string suppliedId, string typeId, string categoryId, string stockTypeId, int offSet, int Rows, string sortBy, string sortDirection);        
    }
    public class ProductSearchService : IProductSearchService
    {
        private IRepository<ProductSearchItem> _productSearchRepository;

        public ProductSearchService(IRepository<ProductSearchItem> productSearchRepository)
        {
            _productSearchRepository = productSearchRepository;
        }

        public ProductSearchItem GetAll(string pname, string description, string pno, string barcode_no, string suppliedId, string typeId, string categoryId, string stockTypeId, int offSet, int Rows, string sortBy, string sortDirection)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "produtsearch";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("pname", pname.Replace("'","''''")); 
            ruleParameters.customParameters.Add("description", description.Replace("'", "''''"));
            ruleParameters.customParameters.Add("pno", pno.Replace("'", "''''"));
            ruleParameters.customParameters.Add("bno", barcode_no.Replace("'", "''''"));
            ruleParameters.customParameters.Add("supplier", suppliedId);
            ruleParameters.customParameters.Add("categoryid", categoryId);
            ruleParameters.customParameters.Add("typeId", typeId);
            ruleParameters.customParameters.Add("stocktype", stockTypeId);

            var result = _productSearchRepository.GetAllMultipleTable(ruleParameters);
            return result;
        }       
    }
}
