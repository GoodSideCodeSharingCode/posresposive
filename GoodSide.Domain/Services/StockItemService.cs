﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Entities.APIEntities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IStockItemService
    {
        StockItem GetAll(string SearchText, int offSet, int Length, string sortBy, string sortDirection);
        StockItem GetSingleRecord(int stockId);
        bool Create(StockManagementInfo stockDetail);
        bool Update(StockManagementInfo stockDetail);
        string Create(Order Order, string source);
        void Delete(int stockId);
        GeneralRuleResult CheckSku(string sku);
    }

    public class StockItemService : IStockItemService
    {
        private IRepository<StockItem> _stockDetailRepository;
        private IRepository<StockManagementInfo> _stockManagementRepository;
        private IRepository<Order> _apiOrderRepository;
        private IRepository<GeneralRuleResult> _orderpayRepository;
        private IRepository<GeneralRuleResult> _checkSkuRepository;
      
      

        public StockItemService(IRepository<StockItem> stockDetailRepository, IRepository<StockManagementInfo> stockManagementRepository, IRepository<Order> apiOrderRepository, IRepository<GeneralRuleResult> orderpayRepository, IRepository<GeneralRuleResult> checkSkuRepository)
        {
            _stockManagementRepository = stockManagementRepository;
            _stockDetailRepository = stockDetailRepository;
            _apiOrderRepository = apiOrderRepository;
            _orderpayRepository = orderpayRepository;
            _checkSkuRepository = checkSkuRepository;
        }

        public StockItem GetAll(string SearchText, int offSet, int Rows, string sortBy, string sortDirection)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.STK;
            ruleParameters.RuleName = "lisForAPI";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("part_no", SearchText);
            ruleParameters.customParameters.Add("product_name", SearchText);
            ruleParameters.customParameters.Add("supplier", SearchText);
            ruleParameters.customParameters.Add("categoryid", SearchText);
            ruleParameters.customParameters.Add("typeid", SearchText);
            ruleParameters.customParameters.Add("barcode_no", SearchText);
            ruleParameters.customParameters.Add("description", SearchText);

            var result = _stockDetailRepository.GetAllMultipleTable(ruleParameters);
            return result;
        }

        public StockItem GetSingleRecord(int stockId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            return new StockItem();
        }

        public bool Create(StockManagementInfo stockInfo)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "stklis";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            var lstFields = typeof(StockManagementInfo).GetProperties();
            foreach (var item in lstFields)
            {

                GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
           (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                if (myDBFieldMapping != null)
                    ruleParameters.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(stockInfo)));
                else
                    ruleParameters.customParameters.Add(item.Name, Convert.ToString(item.GetValue(stockInfo)));

            }

            _stockManagementRepository.Create(ruleParameters);

            return true;
        }

        public string Create(Order order, string source)
        {
            RuleParameters ordruleParameters = new RuleParameters();
            RuleParameters OrderitemParameter = new RuleParameters();
            RuleParameters OrderPaymentParameter = new RuleParameters();
            string StoreId = string.Empty;
            string StorePass = string.Empty;

            switch (source.ToLower())
            {
                case "woocommerce":
                    string WooCommerceCompanyCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                    string[] WooCommerceCredential = WooCommerceCompanyCredentials.Split(':');
                    StoreId = WooCommerceCredential[0].ToString();
                    StorePass = WooCommerceCredential[1].ToString();
                    break;
                case "magento":
                    string MagentoCompanyCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                    string[] MagentoCredential = MagentoCompanyCredentials.Split(':');
                    StoreId = MagentoCredential[0].ToString();
                    StorePass = MagentoCredential[1].ToString();
                    break;
                default:
                    string eBayCompanyCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                    string[] eBayCredential = eBayCompanyCredentials.Split(':');
                    StoreId = eBayCredential[0].ToString();
                    StorePass = eBayCredential[1].ToString();
                    break;
            }

            ordruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            string OrderId = string.Empty;
            string CustomerId = string.Empty;
            string OrderNumber = string.Empty;

            //Order Master Entry
            var lstFields = typeof(Order).GetProperties();
            foreach (var item in lstFields)
            {
                GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
               (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                if (myDBFieldMapping != null)
                    ordruleParameters.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(order)));
                else
                    ordruleParameters.customParameters.Add(item.Name, Convert.ToString(item.GetValue(order)));
            }

            //Customer Enter
            var lstBillingitem = typeof(BillingAddress).GetProperties();
            var lstshippingitem = typeof(ShippingAddress).GetProperties();
            var lstCustitem = typeof(Customer).GetProperties();

            foreach (var custitem in lstCustitem)
            {

                GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
               (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(custitem, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                if (myDBFieldMapping != null)
                    ordruleParameters.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(custitem.GetValue(order.customer)));
                else
                    ordruleParameters.customParameters.Add(custitem.Name, Convert.ToString(custitem.GetValue(order.customer)));
            }
            foreach (var item in lstBillingitem)
            {

                GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
               (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                if (myDBFieldMapping != null)
                    ordruleParameters.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(order.billingAddress)).Replace("'", string.Empty));
                else
                    ordruleParameters.customParameters.Add(item.Name, Convert.ToString(item.GetValue(order.billingAddress)).Replace("'", string.Empty));
            }
            foreach (var item in lstshippingitem)
            {

                GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
               (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                if (myDBFieldMapping != null)
                    ordruleParameters.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(order.shippingAddress)).Replace("'", string.Empty));
                else
                    ordruleParameters.customParameters.Add(item.Name, Convert.ToString(item.GetValue(order.shippingAddress)).Replace("'", string.Empty));
            }

            ordruleParameters.customParameters.Add("source", source);
            CustomerId = InsertCustomer(ordruleParameters);
            ordruleParameters.customParameters.Add("custId", CustomerId);

            string orderdetails = InsertOrdMaster(ordruleParameters);
            OrderId = orderdetails.Split('|')[0];
            OrderNumber = orderdetails.Split('|')[1];

            //OrderItem Parameter
            foreach (var orderitem in order.orderitem)
            {
                var lstOrderitem = typeof(OrderLineItem).GetProperties();
                OrderitemParameter.customParameters = new System.Collections.Specialized.NameValueCollection();
                foreach (var item in lstOrderitem)
                {

                    GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
                   (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                    if (myDBFieldMapping != null)
                        OrderitemParameter.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(orderitem)));
                    else
                        OrderitemParameter.customParameters.Add(item.Name, Convert.ToString(item.GetValue(orderitem)));
                }
                OrderitemParameter.customParameters.Add("orderId", OrderId);
                OrderitemParameter.customParameters.Add("source", source);
                InsertOrderitem(OrderitemParameter);
            }

            //Save Payment Info
            foreach (var paymentitem in order.PaymentInfo)
            {
                var paymentInfoProperties = typeof(OrderPaymentInfo).GetProperties();
                OrderPaymentParameter.customParameters = new System.Collections.Specialized.NameValueCollection();
                foreach (var item in paymentInfoProperties)
                {

                    GoodSide.Domain.Helpers.FieldMappingAttribute myDBFieldMapping =
                   (GoodSide.Domain.Helpers.FieldMappingAttribute)Attribute.GetCustomAttribute(item, typeof(GoodSide.Domain.Helpers.FieldMappingAttribute));
                    if (myDBFieldMapping != null)
                        OrderPaymentParameter.customParameters.Add(myDBFieldMapping.DBFieldName, Convert.ToString(item.GetValue(paymentitem)));
                    else
                        OrderPaymentParameter.customParameters.Add(item.Name, Convert.ToString(item.GetValue(paymentitem)));
                }
                OrderPaymentParameter.customParameters.Add("orderId", OrderId);
                OrderPaymentParameter.customParameters.Add("globalmsl", "1");
                OrderPaymentParameter.customParameters.Add("hidden_image_input", "");
                OrderPaymentParameter.customParameters.Add("useridnew", StorePass);
                OrderPaymentParameter.customParameters.Add("TID", (new Random()).Next(9999).ToString());
                var result = SaveOrderPayment(OrderPaymentParameter);
            }

            return OrderNumber;
        }

        public bool Update(StockManagementInfo stockInfo)
        {
            return true;
        }

        public void Delete(int StockId)
        {
        }

        private string InsertOrdMaster(RuleParameters ordruleParameters)
        {
            // yet to be decided for setting Salesperson Value
            ordruleParameters.customParameters.Add("spersonid", "0");
            ordruleParameters.Action = RuleAction.Set;
            ordruleParameters.RuleFile = RuleFileName.POS;
            ordruleParameters.RuleName = "apinewordermaster";
            return _apiOrderRepository.Create_API(ordruleParameters);
        }
        private string InsertCustomer(RuleParameters ordruleParameters)
        {
            ordruleParameters.Action = RuleAction.Set;
            ordruleParameters.RuleFile = RuleFileName.POS;
            ordruleParameters.RuleName = "apicust";
            return _apiOrderRepository.Create_API(ordruleParameters);
        }
        private bool InsertOrderitem(RuleParameters OrderitemParameter)
        {
            // yet to be decided for setting stocklocationid and poid Value
            OrderitemParameter.customParameters.Add("stocklocationid", "1");
            OrderitemParameter.customParameters.Add("poid", "-1");
            OrderitemParameter.Action = RuleAction.Set;
            OrderitemParameter.RuleFile = RuleFileName.POS;
            OrderitemParameter.RuleName = "apiorderitem";
            _apiOrderRepository.Create(OrderitemParameter);
            return true;
        }

        private GeneralRuleResult SaveOrderPayment(RuleParameters OrderPaymentInfoParameter)
        {
            OrderPaymentInfoParameter.Action = RuleAction.Set;
            OrderPaymentInfoParameter.RuleFile = RuleFileName.POS;
            OrderPaymentInfoParameter.RuleName = "apiordpay";
            return _orderpayRepository.GetSingleRecord(OrderPaymentInfoParameter);

        }
        public GeneralRuleResult CheckSku(string sku)
        {
         
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "produtsearchsku";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("sku", sku);
            var result = _checkSkuRepository.GetSingleRecord(ruleParameters);

            return result;

        }

    }
}
