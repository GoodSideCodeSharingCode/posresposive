﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace GoodSide.Domain.Services
{
    #region "Companies Service"

    public interface IAccountCompanyService
    {
        List<AccountCompany> GetCompanies();
        //AccountUser GetLoggedInUser(string Password, int CompanyId);      
    }

    public class AccountCompanyService : IAccountCompanyService
    {
        private IRepository<AccountCompany> _actCompanyRepository;

        public AccountCompanyService(IRepository<AccountCompany> actCompanyRepository)
        {
            _actCompanyRepository = actCompanyRepository;
        }

        public List<AccountCompany> GetCompanies()
        {
            return _actCompanyRepository.GetCompanies();
        }
    }

    #endregion

    #region "Account User Service"

    public interface IAccountUserService
    {   
        AccountUser GetLoggedInUser(string Password, int CompanyId);      
    }

    public class AccountUserService : IAccountUserService
    {
        private IRepository<AccountUser> _actUserRepository;

        public AccountUserService(IRepository<AccountUser> actUserRepository)
        {
            _actUserRepository = actUserRepository;
        }

        public AccountUser GetLoggedInUser(string Password, int CompanyId)
        {
           return  _actUserRepository.GetLoggedInUser(Password, CompanyId);
        }
    }

    #endregion
}
