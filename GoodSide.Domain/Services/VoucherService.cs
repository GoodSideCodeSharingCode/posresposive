﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
namespace GoodSide.Domain.Services
{
    public interface IVoucherService
    {
       // VoucherSearchItem GetAll(string SearchText, int offSet, int Rows, string sortBy, string sortDirection);
        GeneralRuleResult Update(VoucherItemInfo VoucherItem);
    }

    public class VoucherService : IVoucherService
    {
        private IRepository<GeneralRuleResult> _voucherRepository;

        public VoucherService(IRepository<GeneralRuleResult> voucherRepository)
        {
            _voucherRepository = voucherRepository;
        }

        public GeneralRuleResult Update(VoucherItemInfo VoucherItem)
        {
            // _productRepository.Update(product);

            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "gift";

            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", VoucherItem.voucherid);
            ruleParameters.customParameters.Add("code", VoucherItem.code);
            ruleParameters.customParameters.Add("customername", VoucherItem.customername.Replace("'", "''''"));
            ruleParameters.customParameters.Add("issuedate", VoucherItem.IssueDate);
            ruleParameters.customParameters.Add("expirydate", VoucherItem.ExpiryDate);
            ruleParameters.customParameters.Add("spersonid", VoucherItem.SalePersonId);
            ruleParameters.customParameters.Add("description", (VoucherItem.Description != null ? VoucherItem.Description.Replace("'", "''''") : string.Empty));

            ruleParameters.customParameters.Add("amount", VoucherItem.amount.ToString());
            ruleParameters.customParameters.Add("referenceno", VoucherItem.RefNo);
            ruleParameters.customParameters.Add("pmode", VoucherItem.PaymentType);
            ruleParameters.customParameters.Add("useridnew", VoucherItem.Password);
            var result = _voucherRepository.GetSingleRecord(ruleParameters);
            return result;

        }


       
    }
}
