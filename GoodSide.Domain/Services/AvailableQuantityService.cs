﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IAvailableQuantityService
    {
        List<AvailableQuantity> GetAvailableQuantityByBarcode(string barcodeNo, string productID);
        string CheckDefaultLocationForBarcode(string barcodeNo);
        string GetDefaultLocationForBarCode(string stockid);
        GeneralRuleResult GetProductData(string ProductId);
        List<AvailableQuantity> GetAvailableQuantityByBarcodeMisc(string barcodeNo, string productID);
    }

    public class AvailableQuantityService : IAvailableQuantityService
    {
        private IRepository<AvailableQuantity> _availableQuantityRepository;
        private IRepository<GeneralRuleResult> _getProductData;
        public AvailableQuantityService(IRepository<AvailableQuantity> availableQuantityRepository, IRepository<GeneralRuleResult> getProductData)
        {
            _availableQuantityRepository = availableQuantityRepository;
            _getProductData = getProductData;
        }


        public List<AvailableQuantity> GetAvailableQuantityByBarcode(string barcodeNo, string productID)
        {
            var ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "availableqty";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", productID);
            ruleParameters.customParameters.Add("bno", barcodeNo);
            var result = _availableQuantityRepository.GetAll(ruleParameters);
            return result;
        }

        public string CheckDefaultLocationForBarcode(string barcodeNo)
        {
            var ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "chkquicksaleforloc";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();           
            ruleParameters.customParameters.Add("bno", barcodeNo);
            var result = _availableQuantityRepository.ChkDefaultLoc(ruleParameters);
            return result;

        }
        public string GetDefaultLocationForBarCode(string stockid)
        {
            //GeneralRuleResult result = new GeneralRuleResult();
            var ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "quickselldefaultlocation";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("stockid", stockid);
            var result = _availableQuantityRepository.GetDefaultLocationForBarCode(ruleParameters);
            return result;
        }
        public GeneralRuleResult GetProductData(string ProductId)
        {
            var ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "productdata";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("productid", ProductId);
            var result = _getProductData.GetSingleRecord(ruleParameters);
            return result;

        }
        public List<AvailableQuantity> GetAvailableQuantityByBarcodeMisc(string barcodeNo, string productID)
        {
            var ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "availableqtymisc";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", productID);
            ruleParameters.customParameters.Add("bno", barcodeNo);
            var result = _availableQuantityRepository.GetAll(ruleParameters);
            return result;
        }
    }
}
