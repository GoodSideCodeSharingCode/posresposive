﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IProductService
    {
        Product GetById(int id);
        OpenOrderItemLists GetByOrderid(string orderid);
        Product GetStypeInfo(string Stype,string productid);
    }

    public class ProductService : IProductService
    {
        private IRepository<Product> _productRepository;
        private IRepository<OpenOrderItemLists> _openorderRepository;       
        public ProductService(IRepository<Product> productRepository, IRepository<OpenOrderItemLists> openorderRepository)
        {
            _productRepository = productRepository;
            _openorderRepository = openorderRepository;         
        }

        public Product GetById(int id)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "produtbyid";         
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", id.ToString());
            var result = _productRepository.GetSingleRecord(ruleParameters);
            return result;
        }
        public OpenOrderItemLists GetByOrderid(string orderid)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "openprodutbyid";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", orderid);
            var result = _openorderRepository.GetAllMultipleTable(ruleParameters);
            return result;
        }
        public Product GetStypeInfo(string Stype, string productid)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "getstypeinfo";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("stype", Stype);
            ruleParameters.customParameters.Add("pid", productid);
            var result = _productRepository.GetSingleRecord(ruleParameters);
            return result;
        }
    }
}
