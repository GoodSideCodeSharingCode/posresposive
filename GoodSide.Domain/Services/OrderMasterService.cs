﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Helpers;

namespace GoodSide.Domain.Services
{
    public interface IOrderMasterService
    {
        OrderMaster GetAll(string SearchText, int offSet, int Length, string sortBy, string sortDirection);
        OrderMaster GetSingleRecord(int stockId);
        GeneralRuleResult Create(OrderMaster posorderMaster, string orderStatus);
        OrderMaster Update(OrderMaster posorderMaster);
        void Delete(int stockId);
        GeneralRuleResult CreateOrderitem(OrderDetail posorderMaster, string orderid);
        GeneralRuleResult OrderPaySave(OrderPayItem invoiceItem);
        GeneralRuleResult DeleteOrderItem(string IdstoDelete, string orderid);
    }

    public class OrderMasterService : IOrderMasterService
    {
        private IRepository<OrderMaster> _orderMasterRepository;
        private IRepository<GeneralRuleResult> _orderpayRepository;

        public OrderMasterService(IRepository<OrderMaster> orderMasterRepository, IRepository<GeneralRuleResult> orderpayRepository)
        {
            _orderpayRepository = orderpayRepository;
            _orderMasterRepository = orderMasterRepository;
        }

        public OrderMaster GetAll(string SearchText, int offSet, int Rows, string sortBy, string sortDirection)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.STK;
            ruleParameters.RuleName = "lisForAPI";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("part_no", SearchText);
            ruleParameters.customParameters.Add("product_name", SearchText);
            ruleParameters.customParameters.Add("supplier", SearchText);
            ruleParameters.customParameters.Add("categoryid", SearchText);
            ruleParameters.customParameters.Add("typeid", SearchText);
            ruleParameters.customParameters.Add("barcode_no", SearchText);
            ruleParameters.customParameters.Add("description", SearchText);

            var result = _orderMasterRepository.GetAllMultipleTable(ruleParameters);
            return result;
        }

        public OrderMaster GetSingleRecord(int stockId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            return new OrderMaster();
        }

        public GeneralRuleResult Create(OrderMaster posorderMaster, string orderStatus)
        {
            GeneralRuleResult result = new GeneralRuleResult();
            try
            {

                RuleParameters ruleParameters = new RuleParameters();
                ruleParameters.Action = RuleAction.Set;
                ruleParameters.RuleFile = RuleFileName.POS;
                ruleParameters.RuleName = "apineworder";
                ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
                if (!string.IsNullOrEmpty(posorderMaster.orderid))
                    ruleParameters.customParameters.Add("id", posorderMaster.orderid);
                else
                    ruleParameters.customParameters.Add("id", "0");
                ruleParameters.customParameters.Add("orderno", posorderMaster.temporderid);
                ruleParameters.customParameters.Add("customerId", posorderMaster.customerid);
                ruleParameters.customParameters.Add("spersonid", posorderMaster.salespersonid);
                ruleParameters.customParameters.Add("mcharges", String.Format("{0:0.00}", posorderMaster.misc));
                ruleParameters.customParameters.Add("discgiven", String.Format("{0:0.00}", posorderMaster.discgiven_base));
                ruleParameters.customParameters.Add("comment", "No comment");
                ruleParameters.customParameters.Add("address", "");
                ruleParameters.customParameters.Add("netpayment", String.Format("{0:0.00}", posorderMaster.netpayment));
                ruleParameters.customParameters.Add("orderStatus", orderStatus);  //6 as confirmed, 4 as quote
                ruleParameters.customParameters.Add("exchangerate", String.Format("{0:0.00}", posorderMaster.exchangerate));
                ruleParameters.customParameters.Add("currency", posorderMaster.currency.ToString());
                

                POSLogger.Info("Create Order Parameter Start---------------");
                foreach (string key in ruleParameters.customParameters)
                {
                    POSLogger.Info(key + "::" + ruleParameters.customParameters[key]);
                }
                POSLogger.Info("Create Order Parameter End---------------");
                result = _orderpayRepository.GetSingleRecord(ruleParameters);
                POSLogger.Info("result status:" + result.status);
                POSLogger.Info("result outputvalue:" + result.outputvalue);
                POSLogger.Info("result outputvalue2:" + result.outputvalue2);
            }
            catch (Exception ex)
            {
                result.status = "error";
                result.outputvalue = Convert.ToString(ex.Message);
                POSLogger.Error(ex);
            }
            return result;
        }

        public OrderMaster Update(OrderMaster posorderMaster)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apineworder";
            OrderMaster orderInfoList = _orderMasterRepository.GetSingleRecord(ruleParameters);
            return orderInfoList;
        }

        public void Delete(int StockId)
        {
        }
        public GeneralRuleResult CreateOrderitem(OrderDetail ord, string orderid)
        {
            GeneralRuleResult result = new GeneralRuleResult();
            try
            {
                RuleParameters ruleParameters = new RuleParameters();
                ruleParameters.Action = RuleAction.Set;
                ruleParameters.RuleFile = RuleFileName.POS;
                ruleParameters.RuleName = "orderitem";

                ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

                ruleParameters.customParameters.Add("id2", ord.pid.ToString());
                ruleParameters.customParameters.Add("orderId", orderid);
                ruleParameters.customParameters.Add("qty", ord.qty.ToString());
                ruleParameters.customParameters.Add("gsl", ord.locationid.ToString());
                ruleParameters.customParameters.Add("pomid", ord.poid.ToString());

                ruleParameters.customParameters.Add("dtype", ord.deliverytype.ToString());
                if (ord.deliverydate != null)
                    ruleParameters.customParameters.Add("deliverydate", ord.deliverydate.Replace(@"/","-"));
                
                ruleParameters.customParameters.Add("sellingprice", String.Format("{0:0.00}", ord.slprice_base));
                ruleParameters.customParameters.Add("productdesc", (ord.productdesc != null ? ord.productdesc.ToString().Replace("'", "''''") :string.Empty));
                //ruleParameters.customParameters.Add("productdesc", ord.productdesc.ToString());
                ruleParameters.customParameters.Add("colors", (ord.colors != null ? ord.colors.ToString().Replace("'", "''''") : string.Empty));
                ruleParameters.customParameters.Add("discgiven", String.Format("{0:0.00}", ord.discgiven_base));
                ruleParameters.customParameters.Add("table", ord.table);
                ruleParameters.customParameters.Add("destinationpoint", ord.DestinationPoint.ToString());
                ruleParameters.customParameters.Add("quicksale", (ord.isscanitem == true ? "1":"0"));
                ruleParameters.customParameters.Add("saletype", ord.sale_type.ToString());
                ruleParameters.customParameters.Add("custleadtime",ord.cust_leadtime );
                ruleParameters.customParameters.Add("orderdetailid", ord.OrderDetailId.ToString());
                ruleParameters.customParameters.Add("orderitemdetailid", ord.orderitemdetailid.ToString());
                ruleParameters.customParameters.Add("isscanned", (ord.isscanitem == true ? "1" : "0"));
                ruleParameters.customParameters.Add("discountcomment", ord.Discount_Comment);
                POSLogger.Info("Create Order Item Parameter Start---------------");
                foreach (string key in ruleParameters.customParameters)
                {
                    POSLogger.Info(key + "::" + ruleParameters.customParameters[key]);
                }
                POSLogger.Info("Create Order Item Parameter End---------------");
                
                result = _orderpayRepository.GetSingleRecord(ruleParameters);

                POSLogger.Info("result status:" + result.status);
                POSLogger.Info("result outputvalue:" + result.outputvalue);

            }
            catch (Exception ex)
            {
                result.status = "error";
                result.outputvalue = Convert.ToString(ex.Message);
                POSLogger.Error(ex);
            }
            return result;
        }
        public GeneralRuleResult OrderPaySave(OrderPayItem ordpayItem)
        {
            GeneralRuleResult result = new GeneralRuleResult();
            try
            {
                RuleParameters ruleParameters = new RuleParameters();
                ruleParameters.Action = RuleAction.Set;
                ruleParameters.RuleFile = RuleFileName.POS;
                ruleParameters.RuleName = "ordpay";
                ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

                ruleParameters.customParameters.Add("orderId", ordpayItem.orderId);
                ruleParameters.customParameters.Add("amount", ordpayItem.amount);
                ruleParameters.customParameters.Add("cntotal", ordpayItem.cntotal);
                ruleParameters.customParameters.Add("date", ordpayItem.date);
                ruleParameters.customParameters.Add("reference", (ordpayItem.reference != null ? ordpayItem.reference.Replace("'", "''''") : string.Empty));
                ruleParameters.customParameters.Add("description",(ordpayItem.description != null ? ordpayItem.description.Replace("'", "''''") : string.Empty));

                ruleParameters.customParameters.Add("pmode", ordpayItem.pmode);
                ruleParameters.customParameters.Add("authorized", ordpayItem.authorized);
                ruleParameters.customParameters.Add("voucherslist", ordpayItem.voucherlists);

                ruleParameters.customParameters.Add("voucheramount", ordpayItem.voucheramount);
                ruleParameters.customParameters.Add("useridnew", ordpayItem.useridnew);
                ruleParameters.customParameters.Add("showmaponreceipt", ordpayItem.showmaponreceipt);

                ruleParameters.customParameters.Add("globalmsl", "1");
                ruleParameters.customParameters.Add("minimumpayment", ordpayItem.amount);
                ruleParameters.customParameters.Add("hidden_image_input", "");
                ruleParameters.customParameters.Add("creditnotedetailtable", ordpayItem.cndetailtable);

                ruleParameters.customParameters.Add("cash", ordpayItem.cash);
                ruleParameters.customParameters.Add("visa", ordpayItem.visa);
                ruleParameters.customParameters.Add("mastercard", ordpayItem.mastercard);
                ruleParameters.customParameters.Add("cheque", ordpayItem.cheque);
                ruleParameters.customParameters.Add("laser", ordpayItem.laser);
                ruleParameters.customParameters.Add("paypal", ordpayItem.paypal);

                ruleParameters.customParameters.Add("cashdetail", (ordpayItem.cashdetail != null ?ordpayItem.cashdetail.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("visadetail", (ordpayItem.visadetail != null ?ordpayItem.visadetail.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("mastercarddetail", (ordpayItem.mastercarddetail != null ? ordpayItem.mastercarddetail.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("chequedetail", (ordpayItem.chequedetail != null ? ordpayItem.chequedetail.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("cndetails", (ordpayItem.cndetails != null ? ordpayItem.cndetails.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("vdetail", (ordpayItem.gvdetail != null ? ordpayItem.gvdetail.Replace("'", "''''") :string.Empty));
                ruleParameters.customParameters.Add("paypaldetail", (ordpayItem.paypaldetail != null ? ordpayItem.paypaldetail.Replace("'", "''''") :string.Empty));
               
                ruleParameters.customParameters.Add("amount_tendered", String.Format("{0:0.00}", ordpayItem.amount_tendered));


                POSLogger.Info("Order Pay Set Parameter Start---------------");
                foreach (string key in ruleParameters.customParameters)
                {
                    POSLogger.Info(key + "::" + ruleParameters.customParameters[key]);
                }
                POSLogger.Info("Order Pay Set Parameter End------------------");
                result = _orderpayRepository.GetSingleRecord(ruleParameters);
                POSLogger.Info("result status:" + result.status);
                POSLogger.Info("result outputvalue:" + result.outputvalue);

            }
            catch (Exception ex)
            {
                result.outputvalue = Convert.ToString(ex.Message);
                result.status = "error";
                POSLogger.Error(ex);
            }
            return result;
        }

        public GeneralRuleResult DeleteOrderItem(string IdstoDelete, string orderid)
        {
            GeneralRuleResult result = new GeneralRuleResult();
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "delitem";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("id", IdstoDelete);
            ruleParameters.customParameters.Add("orderId", orderid);

          
            result = _orderpayRepository.GetSingleRecord(ruleParameters);
            POSLogger.Info("result status:" + result.status);
            POSLogger.Info("result outputvalue:" + result.outputvalue);

            return result;
        }
    }
}
