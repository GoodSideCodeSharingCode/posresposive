﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Helpers;
using GoodSide.Domain.Repositories;

namespace GoodSide.Domain.Services
{
    public interface IGeneralSelectListService
    {
        NameValueItem GetAllCountryList();
        NameValueItem GetAllCountyList();
        NameValueItem GetAllSalesPerson();
        NameValueItem GetAboutUsList();
        NameValueItem GetFinancialList();
        NameValueItem GetDeliveryTypeList();
    }

    public class GeneralSelectListService : IGeneralSelectListService
    {
        private IRepository<NameValueItem> _repository;

        public GeneralSelectListService(IRepository<NameValueItem> Repository)
        {
            _repository = Repository;
        }

     
   
        public NameValueItem GetAllCountryList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicountry";// "customerapi";

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }

        public NameValueItem GetAllCountyList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicounty";// "customerapi";

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }


        public NameValueItem GetAllSalesPerson()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apisalesperson";// "customerapi";

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }
        public NameValueItem GetAboutUsList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apiaboutus";// "customerapi";

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }
        public NameValueItem GetFinancialList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apifiancial";

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }
        public NameValueItem GetDeliveryTypeList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apideliverytype";// 

            var result = _repository.GetAllMultipleTable(ruleParameters);

            return result;
        }
    }
}
