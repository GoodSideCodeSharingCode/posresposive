﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Helpers;
using GoodSide.Domain.Repositories;

namespace GoodSide.Domain.Services
{
    public interface ICustomerService
    {
        CustomerSearchItem GetAll(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string RuleName,string Active);

        CustomerViewOrders GetOrdersByCustomer(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string CustomerId , string OrdStatus);
        CustomerViewOrders GetAllOrdersByCustomer(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string CustomerId, string OrdStatus);

        CustomerItem GetCustomerById(string customerId);

        void Create(CustomerItem custItem);

        string Update(CustomerItem custItem);

        void Delete(RuleParameters ruleParameters);

        CustomerItem GetCustomerEdit(string customerId);

        CustomerItem GetCashSaleCustomer();

        OrderSummary OrderSummaryByOrderId(string OrderId);

        //  CustomerSearchItem GetAllSalesPerson();
        // CustomerSearchItem GetAllCountryList();
        // CustomerSearchItem GetAllCountyList();

        string GetNewCustomerCode();

        List<CustomerPhoneSearchItem> CustomerPhoneSearch(string terms);
        string SendOrderEmail(string orderNo, string userid, string templateString, string subjectCompanyName,string emailFrom, string toemail, bool enableSSL, string smtpAddress, int portNumber, string smtpUsername, string smtpPassword);
    }

    public class CustomerService : ICustomerService
    {
        private IRepository<CustomerSearchItem> _customerRepository;
        private IRepository<CustomerViewOrders> _customerViewOrderRepository;
        private IRepository<CustomerItem> _customerItemRepository;
        private IRepository<CustomerPhoneSearchItem> _customerphoneRepository;
        private IRepository<OrderSummary> _customerOrderSummaryRePository;

        public CustomerService(IRepository<CustomerSearchItem> customerRepository, IRepository<CustomerItem> customerItemRepository, IRepository<CustomerPhoneSearchItem> customerphoneRepository, IRepository<CustomerViewOrders> customerViewOrderRepository, IRepository<OrderSummary> customerOrderSummaryRePository)
        {
            _customerRepository = customerRepository;
            _customerItemRepository = customerItemRepository;
            _customerphoneRepository = customerphoneRepository;
            _customerViewOrderRepository = customerViewOrderRepository;
            _customerOrderSummaryRePository = customerOrderSummaryRePository;
        }

        public CustomerSearchItem GetAll(string SearchText, int offSet, int Rows, string sortBy, string sortDirection,string RuleName,string Active)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = RuleName;// "customerapi";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            //ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("search", SearchText);
            ruleParameters.customParameters.Add("searchtype", "2");
            ruleParameters.customParameters.Add("active",Active);
            var result = _customerRepository.GetAllMultipleTable(ruleParameters);

            return result;
        }

        public CustomerViewOrders GetOrdersByCustomer(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string CustomerId , string OrdStatus)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "custorder";// "customerapi";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("customerId", CustomerId);
            ruleParameters.customParameters.Add("search", SearchText);
            ruleParameters.customParameters.Add("ordstatus", OrdStatus);
            var result = _customerViewOrderRepository.GetAllMultipleTable(ruleParameters);

            return result;
        }

        public CustomerViewOrders GetAllOrdersByCustomer(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string CustomerId, string OrdStatus)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "allorder";// "customerapi";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("customerId", CustomerId);
            ruleParameters.customParameters.Add("search", SearchText);
            ruleParameters.customParameters.Add("ordstatus", "0");
            var result = _customerViewOrderRepository.GetAllMultipleTable(ruleParameters);

            return result;
        }

        public CustomerItem GetCustomerById(string customerId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "customerbyid";
            ruleParameters.Offset = 0;
            ruleParameters.Rows = 1;
            ruleParameters.SortBy = "name";
            ruleParameters.SortOrder = SortOrder.DESC;

            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("customerId", customerId);

            CustomerItem customerInfoList = _customerItemRepository.GetSingleRecord(ruleParameters);
            return customerInfoList;
        }

        public string GetNewCustomerCode()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "newcustomercode";
            ruleParameters.Rows = 1;
            CustomerItem customerInfoList = _customerItemRepository.GetSingleRecord(ruleParameters);
            return customerInfoList.code;
        }

        public void Create(CustomerItem custItem)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "cust";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", custItem.id);

            ruleParameters.customParameters.Add("code", custItem.code);
            ruleParameters.customParameters.Add("name", custItem.name.Replace("'", "''''"));
            ruleParameters.customParameters.Add("country", custItem.countryid);
            ruleParameters.customParameters.Add("countyid","0");
            ruleParameters.customParameters.Add("area", "0");
            ruleParameters.customParameters.Add("location", "0");


            ruleParameters.customParameters.Add("salesId", custItem.salespersonid);
            ruleParameters.customParameters.Add("address", custItem.address.Replace("'", "''''"));
            ruleParameters.customParameters.Add("postalcode", custItem.postalcode);

            ruleParameters.customParameters.Add("notes", "");
            ruleParameters.customParameters.Add("phonenumber", custItem.phonenumber);
            ruleParameters.customParameters.Add("mobilenumber", custItem.mobilenumber);
            ruleParameters.customParameters.Add("emailaddress", custItem.email.Replace("'", "''''"));


            _customerRepository.Create(ruleParameters);
        }

        public string Update(CustomerItem custItem)
        {
            // _productRepository.Update(product);

            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "cust";

            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", custItem.id);
            ruleParameters.customParameters.Add("code", custItem.code);
            ruleParameters.customParameters.Add("name", custItem.name.Replace("'","''''"));
            ruleParameters.customParameters.Add("country", "0");
            ruleParameters.customParameters.Add("countyid", "0");
            ruleParameters.customParameters.Add("area", "0");
            ruleParameters.customParameters.Add("location", "0");

            ruleParameters.customParameters.Add("salesId", custItem.salespersonid);
            ruleParameters.customParameters.Add("address", (custItem.address != null ? custItem.address.Replace("'", "''''"):string.Empty));
            ruleParameters.customParameters.Add("postalcode", (custItem.postalcode != null ? custItem.postalcode.Replace("'", "''''"):string.Empty));
            ruleParameters.customParameters.Add("notes", "");
            ruleParameters.customParameters.Add("phonenumber", custItem.phonenumber);
            ruleParameters.customParameters.Add("mobilenumber", custItem.mobilenumber);
            ruleParameters.customParameters.Add("emailaddress", (custItem.email != null ? custItem.email.Replace("'", "''''"):string.Empty));          

            ruleParameters.customParameters.Add("primarycontactnote", custItem.name.Replace("'", "''''"));
            ruleParameters.customParameters.Add("secondarycontactnote", custItem.name.Replace("'", "''''"));
            ruleParameters.customParameters.Add("howdidyouhearaboutus", custItem.howdidyouhearaboutusid);
            ruleParameters.customParameters.Add("financialinstitution", "0");// custItem.financialinstitutionid);
            ruleParameters.customParameters.Add("showmaponreceipt", ((custItem.showmaponreceipt == true) ? "1" : "0"));
            ruleParameters.customParameters.Add("deliveryType", "0");
            ruleParameters.customParameters.Add("defaultcurrency", Convert.ToString(custItem.defaultcurrencyid));
            ruleParameters.customParameters.Add("active", custItem.active == true ? "1" : "0");

            CustomerItem objcustomerItem = _customerItemRepository.GetSingleRecord(ruleParameters);
            return objcustomerItem.custid;



        }

        public void Delete(RuleParameters ruleParameters)
        {
            // _productRepository.Delete(id);
        }

        public CustomerItem GetCustomerEdit(string customerId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Edit;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "cust";
            ruleParameters.Offset = 0;
            ruleParameters.Rows = 1;
            ruleParameters.SortBy = "name";
            ruleParameters.SortOrder = SortOrder.DESC;

            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", customerId);

            CustomerItem customerInfoList = _customerItemRepository.GetSingleRecord(ruleParameters);
            return customerInfoList;
        }
        public List<CustomerPhoneSearchItem> CustomerPhoneSearch(string phonetext)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "customerphonesearch";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("phonesearchtext", phonetext);
            return _customerphoneRepository.GetAll(ruleParameters);
        }

        public CustomerItem GetCashSaleCustomer()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicashsalecustomer";
            return _customerItemRepository.GetSingleRecord(ruleParameters);
        }

        public string SendOrderEmail(string orderNo, string userid, string templateString,string subjectCompanyName, string emailFrom, string toemail,bool enableSSL, string smtpAddress, int portNumber, string smtpUsername, string smtpPassword)
        {
            string result= _customerRepository.SendOrderEmail(orderNo, userid, templateString, subjectCompanyName, emailFrom, toemail, enableSSL, smtpAddress, portNumber, smtpUsername, smtpPassword);
            POSLogger.Info("Order No:" + orderNo + " Email Result:" + result);
            return result;
        }
        public OrderSummary OrderSummaryByOrderId(string OrderId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.Offset = 0;
            ruleParameters.Rows = 1;
            ruleParameters.SortBy = "orderno";
            ruleParameters.SortOrder = SortOrder.DESC;
            ruleParameters.RuleName = "oordersummary";            
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("OrderId", OrderId);     
            var result = _customerOrderSummaryRePository.GetAllMultipleTable(ruleParameters);           
            return result;
        }

    }
}
