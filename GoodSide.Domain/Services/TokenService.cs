﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace GoodSide.Domain.Services
{
    public interface ITokenServices
    {
        #region Interface member methods.
        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Token GenerateToken(int storeID, string userName, string storeName, bool isPasswordOnly, int userRole);

        /// <summary>
        /// Function to validate token againt expiry and existance in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        Token ValidateToken(string tokenId, ref bool isValid);

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId"></param>
        bool Kill(string tokenId);

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool DeleteByUserId(int userId);
        #endregion
    }

    public class TokenService : ITokenServices
    {
        private IRepository<Token> _tokenRespository;

        public TokenService(IRepository<Token> tokenRespository)
        {
            _tokenRespository = tokenRespository;
        }

        #region Public member methods.

        /// <summary>
        ///  Function to generate unique token with expiry against the provided userId.
        ///  Also add a record in database for generated token.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Token GenerateToken(int storeID, string userName, string storeName, bool isPasswordOnly, int userRole)
        {
            string token = Guid.NewGuid().ToString();
            string issuedOn = DateTime.Now.ToString();
            string expiresOn = DateTime.Now.AddSeconds(
                                              Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"])).ToString();
            var tokendomain = new Token
            {
                StoreId = storeID,
                AuthToken = token,
                IssuedOn = issuedOn,
                ExpiresOn = expiresOn
            };

            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "authenticationtoken";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("storeid", storeID.ToString());
            ruleParameters.customParameters.Add("authtoken", token);
            ruleParameters.customParameters.Add("issuedon", issuedOn);
            ruleParameters.customParameters.Add("expireson", expiresOn);
            ruleParameters.customParameters.Add("username", userName);
            ruleParameters.customParameters.Add("storename", storeName);
            ruleParameters.customParameters.Add("passwordonly", isPasswordOnly.ToString());
            ruleParameters.customParameters.Add("userrole", userRole.ToString());            

            var result = _tokenRespository.GetSingleRecord(ruleParameters);
            return result;

        }

        /// <summary>
        /// Method to validate token against expiry and existence in database.
        /// </summary>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public Token ValidateToken(string tokenId, ref bool isValid)
        {
            isValid = false;
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "authenticationtoken";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("authtoken", tokenId);
            var result = _tokenRespository.GetSingleRecord(ruleParameters);

            if (result != null && !(DateTime.Now > Convert.ToDateTime(result.ExpiresOn)))
            {
                result.ExpiresOn = Convert.ToDateTime(result.ExpiresOn).AddSeconds(
                                              Convert.ToDouble(ConfigurationManager.AppSettings["AuthTokenExpiry"])).ToString();
                ruleParameters = new RuleParameters();
                ruleParameters.Action = RuleAction.Set;
                ruleParameters.RuleFile = RuleFileName.POS;
                ruleParameters.RuleName = "authenticationtoken";
                ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
                ruleParameters.customParameters.Add("storeid", result.StoreId.ToString());
                ruleParameters.customParameters.Add("authtoken", result.TokenId.ToString());
                ruleParameters.customParameters.Add("issuedon", result.IssuedOn.ToString());
                ruleParameters.customParameters.Add("expireson", result.ExpiresOn.ToString());
                _tokenRespository.Update(ruleParameters);
                isValid = true;
            }            
            return result;
        }

        /// <summary>
        /// Method to kill the provided token id.
        /// </summary>
        /// <param name="tokenId">true for successful delete</param>
        public bool Kill(string tokenId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "authenticationtokenkill";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("authtoken", tokenId);
            _tokenRespository.Delete(ruleParameters);
            return true;
        }

        /// <summary>
        /// Delete tokens for the specific deleted user
        /// </summary>
        /// <param name="storeId"></param>
        /// <returns>true for successful delete</returns>
        public bool DeleteByUserId(int storeId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "authenticationtokenkillforstore";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("storeid", storeId.ToString());
            _tokenRespository.Delete(ruleParameters);
            return true;
        }

        #endregion
    }
}
