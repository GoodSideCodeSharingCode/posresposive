﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using GoodSide.Domain.Helpers;

namespace GoodSide.Domain.Services
{
    public interface IPOSService
    {
        TemopraryOrderNumber GetTemporaryOrderNumber(string CompanyId);
        Currency GetDefaultCurrency();
        Currency GetCurrencyById(int currencyId);
        StockItemMagento GetAllStockDetailsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate);
        StockItemWooCommerce GetAllStockDetailsWooCommerce(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source,string LastSyncDate);
        StockItemWooCommerce GetSelectedProductsWooCommerce(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source);
        void ResetStockExported(string Ids , string source);
        List<StockDetailSimilarImage> GetSimilarProductInfo(string stockid);
        StockItemMagento GetStockDetailCountforMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate);
        GeneralRuleResult UpdateAllSpecificPrice(string targetDate);
        StockItemImageMagento GetAllStockImageDetailsMagento(string LastSyncDate);
        StockItemEbay GetSelectedProductsEbay(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source);
        StockItemMagento GetAllStockLevelDetailsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate);
        StockItemMagento GetSelectedProductsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source);
        string SetStockChannelDataId(string tabledata, string Source);
    }

    public class POSService : IPOSService
    {
        private IRepository<TemopraryOrderNumber> _temporaryOrderNumberRepository;
        private IRepository<Currency> _currencyRepository;
        private IRepository<StockItemMagento> _stockItemRepositoryMagento;
        private IRepository<StockItemWooCommerce> _stockItemRepositoryWooCommerce;
        private IRepository<StockDetailImagesInfo> _stockImageRepository;
        private IRepository<StockDetailSimilarImage> _stockSimilarProductsRepository;
        private IRepository<GeneralRuleResult> _stockSpecialPriceUpdateRepository;
        private IRepository<StockItemImageMagento> _stockItemImageRepositoryMagento;
        private IRepository<StockItemEbay> _stockItemRepositoryEbay;
        private IRepository<GeneralRuleResult> _stockItemRepositoryGeneraral;
        public POSService(IRepository<TemopraryOrderNumber> temporaryOrderNumberRepository, IRepository<Currency> currencyRepository, IRepository<StockItemMagento> stockItemRepositorymagento, IRepository<StockItemWooCommerce> stockItemRepositorywoocommerce, IRepository<StockDetailImagesInfo> stockImageRepository, IRepository<StockDetailSimilarImage> stockSimilarProductsRepository, IRepository<GeneralRuleResult> stockSpecialPriceUpdateRepository, IRepository<StockItemImageMagento> stockItemImageRepositoryMagento, IRepository<StockItemEbay> stockItemRepositoryEbay, IRepository<GeneralRuleResult> stockItemRepositoryGeneraral)
        {

            _temporaryOrderNumberRepository = temporaryOrderNumberRepository;
            _currencyRepository = currencyRepository;
            _stockItemRepositoryMagento = stockItemRepositorymagento;
            _stockItemRepositoryWooCommerce = stockItemRepositorywoocommerce;
            _stockImageRepository = stockImageRepository;
            _stockSimilarProductsRepository = stockSimilarProductsRepository;
            _stockSpecialPriceUpdateRepository = stockSpecialPriceUpdateRepository;
            _stockItemImageRepositoryMagento = stockItemImageRepositoryMagento;
            _stockItemRepositoryEbay = stockItemRepositoryEbay;
            _stockItemRepositoryGeneraral = stockItemRepositoryGeneraral;
        }

        public TemopraryOrderNumber GetTemporaryOrderNumber(string CompanyId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apigenerateordernumber";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("companyid", CompanyId);
            return _temporaryOrderNumberRepository.GetSingleRecord(ruleParameters);
        }

        public Currency GetDefaultCurrency()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apidefaultcurrency";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            return _currencyRepository.GetSingleRecord(ruleParameters);
        }

        public Currency GetCurrencyById(int currencyId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicurrencybyid";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("currencyid", Convert.ToString(currencyId));
            return _currencyRepository.GetSingleRecord(ruleParameters);
        }
        public StockItemMagento GetAllStockDetailsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportmagento";           
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            //ruleParameters.customParameters.Add("part_no", SearchText);
            //ruleParameters.customParameters.Add("product_name", SearchText);
            //ruleParameters.customParameters.Add("supplier", SearchText);
            //ruleParameters.customParameters.Add("categoryid", SearchText);
            //ruleParameters.customParameters.Add("typeid", SearchText);
            //ruleParameters.customParameters.Add("offset", offSet.ToString());
            //ruleParameters.customParameters.Add("rows", Rows.ToString());
            ruleParameters.customParameters.Add("source", Source);
            ruleParameters.customParameters.Add("lastsyncdate", LastSyncDate);

            POSLogger.Info("GetAllStockDetailsMagento offset: " + offSet.ToString());
            POSLogger.Info("GetAllStockDetailsMagento rows: " + Rows.ToString());
            POSLogger.Info("GetAllStockDetailsMagento lastsyncdate: " + LastSyncDate);

            var result = _stockItemRepositoryMagento.GetAllMultipleTable(ruleParameters);
            POSLogger.Info("GetAllStockDetailsMagento finished: " + (result.stockItemInfoforMagento.Any() ? result.stockItemInfoforMagento.Count.ToString(): "Error"));

            return result;
        }

        public StockItemMagento GetAllStockLevelDetailsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportmagentostocklevel";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            //ruleParameters.customParameters.Add("offset", offSet.ToString());
            //ruleParameters.customParameters.Add("rows", Rows.ToString());
            ruleParameters.customParameters.Add("source", Source);
            ruleParameters.customParameters.Add("lastsyncdate", LastSyncDate);

            POSLogger.Info("GetAllStockLevelDetailsMagento offset: " + offSet.ToString());
            POSLogger.Info("GetAllStockLevelDetailsMagento rows: " + Rows.ToString());
            POSLogger.Info("GetAllStockLevelDetailsMagento lastsyncdate: " + LastSyncDate);

            var result = _stockItemRepositoryMagento.GetAllMultipleTable(ruleParameters);
            POSLogger.Info("GetAllStockLevelDetailsMagento finished: " + (result.stockItemInfoforMagento.Any() ? result.stockItemInfoforMagento.Count.ToString() : "Error"));

            return result;
        }

        public List<StockDetailSimilarImage> GetSimilarProductInfo(string stockid)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "similarproductapi";
            
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

       
            ruleParameters.customParameters.Add("id", stockid);

            var result = _stockSimilarProductsRepository.GetAll(ruleParameters);
            return result;
        }
        
        public StockItemWooCommerce GetAllStockDetailsWooCommerce(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source,string LastSyncDate)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportwoocommerce";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("part_no", SearchText);
            ruleParameters.customParameters.Add("product_name", SearchText);
            ruleParameters.customParameters.Add("supplier", SearchText);
            ruleParameters.customParameters.Add("categoryid", SearchText);
            ruleParameters.customParameters.Add("typeid", SearchText);
            ruleParameters.customParameters.Add("barcode_no", SearchText);
            ruleParameters.customParameters.Add("description", SearchText);
            ruleParameters.customParameters.Add("source", Source);
            ruleParameters.customParameters.Add("lastsyncdate", LastSyncDate);
            var result = _stockItemRepositoryWooCommerce.GetAllMultipleTable(ruleParameters);
            return result;
        }

        public StockItemWooCommerce GetSelectedProductsWooCommerce(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source)
        {
            POSLogger.Info("GetSelectedProductsWooCommerce called");
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportproducts";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("productids", SearchText);           
            ruleParameters.customParameters.Add("source", Source);
            var result = _stockItemRepositoryWooCommerce.GetAllMultipleTable(ruleParameters);
            POSLogger.Info("GetSelectedProductsWooCommerce finished: " + (result.stockItemInfoforWooCommerce.Any() ? result.stockItemInfoforWooCommerce.Count.ToString() : "Error"));

            return result;
        }

        public void ResetStockExported(string Ids, string source)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "resetstockexport";           
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("table", Ids);
            ruleParameters.customParameters.Add("source", source);  
            _stockItemRepositoryMagento.Update(ruleParameters);            
        }

        public StockItemMagento GetStockDetailCountforMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source, string LastSyncDate)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportcount";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("part_no", SearchText);
            ruleParameters.customParameters.Add("product_name", SearchText);
            ruleParameters.customParameters.Add("supplier", SearchText);
            ruleParameters.customParameters.Add("categoryid", SearchText);
            ruleParameters.customParameters.Add("typeid", SearchText);
            ruleParameters.customParameters.Add("barcode_no", SearchText);
            ruleParameters.customParameters.Add("description", SearchText);
            ruleParameters.customParameters.Add("source", Source);
            ruleParameters.customParameters.Add("lastsyncdate", LastSyncDate);
            var result = _stockItemRepositoryMagento.GetAllMultipleTable(ruleParameters);
            return result;
        }
        public GeneralRuleResult UpdateAllSpecificPrice(string targetDate)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "updatespecialprice";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("targetDate", targetDate);

            var result = _stockSpecialPriceUpdateRepository.GetSingleRecord(ruleParameters);
            return result;
        }
        public StockItemImageMagento GetAllStockImageDetailsMagento(string LastSyncDate)
        {
            try
            {
            POSLogger.Info("GetAllStockImageDetailsMagento called");
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportimages";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();

            ruleParameters.customParameters.Add("lastsyncdate", LastSyncDate);
            var result = _stockItemImageRepositoryMagento.GetAllMultipleTable(ruleParameters);
            POSLogger.Info("GetAllStockImageDetailsMagento finished: " + (result.stockItemImageInfoforMagento.Any() ? result.stockItemImageInfoforMagento.Count.ToString() : "Error"));
            return result;
            }
            catch (Exception ex)
            {
                POSLogger.Info("GetAllStockImageDetailsMagento finished" + ex.ToString());
            }
            return null;
        }   
        public StockItemEbay GetSelectedProductsEbay(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportebayproducts";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("productids", SearchText);
            ruleParameters.customParameters.Add("source", Source);
            var result = _stockItemRepositoryEbay.GetAllMultipleTable(ruleParameters);
            return result;
        }

        public StockItemMagento GetSelectedProductsMagento(string SearchText, int offSet, int Rows, string sortBy, string sortDirection, string Source)
        {
            POSLogger.Info("GetSelectedProductsMagento called");
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisforexportmagentoproducts";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("productids", SearchText);
            ruleParameters.customParameters.Add("source", Source);
            var result = _stockItemRepositoryMagento.GetAllMultipleTable(ruleParameters);
            POSLogger.Info("GetSelectedProductsMagento finished: " + (result.stockItemInfoforMagento.Any() ? result.stockItemInfoforMagento.Count.ToString() : "Error"));

            return result;
        }

        public string SetStockChannelDataId(string tabledata, string Source)
        {
            POSLogger.Info("SetStockChannelDataId called");
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "setstockchannelid";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("table", tabledata);
            ruleParameters.customParameters.Add("source", Source);
            var result = _stockItemRepositoryGeneraral.GetSingleRecord(ruleParameters);
            POSLogger.Info("SetStockChannelDataId finished: " + result.status);
            return result.status;
        }

    }
}
