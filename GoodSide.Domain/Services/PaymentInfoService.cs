﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
namespace GoodSide.Domain.Services
{
    public interface IPaymentInfoService
    {
        CreaditNoteSearchItem GetAllCreditNote(string customerid, string crnno, int offSet, int Rows, string sortBy, string sortDirection);
        CreaditNoteSearchItem UpdateCreditnote(CreaditNoteSearchItemInfo itemInfo, OrderMaster or);
        VoucherSearchItem GetAllVoucher(string SearchText, int offSet, int Rows, string sortBy, string sortDirection);
        string GetLoggedInUserEntityId();
    }

    public class PaymentInfoService : IPaymentInfoService
    {
        private IRepository<CreaditNoteSearchItem> _creditNoteRepository;
        private IRepository<VoucherSearchItem> _voucherRepository;
        private IRepository<KeyValueItem> _keyValueRepository;
        public PaymentInfoService(IRepository<CreaditNoteSearchItem> creditNoteRepository, IRepository<VoucherSearchItem> voucherRepository, IRepository<KeyValueItem> keyValueRepository)
        {

            _creditNoteRepository = creditNoteRepository;
            _voucherRepository = voucherRepository;
            _keyValueRepository = keyValueRepository;
        }

        public VoucherSearchItem GetAllVoucher(string SearchText, int offSet, int Rows, string sortBy, string sortDirection)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "voucher";// "customerapi";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("filterstring", SearchText);
            //  ruleParameters.customParameters.Add("searchtype", "2");
            var result = _voucherRepository.GetAllMultipleTable(ruleParameters);

            return result;
        }

        public CreaditNoteSearchItem GetAllCreditNote(string customerid,string crnno, int offSet, int Rows, string sortBy, string sortDirection)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "creditnote";// "customerapi";
            ruleParameters.Offset = offSet;
            ruleParameters.Rows = Rows;
            ruleParameters.SortOrder = sortDirection.ToLower() == "desc" ? SortOrder.DESC : SortOrder.ASC;
            ruleParameters.SortBy = sortBy;
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("customerid", customerid);
            ruleParameters.customParameters.Add("crnno", crnno);
            var result = _creditNoteRepository.GetAllMultipleTable(ruleParameters);

            return result;
        } 
        public CreaditNoteSearchItem UpdateCreditnote(CreaditNoteSearchItemInfo itemInfo,OrderMaster or)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Set;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "creditnote";// "customerapi";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("crorderid", itemInfo.ono);
            ruleParameters.customParameters.Add("crnno", itemInfo.cnno);
            ruleParameters.customParameters.Add("customerid", or.customerid);
            ruleParameters.customParameters.Add("amount", itemInfo.amount.ToString());
            ruleParameters.customParameters.Add("amountpaid", itemInfo.amountpaid.ToString());
            ruleParameters.customParameters.Add("id", itemInfo.id);
            // _creditNoteRepository.Update(ruleParameters);
            CreaditNoteSearchItem objcustomerItem = _creditNoteRepository.GetSingleRecord(ruleParameters);
            return objcustomerItem;

        }
        public string GetLoggedInUserEntityId()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "entityidbyloggedinuser";// "customerapi";           
            var EntityId = _keyValueRepository.GetSingleRecord(ruleParameters);
            return EntityId.id;

        }

    }
}
