﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IKeyValueItemService
    {
        List<KeyValueItem> GetSuppliers();
        List<KeyValueItem> GetCategories();
        List<KeyValueItem> GetStockTypes();
        List<KeyValueItem> GetCurrencies();

        List<KeyValueItem> GetAllCountryList();
        List<KeyValueItem> GetAllCountyList();
        List<KeyValueItem> GetAllSalesPerson();
        List<KeyValueItem> GetAboutUsList();
        List<KeyValueItem> GetFinancialList();
        List<KeyValueItem> GetDeliveryTypeList();
        List<KeyValueItem> GetDestinationLocation();
        List<KeyValueItem> GetPaymentTypes();
        List<KeyValueItem> GetProductType(string categoryid);
    }

    public class KeyValueItemService : IKeyValueItemService
    {
        private IRepository<KeyValueItem> _keyValueRepository;

        public KeyValueItemService(IRepository<KeyValueItem> keyValueRepository)
        {
            _keyValueRepository = keyValueRepository;
        }       

        public List<KeyValueItem> GetSuppliers()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apisupplier";
            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }

        public List<KeyValueItem> GetCategories()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicategory";
            var result = _keyValueRepository.GetAll(ruleParameters);
            return result;
        }
        public List<KeyValueItem> GetProductType(string categoryid)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apitype";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("categoryid", categoryid);
            var result = _keyValueRepository.GetAll(ruleParameters);
            return result;
        }


        public List<KeyValueItem> GetStockTypes()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apistocktype";
            var result = _keyValueRepository.GetAll(ruleParameters);
            return result;
        }

        public List<KeyValueItem> GetCurrencies()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicurrency";
            var result = _keyValueRepository.GetAll(ruleParameters);
            return result;
        }
        public List<KeyValueItem> GetAllCountryList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicountry";// "customerapi";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }

        public List<KeyValueItem> GetAllCountyList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apicounty";// "customerapi";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }


        public List<KeyValueItem> GetAllSalesPerson()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apisalesperson";// "customerapi";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }
        public List<KeyValueItem> GetAboutUsList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apiaboutus";// "customerapi";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }
        public List<KeyValueItem> GetFinancialList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apifiancial";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }
        public List<KeyValueItem> GetDeliveryTypeList()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apideliverytype"; 

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }
        public List<KeyValueItem> GetDestinationLocation()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apilocation";

            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }

        public List<KeyValueItem> GetPaymentTypes()
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "apipaytypes";
            var result = _keyValueRepository.GetAll(ruleParameters);

            return result;
        }
    }
}
