﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Services
{
    public interface IStockDetailService
    {        
        StockDetailInfo GetStockDetailedInfo(int stockId);
        StockDetailImagesInfo GetStockImageInfo(int stockId);
    }

    public class StockDetailService : IStockDetailService
    {
        private IRepository<StockDetailInfo> _stockDetailRepository;
        private IRepository<StockDetailImagesInfo> _stockImageRepository;

        public StockDetailService(IRepository<StockDetailInfo> stockDetailRepository, IRepository<StockDetailImagesInfo> stockImageRepository)
        {
            _stockDetailRepository = stockDetailRepository;
            _stockImageRepository = stockImageRepository;
        }

        public StockDetailInfo GetStockDetailedInfo(int stockId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Edit;
            ruleParameters.RuleFile = RuleFileName.STK;
            ruleParameters.RuleName = "lisapi";            
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", stockId.ToString());
            return _stockDetailRepository.GetAllMultipleTable(ruleParameters);
        }
        public StockDetailImagesInfo GetStockImageInfo(int stockId)
        {
            RuleParameters ruleParameters = new RuleParameters();
            ruleParameters.Action = RuleAction.Get;
            ruleParameters.RuleFile = RuleFileName.POS;
            ruleParameters.RuleName = "lisapi";
            ruleParameters.customParameters = new System.Collections.Specialized.NameValueCollection();
            ruleParameters.customParameters.Add("id", stockId.ToString());
            return _stockImageRepository.GetAllMultipleTable(ruleParameters);
        }
    }
}
