﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Helpers
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldMappingAttribute : Attribute
    {
        // Private fields.
        private string _dbFieldName;     
        
        public FieldMappingAttribute(string fieldName)
        {
            _dbFieldName = fieldName;
        }

        // Define Name property.
        // This is a read-only attribute.
        public virtual string DBFieldName
        {
            get { return _dbFieldName; }
        }      
    }
}
