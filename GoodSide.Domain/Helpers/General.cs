﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.Domain.Helpers
{
    public static class General
    {
        public static String AsText(this CoreParameters paramters)
        {
            var parameterName = string.Empty;   
            switch (paramters)
            {
                case CoreParameters._SORT:
                    parameterName= "sort";
                    break;
                case CoreParameters._ORDER:
                    parameterName=  "order";
                    break;
                case CoreParameters._OFFSET:
                    parameterName = "offset";
                    break;
                case CoreParameters._ROWS:
                    parameterName = "rows";
                    break;
                case CoreParameters._RULENAME:
                    parameterName = "subject";
                    break;
                case CoreParameters._RULEFILE:
                    parameterName = "application";
                    break;
                case CoreParameters._ACTION:
                    parameterName = "action";
                    break;
                case CoreParameters._FILTER:
                    parameterName = "filter";
                    break;
                default:
                    break;
            }
            return parameterName;
        }
    }
}
