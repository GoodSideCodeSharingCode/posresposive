﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Domain.Entities;
using System.Collections.Specialized;

namespace GoodSide.Domain.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        List<T> GetAll(RuleParameters ruleParameters);
        T GetAllMultipleTable(RuleParameters ruleParameters);        
        T GetSingleRecord(RuleParameters ruleParameters);
        void Create(RuleParameters ruleParameters);
        string Create_API(RuleParameters ruleParameters);
        void Update(RuleParameters ruleParameters);
        void Delete(RuleParameters ruleParameters);
        List<T> GetCompanies();
        T GetLoggedInUser(string Password, int CompanyId);
        string SendOrderEmail(string orderNo, string userid, string templateString, string subjectCompanyName, string emailFrom, string toemail, bool enableSSL, string smtpAddress, int portNumber, string smtpUsername, string smtpPassword);
        string ChkDefaultLoc(RuleParameters ruleParameters);
        string GetDefaultLocationForBarCode(RuleParameters ruleParameters);
        
    }
}
