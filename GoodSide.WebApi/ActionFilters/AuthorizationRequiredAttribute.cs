﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net;
using System.Net.Http;
using GoodSide.Domain.Services;
using GoodSide.Domain.Entities;
using GoodSide.WebApi.Helpers;

namespace GoodSide.WebApi.ActionFilters
{
    public class AuthorizationRequiredAttribute : ActionFilterAttribute
    {
        private const string TokenHeader = "Authorization";

        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            //  Get API key provider
            var provider = filterContext.ControllerContext.Configuration
                .DependencyResolver.GetService(typeof(ITokenServices)) as ITokenServices;

            if (filterContext.Request.Headers.Contains(TokenHeader))
            {
                var tokenValue = filterContext.Request.Headers.GetValues(TokenHeader).First();

                bool isValidToken = false;

                Token authToken = provider.ValidateToken(tokenValue, ref isValidToken);

                // Validate Token
                if (provider != null && !isValidToken)
                {
                    var responseMessage = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Invalid Request" };
                    filterContext.Response = responseMessage;
                }

                addUserDetailsToSession(authToken);
            }
            else
            {
                filterContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Add user to current session for navigation
        /// </summary>
        /// <param name="user">Current User</param>
        private void addUserDetailsToSession(Token authToken)
        {
            if (authToken != null)
            {
                var session = System.Web.HttpContext.Current.Session;
                session.Add(SessionConstants.userid.ToString(), authToken.UserId);
                session.Add(SessionConstants.companyid.ToString(), authToken.CompanyId);
                session.Add(SessionConstants.username.ToString(), authToken.UserName);
                session.Add(SessionConstants.userrole.ToString(), authToken.UserRole);
                session.Add(SessionConstants.passwordOnly.ToString(), authToken.PasswordOnly);
                session.Add(SessionConstants.storename.ToString(), authToken.StoreName);
            }
        }
    }
}