﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodSide.WebApi.Helpers
{
    public enum SessionConstants
    {
        companyid,      // please Never change it. Use internally for execute Rules 
        userid,         // please Never change it. Use internally for execute Rules
        username,       // please Never change it. Use internally for execute Rules
        userrole,       // please Never change it. Use internally for execute Rules
        storename,      // please Never change it. Use internally for execute Rules
        passwordOnly,   // please Never change it. Use internally for execute Rules
        customerId,      // stored selected customer for pos order,
        POSSelectedProducts,// Use to add products to session at time of order place.
        POSOrderMaster,//use to store ordermaster to session, it will be use as customer select,payment and other screens
        VatRegNo
    }

    public class General
    {
        public static string _SORT = "sort", _ORDER = "order", _OFFSET = "offset", _ROWS = "rows", _SUBJECT = "subject", _APPLICATION = "application", _ACTION = "action";
    }

    public static class Utility
    {
        public static string GetDimentionString(string item_length, string item_width, string item_height)
        {
            //Format :- Length X Width X Hight CM
            if (!string.IsNullOrEmpty(item_length) || !string.IsNullOrEmpty(item_width) || !string.IsNullOrEmpty(item_height))
                return string.Format("W {0}cm  D {1}cm  H {2}cm", item_width , item_length,  item_height);
            else
                return null;
        }
    }
}