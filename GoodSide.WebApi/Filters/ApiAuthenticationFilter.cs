﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;
using GoodSide.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;

namespace GoodSide.WebApi.Filters
{
    public class ApiAuthenticationFilter : GenericAuthenticationFilter
    {
        /// <summary>
        /// Default Authentication Constructor
        /// </summary>
        public ApiAuthenticationFilter()
        {
        }

        /// <summary>
        /// AuthenticationFilter constructor with isActive parameter
        /// </summary>
        /// <param name="isActive"></param>
        public ApiAuthenticationFilter(bool isActive)
            : base(isActive)
        {
        }

        /// <summary>
        /// Protected overriden method for authorizing user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        protected override bool OnAuthorizeUser(int storeID, string password, HttpActionContext actionContext)
        {
            var provider = actionContext.ControllerContext.Configuration
                               .DependencyResolver.GetService(typeof(IAccountUserService)) as IAccountUserService;
            var _actCompanyService = actionContext.ControllerContext.Configuration
                              .DependencyResolver.GetService(typeof(IAccountCompanyService)) as IAccountCompanyService;
            if (provider != null)
            {
                var loggedinUser = provider.GetLoggedInUser(password, storeID);
                if (loggedinUser != null)
                {
                    // Get companies
                    IList<AccountCompany> lstCompanies = _actCompanyService.GetCompanies();

                    //Get Selected Company Name 
                    string CompanyName = (lstCompanies.Where(item => item.CompanyId == storeID)).Select(c => c.Name).SingleOrDefault();

                    var basicAuthenticationIdentity = Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                    if (basicAuthenticationIdentity != null)
                    {
                        basicAuthenticationIdentity.StoreId = storeID;
                        addUserDetailsToSession(loggedinUser, storeID, CompanyName);
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Add user to current session for navigation
        /// </summary>
        /// <param name="user">Current User</param>
        private void addUserDetailsToSession(AccountUser user, int CompanyId, string StoreName)
        {
            if (user != null)
            {
                var session = System.Web.HttpContext.Current.Session;
                session.Add(SessionConstants.userid.ToString(), user.UserId);
                session.Add(SessionConstants.companyid.ToString(), CompanyId);
                session.Add(SessionConstants.username.ToString(), user.UserName);
                session.Add(SessionConstants.userrole.ToString(), user.UserRole);
                session.Add(SessionConstants.passwordOnly.ToString(), true);
                session.Add(SessionConstants.storename.ToString(), StoreName);
            }
        }
    }
}