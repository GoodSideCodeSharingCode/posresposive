﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using GoodSide.WebApi.Helpers;
using System.Net.Http.Headers;
using GoodSide.WebApi.Filters;
using System.Web.Http.Cors;
using System.Net.Http;

namespace GoodSide.WebApi
{
    //public class CorsPolicyFactory : ICorsPolicyProviderFactory
    //{
    //    ICorsPolicyProvider _provider = new MyCorsPolicyProvider();

    //    public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
    //    {
    //        return _provider;
    //    }
    //} 

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();


            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{id}",
                 defaults: new { id = RouteParameter.Optional }
             );
            // add UoW action filter globally
            config.Filters.Add(new UnitOfWorkActionFilter());
            //config.Filters.Add(new ApiAuthenticationFilter());

            //config.SetCorsPolicyProviderFactory(new CorsPolicyFactory());
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors();

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            
        }
    }
}
