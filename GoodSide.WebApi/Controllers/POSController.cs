﻿using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Entities.APIEntities;
using GoodSide.Domain.Helpers;
using GoodSide.Domain.Services;
using GoodSide.WebApi.ActionFilters;
using GoodSide.WebApi.Helpers;
using Magento.RestApi;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using MagentoV2.RestApi;
using Newtonsoft.Json.Linq;
using MagentoV2.RestApi.Models;

namespace GoodSide.WebApi.Controllers
{

    /// <summary>
    /// Main POs Controller method
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [AuthorizationRequired]
    //[RoutePrefix("pos")]       
    public class POSController : ApiController
    {
        private static ApiContext apiContext = null;
        private ItemType fetchedItem;
        private IProductService _productSearchService;
        private IStockItemService _stockItemService;
        private IPOSService _posService;
        public string FtpURL { get; set; }

        public string FtpFolderName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        /// <summary>
        /// test
        /// </summary>
        /// <param name="productSearchService">test</param>
        /// <param name="stockItemService">test</param>
        /// <param name="posService">test</param>
        public POSController(IProductService productSearchService, IStockItemService stockItemService, IPOSService posService)
        {
            _productSearchService = productSearchService;
            _stockItemService = stockItemService;
            _posService = posService;
        }

        /// <summary>
        /// test1
        /// </summary>
        /// <param name="offset">test</param>
        /// <param name="pageSize">test</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/pos/GetStockDetail")]
        public GoodSide.Domain.Entities.StockItem GetStockDetail(int offset, int pageSize)
        {
            string sortDirection = "asc";
            var lstProperties = typeof(StockItemInfo).GetProperties().ToList();
            var result = _stockItemService.GetAll("%25%25", offset, pageSize, lstProperties[0].Name, sortDirection);
            return result;
        }

        [HttpGet]
        [Route("api/pos/GetStockDetailforMagento")]
        public StockItemMagento GetStockDetailforMagento(int offset, int pageSize, string Lastsyncdate)
        {
            string sortDirection = "asc";
            var lstProperties = typeof(StockItemInfoforMagento).GetProperties().ToList();
            var result = _posService.GetAllStockDetailsMagento("%25%25", offset, pageSize, lstProperties[0].Name, sortDirection, "Magento", Lastsyncdate);
            return result;
        }

        [HttpGet]
        [Route("api/pos/GetStockLevelDetailforMagento")]
        public StockItemMagento GetStockLevelDetailforMagento(int offset, int pageSize, string Lastsyncdate)
        {
            string sortDirection = "asc";
            var lstProperties = typeof(StockItemInfoforMagento).GetProperties().ToList();
            var result = _posService.GetAllStockLevelDetailsMagento("%25%25", offset, pageSize, lstProperties[0].Name, sortDirection, "Magento", Lastsyncdate);
            return result;
        }


        [HttpGet]
        [Route("api/pos/GetStockImageDetailforMagento")]
        public StockItemImageMagento GetStockImageDetailforMagento(string Lastsyncdate)
        {
            string sortDirection = "asc";
            var result = _posService.GetAllStockImageDetailsMagento(Lastsyncdate);
            return result;
        }

        [HttpGet]
        [Route("api/pos/UpdateSpecialPriceForWebOutput")]
        public GeneralRuleResult UpdateSpecialPriceForWebOutput(string targetDate)
        {
            var result = _posService.UpdateAllSpecificPrice(targetDate);
            return result;
        }

        [HttpGet]
        [Route("api/pos/GetStockDetailCountforMagento")]
        public StockItemMagento GetStockDetailCountforMagento(int offset, int pageSize, string Lastsyncdate)
        {
            string sortDirection = "asc";
            var lstProperties = typeof(StockItemInfoforMagento).GetProperties().ToList();
            var result = _posService.GetStockDetailCountforMagento("%25%25", offset, pageSize, lstProperties[0].Name, sortDirection, "Magento", Lastsyncdate);
            return result;
        }

        [HttpGet]
        [Route("api/pos/GetStockDetailforWooCommerce")]
        public StockItemWooCommerce GetStockDetailforWooCommerce(int offset, int pageSize, string Lastsyncdate)
        {
            string sortDirection = "asc";
            var lstProperties = typeof(StockItemforWooCommerce).GetProperties().ToList();
            var result = _posService.GetAllStockDetailsWooCommerce("%25%25", offset, pageSize, lstProperties[0].Name, sortDirection, "WooCommerce", Lastsyncdate);
            return result;
        }

        /// <summary>
        /// test
        /// </summary>
        /// <param name="stockInfo">test</param>
        /// <returns>test</returns>
        [HttpPost]
        [Route("api/pos/AddStockDetail")]
        public bool AddStockDetail(StockManagementInfo stockInfo)
        {
            var result = _stockItemService.Create(stockInfo);
            return result;
        }

        [HttpPost]
        [Route("api/pos/ResetStockExported")]
        public void ResetStockExported(string Ids, string source)
        {
            _posService.ResetStockExported(Ids, source);
        }

        [HttpPost]
        [Route("api/pos/SetStockChannelDataId")]
        public string SetStockChannelDataId(string Ids, string source)
        {
           return _posService.SetStockChannelDataId(Ids, source);
        }

        [HttpPost]
        [Route("api/pos/CreateOrder")]
        public Result CreateOrder(GoodSide.Domain.Entities.APIEntities.Order ordobj, string source)
        {
            Result apiResult = new Result();
            try
            {
                //string contents = System.IO.File.ReadAllText(@"D:\Projects\Goodside\GoodSide_NewFramework\GoodSide.APITest\DummyData\OrderJson.txt");

                //var orderobj = JsonConvert.DeserializeObject<Order>(contents);

                StringBuilder errorDetail = new StringBuilder(500);
                if (ordobj != null)
                {
                    if (validateOrderData(ordobj, out errorDetail))
                    {
                        if (validateOrder(ordobj, source, out errorDetail))
                        {
                            var result = _stockItemService.Create(ordobj, source);
                            apiResult.status = true;
                            apiResult.Id = result;
                            apiResult.message = "Success : Generated OrderID " + result;
                        }
                        else
                        {
                            apiResult.status = false;
                            apiResult.Id = "0";
                            apiResult.message = errorDetail.ToString();
                        }
                    }
                    else
                    {
                        apiResult.status = false;
                        apiResult.Id = "0";
                        apiResult.message = errorDetail.ToString();
                    }
                }
                else
                {
                    apiResult.status = false;
                    apiResult.Id = "0";
                    apiResult.message = string.IsNullOrEmpty(errorDetail.ToString()) ? "Error : Not able to parse the request data" : errorDetail.ToString();
                }
            }
            catch (Exception ex)
            {
                apiResult.status = false;
                apiResult.Id = "0";
                apiResult.message = ex.Message;
                //return "Error :" + ex.Message;
            }
            return apiResult;
        }

        private bool validateOrderData(GoodSide.Domain.Entities.APIEntities.Order ordobj, out StringBuilder errorDetail)
        {
            var isSuccess = true;
            errorDetail = new StringBuilder();
            if (ordobj.billingAddress == null)
            {
                errorDetail.Append("Please provide Valid Json Data For Billing Address  " + Environment.NewLine);
                isSuccess = false;
            }
            if (ordobj.shippingAddress == null)
            {
                errorDetail.Append("Please provide Valid Json Data For Shipping Address  " + Environment.NewLine);
                isSuccess = false;
            }
            if (ordobj.PaymentInfo == null)
            {
                errorDetail.Append("Payment Info is a list of object ,Please provide Valid Json Data For Payment Info" + Environment.NewLine);
                isSuccess = false;
            }
            if (ordobj.orderitem == null)
            {
                errorDetail.Append("OrderItem is a list of object ,Please provide Valid Json Data For Order Items" + Environment.NewLine);
                isSuccess = false;
            }

            return isSuccess;
        }

        private bool validateOrder(GoodSide.Domain.Entities.APIEntities.Order ordobj, string source, out StringBuilder errorDetail)
        {
            var isSuccess = true;
            errorDetail = new StringBuilder();

            string[] Sourcelist = { "woocommerce", "magento" };
            if (!(Array.IndexOf(Sourcelist, source.ToLower()) >= 0))
            {
                errorDetail.Append("Please provide Valid Source, Like  " + string.Join(",", Sourcelist) + Environment.NewLine);
                isSuccess = false;
            }
            if (ordobj.Is_Active.ToString() == string.Empty)
            {
                errorDetail.Append("Is_Active cannot be Empty,Please Provide Value 4 as Its Always Quotes initially " + Environment.NewLine);
                isSuccess = false;
            }
            if (ordobj.Is_Active.ToString() != "4")
            {
                errorDetail.Append("Is_Active cannot be set value other than 4 as its quotes initially" + Environment.NewLine);
                isSuccess = false;
            }
            if (string.IsNullOrEmpty(ordobj.Grand_Total.ToString()))
            {
                errorDetail.Append("Grand Total Cannot be null or Empty" + Environment.NewLine);
                isSuccess = false;
            }
            string[] Curruncies = { "EUR", "GBP", "USD" };
            if (string.IsNullOrEmpty(ordobj.Order_Currency_Code))
            {
                errorDetail.Append("Currency Code Cannot be null or Empty, Plese provide value Like  " + string.Join(",", Curruncies) + Environment.NewLine);
                isSuccess = false;
            }
            if (!(Array.IndexOf(Curruncies, ordobj.Order_Currency_Code) >= 0))
            {
                errorDetail.Append("Please provide Valid currency Code, Like  " + string.Join(",", Curruncies) + Environment.NewLine);
                isSuccess = false;
            }
            if (string.IsNullOrEmpty(ordobj.customer.Customer_Id.ToString()))
            {
                errorDetail.Append("CustomerId Cannot be null or Empty" + Environment.NewLine);
                isSuccess = false;
            }
            foreach (var item in ordobj.orderitem)
            {
                if (!string.IsNullOrEmpty(item.SKU))
                {
                    var CheckSku = _stockItemService.CheckSku(item.SKU);
                    if (CheckSku == null)
                    {
                        errorDetail.Append("SKU Or Part_No Not Found" + Environment.NewLine);
                        isSuccess = false;
                    }
                }

                if (string.IsNullOrEmpty(item.SKU))
                {
                    errorDetail.Append("SKU Cannot be null or Empty" + Environment.NewLine);
                    isSuccess = false;
                }
                if (string.IsNullOrEmpty(item.qty_ordered.ToString()))
                {
                    errorDetail.Append("Qty Cannot be null or Empty" + Environment.NewLine);
                    isSuccess = false;
                }
                if (string.IsNullOrEmpty(item.price.ToString()))
                {
                    errorDetail.Append("Price Cannot be null or Empty" + Environment.NewLine);
                    isSuccess = false;
                }
                if (string.IsNullOrEmpty(item.row_total.ToString()))
                {
                    errorDetail.Append("row_total Cannot be null or Empty" + Environment.NewLine);
                    isSuccess = false;
                }
            }
            string[] Paymenttypes = { " 1  - Visa", " 2  - Mastercard", " 3  - Laser", " 5  - Cash", " 16  - Paypal", };
            string[] PaymentMethod = { "1", "2", "3", "5", "16" };
            foreach (var item in ordobj.PaymentInfo)
            {
                if (string.IsNullOrEmpty(item.method) || item.method == "0")
                {
                    errorDetail.Append("Method value is Payment types , value cannot be null or Empty or Zero  " + Environment.NewLine);
                    isSuccess = false;
                }
                if (!(Array.IndexOf(PaymentMethod, item.method) >= 0))
                {
                    errorDetail.Append("Provide Proper Payment Method value, Like " + string.Join(",", Paymenttypes) + Environment.NewLine);
                    isSuccess = false;
                }
            }

            return isSuccess;

        }

        [HttpPost]
        [Route("api/pos/AddProductToChannel")]
        public async Task<string> AddProductToChannel(string stockIds, string channelType)
        {
            string result = string.Empty;

            String[] Channels = channelType.Split(',');

            foreach (string channel in Channels)
            {

                try
                {
                    switch (channel.ToLower())
                    {
                        case "magentov1":
                            AddProductToMagentoV1(stockIds);
                            break;
                        case "magentov2":
                            AddProductToMagentoV2(stockIds);
                            break;
                        case "woocommerce":
                            result = AddProductToWooCommerce(stockIds);
                            break;
                        case "ebay":
                            result = AddProductToEbay(stockIds);
                            break;
                        default:
                            break;
                    }

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return "Success";
        }

        private string AddProductToMagentoV1(string productIds)
        {
            try
            {
                POSLogger.Info("AddProductToMagentoV1 Start");

                string[] listOfIds = productIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];

                string magentoURL = System.Configuration.ConfigurationManager.AppSettings["MagentoURL"];
                string magentoConsumerKey = System.Configuration.ConfigurationManager.AppSettings["MagentoConsumerKey"];
                string magentoConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["MagentoConsumerSecret"];
                string magentoAdminURL = System.Configuration.ConfigurationManager.AppSettings["MagentoAdminURL"];
                string magentoAdminUserName = System.Configuration.ConfigurationManager.AppSettings["MagentoAdminUserName"];
                string magentoAdminPassword = System.Configuration.ConfigurationManager.AppSettings["MagentoAdminPassword"];

                POSLogger.Info("Init magentoClient Object");

                var magentoClient = new Magento.RestApi.MagentoApi().SetCustomAdminUrlPart(magentoAdminURL).Initialize(magentoURL, magentoConsumerKey, magentoConsumerSecret)
                .AuthenticateAdmin(magentoAdminUserName, magentoAdminPassword);

                string sortDirection = "asc";
                var lstProperties = typeof(StockItemInfoforMagento).GetProperties().ToList();
                var lstOfProducts = _posService.GetSelectedProductsMagento(productIds, 0, 55555, lstProperties[1].Name, sortDirection, "MagentoV1");

                POSLogger.Info("AddProductToMagentoV1 Get Stock" + lstOfProducts.stockItemInfoforMagento.Count);

                StringBuilder lstOfFailedProductAdd = new StringBuilder();
                Dictionary<string, string> SkuImagePath = new Dictionary<string, string>();

                //Loop Each Item
                foreach (var productInfo in lstOfProducts.stockItemInfoforMagento)
                {
                    try
                    {

                        Magento.RestApi.MagentoApiResponse<Magento.RestApi.Models.Product> resultExistingProduct = magentoClient.GetProductBySku(productInfo.part_no);

                        POSLogger.Info(String.Format("Search with SKU:{0} and Result:{1}", productInfo.part_no, (resultExistingProduct.Result == null ? "false" : "true")));

                        var product = new Magento.RestApi.Models.Product();

                        if (resultExistingProduct.Result != null)
                        {
                            product = resultExistingProduct.Result;
                        }
                        // create product with minimal required fields

                        product.name = productInfo.name;
                        product.description = productInfo.description;
                        product.short_description = productInfo.short_description;

                        if (!string.IsNullOrEmpty(productInfo.price))
                            product.price = Convert.ToDouble(productInfo.price);

                        product.sku = productInfo.part_no;

                        if (!string.IsNullOrEmpty(productInfo.visibility))
                        {
                            switch (productInfo.visibility.ToLower())
                            {
                                case "catalogue, search":
                                    product.visibility = Magento.RestApi.Models.ProductVisibility.CatalogSearch;
                                    break;
                                case "catalogue only":
                                    product.visibility = Magento.RestApi.Models.ProductVisibility.Catalog;
                                    break;
                                case "search only":
                                    product.visibility = Magento.RestApi.Models.ProductVisibility.Search;
                                    break;
                                case "nowhere":
                                    product.visibility = Magento.RestApi.Models.ProductVisibility.NotVisibleIndividually;
                                    break;
                                default:
                                    break;
                            }
                        }

                        product.status = Magento.RestApi.Models.ProductStatus.Enabled;
                        //TODO product.weight = productInfo,
                        product.tax_class_id = 2;
                        product.type_id = productInfo.product_type;
                        product.attribute_set_id = 4;

                        product.meta_description = productInfo.meta_description;
                        product.meta_keyword = productInfo.meta_keywords;
                        product.meta_title = productInfo.meta_title;

                        if (!string.IsNullOrEmpty(productInfo.special_price))
                            product.special_price = Convert.ToDouble(productInfo.special_price);

                        if (!string.IsNullOrEmpty(productInfo.special_price) && (!string.IsNullOrEmpty(productInfo.special_price) && productInfo.special_price_from_date != DateTime.MinValue))
                            product.special_from_date = productInfo.special_price_from_date;

                        if (!string.IsNullOrEmpty(productInfo.special_price) && productInfo.special_price_from_date != DateTime.MinValue)
                            product.special_from_date = productInfo.special_price_from_date;

                        var stock_data = new Magento.RestApi.Models.StockData();
                        if (resultExistingProduct.Result != null && resultExistingProduct.Result.stock_data != null)
                        {
                            stock_data = resultExistingProduct.Result.stock_data;
                        }

                        stock_data.is_in_stock = (productInfo.is_in_stock == "1" ? true : false);
                        stock_data.is_qty_decimal = false;
                        stock_data.manage_stock = true;
                        stock_data.qty = (string.IsNullOrEmpty(productInfo.qty) ? 0 : (Convert.ToDouble(productInfo.qty) > 0 ? Convert.ToDouble(productInfo.qty) : 0));

                        if (resultExistingProduct.Result != null)
                        {
                            var updatedProduct = magentoClient.UpdateProduct(product);
                            POSLogger.Info("Updated Product");
                        }
                        else
                        {
                            // Act
                            var newProduct = magentoClient.CreateNewProduct(product).Result;
                            Magento.RestApi.MagentoApiResponse<Magento.RestApi.Models.Product> returned = magentoClient.GetProductBySku(productInfo.part_no);
                            if (returned.Result != null)
                            {
                                //Created.
                                POSLogger.Info("Product Created");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        POSLogger.Info("Error " + ex.ToString());
                    }
                    //For Loop End
                }

                if (lstOfFailedProductAdd.ToString().Length > 0)
                {
                    ResetStockExported(lstOfFailedProductAdd.ToString().TrimEnd('|'), "MagentoV1");
                }

                //ftpimageUpload(SkuImagePath);
                return "success";
            }
            catch (Exception ex)
            {
                POSLogger.Info("Error " + ex.ToString());
                return ex.Message;
            }

        }

        private string AddProductToMagentoV2(string productIds)
        {
            try
            {
                POSLogger.Info("AddProductToMagentoV1 Start");

                string[] listOfIds = productIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];

                string magentoURL = System.Configuration.ConfigurationManager.AppSettings["MagentoV2URL"];     
                string magentoAdminUserName = System.Configuration.ConfigurationManager.AppSettings["MagentoV2AdminUserName"];
                string magentoAdminPassword = System.Configuration.ConfigurationManager.AppSettings["MagentoV2AdminPassword"];

                POSLogger.Info("Init magentoClient Object");

                var magentoClient = new MagentoV2.RestApi.MagentoApi().Initialize(magentoURL, magentoAdminUserName, magentoAdminPassword);
                magentoClient.Authenticate();

                string sortDirection = "asc";
                var lstProperties = typeof(StockItemInfoforMagento).GetProperties().ToList();
                var lstOfProducts = _posService.GetSelectedProductsMagento(productIds, 0, 55555, lstProperties[1].Name, sortDirection, "MagentoV2");

                POSLogger.Info("AddProductToMagentoV2 Get Stock" + lstOfProducts.stockItemInfoforMagento.Count);

                StringBuilder lstOfFailedProductAdd = new StringBuilder();
                Dictionary<string, string> SkuImagePath = new Dictionary<string, string>();

                var maincategoriesList = magentoClient.GetCategoriesList(null, null);

                //Loop Each Item
                foreach (var productInfo in lstOfProducts.stockItemInfoforMagento)
                {
                    try
                    {

                        var resultExistingProduct = magentoClient.GetProductBySku(productInfo.part_no);

                        POSLogger.Info(String.Format("Search with SKU:{0} and Result:{1}", productInfo.part_no, (resultExistingProduct.Result == null ? "false" : "true")));

                        if (!resultExistingProduct.HasErrors && resultExistingProduct.Result != null)
                        {
                            //Update product
                            var categoryattributeitem = resultExistingProduct.Result.custom_attributes.SingleOrDefault(c => c.attribute_code == "category_ids");

                            List<string> strCatList = new List<string>();
                            SearchResult productCategoryFound = Find(maincategoriesList.Result.children_data, "categoryname");

                            if (categoryattributeitem != null)
                            {
                                strCatList = JArray.Parse(categoryattributeitem.value.ToString()).ToObject<string[]>().ToList();
                                if (productCategoryFound.Found != null)
                                    strCatList.Add(productCategoryFound.Found.id.ToString());
                            }
                            else
                            {
                                if (productCategoryFound.Found != null)
                                {
                                    strCatList.Add(productCategoryFound.Found.id.ToString());
                                }
                            }

                            //update some fields 
                            resultExistingProduct.Result.name = productInfo.name;
                            if (!string.IsNullOrEmpty(productInfo.visibility))
                            {
                                switch (productInfo.visibility.ToLower())
                                {
                                    case "catalogue, search":
                                        resultExistingProduct.Result.visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.CatalogSearch;
                                        break;
                                    case "catalogue only":
                                        resultExistingProduct.Result.visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.Catalog;
                                        break;
                                    case "search only":
                                        resultExistingProduct.Result.visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.Search;
                                        break;
                                    case "nowhere":
                                        resultExistingProduct.Result.visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.NotVisibleIndividually;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (!string.IsNullOrEmpty(productInfo.price))
                            resultExistingProduct.Result.price = Convert.ToDouble(productInfo.price);

                            resultExistingProduct.Result.sku = productInfo.part_no;
                            resultExistingProduct.Result.attribute_set_id = 4;
                            resultExistingProduct.Result.status = (int)MagentoV2.RestApi.Models.ProductStatus.Enabled;
                            resultExistingProduct.Result.extension_attributes = (new MagentoV2.RestApi.Models.Extension_Attributes { stock_item = new MagentoV2.RestApi.Models.Stock_Item { stock_id = null, item_id = null, product_id = null, qty = (string.IsNullOrEmpty(productInfo.qty) ? 0 : (Convert.ToInt32(productInfo.qty) > 0 ? Convert.ToInt32(productInfo.qty) : 0)), is_in_stock = (productInfo.is_in_stock == "1" ? true : false) } });
                            resultExistingProduct.Result.weight = 5; // productInfo.weight : need to add this field.
                            resultExistingProduct.Result.type_id = productInfo.product_type;
                            resultExistingProduct.Result.custom_attributes = new MagentoV2.RestApi.Models.Custom_Attributes[] { (new MagentoV2.RestApi.Models.Custom_Attributes { attribute_code = "description", value = productInfo.description })};   //(new MagentoV2.RestApi.Models.Custom_Attributes { attribute_code = "category_ids", value = strCatList }) 


                            //Assign new Fields

                            MagentoV2.RestApi.Models.Mainproduct MainProduct = new MagentoV2.RestApi.Models.Mainproduct();
                            categoryattributeitem.value = strCatList.ToArray();
                            MainProduct.product = resultExistingProduct.Result;
                            MainProduct.saveOptions = true;
                            magentoClient.UpdateProduct(MainProduct);
                        }
                        else
                        {

                            //first identify item if available category
                            //var item = responsedata1.Result.custom_attributes.Select(c => c.attribute_code == "category_ids").ToList();
                            //var item = responsedata1.Result.custom_attributes.SingleOrDefault(c => c.attribute_code == "category_ids");
                            //var strCatList = JArray.Parse(item.value.ToString()).ToObject<string[]>().ToList();

                            //SearchResult data1 = Find(response1.Result.children_data, "Men");
                            //SearchResult data2 = Find(response1.Result.children_data, "Women");

                            //string[] categories = string.Concat(data1.Found.id.ToString(), ",", data2.Found.id.ToString()).Split(',');
                            ////strCatList.Add(data2.Found.id.ToString());
                            //strCatList.Add(data1.Found.id.ToString());
                            List<string> strCatList = new List<string>();
                            SearchResult productCategoryFound = Find(maincategoriesList.Result.children_data, "categoryname");

                            if (productCategoryFound.Found != null)
                            {
                                strCatList.Add(productCategoryFound.Found.id.ToString());
                            }


                            int product_Visibility = 0;

                            if (!string.IsNullOrEmpty(productInfo.visibility))
                            {
                                switch (productInfo.visibility.ToLower())
                                {
                                    case "catalogue, search":
                                        product_Visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.CatalogSearch;
                                        break;
                                    case "catalogue only":
                                        product_Visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.Catalog;
                                        break;
                                    case "search only":
                                        product_Visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.Search;
                                        break;
                                    case "nowhere":
                                        product_Visibility = (int)MagentoV2.RestApi.Models.ProductVisibility.NotVisibleIndividually;
                                        break;
                                    default:
                                        break;
                                }
                            }


                            MagentoV2.RestApi.Models.Product product = new MagentoV2.RestApi.Models.Product
                            {
                                sku = productInfo.part_no,
                                visibility = product_Visibility,
                                status = (int)MagentoV2.RestApi.Models.ProductStatus.Enabled,
                                name = productInfo.name,
                                attribute_set_id = 4,
                                price = (!string.IsNullOrEmpty(productInfo.price) ? Convert.ToDouble(productInfo.price) : 0.00) ,
                                extension_attributes = (new MagentoV2.RestApi.Models.Extension_Attributes { stock_item = new MagentoV2.RestApi.Models.Stock_Item { stock_id = null, item_id = null, product_id = null, qty = (string.IsNullOrEmpty(productInfo.qty) ? 0 : (Convert.ToInt32(productInfo.qty) > 0 ? Convert.ToInt32(productInfo.qty) : 0)), is_in_stock = (productInfo.is_in_stock == "1" ? true : false) } }),
                                weight = 10,
                                type_id = productInfo.product_type,
                                custom_attributes = new MagentoV2.RestApi.Models.Custom_Attributes[] { (new MagentoV2.RestApi.Models.Custom_Attributes { attribute_code = "description", value = productInfo.description }) } // , this need to add : (new MagentoV2.RestApi.Models.Custom_Attributes { attribute_code = "category_ids", value = categories }) 
                            };

                            MagentoV2.RestApi.Models.Mainproduct MainProduct = new MagentoV2.RestApi.Models.Mainproduct();
                            MainProduct.product = product;
                            MainProduct.saveOptions = true;
                            magentoClient.CreateNewProduct(MainProduct);
                        }
                    }
                    catch (Exception ex)
                    {
                        POSLogger.Info("Error " + ex.ToString());
                    }
                    //For Loop End
                }

                //if (lstOfFailedProductAdd.ToString().Length > 0)
                //{
                //    ResetStockExported(lstOfFailedProductAdd.ToString().TrimEnd('|'), "MagentoV1");
                //}

                //ftpimageUpload(SkuImagePath);
                return "success";
            }
            catch (Exception ex)
            {
                POSLogger.Info("Error " + ex.ToString());
                return ex.Message;
            }

        }

        public static SearchResult Find(Children_CategoryData[] node, string name)
        {

            if (node == null)
                return null;

            //if (node.name == name)
            //    return new SearchResult { Found = node, Parent = null };

            foreach (var child in node)
            {
                var found = FindChilden(child, name);
                if (found != null)
                    return new SearchResult { Found = found };
            }

            return null;
        }
        public static Children_CategoryData FindChilden(Children_CategoryData node, string name)
        {

            if (node == null)
                return null;

            if (node.name == name)
                return node;

            foreach (var child in node.children_data)
            {
                var found = FindChilden(child, name);
                if (found != null)
                    return found;
            }

            return null;
        }
        private string AddProductToWooCommerce(string productIds)
        {

            try
            {
                POSLogger.Info("AddProductToWooCommerce Start");

                string[] listOfIds = productIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];

                string wooCommerceURL = System.Configuration.ConfigurationManager.AppSettings["WooCommerceAPIUrl"];
                string wooCommerceConsumerKey = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerKey"];
                string wooCommerceConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerSecret"];
                string HTTPSEnable = Convert.ToString((System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"] == null ? "false" : System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"]));

                WooCommerceNET.RestAPI rest;

                if (HTTPSEnable.ToLower() == "true") //For HTTS/ TSL Authentication
                    rest = new WooCommerceNET.RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, false);
                else
                    rest = new WooCommerceNET.RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, true);

                WooCommerceNET.WooCommerce.WCObject wc = new WooCommerceNET.WooCommerce.WCObject(rest);

                POSLogger.Info("Init WooCommerce Object");

                string sortDirection = "asc";
                var lstProperties = typeof(StockItemforWooCommerce).GetProperties().ToList();
                var lstOfProducts = _posService.GetSelectedProductsWooCommerce(productIds, 0, 55555, lstProperties[0].Name, sortDirection, "WooCommerce");

                POSLogger.Info("AddProductToWooCommerce Get Stock" + lstOfProducts.stockItemInfoforWooCommerce.Count);

                StringBuilder lstOfFailedProductAdd = new StringBuilder();
                Dictionary<string, string> SkuImagePath =
                                         new Dictionary<string, string>();

                foreach (var productInfo in lstOfProducts.stockItemInfoforWooCommerce)
                {
                    try
                    {

                        Dictionary<string, string> lstParameters = new Dictionary<string, string>();

                        lstParameters.Add("sku", productInfo.part_no);

                        var resultExistingProduct = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.Product>>(() => wc.GetProducts(lstParameters));

                        POSLogger.Info(String.Format("Search with SKU:{0} and Result:{1}", productInfo.part_no, resultExistingProduct.Count));

                        List<WooCommerceNET.WooCommerce.Category> catList; //new List<WooCommerceNET.WooCommerce.Category>();

                        if (resultExistingProduct.Count > 0)
                        {
                            catList = resultExistingProduct[0].categories;
                        }
                        else
                        {
                            catList = new List<WooCommerceNET.WooCommerce.Category>();
                        }

                        if (!string.IsNullOrWhiteSpace(productInfo.categoryid))
                        {
                            //First Find Category if Exists then Set. 
                            Dictionary<string, string> lstParametersCat = new Dictionary<string, string>();
                            lstParametersCat.Add("search", productInfo.categoryid);
                            lstParametersCat.Add("per_page", "100");

                            var categoryList = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.ProductCategory>>(() => wc.GetProductCategories(lstParametersCat));
                            if (categoryList.Any())
                            {
                                POSLogger.Info(String.Format("Find with Category:Name{0}", productInfo.categoryid));
                                var resultcat = categoryList.Find(c => c.name.ToLower() == productInfo.categoryid.ToLower());
                                if (resultcat != null)
                                {
                                    POSLogger.Info(String.Format("Added with Category:ID{0} Name{1}", resultcat.id.Value.ToString(), resultcat.name));
                                    WooCommerceNET.WooCommerce.Category objCat = new WooCommerceNET.WooCommerce.Category();
                                    objCat.id = resultcat.id;

                                    if (catList.Count == 0 || (catList.Count > 0 && catList.Find(a => a.id == resultcat.id) == null))
                                        catList.Add(objCat);
                                }
                            }
                        }

                        //Add new product
                        WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product();

                        p.name = productInfo.product_name;

                        if (!string.IsNullOrWhiteSpace(productInfo.description))
                        {
                            p.description = productInfo.description;
                        }

                        //Append Supplier and Dimention/Color Info
                        string dimentionInfo = Utility.GetDimentionString(productInfo.item_length, productInfo.item_width, productInfo.item_height);
                        StringBuilder footerInfo = new StringBuilder();
                        if (!string.IsNullOrEmpty(productInfo.supplier) || (!string.IsNullOrEmpty(dimentionInfo)) || (!string.IsNullOrEmpty(productInfo.product_colour)))
                        {

                            footerInfo = footerInfo.Append(@"< hr />");
                            footerInfo = footerInfo.Append(@"< p > ");

                            if (!string.IsNullOrEmpty(productInfo.supplier))
                                footerInfo = footerInfo.AppendFormat(@"Info: {0}  |", productInfo.supplier);

                            if (!string.IsNullOrEmpty(dimentionInfo))
                            {
                                footerInfo = footerInfo.AppendFormat(@" Dimensions: {0} ", dimentionInfo);
                                p.dimensions = new WooCommerceNET.WooCommerce.Dimension() { height = productInfo.item_height, length = productInfo.item_length, width = productInfo.item_width };
                            }

                            if (!string.IsNullOrEmpty(productInfo.product_colour))
                                footerInfo = footerInfo.AppendFormat(@"| Colour: {0} ", productInfo.product_colour);

                            footerInfo = footerInfo.Append("</ p >");
                        }

                        if (footerInfo.Length > 0 && !string.IsNullOrWhiteSpace(productInfo.description))
                        {
                            p.description = p.description + footerInfo.ToString();
                        }
                        //< p > Info: Newgate Clocks Ltd | Dimensions: 17 X 11.7 X 5.5 CM </ p >

                        if (!string.IsNullOrWhiteSpace(productInfo.short_description))
                            p.short_description = productInfo.short_description;

                        //Regular Price/ Retail Price
                        if (string.IsNullOrWhiteSpace(productInfo.regularprice))
                        {
                            p.regular_price = null;
                        }
                        else
                        {
                            p.regular_price = Convert.ToDecimal(productInfo.regularprice);
                        }

                        //Selling Price
                        if (string.IsNullOrWhiteSpace(productInfo.sellingprice))
                        {
                            p.sale_price = null;
                        }
                        else
                        {
                            p.sale_price = Convert.ToDecimal(productInfo.sellingprice);
                        }

                        //If Both Same After Convert  to Decimal Keep Single Value.
                        if (p.regular_price == p.sale_price)
                        {
                            p.regular_price = p.sale_price;
                        }

                        p.price = string.IsNullOrEmpty(productInfo.sellingprice) ? 0 : Convert.ToDecimal(productInfo.sellingprice);
                        p.sku = productInfo.part_no;
                        p.in_stock = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? false : ((Convert.ToInt32(productInfo.unallocatedstocklevel) > 0));
                        p.categories = catList;
                        p.slug = productInfo.product_name;
                        p.manage_stock = true;
                        p.stock_quantity = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? 0 : Convert.ToInt32((productInfo.unallocatedstocklevel));

                        if (!string.IsNullOrEmpty(productInfo.Weight))
                        {
                            p.weight = Convert.ToDecimal(productInfo.Weight);
                        }

                        if (!string.IsNullOrEmpty(productInfo.product_colour))
                        {
                            //Set Colour Attribute
                            if (resultExistingProduct.Any() && resultExistingProduct[0].attributes.Any() && resultExistingProduct[0].attributes.Find(a => a.name.ToLower() == "colour") != null)
                            {
                                WooCommerceNET.WooCommerce.Attribute colorAtrribute = resultExistingProduct[0].attributes.Find(a => a.name.ToLower() == "colour");
                                if (!colorAtrribute.options.Contains(productInfo.product_colour))
                                {
                                    colorAtrribute.options.Add(productInfo.product_colour);
                                }
                                colorAtrribute.visible = true;
                                p.attributes = resultExistingProduct[0].attributes;
                            }
                            else
                            {
                                p.attributes = new List<WooCommerceNET.WooCommerce.Attribute>();
                                p.attributes.Add(new WooCommerceNET.WooCommerce.Attribute() { name = "Colour", options = new List<string> { productInfo.product_colour }, visible = true });
                            }
                        }

                        if (!string.IsNullOrEmpty(productInfo.visibility))
                        {
                            switch (productInfo.visibility.ToLower())
                            {
                                case "catalogue, search":
                                    p.catalog_visibility = "visible";
                                    break;
                                case "catalogue only":
                                    p.catalog_visibility = "catalog";
                                    break;
                                case "search only":
                                    p.catalog_visibility = "search";
                                    break;
                                case "nowhere":
                                    p.catalog_visibility = "hidden";
                                    break;
                                default:
                                    break;
                            }
                        }

                        if (!String.IsNullOrEmpty(productInfo.OnWeb))
                        {
                            switch (productInfo.OnWeb)
                            {

                                case "Enabled":
                                    p.status = "publish";
                                    break;
                                case "Disabled":
                                    p.status = "draft";
                                    break;
                            }
                        }

                        string productrestult = string.Empty;
                        if (resultExistingProduct.Count > 0)
                        {
                            var id = Convert.ToInt32(resultExistingProduct[0].id);
                            POSLogger.Info("R-Price: " + p.regular_price.ToString());
                            POSLogger.Info("S-Price: " + p.sale_price.ToString());
                            productrestult = AsyncHelper.RunSync<string>(() => wc.UpdateProduct(id, p));
                            POSLogger.Info("Updated Product");
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(p.catalog_visibility))
                                p.catalog_visibility = "visible";

                            p.type = "simple";
                            POSLogger.Info("R-Price: " + p.regular_price.ToString());
                            POSLogger.Info("S-Price: " + p.sale_price.ToString());
                            productrestult = AsyncHelper.RunSync<string>(() => wc.PostProduct(p));
                            POSLogger.Info("Added Product");
                        }
                        if (string.IsNullOrEmpty(productrestult) || productrestult.ToLower().Contains("error"))
                        {
                            lstOfFailedProductAdd.Append(productInfo.id + "|");
                        }

                        SkuImagePath.Add(productInfo.part_no, productInfo.imagepaths);

                    }
                    catch (Exception ex)
                    {
                        POSLogger.Info("Error " + ex.ToString());
                    }
                    //For Loop End
                }

                //if (lstOfFailedProductAdd.ToString().Length > 0)
                //{
                //    ResetStockExported(lstOfFailedProductAdd.ToString().TrimEnd('|'), "WooCommerce");
                //}

                ftpimageUpload(SkuImagePath);

                return "success";
            }
            catch (Exception ex)
            {
                POSLogger.Info("Error " + ex.ToString());
                return ex.Message;
            }
        }

        private string AddProductToEbay(string productIds)
        {

            try
            {
                string[] listOfIds = productIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

                //string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                ApiContext apiContext = GetApiContext();

                string sortDirection = "asc";
                var lstProperties = typeof(StockItemforEbay).GetProperties().ToList();
                var lstOfProducts = _posService.GetSelectedProductsEbay(productIds, 0, 55555, lstProperties[0].Name, sortDirection, "WooCommerce");

                POSLogger.Info("AddProductToEbay Get Stock" + lstOfProducts.stockItemInfoforEbay.Count);

                foreach (var productInfo in lstOfProducts.stockItemInfoforEbay)
                {
                    //[Step 2] Create a new ItemType object

                    if (!string.IsNullOrEmpty(productInfo.eBayItemId))
                    {
                        string defaultapiCall = "Inventory";

                        GetItemCall apicall = new GetItemCall(apiContext);
                        apicall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
                        fetchedItem = apicall.GetItem(productInfo.eBayItemId);

                        if (defaultapiCall == "Inventory")
                            ReviseInventoryStatus(productInfo);
                        else
                        {
                            ItemType item = RevisedItem(productInfo);

                            POSLogger.Info("Item no" + item.ItemID + "Revised Successfully");
                        }
                    }
                    else
                    {
                        ItemType item = BuildItem(productInfo);
                        //[Step 3] Create Call object and execute the Call
                        AddItemCall apiCall = new AddItemCall(apiContext);
                        FeeTypeCollection fees = apiCall.AddItem(item);

                        double listingFee = 0.0;
                        foreach (FeeType fee in fees)
                        {
                            if (fee.Name == "ListingFee")
                            {
                                listingFee = fee.Fee.Value;
                            }
                        }
                        POSLogger.Info("Listing fee is" + listingFee);
                        POSLogger.Info("Listed Item ID:" + item.ItemID);
                    }

                }

                return "success";
            }
            catch (Exception ex)
            {
                POSLogger.Info("Error " + ex.ToString());
                return ex.Message;
            }
        }

        public bool ftpimageUpload(Dictionary<string, string> SkuImagePaths)
        {
            try
            {
                FtpURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceHostname"]);
                FtpFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceFtpFolderName"]);
                Username = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceUsername"]);
                Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommercePassword"]);

                CreateDirectory();

                foreach (KeyValuePair<string, string> SKU_Imagepath in SkuImagePaths)
                {
                    if (!string.IsNullOrEmpty(SKU_Imagepath.Value))
                    {

                        CreateImageDirectory(SKU_Imagepath.Key);

                        string[] Images = SKU_Imagepath.Value.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (var image in Images)
                        {
                            string filepath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"] + image;
                            byte[] fileBytes = null;

                            bool IsExists = true;

                            string fileName = Path.GetFileName(filepath);

                            string fileextention = System.IO.Path.GetExtension(filepath);



                            using (StreamReader fileStream = new StreamReader(filepath))
                            {

                                fileBytes = File.ReadAllBytes(filepath);
                                fileStream.Close();
                            }

                            //Create FTP Request.
                            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/" + SKU_Imagepath.Key + "/" + fileName); //add folder name aftre ftpurl
                            request.Method = WebRequestMethods.Ftp.UploadFile;

                            //Enter FTP Server credentials.
                            request.Credentials = new NetworkCredential(Username, Password);
                            request.ContentLength = fileBytes.Length;
                            request.KeepAlive = false;

                            using (Stream requestStream = request.GetRequestStream())
                            {
                                requestStream.Write(fileBytes, 0, fileBytes.Length);
                                requestStream.Close();
                            }
                            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                            response.Close();
                        }
                    }

                }
                return true;
            }
            catch (WebException ex)
            {
                return false;
            }
        }

        protected void CreateDirectory()
        {
            if (!DirectoryExists(FtpFolderName))
            {
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        public bool DirectoryExists(string FTPDirectory)
        {
            bool directoryExists;

            var filePath = FtpURL + "/" + FTPDirectory + "/";
            var request = (FtpWebRequest)FtpWebRequest.Create(filePath);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(Username, Password);

            try
            {
                using (request.GetResponse())
                {
                    directoryExists = true;
                }
            }
            catch (WebException)
            {
                directoryExists = false;
            }

            return directoryExists;
        }

        protected void CreateImageDirectory(string SKU)
        {
            if (!DirectoryExists((FtpFolderName + "/" + SKU)))
            {
                var ImageFolder = "/" + FtpFolderName + "/" + SKU;
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + ImageFolder + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        /// <summary>
        /// Populate eBay SDK ApiContext object with data from application configuration file
        /// </summary>
        /// <returns>ApiContext object</returns>
        static ApiContext GetApiContext()
        {
            //apiContext is a singleton,
            //to avoid duplicate configuration reading
            if (apiContext != null)
            {
                return apiContext;
            }
            else
            {
                apiContext = new ApiContext();

                //set Api Server Url
                apiContext.SoapApiServerUrl =
                    ConfigurationManager.AppSettings["eBayApiServerUrl"];
                //set Api Token to access eBay Api Server
                ApiCredential apiCredential = new ApiCredential();
                apiCredential.eBayToken =
                    ConfigurationManager.AppSettings["eBayApiToken"];
                apiContext.ApiCredential = apiCredential;
                //set eBay Site target to US
                apiContext.Site = SiteCodeType.US;

                //set Api logging
                apiContext.ApiLogManager = new ApiLogManager();
                apiContext.ApiLogManager.ApiLoggerList.Add(
                    new FileLogger("listing_log.txt", true, true, true)
                    );
                apiContext.ApiLogManager.EnableLogging = true;


                return apiContext;
            }
        }

        /// <summary>
        /// Build a sample item
        /// </summary>
        /// <returns>ItemType object</returns>
        static ItemType BuildItem(StockItemforEbay product)
        {
            ItemType item = new ItemType();


            // item title
            item.Title = product.product_name;
            // item description
            item.Description = product.Description;

            // listing type
            if (!string.IsNullOrEmpty(product.ListingType))
            {
                //item.ListingType = ListingTypeCodeType.Chinese;
                item.ListingType = (ListingTypeCodeType)Enum.Parse(typeof(ListingTypeCodeType), product.ListingType);
            }
            // listing price
            if (!string.IsNullOrEmpty(product.CurrencyCode))
            {
                // item.Currency = CurrencyCodeType.USD;
                item.Currency = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), product.CurrencyCode);
            }

            item.StartPrice = new AmountType();
            item.StartPrice.Value = product.Listing_StartPrice_Value;
            if (!string.IsNullOrEmpty(product.Listing_StartPrice_CurrencyCode))
            {
                // item.StartPrice.currencyID = CurrencyCodeType.USD;
                item.StartPrice.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), product.Listing_StartPrice_CurrencyCode);
            }

            // listing duration
            item.ListingDuration = product.ListingDuration;

            // item location and country
            item.Location = product.Location;

            if (!string.IsNullOrEmpty(product.CountryCode))
            {
                // item.Country = CountryCodeType.US;
                item.Country = (CountryCodeType)Enum.Parse(typeof(CountryCodeType), product.CountryCode);
            }


            // listing category, 
            CategoryType category = new CategoryType();
            category.CategoryID = product.CategoryID; //CategoryID = 11104 (CookBooks) , Parent CategoryID=267(Books)
            item.PrimaryCategory = category;

            // item quality
            item.Quantity = product.Quantity;

            // item condition, New
            item.ConditionID = product.ConditionID;

            // item specifics
            item.ItemSpecifics = buildItemSpecifics();

            //POSLogger.Info("Do you want to use Business policy profiles to list this item? y/n");
            //String input = Console.ReadLine();
            //if (input.ToLower().Equals("y"))
            //{
            //    item.SellerProfiles = BuildSellerProfiles();
            //}
            //else
            //{
            // payment methods
            item.PaymentMethods = new BuyerPaymentMethodCodeTypeCollection();
            item.PaymentMethods.AddRange(
                new BuyerPaymentMethodCodeType[] { BuyerPaymentMethodCodeType.PayPal }
                );
            // email is required if paypal is used as payment method
            item.PayPalEmailAddress = product.PayPalEmailAddress;

            // handling time is required
            item.DispatchTimeMax = product.DispatchTimeMax;
            // shipping details
            item.ShippingDetails = BuildShippingDetails();

            // return policy
            item.ReturnPolicy = new ReturnPolicyType();
            item.ReturnPolicy.ReturnsAcceptedOption = product.ReturnsAcceptedOption;
            //}
            //item Start Price
            AmountType amount = new AmountType();
            amount.Value = product.Item_StartPrice_Value;
            if (!string.IsNullOrEmpty(product.Item_StartPrice_CurrencyCode))
            {
                // amount.currencyID = CurrencyCodeType.USD;
                amount.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), product.Item_StartPrice_CurrencyCode);
            }
            item.StartPrice = amount;
            return item;
        }

        /// <summary>
        /// Revised a sample item
        /// </summary>
        /// <returns>ItemType object</returns>
        static ItemType RevisedItem(StockItemforEbay product)
        {
            ItemType item = new ItemType();
            item.ItemID = product.eBayItemId;

            CurrencyCodeType currencyCode = CurrencyUtility.GetDefaultCurrencyCodeType(apiContext.Site);


            if (product.product_name.Length > 0)
            {
                item.Title = product.product_name;
            }

            if (product.Description.Length > 0)
            {
                item.Description = product.Description;
            }

            if (product.ListingDuration.Length > 0)
            {
                item.ListingDuration = product.ListingDuration;
            }

            if (Convert.ToString(product.Item_StartPrice_Value).Length > 0)
            {
                item.StartPrice = new AmountType();
                item.StartPrice.currencyID = currencyCode;
                item.StartPrice.Value = product.Item_StartPrice_Value;
            }
            //if (TxtReservePrice.Text.Length > 0)
            //{
            //    item.ReservePrice = new AmountType();
            //    item.ReservePrice.currencyID = currencyCode;
            //    item.ReservePrice.Value = Double.Parse(this.TxtReservePrice.Text, NumberStyles.Currency);
            //}
            //if (TxtBuyItNowPrice.Text.Length > 0)
            //{
            //    item.BuyItNowPrice = new AmountType();
            //    item.BuyItNowPrice.currencyID = currencyCode;
            //    item.BuyItNowPrice.Value = Double.Parse(this.TxtBuyItNowPrice.Text, NumberStyles.Currency);
            //}

            //if (CboEnableBestOffer.SelectedIndex != -1)
            //{
            //    item.BestOfferDetails = new BestOfferDetailsType();
            //    item.BestOfferDetails.BestOfferEnabled = Boolean.Parse(CboEnableBestOffer.SelectedItem.ToString());
            //}

            StringCollection deletedFields = new StringCollection();

            //if (ChkPayPalEmailAddress.Checked)
            //    deletedFields.Add("Item.payPalEmailAddress");

            //if (ChkApplicationData.Checked)
            //    deletedFields.Add("Item.applicationData");

            //ReviseItemCall apicall = new ReviseItemCall(Context);
            //if (ListPictures.Items.Count > 0)
            //{
            //    apicall.PictureFileList = new StringCollection();
            //    item.PictureDetails = new PictureDetailsType();
            //    item.PictureDetails.PhotoDisplay = (PhotoDisplayCodeType)Enum.Parse(typeof(PhotoDisplayCodeType), CboPicDisplay.SelectedItem.ToString());
            //}

            //foreach (string pic in ListPictures.Items)
            //{
            //    apicall.PictureFileList.Add(pic);
            //}

            ReviseItemCall apicall = new ReviseItemCall(apiContext);
            apicall.DeletedFieldList = deletedFields;

            apicall.ReviseItem(item, deletedFields, false);
            //TxtItemId.Text = item.ItemID;

            FeeTypeCollection fees = apicall.FeeList;


            foreach (FeeType fee in fees)
            {
                if (fee.Name == "ListingFee")
                {

                }
            }
            return item;
        }

        /// <summary>
        /// Build sample item specifics
        /// </summary>
        /// <returns>ItemSpecifics object</returns>
        static NameValueListTypeCollection buildItemSpecifics()
        {
            //create the content of item specifics
            NameValueListTypeCollection nvCollection = new NameValueListTypeCollection();
            NameValueListType nv1 = new NameValueListType();
            nv1.Name = "Platform";
            StringCollection nv1Col = new StringCollection();
            String[] strArr1 = new string[] { "Microsoft Xbox 360" };
            nv1Col.AddRange(strArr1);
            nv1.Value = nv1Col;
            NameValueListType nv2 = new NameValueListType();
            nv2.Name = "Genre";
            StringCollection nv2Col = new StringCollection();
            String[] strArr2 = new string[] { "Simulation" };
            nv2Col.AddRange(strArr2);
            nv2.Value = nv2Col;
            nvCollection.Add(nv1);
            nvCollection.Add(nv2);
            return nvCollection;
        }
        /// <summary>
        /// Build sample SellerProfile details
        /// </summary>
        /// <returns></returns>
        static SellerProfilesType BuildSellerProfiles()
        {
            /*
             * Beginning with release 763, some of the item fields from
             * the AddItem/ReviseItem/VerifyItem family of calls have been
             * moved to the Business Policies API. 
             * See http://developer.ebay.com/Devzone/business-policies/Concepts/BusinessPoliciesAPIGuide.html for more
             * 
             * This example uses profiles that were previously created using this api.
             */

            SellerProfilesType sellerProfile = new SellerProfilesType();

            POSLogger.Info("Enter Return policy profile Id:");
            sellerProfile.SellerReturnProfile = new SellerReturnProfileType();
            sellerProfile.SellerReturnProfile.ReturnProfileID = Int64.Parse(Console.ReadLine());

            POSLogger.Info("Enter Shipping profile Id:");
            sellerProfile.SellerShippingProfile = new SellerShippingProfileType();
            sellerProfile.SellerShippingProfile.ShippingProfileID = Int64.Parse(Console.ReadLine());

            POSLogger.Info("Enter Payment profile Id:");
            sellerProfile.SellerPaymentProfile = new SellerPaymentProfileType();
            sellerProfile.SellerPaymentProfile.PaymentProfileID = Int64.Parse(Console.ReadLine());

            return sellerProfile;
        }

        /// <summary>
        /// Build sample shipping details
        /// </summary>
        /// <returns>ShippingDetailsType object</returns>
        static ShippingDetailsType BuildShippingDetails()
        {
            // Shipping details
            ShippingDetailsType sd = new ShippingDetailsType();

            sd.ApplyShippingDiscount = true;
            AmountType amount = new AmountType();
            amount.Value = 2.5;
            amount.currencyID = CurrencyCodeType.USD;
            sd.PaymentInstructions = "eBay .Net SDK test instruction.";

            // Shipping type and shipping service options
            sd.ShippingType = ShippingTypeCodeType.Flat;
            ShippingServiceOptionsType shippingOptions = new ShippingServiceOptionsType();
            shippingOptions.ShippingService =
                ShippingServiceCodeType.ShippingMethodStandard.ToString();
            amount = new AmountType();
            amount.Value = 2.0;
            amount.currencyID = CurrencyCodeType.USD;
            shippingOptions.ShippingServiceAdditionalCost = amount;
            amount = new AmountType();
            amount.Value = 1;
            amount.currencyID = CurrencyCodeType.USD;
            shippingOptions.ShippingServiceCost = amount;
            shippingOptions.ShippingServicePriority = 1;
            amount = new AmountType();
            amount.Value = 1.0;
            amount.currencyID = CurrencyCodeType.USD;
            shippingOptions.ShippingInsuranceCost = amount;

            sd.ShippingServiceOptions = new ShippingServiceOptionsTypeCollection(
                new ShippingServiceOptionsType[] { shippingOptions }
                );

            return sd;
        }

        private async Task<string> AddProductAsync(WooCommerceNET.WooCommerce.Product p, WooCommerceNET.WooCommerce.WCObject wc)
        {
            return await wc.PostProduct(p);
        }

        private void ReviseInventoryStatus(StockItemforEbay product)
        {

            //create the context

            ReviseInventoryStatusCall ris = new ReviseInventoryStatusCall(apiContext);

            //RIS is a light-weight call that allows you to revise price and/qty of 4 SKUs at a time
            ris.InventoryStatuList = new InventoryStatusTypeCollection();
            AmountType atUp = new AmountType();
            atUp.currencyID = CurrencyCodeType.USD;
            atUp.Value = 0.50;

            AmountType atDown = new AmountType();
            atDown.currencyID = CurrencyCodeType.USD;
            atDown.Value = 0.25;

            InventoryStatusType InvStatus1 = new InventoryStatusType();

            //Assumption item is listed with InventoryTrackingMethod = SKU. This SKU could be the SKU of a regular fixed price item
            //or the SKU of one of the variations of a multi-variations item
            // InvStatus1.SKU = "1359";
            InvStatus1.ItemID = "110190286051";
            //Revising price and qty
            InvStatus1.StartPrice = atUp;
            InvStatus1.Quantity = 1;

            ris.InventoryStatuList.Add(InvStatus1);

            //InventoryStatusType InvStatus2 = new InventoryStatusType();
            //InvStatus2.SKU = "9013";
            //InvStatus2.StartPrice = atUp;
            //InvStatus2.Quantity = 20;

            //ris.InventoryStatuList.Add(InvStatus2);

            ////Revising price only
            //InventoryStatusType InvStatus3 = new InventoryStatusType();
            //InvStatus3.SKU = "1724";
            //InvStatus3.StartPrice = atDown;

            //ris.InventoryStatuList.Add(InvStatus3);

            ////Revising qty only
            //InventoryStatusType InvStatus4 = new InventoryStatusType();
            //InvStatus4.SKU = "4539";
            //InvStatus4.Quantity = 20;

            //ris.InventoryStatuList.Add(InvStatus4);

            ris.Execute();
            POSLogger.Info("Item no" + InvStatus1.ItemID + "Inventory Updated");

        }

    }
    public class SearchResult
    {
        public Children_CategoryData Found;
    }
}

internal static class AsyncHelper
{
    private static readonly TaskFactory _myTaskFactory = new
      TaskFactory(CancellationToken.None,
                  TaskCreationOptions.None,
                  TaskContinuationOptions.None,
                  TaskScheduler.Default);

    public static TResult RunSync<TResult>(Func<Task<TResult>> func)
    {
        return AsyncHelper._myTaskFactory
          .StartNew<Task<TResult>>(func)
          .Unwrap<TResult>()
          .GetAwaiter()
          .GetResult();
    }

    public static void RunSync(Func<Task> func)
    {
        AsyncHelper._myTaskFactory
          .StartNew<Task>(func)
          .Unwrap()
          .GetAwaiter()
          .GetResult();
    }
}
