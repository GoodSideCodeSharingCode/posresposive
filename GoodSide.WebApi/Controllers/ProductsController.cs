﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;

namespace GoodSide.WebApi.Controllers
{
    [RoutePrefix("products")]
    public class ProductsController : ApiController
    {
        private IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        
        public IHttpActionResult GetProducts()
        {
            RuleParameters ruleParameters = new RuleParameters();

            //// ensure there are products for the example
            //if (!_productService.GetAll(ruleParameters).Any())
            //{
            //    //_productService.Create(new Product { Name = "Product 1" });
            //    //_productService.Create(new Product { Name = "Product 2" });
            //    //_productService.Create(new Product { Name = "Product 3" });
            //}

            //return Ok(_productService.GetAll(ruleParameters));
            return Ok();
        }
    }
}
