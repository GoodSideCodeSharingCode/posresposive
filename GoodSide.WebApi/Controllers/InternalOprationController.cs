﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoodSide.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InternalOprationController : ApiController
    {
        private IProductService _productSearchService;
        private IStockItemService _stockItemService;
        private IPOSService _posService;

           /// <summary>
        /// test
        /// </summary>
        /// <param name="productSearchService">test</param>
        /// <param name="stockItemService">test</param>
        /// <param name="posService">test</param>
        public InternalOprationController(IProductService productSearchService, IStockItemService stockItemService, IPOSService posService)
        {
            _productSearchService = productSearchService;
            _stockItemService = stockItemService;
            _posService = posService;
        }


        [HttpPost]
        [Route("api/InternalOpration/AddProductToChannel")]
        public async Task<string> AddProductToChannel(string stockIds, string channelType)
        {
            string result = string.Empty;

            String[] Channels = channelType.Split(',');

            foreach (string channel in Channels)
            {

                try
                {
                    switch (channel.ToLower())
                    {
                        case "magento":
                            AddProductToMagento(stockIds);
                            break;
                        case "woocommerce":
                            result = AddProductToWooCommerce(stockIds);
                            break;
                        default:
                            break;
                    }

                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return "Success";
        }

        private void AddProductToMagento(string productIds)
        {

        }
        private string AddProductToWooCommerce(string productIds)
        {
            try
            {
                string[] listOfIds = productIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                var rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~");

                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];

                string wooCommerceURL = System.Configuration.ConfigurationManager.AppSettings["WooCommerceAPIUrl"];
                string wooCommerceConsumerKey = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerKey"];
                string wooCommerceConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerSecret"];
                string HTTPSEnable = Convert.ToString((System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"] == null ? "false" : System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"]));

                WooCommerceNET.RestAPI rest;

                if (HTTPSEnable.ToLower() == "true") //For HTTS/ TSL Authentication
                    rest = new WooCommerceNET.RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, false);
                else
                    rest = new WooCommerceNET.RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, true);

                WooCommerceNET.WooCommerce.WCObject wc = new WooCommerceNET.WooCommerce.WCObject(rest);

                string sortDirection = "asc";
                var lstProperties = typeof(StockItemforWooCommerce).GetProperties().ToList();
                var lstOfProducts = _posService.GetSelectedProductsWooCommerce(productIds, 0, 55555, lstProperties[0].Name, sortDirection, "WooCommerce");

                StringBuilder lstOfFailedProductAdd = new StringBuilder();

                foreach (var productInfo in lstOfProducts.stockItemInfoforWooCommerce)
                {

                    List<WooCommerceNET.WooCommerce.Category> catList = new List<WooCommerceNET.WooCommerce.Category>();
                    WooCommerceNET.WooCommerce.Category objCat = new WooCommerceNET.WooCommerce.Category();

                    
                    objCat = new WooCommerceNET.WooCommerce.Category();
                    objCat.name = productInfo.categoryid;
                    objCat.slug = productInfo.categoryid;

                    catList.Add(objCat);
                    //Add new product
                    WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product()
                    {
                        name = productInfo.product_name,
                        description = productInfo.description,
                        price = Convert.ToDecimal(productInfo.sellingprice),
                        sku = productInfo.part_no,
                        in_stock = (Convert.ToInt32(productInfo.unallocatedstocklevel) > 0),
                        categories = catList,
                        catalog_visibility = "catalog",
                        slug = productInfo.product_name,
                        stock_quantity = Convert.ToInt32(productInfo.unallocatedstocklevel),
                        weight = string.IsNullOrEmpty(productInfo.Weight) ? 0 : Convert.ToDecimal(productInfo.Weight),
                        type = "simple"
                    };

                    var addProduct = AsyncHelper.RunSync<string>(() => wc.PostProduct(p));

                    if (string.IsNullOrEmpty(addProduct) || addProduct.ToLower().Contains("error"))
                    {
                        lstOfFailedProductAdd.Append(productInfo.id + "|");
                    }
                }

                if (lstOfFailedProductAdd.ToString().Length > 0)
                {
                  //  ResetStockExported(lstOfFailedProductAdd.ToString().TrimEnd('|'), "WooCommerce");
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private async Task<string> AddProductAsync(WooCommerceNET.WooCommerce.Product p, WooCommerceNET.WooCommerce.WCObject wc)
        {
            return await wc.PostProduct(p);
        }
    }
}
