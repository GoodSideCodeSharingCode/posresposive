﻿using AttributeRouting.Web.Http;
using GoodSide.Domain.Services;
using GoodSide.WebApi.Filters;
using GoodSide.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GoodSide.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [ApiAuthenticationFilter]
    public class AuthenticateController : ApiController
    {
        #region Private variable.

        private readonly ITokenServices _tokenServices;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public AuthenticateController(ITokenServices tokenServices)
        {
            _tokenServices = tokenServices;
        }

        #endregion

        /// <summary>
        /// Authenticates user and returns token with expiry.
        /// </summary>
        /// <returns></returns>
        //[POST("login")]
        //[POST("authenticate")]
        //[POST("get/token")]
        [HttpPost]
        public HttpResponseMessage Authenticate()
        {
            if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.Identity.IsAuthenticated)
            {
                var basicAuthenticationIdentity = System.Threading.Thread.CurrentPrincipal.Identity as BasicAuthenticationIdentity;
                if (basicAuthenticationIdentity != null)
                {
                    var storeId = basicAuthenticationIdentity.StoreId;
                    return GetAuthToken(storeId);
                }
            }
            return null;
        }

        /// <summary>
        /// Returns auth token for the validated user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private HttpResponseMessage GetAuthToken(int storeId)
        {
            var token = _tokenServices.GenerateToken(storeId, System.Web.HttpContext.Current.Session[SessionConstants.username.ToString()].ToString(), System.Web.HttpContext.Current.Session[SessionConstants.storename.ToString()].ToString(), Convert.ToBoolean(System.Web.HttpContext.Current.Session[SessionConstants.passwordOnly.ToString()]), Convert.ToInt32(System.Web.HttpContext.Current.Session[SessionConstants.userrole.ToString()]));
            var response = Request.CreateResponse(HttpStatusCode.OK, "Authorized");
            response.Headers.Add("Token", token.AuthToken);
            response.Headers.Add("TokenExpiry", ConfigurationManager.AppSettings["AuthTokenExpiry"]);
            response.Headers.Add("Access-Control-Expose-Headers", "Token,TokenExpiry");
            return response;
        }
    }
}
