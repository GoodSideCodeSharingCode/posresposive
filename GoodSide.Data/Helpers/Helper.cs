﻿using GoodSide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GoodSide.Data.Helpers
{


    public static class Helper
    {

        public static T GetObject<T>() where T : new()
        {
            return new T();
        }

        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> DataTableToList<T>(this DataTable table) where T : IEntity
        {
            try
            {
                List<T> list = new List<T>();
                var tClass = typeof(T);
                foreach (var row in table.AsEnumerable())
                {

                    //T obj = new T();

                    T obj = (T)Activator.CreateInstance(tClass);

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// Converts a DataTable to a list with generic objects
        /// </summary>
        /// <typeparam name="T">Generic object</typeparam>
        /// <param name="table">DataTable</param>
        /// <returns>List with generic objects</returns>
        public static List<T> GetListFromDataTable<T>(DataTable table) where T : IEntity
        {
            try
            {
                List<T> list = new List<T>();
                var tClass = typeof(T);
                foreach (var row in table.AsEnumerable())
                {

                    //T obj = new T();

                    T obj = (T)Activator.CreateInstance(tClass);

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        ///// <summary>
        ///// Converts a DataTable to a list with generic objects
        ///// </summary>
        ///// <typeparam name="T">Generic object</typeparam>
        ///// <param name="table">DataTable</param>
        ///// <returns>List with generic objects</returns>
        //public static List<object> GetListFromDataTable(DataTable table, Type T)
        //{
        //    try
        //    {
        //        List<object> list = new List<object>();

        //        foreach (var row in table.AsEnumerable())
        //        {

        //            //T obj = new T();

        //            object obj = Activator.CreateInstance(T);

        //            foreach (var prop in obj.GetType().GetProperties())
        //            {
        //                try
        //                {
        //                    PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
        //                    propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
        //                }
        //                catch
        //                {
        //                    continue;
        //                }
        //            }

        //            list.Add(obj);
        //        }

        //        return list;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}


    }
}