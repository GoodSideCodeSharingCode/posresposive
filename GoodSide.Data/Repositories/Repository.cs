﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodSide.Data.Helpers;
using GoodSide.Domain.Entities;
using GoodSide.Domain.Repositories;
using GoodSide.Domain.Helpers;
using System.Collections.Specialized;
using System.Data;
using GoodSide.SkylineService.Service;
using ApplicationLayers.BAL.Utils;
using AutoMapper;
using System.Reflection;
using System.Net.Mail;

namespace GoodSide.Data.Repositories
{

    public class Repository<T> : IRepository<T> where T : IEntity
    {
        static PropertyInfo _ruleParameterProperty = typeof(T).GetProperty("ruleParameters");

        public List<T> GetAll(RuleParameters ruleParameters)
        {
            //RuleParameters ruleParameters = (RuleParameters)_ruleParameterProperty.GetValue(this);
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(DataSet))
            {
                DataSet dsResult = (DataSet)result;
                DataTable dt = dsResult.Tables[0];
                return dt.DataTableToList<T>();
            }
            else
            {
                return new List<T>();
            }
        }


        public T GetAllMultipleTable(RuleParameters ruleParameters)
        {
            var tClass = typeof(T);
            T objCaller = (T)Activator.CreateInstance(tClass);

            //RuleParameters ruleParameters = (RuleParameters)_ruleParameterProperty.GetValue(this);
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(DataSet))
            {
                var propertiesInfo = typeof(T).GetProperties().ToList();

                //var isMultipleTable = propertiesInfo.Where(p => p.GetType().GetGenericArguments()[0].IsAssignableFrom(typeof(IEntity))).ToList();
                DataSet dsResult = (DataSet)result;

                int tableIndex = 0;
                foreach (PropertyInfo entityClass in propertiesInfo)
                {
                    if (tableIndex > dsResult.Tables.Count - 1) break;

                    var lstclasses = entityClass.PropertyType.GetGenericArguments();
                    if (lstclasses.Count() > 0)
                    {
                        var interfaceImplemented = lstclasses.Where(p => p.GetInterfaces().Contains(typeof(IEntity)));

                        if (interfaceImplemented.Count() > 0)
                        {
                            DataTable dt = dsResult.Tables[tableIndex];// Some times firs table come as column name null so needs to skip that table until find actual table.
                            var isNullColumns = dt.Columns.Cast<DataColumn>().Where(p => p.ColumnName.ToLower() == "null").ToList();
                            while (isNullColumns.Count > 0)
                            {
                                tableIndex++;
                                dt = dsResult.Tables[tableIndex];
                                isNullColumns = dt.Columns.Cast<DataColumn>().Where(p => p.ColumnName.ToLower() == "null").ToList();
                            }

                            if (isNullColumns.Count == 0)
                            {
                                MethodInfo MI = typeof(Helper).GetMethod("GetListFromDataTable");
                                MI = MI.MakeGenericMethod(new[] { lstclasses[0] });
                                var lstItems = MI.Invoke(null, new[] { dt });
                                entityClass.SetValue(objCaller, lstItems);
                                tableIndex++;
                            }

                        }
                    }
                }
            }
            else if(result.GetType() == typeof(String))
            {
                POSLogger.Info("GetAllMultipleTable Error:" + result);
            }

            return objCaller;
        }

        public T GetSingleRecord(RuleParameters ruleParameters)
        {
            DataTable dt = new DataTable();
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(String))
            {
                POSLogger.Error(result);
                throw new Exception(result.ToString());
            }
            if (result.GetType() == typeof(DataSet))
            {
                DataSet dsResult = (DataSet)result;
                dt = dsResult.Tables[0];

            }
            return dt.DataTableToList<T>().FirstOrDefault();
            // return T;
        }

        public void Create(RuleParameters ruleParameters)
        {
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
        }

        public string Create_API(RuleParameters ruleParameters)
        {
            DataTable dt = null;
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(DataSet))
            {
                DataSet dsResult = (DataSet)result;
                dt = dsResult.Tables[0];
            }            
            return dt.Rows[0]["id"].ToString();
        }
        public string ChkDefaultLoc(RuleParameters ruleParameters)
        {
            DataTable dt = null;
             DataTable dt1 = null;
             DataTable dt2 = null;
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(DataSet))
            {
                DataSet dsResult = (DataSet)result;
                dt = dsResult.Tables[0];
                dt1 = dsResult.Tables[1];
                dt2 = dsResult.Tables[2];
            }
            if(dt.Rows.Count>0 && dt1.Rows.Count>0)
                return string.Join("|", dt.Rows[0]["result"].ToString(), dt1.Rows[0]["str"].ToString(), dt2.Rows[0]["id"].ToString());
            else
                return string.Join("|", "0", "0","0");
        }
        public void Update(RuleParameters ruleParameters)
        {
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
        }
        public void Delete(RuleParameters ruleParameters)
        {
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
        }

        public List<T> GetCompanies()
        {
            Companies cmps = new Companies();
            IList<BusinessObjects.Utils.Companies> lstCompanies =
                cmps.getBusinnessObjectCollection(
                    new BusinessObjects.Utils.Companies()).ToList();

            Mapper.CreateMap<BusinessObjects.Utils.Companies, AccountCompany>();

            var list = Mapper.Map<IList<BusinessObjects.Utils.Companies>, IList<T>>(lstCompanies);

            return list.ToList();
        }

        public T GetLoggedInUser(string Password, int CompanyId)
        {
            BusinessObjects.Login.SystemUser sysUsr = null;
            // Business Access Layer User
            ApplicationLayers.BAL.Login.SystemUser BALSysuser
                = new ApplicationLayers.BAL.Login.SystemUser();

            // Try and log the user in
            sysUsr = BALSysuser.userLogin(Password, CompanyId);

            Mapper.CreateMap<BusinessObjects.Login.SystemUser, AccountUser>();

            var loggedInUser = Mapper.Map<BusinessObjects.Login.SystemUser, T>(sysUsr);

            return loggedInUser;
        }

        private NameValueCollection PrepareParameters(RuleParameters ruleParameters)
        {
            NameValueCollection lstofParameters = new NameValueCollection();
            lstofParameters.Add(CoreParameters._RULENAME.AsText(), ruleParameters.RuleName.ToLower());
            lstofParameters.Add(CoreParameters._RULEFILE.AsText(), ruleParameters.RuleFile.ToString().ToLower());
            lstofParameters.Add(CoreParameters._ACTION.AsText(), ruleParameters.Action.ToString().ToLower());
            if (!string.IsNullOrEmpty(ruleParameters.Filter)) lstofParameters.Add(CoreParameters._FILTER.AsText(), ruleParameters.Action.ToString().ToLower());
            if (!string.IsNullOrEmpty(ruleParameters.SortBy)) lstofParameters.Add(CoreParameters._SORT.AsText(), ruleParameters.SortBy.ToString());
            if (ruleParameters.SortOrder != null) lstofParameters.Add(CoreParameters._ORDER.AsText(), ruleParameters.SortOrder.ToString().ToLower());
            if (ruleParameters.Offset >= 0) lstofParameters.Add(CoreParameters._OFFSET.AsText(), ruleParameters.Offset.ToString());
            if (ruleParameters.Rows > 0) lstofParameters.Add(CoreParameters._ROWS.AsText(), ruleParameters.Rows.ToString());
            if (ruleParameters.customParameters != null && ruleParameters.customParameters.Count > 0)
            {
                lstofParameters.Add(ruleParameters.customParameters);
            }

            return lstofParameters;
        }

        public string SendOrderEmail(string orderNo, string userid, string templateString,string subjectCompanyName,string emailFrom, string emailTo, bool enableSSL, string smtpAddress, int portNumber, string smtpUsername, string smtpPassword)
        {
            string mailBCC = string.Empty;
            string mailSubject = string.Empty;
            string mailBody = string.Empty;
            string mailCC = string.Empty;
            string retValue=string.Empty;
            if (!string.IsNullOrEmpty(orderNo))
            {
                ApplicationLayers.BAL.Email.Email objEmail = new ApplicationLayers.BAL.Email.Email();
                bool valOrderWiseEmailSetting = objEmail.orderWiseEmailSetting(orderNo);


                ////  emailTo = "chanchal2u@gmail.com"; // static email id 
                //if (!string.IsNullOrEmpty(objEmail.getCompanyEmailAddress(orderNo)))
                //{
                //    mailBCC = objEmail.getCompanyEmailAddress(orderNo);
                //}

                mailSubject = String.Format("{0}: Order # {1} is Confirmed", subjectCompanyName, orderNo);

                //LinkedResource inlineLogo = new LinkedResource(Server.MapPath("~/images/homepage/ribbonlogo.png"));
                //inlineLogo.ContentId = Guid.NewGuid().ToString();
                //string logo = string.Format(@"<img border=""0"" src=""cid:{0}"" alt=""www.ezliving.ie"" class=""CToWUd"">", inlineLogo.ContentId);
                mailBody = objEmail.MailBody(orderNo, templateString, string.Empty, string.Empty,string.Empty,string.Empty);
                AlternateView view = AlternateView.CreateAlternateViewFromString(mailBody, null, "text/html");
                //view.LinkedResources.Add(inlineLogo);
                string[] attachedFilePath = null;
                retValue = objEmail.sendMail(emailFrom, emailTo, mailCC, mailBCC, mailBody, mailSubject, attachedFilePath, enableSSL, smtpAddress, portNumber, smtpUsername, smtpPassword, view);
                if (string.IsNullOrEmpty(retValue))
                {
                    return string.Format("Email sent, order {0}, customer {1}, email address {2}", orderNo, objEmail.getCustomerName(), emailTo);
                }

            }
            return retValue;
        }
        public string GetDefaultLocationForBarCode(RuleParameters ruleParameters)
        {
            DataTable dt = null;           
            NameValueCollection lstParameters = PrepareParameters(ruleParameters);
            Library app = new Library();
            var result = app.WorkflowPOSUpdated(lstParameters);
            if (result.GetType() == typeof(DataSet))
            {
                DataSet dsResult = (DataSet)result;
                dt = dsResult.Tables[0];               
            }
            return  dt.Rows[0]["result"].ToString();
        }
    }
}
