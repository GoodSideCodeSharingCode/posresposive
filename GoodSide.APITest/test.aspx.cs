﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WooCommerceNET;
using WooCommerceNET.WooCommerce;

namespace GoodSide.APITest
{
    public partial class test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {

            }
        }

        private async void AddProudctToWooCommerce()
        {
            RestAPI rest = new RestAPI("http://ssf.sharpmonkeys.co.uk/wp-json/wc/v1/", "ck_416a1f074f9ef016e7eff7c9949c82e3876ada94", "cs_49b073a9a3b2a8bf5682dab7655aac71d2795b62");
            WCObject wc = new WCObject(rest);

            CategoryList catList = new CategoryList();
            Category objCat = new Category();
            objCat.name = "my test cat";
            objCat.slug = "my test cat";

            catList.Add(objCat);
            //Add new product
            WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product()
            {
                name = "Product 1",
                description = "Product 1",
                price = Convert.ToDecimal(5),
                sku = "Product 1",
                in_stock = true,
                categories = catList,
                catalog_visibility = "catalog",
                slug = "Product 1",
                stock_quantity = Convert.ToInt32(10),
                weight = Convert.ToDecimal(5),
                type = "simple"
            };
            var addProduct = await wc.PostProduct(p);
            
        }

        protected void btnAddProduct_Click(object sender, EventArgs e)
        {
            AddProudctToWooCommerce();
        }
    }
}
