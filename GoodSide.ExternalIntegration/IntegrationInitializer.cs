﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ninject;
using System.Reflection;
using GoodSide.ExternalIntegration.Magento;
using GoodSide.ExternalIntegration.General;

namespace GoodSide.ExternalIntegration
{
    public class IntegrationInitializer
    {
        public void Init()
        {
            try
            {
                System.Threading.Thread.Sleep(10000);

                //ConfigureNinject();           
                LogHelper.EnsureInitialized();
                PluginCollection objPluginsFac = new PluginCollection(); // Plugins collection
                List<IIntegrationProvider> objListPlugins = objPluginsFac.GetPluginCollection(); // read plugin colection from plugin factory                
                
                foreach (var plugin in objListPlugins)
                {

                    plugin.SchedulePushPull();

                    //if (plugin.GetType() == typeof(Stock))
                    //{
                    //    IIntegrationProvider ninjectObject = IocKernel.Get<Stock>();
                    //    ninjectObject.SchedulePushPull();
                    //}
                    //else if (plugin.GetType() == typeof(Order))
                    //{
                    //    IIntegrationProvider ninjectObject = IocKernel.Get<Order>();
                    //    ninjectObject.SchedulePushPull();
                    //}
                    //else if (plugin.GetType() == typeof(WooCommerce.Stock))
                    //{
                    //    IIntegrationProvider ninjectObject = IocKernel.Get<WooCommerce.Stock>();
                    //    ninjectObject.SchedulePushPull();
                    //}            
                }
            }
            catch (Exception)
            {
            }
        }

        private void ConfigureNinject()
        {

        }
    }
}
