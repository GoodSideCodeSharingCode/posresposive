﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace GoodSide.ExternalIntegration
{
    public partial class ExternalAPIIntegratior : ServiceBase
    {
        private IntegrationInitializer objIntegrationInitialize; 
        public ExternalAPIIntegratior()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {               

                objIntegrationInitialize = new IntegrationInitializer();

                System.Threading.Thread mThread;
                System.Threading.ThreadStart mThradStart = new System.Threading.ThreadStart(objIntegrationInitialize.Init);
                mThread = new System.Threading.Thread(mThradStart);
                mThread.Start();
            }
            catch
            {

            }
        }

        protected override void OnStop()
        {
        }
    }
}
