﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.ExternalIntegration
{
    /// <summary>
    /// Plugin collection class which is used to collect
    /// all plugins which needs to be loaded for
    /// this application. It will find all plugins inside
    /// monitor folder and load it.
    /// </summary>
    public class PluginCollection
    {
        [ImportMany]
        private Lazy<IIntegrationProvider, IDictionary<string, object>>[] PluginsList { get; set; }

        /// <summary>
        /// Constructor used to load all plugins
        /// dll in memory and read interface information
        /// </summary>
        public PluginCollection()
        {
            var Catalog = new AggregateCatalog();
            var assemblyCatalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            Catalog.Catalogs.Add(assemblyCatalog);
            var Container = new CompositionContainer(Catalog);
            Container.ComposeParts(this);
        }

        /// <summary>
        /// Once all plugins loaded into memory read it inside interface
        /// list and return it. this method used inside service to load 
        /// all plugins informations
        /// </summary>
        /// <returns></returns>
        public List<IIntegrationProvider> GetPluginCollection()
        {
            List<IIntegrationProvider> collPlugins = new List<IIntegrationProvider>();
            foreach (var PluginObj in PluginsList)
            {
                collPlugins.Add(PluginObj.Value);
            }
            return collPlugins;
        }
    }
}
