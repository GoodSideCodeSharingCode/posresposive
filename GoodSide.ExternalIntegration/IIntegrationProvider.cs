﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodSide.ExternalIntegration
{
    public enum AllowedOperation
    {
        Push,
        Pull,
        ExportCSV,
        All,
        PushStockLevel
    }

    public interface IIntegrationProvider
    {
        string ProviderName { get; }

        Guid ProviderID { get; }

        int PushInterval { get; set; }

        int PullInterval { get; set; }

        bool ReadConfiguration();

        void SchedulePushPull();

        void PushData(object args);

        void PullData(object args);

        AllowedOperation alloweOperation { get; set; }
    }
}
