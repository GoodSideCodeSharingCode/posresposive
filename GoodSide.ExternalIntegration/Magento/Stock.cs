﻿using Magento.RestApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GoodSide.Domain;
using GoodSide.Domain.Services;
using System.ComponentModel.Composition;
using Ninject;
using GoodSide.Domain.Entities;
using RestSharp;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Net;
using GoodSide.ExternalIntegration.General;
using WinSCP;
using System.Configuration;
using System.Reflection;
using System.Globalization;

namespace GoodSide.ExternalIntegration.Magento
{
    [Export(typeof(IIntegrationProvider))]
    public class Stock : IIntegrationProvider
    {
        #region "General Parameters"

        private IStockItemService _stockItemService;

        private static bool PullinProcess = false;

        private static bool PushinProcess = false;

        private static bool ExportinProcess = false;

        private static bool PushStockLevelinProcess = false;

        Timer pushCheck = null;

        Timer pullCheck = null;

        Timer exportCSVCheck = null;

        Timer pushStockLevelCheck = null;

        public int PushInterval
        {
            get;
            set;
        }

        public int PullInterval
        {
            get;
            set;
        }

        public int ExportCSVInternal
        {
            get;
            set;
        }

        public AllowedOperation alloweOperation
        {
            get;
            set;
        }

        public string ProviderName
        {
            get { return "Stock"; }
        }

        public Guid ProviderID
        {
            get { return new Guid("370A8AE7-458A-429D-9062-AB0EB001EDC7"); }
        }

        public string MagentoURL { get; set; }

        public string ConsumerKey { get; set; }

        public string ConsumerSecret { get; set; }

        public string AccessTokenKey { get; set; }

        public string AccessTokenSecret { get; set; }

        public string FtpURL { get; set; }

        public string FtpFolderName { get; set; }

        public string FtpImageFolderName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public int PortNo { get; set; }

        public string SshHostKeyFingerprint { get; set; }

        public string FtpImageCSVFolderName { get; set; }

        public string MagentoExportCSVTime { get; set; }

        public string LastProductExportRun { get; set; }

        public string LastImageExportRun { get; set; }

        public string MagentoFtpInventoryFolderName { get; set; }

        public int PushStockLevelInterval
        {
            get;
            set;
        }

        public string ServiceBindToChannel{ get; set; }


        #endregion

        #region "Constructor to set default values"

        public Stock()
        {

        }

        [Inject]
        public Stock(IStockItemService stockItemService)
        {
            _stockItemService = stockItemService;
            PushInterval = 18000000;
            PullInterval = 18000000;
            PushStockLevelInterval = 600000;
            alloweOperation = AllowedOperation.All;
        }

        #endregion

        #region "General Methods"

        public bool ReadConfiguration()
        {
            try
            {
                alloweOperation = AllowedOperation.All;

                PushInterval = PullInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MagentoPushPullInterval"]);
                ExportCSVInternal = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MagentoExportInterval"]);

                MagentoURL = System.Configuration.ConfigurationManager.AppSettings["MagentoURL"];
                ConsumerKey = System.Configuration.ConfigurationManager.AppSettings["MagentoConsumerKey"];
                ConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["MagentoConsumerSecret"];
                AccessTokenKey = System.Configuration.ConfigurationManager.AppSettings["MagentoAccessTokenKey"];
                AccessTokenSecret = System.Configuration.ConfigurationManager.AppSettings["MagentoAccessTokenSecret"];

                FtpURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoHostname"]);
                FtpFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoFtpFolderName"]);
                Username = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoUsername"]);
                Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoPassword"]);
                PortNo = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MagentosFtpPort"]);
                SshHostKeyFingerprint = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoSshHostKeyFingerprint"]);
                FtpImageFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoFtpImageFolderName"]);
                FtpImageCSVFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MagentoFtpImageCSVFolderName"]);

                MagentoExportCSVTime = System.Configuration.ConfigurationManager.AppSettings["MagentoExportCSVTime"];
                LastProductExportRun = System.Configuration.ConfigurationManager.AppSettings["LastProductExportRun"];
                LastImageExportRun = System.Configuration.ConfigurationManager.AppSettings["LastImageExportRun"];
                PushStockLevelInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MagentoPushStockLevelInterval"]);
                MagentoFtpInventoryFolderName = System.Configuration.ConfigurationManager.AppSettings["MagentoFtpInventoryFolderName"];

                return true;

            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Read Configuration issue", ex);
                return false;
            }
        }

        public void SchedulePushPull()
        {
            //Deploying for the Woocommerde so avoid this operation  

            ServiceBindToChannel = System.Configuration.ConfigurationManager.AppSettings["ServiceBindToChannel"];

            if (ServiceBindToChannel.ToLower() == "magento")
            {


                if (ReadConfiguration())
                {
                    if (alloweOperation == AllowedOperation.Push || alloweOperation == AllowedOperation.All)
                    {
                        //start pull process at regular interval
                        TimerCallback callback = PerformPushOperation;
                        pushCheck = new Timer(callback);
                    pushCheck.Change(TimeSpan.FromMilliseconds(PushInterval), TimeSpan.FromMilliseconds(PushInterval));
                        //pushCheck = new Timer(new TimerCallback(PushData), null, System.Threading.Timeout.Infinite, PushInterval);
                        //pushCheck.Change(PushInterval, PushInterval);
                    }

                    if (alloweOperation == AllowedOperation.PushStockLevel || alloweOperation == AllowedOperation.All)
                    {
                        //start pull process at regular interval
                        TimerCallback callback = PerformPushStockLevelOperation;
                        pushStockLevelCheck = new Timer(callback);
                        pushStockLevelCheck.Change(TimeSpan.FromMilliseconds(PushStockLevelInterval), TimeSpan.FromMilliseconds(PushStockLevelInterval));
                        //pushCheck = new Timer(new TimerCallback(PushData), null, System.Threading.Timeout.Infinite, PushInterval);
                        //pushCheck.Change(PushInterval, PushInterval);
                    }

                    if (alloweOperation == AllowedOperation.Pull || alloweOperation == AllowedOperation.All)
                    {
                        //start push process at regular interval
                        TimerCallback callback = PerformPullOperation;
                        pullCheck = new Timer(callback);
                    pullCheck.Change(TimeSpan.FromMilliseconds(PullInterval), TimeSpan.FromMilliseconds(PullInterval));
                        //pullCheck = new Timer(new TimerCallback(PullData), null, System.Threading.Timeout.Infinite, PullInterval);
                        //pullCheck.Change(PullInterval, PullInterval);
                    }

                    if (alloweOperation == AllowedOperation.ExportCSV || alloweOperation == AllowedOperation.All)
                    {
                        //start push process at regular interval
                        TimerCallback callback = PerformExportOperation;
                        exportCSVCheck = new Timer(callback);
                    exportCSVCheck.Change(TimeSpan.FromMilliseconds(ExportCSVInternal), TimeSpan.FromMilliseconds(ExportCSVInternal));
                        //exportCSVCheck = new Timer(new TimerCallback(GenerateCSV), null, System.Threading.Timeout.Infinite, ExportCSVInternal);
                        //exportCSVCheck.Change(ExportCSVInternal, ExportCSVInternal);
                    }
                }
            }
            else
            {
                LogHelper.Info(typeof(Stock), "magento channel is not currently bind to service");
                return;
            }
        }

        private bool CheckExportTime()
        {

            string[] Exporttime = MagentoExportCSVTime.Split(':');
            TimeSpan OperationStartTime = new TimeSpan(Convert.ToInt32(Exporttime[0]), Convert.ToInt32(Exporttime[1]), Convert.ToInt32(Exporttime[2]));
            TimeSpan timeNow = DateTime.Now.TimeOfDay;
            TimeSpan currentTime = new TimeSpan(timeNow.Hours, timeNow.Minutes, timeNow.Seconds);
            if (currentTime >= OperationStartTime)
                return true;
            else
                return false;
        }

        private void PerformPushOperation(object state)
        {
            CultureInfo culture = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
            if (!PushinProcess)
                PushData(null);
            else
                LogHelper.Error(typeof(Stock), " Push already in process so aborting another process");
        }
        private void PerformPullOperation(object state)
        {
            if (!PullinProcess)
                PullData(null);
            else
                LogHelper.Error(typeof(Stock), "Pull already in process so aborting another  process");
        }
        private void PerformExportOperation(object state)
        {
           
            CultureInfo culture = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
           
            if (CheckExportTime() && DateTime.Now.Date != Convert.ToDateTime(LastProductExportRun).Date)
            {
                if (!ExportinProcess)
                    GenerateCSV(null);
                else
                    LogHelper.Error(typeof(Stock), "Export already in process so aborting another process");
            }
            else
            {
                var src = DateTime.Now;
                LogHelper.Error(typeof(Stock), "Export Time is " + MagentoExportCSVTime + " and current time is " + src.Hour + ":" + src.Minute + ":" + src.Second);
                LogHelper.Error(typeof(Stock), "Last Product Export Run Time " + LastProductExportRun);
            }
        }
        private void PerformPushStockLevelOperation(object state)
        {
            CultureInfo culture = new CultureInfo("en-GB");
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            if (!PushStockLevelinProcess)
                PushStockLevel(null);
            else
                LogHelper.Error(typeof(Stock), "Push Stock Level already in process so aborting another  process");
        }

        #endregion

        #region "Push  Operation"

        public void GenerateCSV(object args)
        {
                        
            try
            {
                ExportinProcess = true;
                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateMagento"];
                string offset= System.Configuration.ConfigurationManager.AppSettings["MagentoOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["MagentoPageSize"];
                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;

                //var requestStockCountforMagento = new RestRequest("api/pos/GetStockDetailCountforMagento?offset=0&pageSize=10", Method.GET);
                //requestStockCountforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                //requestStockCountforMagento.AddHeader("Authorization", accessToken.ToString());
                //IRestResponse<StockItemMagento> stockInfoCountResponseforMagento = goodSideClient.Execute<StockItemMagento>(requestStockCountforMagento);
                //if (stockInfoCountResponseforMagento.Data.pagingInfo.FirstOrDefault().t > 0)
                //{
                //var CheckPriceUpdated = new RestRequest("api/pos/UpdateSpecialPriceForWebOutput", Method.GET);
                //CheckPriceUpdated.AddParameter("targetDate", DateTime.Now.ToString("dd/MM/yyyy"), ParameterType.QueryString);
                //CheckPriceUpdated.AddHeader("Authorization", accessToken.ToString());
                //IRestResponse<GeneralRuleResult> specialPriceUpdated = goodSideClient.Execute<GeneralRuleResult>(CheckPriceUpdated);


                //if (specialPriceUpdated.Data.status == "success")
                //{
                //Get Data with 3 Steps 10000
                //var requestStockforMagentoPage1 = new RestRequest("api/pos/GetStockDetailforMagento?offset=0&pageSize=5000", Method.GET);

                //requestStockforMagentoPage1.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                //requestStockforMagentoPage1.AddHeader("Authorization", accessToken.ToString());
                //IRestResponse<StockItemMagento> stockInfoResponseforMagento1 = goodSideClient.Execute<StockItemMagento>(requestStockforMagentoPage1);

                //var requestStockforMagentoPage2 = new RestRequest("api/pos/GetStockDetailforMagento?offset=5000&pageSize=5000", Method.GET);

                //requestStockforMagentoPage2.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                //requestStockforMagentoPage2.AddHeader("Authorization", accessToken.ToString());
                //IRestResponse<StockItemMagento> stockInfoResponseforMagento2 = goodSideClient.Execute<StockItemMagento>(requestStockforMagentoPage2);


                //var stockItmsInfoPage1 = stockInfoResponseforMagento1.Data.stockItemInfoforMagento;
                //var stockItmsInfoPage2 = stockInfoResponseforMagento2.Data.stockItemInfoforMagento;
                //stockItmsInfoPage1.AddRange(stockItmsInfoPage2);

                var requestStockforMagento = new RestRequest("api/pos/GetStockDetailforMagento?offset=" + offset + "&pageSize=" + pagesize, Method.GET);

                requestStockforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforMagento.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemMagento> stockInfoResponseforMagento = goodSideClient.Execute<StockItemMagento>(requestStockforMagento);
                var stockItmsInfo = stockInfoResponseforMagento.Data.stockItemInfoforMagento;

                LogHelper.Info(typeof(Stock), "GenerateCSV Stock Total:" + stockItmsInfo.Count);

                List<CSVProduct> objListOfProduct = new List<CSVProduct>();
                CSVProduct csvProductInfo;

                string timestamp = DateTime.Now.ToString("MMddyyHHmm");
                string ftpImagePath = "/" + FtpImageFolderName;
                Dictionary<string, string> SkuImagePath =
                                        new Dictionary<string, string>();

                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                foreach (var ordoriteitem in stockItmsInfo)
                {
                    csvProductInfo = new CSVProduct();

                    //Set default values
                    string path = Path.Combine(rootPath, "DefaultProductImportValues_Magento.xml");
                    XmlSerializer serializer = new XmlSerializer(typeof(CSVProduct));
                    StreamReader reader = new StreamReader(path);
                    csvProductInfo = (CSVProduct)serializer.Deserialize(reader);
                    reader.Close();

                    //Map Ordorite database information
                    csvProductInfo.sku = ordoriteitem.sku.ToString();  // Need TO BE Change 

                    csvProductInfo.categories = ordoriteitem.categories;

                    csvProductInfo.name = ordoriteitem.name;

                    csvProductInfo.description = ordoriteitem.description; // this field need to be changes after created new fields web_long_description

                    csvProductInfo.short_description = ordoriteitem.short_description;
                    //csvProductInfo.Weight = ordoriteitem.Weight;// To be read from database

                    csvProductInfo.price = ordoriteitem.price;

                    csvProductInfo.special_price = ordoriteitem.special_price; // to be read from database. Needss to take based on applied promotion

                    if (!string.IsNullOrEmpty(ordoriteitem.special_price))
                        csvProductInfo.special_price_from_date = (ordoriteitem.special_price_from_date == DateTime.MinValue ? string.Empty : ordoriteitem.special_price_from_date.ToString("MM/dd/yy HH:mm", CultureInfo.InvariantCulture)); // to be read from database. Needs to take based on applied promotion and promotion start date
                    if (!string.IsNullOrEmpty(ordoriteitem.special_price))
                        csvProductInfo.special_price_to_date = (ordoriteitem.special_price_to_date == DateTime.MinValue ? string.Empty : ordoriteitem.special_price_to_date.ToString("MM/dd/yy HH:mm", CultureInfo.InvariantCulture)); // to be read from database. Needs to take based on applied promotion and promotion end date.

                    //csvProductInfo.Created_at = DateTime.Now.ToString(); // To be read fromd database

                    //csvProductInfo.Updated_at = DateTime.Now.ToString(); // To be read fromd database

                    //csvProductInfo.Msrp_price = ordoriteitem.spnow;                  

                    //csvProductInfo.ImagesPaths = ordoriteitem.imagepaths == null ? string.Empty : (ordoriteitem.imagepaths.Replace("productimages", ftpImagePath + "/" + ordoriteitem.part_no)).Replace(',', '|');

                    csvProductInfo.qty = (string.IsNullOrEmpty(ordoriteitem.qty) ? "0" : Convert.ToInt32(ordoriteitem.qty) > 0 ? ordoriteitem.qty : "0");

                    csvProductInfo.is_in_stock = ordoriteitem.is_in_stock;//  To be set as per unallocatedstocklevel data

                    csvProductInfo.allow_backorders = ordoriteitem.allow_backorders;

                    csvProductInfo.product_online = ordoriteitem.product_online;

                    if (ordoriteitem.created_at > DateTime.MinValue)
                        csvProductInfo.created_at = ordoriteitem.created_at.ToString("MM/dd/yy HH:mm",CultureInfo.InvariantCulture); //ordoriteitem.stockcreateddate;
                    if (ordoriteitem.updated_at > DateTime.MinValue)
                        csvProductInfo.updated_at = ordoriteitem.updated_at.ToString("MM/dd/yy HH:mm", CultureInfo.InvariantCulture);//ordoriteitem.stockmodifieddate;
                    if (ordoriteitem.new_from_date > DateTime.MinValue)
                        csvProductInfo.new_from_date = ordoriteitem.new_from_date.ToString("MM/dd/yy HH:mm", CultureInfo.InvariantCulture);
                    if (ordoriteitem.new_to_date > DateTime.MinValue)
                        csvProductInfo.new_to_date = ordoriteitem.new_to_date.ToString("MM/dd/yy HH:mm", CultureInfo.InvariantCulture);

                    csvProductInfo.tax_class_name = ordoriteitem.tax_class_name;

                    csvProductInfo.url_key = ordoriteitem.url_key;

                    csvProductInfo.meta_title = ordoriteitem.meta_title;

                    csvProductInfo.meta_keywords = ordoriteitem.meta_keywords;

                    csvProductInfo.meta_description = ordoriteitem.meta_description;

                    csvProductInfo.visibility = ordoriteitem.visibility;

                    csvProductInfo.out_of_stock_qty = ordoriteitem.out_of_stock_qty;

                    csvProductInfo.additional_attributes = ordoriteitem.additional_attribute;

                    objListOfProduct.Add(csvProductInfo);

                    if (!SkuImagePath.ContainsKey(ordoriteitem.part_no))
                        SkuImagePath.Add(ordoriteitem.part_no, ordoriteitem.imagepaths);
                    else
                    {
                        var currentimagepaths = SkuImagePath[ordoriteitem.part_no];
                        currentimagepaths += "|" + ordoriteitem.imagepaths;
                        SkuImagePath[ordoriteitem.part_no] = currentimagepaths;
                    }
                }

                csvProductInfo = new CSVProduct();
                var output = csvProductInfo.ToCsvHeader();
                output += Environment.NewLine;

                objListOfProduct.ForEach(csvProduct =>
                {
                    output += csvProduct.ToCsvRow();
                    output += Environment.NewLine;
                });

                var CSVFileWithData = Path.Combine(rootPath, "catalogue.csv");              

                if (DateTime.Now.Date != Convert.ToDateTime(LastImageExportRun).Date)
                    GenerateImageCSV(null);

                if (objListOfProduct.Count >= 1)
                {
                    System.IO.File.WriteAllText(CSVFileWithData, output);

                    bool IsSuccess = sftpUpload(CSVFileWithData,FtpFolderName);

                    //ftpimageUpload(SkuImagePath);

                    //If images are doesnt exist on specified path it will not be uploaded. 
                    //Any exception generated due to missing files on system is ignored.
                    //sftpimageUpload(SkuImagePath);                  

                    if (IsSuccess)
                    {
                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                        // Add an Application Setting.
                        string LastProductExport = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        config.AppSettings.Settings["LastProductExportRun"].Value = LastProductExport;
                        config.Save(ConfigurationSaveMode.Modified, true);
                        // Force a reload of a changed section.
                        ConfigurationManager.RefreshSection("appSettings");

                        LastProductExportRun = LastProductExport;


                        //if ((System.IO.File.Exists(CSVFileWithData)))
                        //{
                        //    System.IO.File.Delete(CSVFileWithData);
                        //}
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Generate CSV issue", ex);
            }
            finally
            {
                ExportinProcess = false;
            }
        }

        public void GenerateImageCSV(object args)
        {
            try
            {


                ExportinProcess = true;
                LogHelper.Info(typeof(Stock), "GenerateImageCSV Called");
                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];
                string LastImageSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastImageExportRun"];

                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;

                var requestStockImageforMagento = new RestRequest("api/pos/GetStockImageDetailforMagento", Method.GET);
                requestStockImageforMagento.AddParameter("LastSyncDate", LastImageSyncDate, ParameterType.QueryString);
                requestStockImageforMagento.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemImageMagento> stockInfoResponseforMagento = goodSideClient.Execute<StockItemImageMagento>(requestStockImageforMagento);

                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                var stockImageItmsInfo = stockInfoResponseforMagento.Data.stockItemImageInfoforMagento;
                LogHelper.Info(typeof(Stock), "GenerateImageCSV Stock Count:" + stockImageItmsInfo.Count);

                List<CSVProductImage> objListOfProduct = new List<CSVProductImage>();
                CSVProductImage csvProductImageInfo;

                string timestamp = DateTime.Now.ToString("MMddyyHHmm");
                string ftpImagePath = "/" + FtpImageFolderName;
                Dictionary<string, CSVProductImage> SkuImagePath =
                                        new Dictionary<string, CSVProductImage>();

                foreach (var ordoriteitem in stockImageItmsInfo)
                {
                    csvProductImageInfo = new CSVProductImage();

                    ////Set default values
                    //string path = Path.Combine(rootPath, "DefaultProductImportValues_Magento.xml");
                    //XmlSerializer serializer = new XmlSerializer(typeof(CSVProduct));
                    //StreamReader reader = new StreamReader(path);
                    //csvProductInfo = (CSVProduct)serializer.Deserialize(reader);
                    //reader.Close();

                    //Map Ordorite database information
                    csvProductImageInfo.sku = ordoriteitem.sku.ToString();  // Need TO BE Change 

                    //This all fields mapped as per latest SP send by Donal
                    csvProductImageInfo.base_image = ordoriteitem.base_image;

                    csvProductImageInfo.base_image_label = ordoriteitem.base_image_label;

                    csvProductImageInfo.small_image = ordoriteitem.small_image; // 

                    csvProductImageInfo.small_image_label = ordoriteitem.small_image_label;

                    csvProductImageInfo.thumbnail_image = ordoriteitem.thumbnail_image;

                    csvProductImageInfo.thumbnail_image_label = ordoriteitem.thumbnail_image_label;

                    csvProductImageInfo.additional_images = ordoriteitem.additional_images;

                    csvProductImageInfo.additional_images_label = ordoriteitem.additional_images_label;

                    objListOfProduct.Add(csvProductImageInfo);

                    SkuImagePath.Add(ordoriteitem.sku, csvProductImageInfo);

                }

                csvProductImageInfo = new CSVProductImage();
                var output = csvProductImageInfo.ToCsvHeader();
                output += Environment.NewLine;

                objListOfProduct.ForEach(csvProductImage =>
                {
                    output += csvProductImage.ToCsvRow();
                    output += Environment.NewLine;
                });

                var CSVFileWithData = Path.Combine(rootPath, "images_" + timestamp) + ".csv";

                if (objListOfProduct.Count >= 1)
                {
                    System.IO.File.WriteAllText(CSVFileWithData, output);

                    bool IsSuccess = sftpImageCsvUpload(CSVFileWithData);

                    sftpimageUpload(SkuImagePath);
                    if (IsSuccess)
                    {
                        //If images are doesnt exist on specified path it will not be uploaded. 
                        //Any exception generated due to missing files on system is ignored.    
                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                        // Add an Application Setting.
                        string LastImageExport = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        config.AppSettings.Settings["LastImageExportRun"].Value = LastImageExport;
                        config.Save(ConfigurationSaveMode.Modified, true);
                        // Force a reload of a changed section.
                        ConfigurationManager.RefreshSection("appSettings");

                        LastImageExportRun = LastImageExport;

                        //if ((System.IO.File.Exists(CSVFileWithData)))
                        //{
                        //    System.IO.File.Delete(CSVFileWithData);
                        //}
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Generate Image CSV issue", ex);
            }
        }

        public bool ftpimageUpload(Dictionary<string, string> SkuImagePaths)
        {
            try
            {
                CreateImageDirectory();
                foreach (KeyValuePair<string, string> SKU_Imagepath in SkuImagePaths)
                {

                    if (!string.IsNullOrEmpty(SKU_Imagepath.Value))
                    {
                        CreateSKUDirectory(SKU_Imagepath.Key);

                        string[] Images = SKU_Imagepath.Value.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (var image in Images)
                        {
                            string filepath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"] + image;
                            byte[] fileBytes = null;

                            string fileName = Path.GetFileName(filepath);

                            string fileextention = System.IO.Path.GetExtension(filepath);

                            using (StreamReader fileStream = new StreamReader(filepath))
                            {

                                fileBytes = File.ReadAllBytes(filepath);
                                fileStream.Close();
                            }

                            //Create FTP Request.
                            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpImageFolderName + "/" + SKU_Imagepath.Key + "/" + fileName); //add folder name aftre ftpurl
                            request.Method = WebRequestMethods.Ftp.UploadFile;

                            //Enter FTP Server credentials.
                            request.Credentials = new NetworkCredential(Username, Password);
                            request.ContentLength = fileBytes.Length;
                            request.KeepAlive = false;

                            using (Stream requestStream = request.GetRequestStream())
                            {
                                requestStream.Write(fileBytes, 0, fileBytes.Length);
                                requestStream.Close();
                            }
                            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                            response.Close();
                        }
                    }
                }
                return true;
            }
            catch (WebException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool sftpimageUpload(Dictionary<string, CSVProductImage> SkuImagePaths)
        {
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = FtpURL,
                    UserName = Username,
                    Password = Password,
                    PortNumber = PortNo,
                    SshHostKeyFingerprint = SshHostKeyFingerprint
                };

                using (Session session = new Session())
                {
                    session.Open(sessionOptions);

                    if (!sFtpDirectoryExists(session, FtpImageFolderName))
                        session.CreateDirectory(FtpImageFolderName);
                    foreach (KeyValuePair<string, CSVProductImage> SKU_Imagepath in SkuImagePaths)
                    {
                        var lstProperties = typeof(CSVProductImage).GetProperties().ToList();

                        foreach (var property in lstProperties)
                        {
                            string propertyName = property.Name;
                            if (propertyName == "base_image" || propertyName == "small_image" || propertyName == "thumbnail_image" || propertyName == "additional_images")
                            {
                                if (!string.IsNullOrEmpty(property.GetValue(SKU_Imagepath.Value).ToString()))
                                {
                                    string Imagepath = property.GetValue(SKU_Imagepath.Value).ToString();

                                    if (!sFtpDirectoryExists(session, FtpImageFolderName + SKU_Imagepath.Key))
                                        session.CreateDirectory(FtpImageFolderName + SKU_Imagepath.Key);

                                    string[] Images = Imagepath.Split(new string[] { "," }, StringSplitOptions.None);
                                    foreach (var image in Images)
                                    {
                                        string filepath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"] + image.Replace("/", "\\");
                                        if (System.IO.File.Exists(filepath))
                                        {
                                            byte[] fileBytes = null;

                                            string fileName = Path.GetFileName(filepath);


                                            // Upload files
                                            TransferOptions transferOptions = new TransferOptions();
                                            transferOptions.TransferMode = TransferMode.Binary;

                                            TransferOperationResult transferResult;
                                            transferResult = session.PutFiles(filepath, FtpImageFolderName + SKU_Imagepath.Key + "/", false, transferOptions);

                                            // Throw on any error
                                            transferResult.Check();
                                        }

                                    }
                                }
                            }
                        }
                    }
                }


                return true;
            }
            catch (WebException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool ftpUpload(string FiletoUpload)
        {
            try
            {

                byte[] fileBytes = null;

                string fileName = Path.GetFileName(FiletoUpload);

                string fileextention = System.IO.Path.GetExtension(FiletoUpload);

                CreateDirectory();

                using (StreamReader fileStream = new StreamReader(FiletoUpload))
                {

                    fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
                    fileStream.Close();
                }

                //Create FTP Request.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/" + fileName); //add folder name aftre ftpurl
                request.Method = WebRequestMethods.Ftp.UploadFile;

                //Enter FTP Server credentials.
                request.Credentials = new NetworkCredential(Username, Password);
                request.ContentLength = fileBytes.Length;
                request.KeepAlive = false;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileBytes, 0, fileBytes.Length);
                    requestStream.Close();

                }
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                response.Close();

                return true;
            }
            catch (WebException ex)
            {
                LogHelper.Error(typeof(Stock), "FTP upload issue", ex);
                return false;
            }
        }

        public bool sftpUpload(string FiletoUpload , string foldername)
        {
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = FtpURL,
                    UserName = Username,
                    Password = Password,
                    PortNumber = PortNo,
                    SshHostKeyFingerprint = SshHostKeyFingerprint
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    if (!sFtpDirectoryExists(session, foldername))
                        session.CreateDirectory(foldername);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(FiletoUpload, foldername, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        //Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                return false;
            }
        }

        public bool sftpImageCsvUpload(string FiletoUpload)
        {
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = FtpURL,
                    UserName = Username,
                    Password = Password,
                    PortNumber = PortNo,
                    SshHostKeyFingerprint = SshHostKeyFingerprint
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    if (!sFtpDirectoryExists(session, FtpImageCSVFolderName))
                        session.CreateDirectory(FtpImageCSVFolderName);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(FiletoUpload, FtpImageCSVFolderName, false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        //Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                return false;
            }
        }

        protected void CreateDirectory()
        {
            if (!DirectoryExists(FtpFolderName))
            {
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        protected void CreateImageDirectory()
        {
            if (!DirectoryExists(FtpImageFolderName))
            {
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpImageFolderName + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        public bool DirectoryExists(string FTPDirectory)
        {
            bool directoryExists;

            var filePath = FtpURL + "/" + FTPDirectory + "/";
            var request = (FtpWebRequest)FtpWebRequest.Create(filePath);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(Username, Password);

            try
            {
                using (request.GetResponse())
                {
                    directoryExists = true;
                }
            }
            catch (WebException)
            {
                directoryExists = false;
            }

            return directoryExists;
        }

        public bool sFtpDirectoryExists(Session session, string FolderName)
        {
            bool directoryExists = true;

            try
            {
                session.ListDirectory(FolderName);
            }
            catch (SessionRemoteException ex)
            {
                directoryExists = false;
            }

            return directoryExists;
        }

        protected void CreateSKUDirectory(string SKU)
        {
            if (!DirectoryExists((FtpImageFolderName + "/" + SKU)))
            {
                var ImageFolder = "/" + FtpImageFolderName + "/" + SKU;
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + ImageFolder + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        public void PushData(object args)
        {
            try
            {


                return;
                PushinProcess = true;
                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateMagento"];
                string offset = System.Configuration.ConfigurationManager.AppSettings["MagentoOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["MagentoPageSize"];
                //Authenticate
                var goodSideClient = new RestClient(apiURL);

                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;

                var requestStockforMagento = new RestRequest("api/pos/GetStockDetailforMagento?offset=" + offset + "&pageSize=" + pagesize, Method.GET);
                requestStockforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforMagento.AddHeader("Authorization", accessToken.ToString());
           
                IRestResponse<StockItemMagento> stockInfoResponseforMagento = goodSideClient.Execute<StockItemMagento>(requestStockforMagento);

                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var stockItmsInfo = stockInfoResponseforMagento.Data.stockItemInfoforMagento;

                StockItemInfoforMagento ordoriteitem = stockItmsInfo.First();

                CSVProduct csvProductInfo;
                csvProductInfo = new CSVProduct();

                //Set default values
                string path = Path.Combine(rootPath, "DefaultProductImportValues_Magento.xml");
                XmlSerializer serializer = new XmlSerializer(typeof(CSVProduct));
                StreamReader reader = new StreamReader(path);
                csvProductInfo = (CSVProduct)serializer.Deserialize(reader);
                reader.Close();

                //Map Ordorite database information
                csvProductInfo.sku = ordoriteitem.part_no;

                csvProductInfo.categories = ordoriteitem.categories;

                csvProductInfo.name = ordoriteitem.name;

                csvProductInfo.description = ordoriteitem.description;

                //csvProductInfo.Weight = ordoriteitem.Weight;// To be read from database

                csvProductInfo.price = ordoriteitem.price;

                csvProductInfo.special_price = ordoriteitem.special_price; // to be read from database. Needss to take based on applied promotion

                if (ordoriteitem.special_price_from_date != null)
                    csvProductInfo.special_price_from_date = ordoriteitem.special_price_from_date.ToShortDateString(); // to be read from database. Needs to take based on applied promotion and promotion start date
                if (ordoriteitem.special_price_to_date != null)
                    csvProductInfo.special_price_to_date = ordoriteitem.special_price_to_date.ToShortDateString(); // to be read from database. Needs to take based on applied promotion and promotion end date.

                //csvProductInfo.Created_at = DateTime.Now.ToString(); // To be read fromd database

                //csvProductInfo.Updated_at = DateTime.Now.ToString(); // To be read fromd database

                //csvProductInfo.Msrp_price = ordoriteitem.spnow;

                csvProductInfo.qty = ordoriteitem.qty; //  To be read from database


                global::Magento.RestApi.Models.Product magentoProduct = new global::Magento.RestApi.Models.Product();

                // Arrange
                var sku = "200000";
                // create product with minimal required fields
                var product = new global::Magento.RestApi.Models.Product
                {
                    description = "A long description of the new product",
                    short_description = "A short description of the new product",
                    price = 12.5,
                    sku = sku,
                    visibility = global::Magento.RestApi.Models.ProductVisibility.CatalogSearch,
                    status = global::Magento.RestApi.Models.ProductStatus.Enabled,
                    name = "New product",
                    weight = 10,
                    tax_class_id = 2,
                    type_id = "simple",
                    attribute_set_id = 4 // default
                };
                //var existingProduct = client.GetProductBySku(sku).Result;
                //if (existingProduct.Result != null)
                //{
                //    var delete = client.DeleteProduct(existingProduct.Result.entity_id).Result;
                //    if (delete.HasErrors) throw new Exception(delete.Errors.First().Message);
                //}

                //// Act
                //var responsetest = client.CreateNewProduct(product).Result;

                magentoProduct.name = ordoriteitem.name;
                magentoProduct.sku = "mytestSKU-18AUG";
                magentoProduct.weight = 5;// Needs to be added
                magentoProduct.status = global::Magento.RestApi.Models.ProductStatus.Enabled;// Needs to be added
                magentoProduct.price = Convert.ToDouble(ordoriteitem.price);
                //magentoProduct.special_price = 30;// Needs to be added
                //magentoProduct.special_from_date = DateTime.Now; // Needs to be added
                //magentoProduct.special_to_date = DateTime.Now.AddDays(30); // Needs to be added            
                magentoProduct.description = "my test description";
                magentoProduct.type_id = "simple";
                //magentoProduct.visibility = global::Magento.RestApi.Models.ProductVisibility.CatalogSearch;

                //  var responseCreateProduct = client.CreateNewProduct(magentoProduct);

                //responseCreateProduct.Wait();

                //var result = responseCreateProduct.Result;


                // var fidnResponse = client.GetProductBySku("mytestSKU-18AUG");

                // fidnResponse.Wait();

                #region "REST Sharp Sample requests"
                //Example request 

                // add files to upload (works with compatible verbs)
                //request.AddFile(path);

                //// execute the request
                //IRestResponse response = client.Execute(request);
                //var content = response.Content; // raw content as string

                //// or automatically deserialize result
                //// return content type is sniffed but can be explicitly set via RestClient.AddHandler();
                //RestResponse<Person> response2 = client.Execute<Person>(request);
                //var name = response2.Data.Name;

                //// easy async support
                //client.ExecuteAsync(request, response =>
                //{
                //    Console.WriteLine(response.Content);
                //});

                //// async with deserialization
                //var asyncHandle = client.ExecuteAsync<Person>(request, response =>
                //{
                //    Console.WriteLine(response.Data.Name);
                //});

                //example request ends
                #endregion
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "PushData issue", ex);
            }
            finally
            {
                PushinProcess = false;
            }
        }

        public void PushStockLevel(object args)
        {            
            try
            {
                PushStockLevelinProcess = true;
                LogHelper.Info(typeof(Stock), "PushStockLevel");
                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["MagentoCompanyCredentials"];
                //string LastInventorySyncDate = System.Configuration.ConfigurationManager.AppSettings["LastInventoryExportRun"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateMagento"];
                string offset = System.Configuration.ConfigurationManager.AppSettings["MagentoOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["MagentoPageSize"];
                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;


                var requestStockforMagento = new RestRequest("api/pos/GetStockLevelDetailforMagento?offset=" + offset + "&pageSize=" + pagesize, Method.GET);
                requestStockforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforMagento.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemMagento> stockInfoResponseforMagento = goodSideClient.Execute<StockItemMagento>(requestStockforMagento);

                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                var stockItmsInfo = stockInfoResponseforMagento.Data.stockItemInfoforMagento;
                LogHelper.Info(typeof(Stock), "PushStockLevel Stock Count:" + stockItmsInfo.Count);

                List<CSVInventory> objListOfInventory = new List<CSVInventory>();
                CSVInventory csvInventoryInfo;

                string timestamp = DateTime.Now.ToString("MMddyyHHmm");                              

                foreach (var ordoriteitem in stockItmsInfo)
                {
                    csvInventoryInfo = new CSVInventory();

                    //Map Ordorite database information
                    csvInventoryInfo.sku = ordoriteitem.sku.ToString();  // Need TO BE Change         

                    csvInventoryInfo.qty = (string.IsNullOrEmpty(ordoriteitem.qty) ? "0" :ordoriteitem.qty); 

                    csvInventoryInfo.is_in_stock = ordoriteitem.is_in_stock;//  To be set as per unallocatedstocklevel data

                    csvInventoryInfo.allow_backorders = ordoriteitem.allow_backorders;

                    csvInventoryInfo.out_of_stock_qty = ordoriteitem.out_of_stock_qty;

                    csvInventoryInfo.additional_attributes = ordoriteitem.additional_attribute;

                    objListOfInventory.Add(csvInventoryInfo);
                }

                csvInventoryInfo = new CSVInventory();
                var output = csvInventoryInfo.ToCsvHeader();
                output += Environment.NewLine;

                objListOfInventory.ForEach(csvProduct =>
                {
                    output += csvProduct.ToCsvRow();
                    output += Environment.NewLine;
                });

                var CSVFileWithData = Path.Combine(rootPath, "stock_" + timestamp) + ".csv";
                
                if (objListOfInventory.Count >= 1)
                {
                    System.IO.File.WriteAllText(CSVFileWithData, output);

                    bool IsSuccess = sftpUpload(CSVFileWithData, MagentoFtpInventoryFolderName);                                    

                    LogHelper.Info(typeof(Stock), "Inventory CSV Created");

                    if (IsSuccess)
                    {
                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                        // Add an Application Setting.
                        string LastInventoryExport = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        config.AppSettings.Settings["LastInventoryExportRun"].Value = LastInventoryExport;
                        config.Save(ConfigurationSaveMode.Modified, true);
                        // Force a reload of a changed section.
                        ConfigurationManager.RefreshSection("appSettings");                   

                        //if ((System.IO.File.Exists(CSVFileWithData)))
                        //{
                        //    System.IO.File.Delete(CSVFileWithData);
                        //}
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Inventory CSV Generate issue", ex);
            }
            finally
            {
                PushStockLevelinProcess = false;
            }

        }

        #endregion

        #region "Pull Operation"

        public void PullData(object args)
        {
            PullinProcess = true;

            try
            {

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                PullinProcess = false;
            }
        }

        #endregion
    }
}
