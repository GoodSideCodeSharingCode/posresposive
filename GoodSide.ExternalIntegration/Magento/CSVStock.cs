﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GoodSide.ExternalIntegration.Magento
{
    [XmlRoot(ElementName = "CSVProduct")]
    public class CSVProduct
    {
        //[XmlElement(ElementName = "sku")]
        //public string Sku { get; set; }
        //[XmlElement(ElementName = "store_view_code")]
        //public string Store_view_code { get; set; }
        //[XmlElement(ElementName = "attribute_set_code")]
        //public string Attribute_set_code { get; set; }
        //[XmlElement(ElementName = "product_type")]
        //public string Product_type { get; set; }
        //[XmlElement(ElementName = "categories")]
        //public string Categories { get; set; }
        //[XmlElement(ElementName = "product_websites")]
        //public string Product_websites { get; set; }
        //[XmlElement(ElementName = "name")]
        //public string Name { get; set; }
        //[XmlElement(ElementName = "description")]
        //public string Description { get; set; }
        //[XmlElement(ElementName = "short_description")]
        //public string Short_description { get; set; }
        //[XmlElement(ElementName = "weight")]
        //public string Weight { get; set; }
        //[XmlElement(ElementName = "product_online")]
        //public string Product_online { get; set; }
        //[XmlElement(ElementName = "tax_class_name")]
        //public string Tax_class_name { get; set; }
        //[XmlElement(ElementName = "visibility")]
        //public string Visibility { get; set; }
        //[XmlElement(ElementName = "price")]
        //public string Price { get; set; }
        //[XmlElement(ElementName = "special_price")]
        //public string Special_price { get; set; }
        //[XmlElement(ElementName = "special_price_from_date")]
        //public string Special_price_from_date { get; set; }
        //[XmlElement(ElementName = "special_price_to_date")]
        //public string Special_price_to_date { get; set; }
        //[XmlElement(ElementName = "url_key")]
        //public string Url_key { get; set; }
        //[XmlElement(ElementName = "meta_title")]
        //public string Meta_title { get; set; }
        //[XmlElement(ElementName = "meta_keywords")]
        //public string Meta_keywords { get; set; }
        //[XmlElement(ElementName = "meta_description")]
        //public string Meta_description { get; set; }
        //[XmlElement(ElementName = "created_at")]
        //public string Created_at { get; set; }
        //[XmlElement(ElementName = "updated_at")]
        //public string Updated_at { get; set; }
        //[XmlElement(ElementName = "new_from_date")]
        //public string New_from_date { get; set; }
        //[XmlElement(ElementName = "new_to_date")]
        //public string New_to_date { get; set; }
        //[XmlElement(ElementName = "display_product_options_in")]
        //public string Display_product_options_in { get; set; }
        //[XmlElement(ElementName = "map_price")]
        //public string Map_price { get; set; }
        //[XmlElement(ElementName = "msrp_price")]
        //public string Msrp_price { get; set; }
        //[XmlElement(ElementName = "map_enabled")]
        //public string Map_enabled { get; set; }
        //[XmlElement(ElementName = "gift_message_available")]
        //public string Gift_message_available { get; set; }
        //[XmlElement(ElementName = "custom_design")]
        //public string Custom_design { get; set; }
        //[XmlElement(ElementName = "custom_design_from")]
        //public string Custom_design_from { get; set; }
        //[XmlElement(ElementName = "custom_design_to")]
        //public string Custom_design_to { get; set; }
        //[XmlElement(ElementName = "custom_layout_update")]
        //public string Custom_layout_update { get; set; }
        //[XmlElement(ElementName = "page_layout")]
        //public string Page_layout { get; set; }
        //[XmlElement(ElementName = "product_options_container")]
        //public string Product_options_container { get; set; }
        //[XmlElement(ElementName = "msrp_display_actual_price_type")]
        //public string Msrp_display_actual_price_type { get; set; }
        //[XmlElement(ElementName = "country_of_manufacture")]
        //public string Country_of_manufacture { get; set; }
      
        //[XmlElement(ElementName = "qty")]
        //public string Qty { get; set; }
        //[XmlElement(ElementName = "out_of_stock_qty")]
        //public string Out_of_stock_qty { get; set; }
        //[XmlElement(ElementName = "use_config_min_qty")]
        //public string Use_config_min_qty { get; set; }
        //[XmlElement(ElementName = "is_qty_decimal")]
        //public string Is_qty_decimal { get; set; }
        //[XmlElement(ElementName = "allow_backorders")]
        //public string Allow_backorders { get; set; }
        //[XmlElement(ElementName = "use_config_backorders")]
        //public string Use_config_backorders { get; set; }
        //[XmlElement(ElementName = "min_cart_qty")]
        //public string Min_cart_qty { get; set; }
        //[XmlElement(ElementName = "use_config_min_sale_qty")]
        //public string Use_config_min_sale_qty { get; set; }
        //[XmlElement(ElementName = "max_cart_qty")]
        //public string Max_cart_qty { get; set; }
        //[XmlElement(ElementName = "use_config_max_sale_qty")]
        //public string Use_config_max_sale_qty { get; set; }
        //[XmlElement(ElementName = "is_in_stock")]
        //public string Is_in_stock { get; set; }
        //[XmlElement(ElementName = "notify_on_stock_below")]
        //public string Notify_on_stock_below { get; set; }
        //[XmlElement(ElementName = "use_config_notify_stock_qty")]
        //public string Use_config_notify_stock_qty { get; set; }
        //[XmlElement(ElementName = "manage_stock")]
        //public string Manage_stock { get; set; }
        //[XmlElement(ElementName = "use_config_manage_stock")]
        //public string Use_config_manage_stock { get; set; }
        //[XmlElement(ElementName = "use_config_qty_increments")]
        //public string Use_config_qty_increments { get; set; }
        //[XmlElement(ElementName = "qty_increments")]
        //public string Qty_increments { get; set; }
        //[XmlElement(ElementName = "use_config_enable_qty_inc")]
        //public string Use_config_enable_qty_inc { get; set; }
        //[XmlElement(ElementName = "enable_qty_increments")]
        //public string Enable_qty_increments { get; set; }
        //[XmlElement(ElementName = "is_decimal_divided")]
        //public string Is_decimal_divided { get; set; }
        //[XmlElement(ElementName = "website_id")]
        //public string Website_id { get; set; }
        //[XmlElement(ElementName = "deferred_stock_update")]
        //public string Deferred_stock_update { get; set; }
        //[XmlElement(ElementName = "use_config_deferred_stock_update")]
        //public string Use_config_deferred_stock_update { get; set; }
        //[XmlElement(ElementName = "related_skus")]
        //public string Related_skus { get; set; }
        //[XmlElement(ElementName = "crosssell_skus")]
        //public string Crosssell_skus { get; set; }
        //[XmlElement(ElementName = "upsell_skus")]
        //public string Upsell_skus { get; set; }
        //[XmlElement(ElementName = "hide_from_product_page")]
        //public string Hide_from_product_page { get; set; }
        //[XmlElement(ElementName = "custom_options")]
        //public string Custom_options { get; set; }
        //[XmlElement(ElementName = "bundle_price_type")]
        //public string Bundle_price_type { get; set; }
        //[XmlElement(ElementName = "bundle_sku_type")]
        //public string Bundle_sku_type { get; set; }
        //[XmlElement(ElementName = "bundle_price_view")]
        //public string Bundle_price_view { get; set; }
        //[XmlElement(ElementName = "bundle_weight_type")]
        //public string Bundle_weight_type { get; set; }
        //[XmlElement(ElementName = "bundle_values")]
        //public string Bundle_values { get; set; }
        //[XmlElement(ElementName = "associated_skus")]
        //public string Associated_skus { get; set; }
        //[XmlElement(ElementName = "ImagesPaths")]
        //public string ImagesPaths { get; set; }

        [XmlElement(ElementName = "visibility")]
        public string visibility { get; set; }
        [XmlElement(ElementName = "attribute_set_code")]
        public string attribute_set_code { get; set; }
        [XmlElement(ElementName = "product_type")]
        public string product_type { get; set; }
        [XmlElement(ElementName = "name")]
        public string name { get; set; }
        [XmlElement(ElementName = "sku")]
        public string sku { get; set; }
        [XmlElement(ElementName = "status")]
        public string product_online { get; set; }
        [XmlElement(ElementName = "new_from_date")]
        public string new_from_date { get; set; }
        [XmlElement(ElementName = "new_to_date")]
        public string new_to_date { get; set; }
        [XmlElement(ElementName = "categories")]
        public string categories { get; set; }
        [XmlElement(ElementName = "product_websites")]
        public string product_websites { get; set; }
        [XmlElement(ElementName = "created_at")]
        public string created_at { get; set; }
        [XmlElement(ElementName = "updated_at")]
        public string updated_at { get; set; }
        [XmlElement(ElementName = "short_description")]
        public string short_description { get; set; }
        [XmlElement(ElementName = "description")]
        public string description { get; set; }
        [XmlElement(ElementName = "price")]
        public string price { get; set; }
        [XmlElement(ElementName = "special_price")]
        public string special_price { get; set; }
        [XmlElement(ElementName = "special_price_from_date")]
        public string special_price_from_date { get; set; }
        [XmlElement(ElementName = "special_price_to_date")]
        public string special_price_to_date { get; set; }
        [XmlElement(ElementName = "tax_class_name")]
        public string tax_class_name { get; set; }
        [XmlElement(ElementName = "cost")]
        public string cost { get; set; }
        [XmlElement(ElementName = "url_key")]
        public string url_key { get; set; }
        [XmlElement(ElementName = "meta_title")]
        public string meta_title { get; set; }
        [XmlElement(ElementName = "meta_keywords")]
        public string meta_keywords { get; set; }
        [XmlElement(ElementName = "meta_description")]
        public string meta_description { get; set; }
        [XmlElement(ElementName = "qty")]
        public string qty { get; set; }
        [XmlElement(ElementName = "stock_status")]
        public string is_in_stock { get; set; }
        [XmlElement(ElementName = "out_of_stock_qty")]
        public string out_of_stock_qty { get; set; }
        [XmlElement(ElementName = "allow_backorders")]
        public string allow_backorders { get; set; }

        [XmlElement(ElementName = "additional_attributes")]
        public string additional_attributes { get; set; }
    }

    public class CSVProductImage
    {
        public string sku { get; set; }
        public string base_image { get; set; }
        public string base_image_label { get; set; }
        public string small_image { get; set; }
        public string small_image_label { get; set; }
        public string thumbnail_image { get; set; }
        public string thumbnail_image_label { get; set; }
        public string additional_images { get; set; }
        public string additional_images_label { get; set; }       
    }
    public class CSVInventory
    {       
        [XmlElement(ElementName = "sku")]
        public string sku { get; set; }       
        [XmlElement(ElementName = "qty")]
        public string qty { get; set; }
        [XmlElement(ElementName = "stock_status")]
        public string is_in_stock { get; set; }
        [XmlElement(ElementName = "out_of_stock_qty")]
        public string out_of_stock_qty { get; set; }
        [XmlElement(ElementName = "allow_backorders")]
        public string allow_backorders { get; set; }

        [XmlElement(ElementName = "additional_attributes")]
        public string additional_attributes { get; set; }
    }
}
