﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace GoodSide.ExternalIntegration.Magento
{
    [Export(typeof(IIntegrationProvider))]
    public class Order : IIntegrationProvider
    {
        public string ProviderName
        {
            get { return "Order"; }
        }

        public Guid ProviderID
        {
            get { return new Guid("1BBDD522-3EE3-4290-83D1-2633610760BE"); }
        }

        public int PushInterval
        {
            get;
            set;
        }

        public int PullInterval
        {
            get;
            set;
        }

        public AllowedOperation alloweOperation
        {
            get;
            set;
        }       

        public Order()
        {
            PushInterval = 18000000;
            PullInterval = 18000000;
            alloweOperation = AllowedOperation.Push;
        }

        public bool ReadConfiguration()
        {
            return true;
        }

        public void PushData(object args)
        {

        }

        public void PullData(object args)
        {

        }

        public void SchedulePushPull()
        {
            if (ReadConfiguration())
            {

            }
        }
    }
}
