﻿using GoodSide.Domain.Entities;
using GoodSide.Domain.Services;
using GoodSide.ExternalIntegration.General;
using Ninject;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WooCommerceNET;
using WooCommerceNET.WooCommerce;

namespace GoodSide.ExternalIntegration.WooCommerce
{
    [Export(typeof(IIntegrationProvider))]
    public class Stock : IIntegrationProvider
    {
        #region "General Parameters"

        private IStockItemService _stockItemService;

        private static bool PullinProcess = false;

        private static bool PushinProcess = false;

        private static bool PushStockLevelinProcess = false;

        private static bool ExportinProcess = false;

        Timer pushCheck = null;

        Timer pushStockLevelCheck = null;
        Timer pullCheck = null;

        Timer exportCSVCheck = null;

        public int PushInterval
        {
            get;
            set;
        }

        public int PushStockLevelInterval
        {
            get;
            set;
        }

        public int PullInterval
        {
            get;
            set;
        }

        public int ExportCSVInternal
        {
            get;
            set;
        }

        public AllowedOperation alloweOperation
        {
            get;
            set;
        }

        public string ProviderName
        {
            get { return "Stock"; }
        }

        public Guid ProviderID
        {
            get { return new Guid("370A8AE7-458A-429D-9062-AB0EB001EDC7"); }
        }

        public string WooCommerceURL { get; set; }

        public string ConsumerKey { get; set; }

        public string ConsumerSecret { get; set; }

        public string FtpURL { get; set; }

        public string FtpFolderName { get; set; }

        public string FtpImageFolderName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string HTTPSEnable { get; set; }

        public string ServiceBindToChannel { get; set; }

        public string WooCommerceCSVExportDayTime { get; set; }

        public string CSVLastExportRun { get; set; }

        #endregion

        #region "Constructor to set default values"

        public Stock()
        {

        }

        [Inject]
        public Stock(IStockItemService stockItemService)
        {
            _stockItemService = stockItemService;
            PushInterval = 18000000;
            PullInterval = 18000000;
            alloweOperation = AllowedOperation.All;
        }

        #endregion

        #region "General Methods"

        public bool ReadConfiguration()
        {
            try
            {
                PushInterval = 20000;
                PullInterval = 600000;
                ExportCSVInternal = 600000;
                PushStockLevelInterval = 600000;

                PushInterval = PullInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WooCommercePushPullInterval"]);
                PushStockLevelInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WooCommercePushStockLevelInterval"]);
                ExportCSVInternal = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["WooCommerceExportInterval"]);

                FtpURL = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceHostname"]);
                FtpFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceFtpFolderName"]);
                Username = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceUsername"]);
                Password = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommercePassword"]);
                FtpImageFolderName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WooCommerceFtpImageFolderName"]);
                HTTPSEnable = Convert.ToString((System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"] == null ? "false" : System.Configuration.ConfigurationManager.AppSettings["WooCommerceHTTPSEnable"]));
                WooCommerceCSVExportDayTime = Convert.ToString((System.Configuration.ConfigurationManager.AppSettings["WooCommerceCSVExportDayTime"] == null ? "01:02:00" : System.Configuration.ConfigurationManager.AppSettings["WooCommerceCSVExportDayTime"]));
                CSVLastExportRun = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCSVLastExportRun"];

                alloweOperation = AllowedOperation.All;
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Read Configuration issue", ex);
                return false;
            }

        }

        public void SchedulePushPull()
        {


            try
            {
                ServiceBindToChannel = System.Configuration.ConfigurationManager.AppSettings["ServiceBindToChannel"];

                if (ServiceBindToChannel.ToLower() == "woocommerce")
                {

                    if (ReadConfiguration())
                    {

                        if (alloweOperation == AllowedOperation.Push || alloweOperation == AllowedOperation.All)
                        {
                            //start pull process at regular interval
                            TimerCallback callback = PerformPushOperation;
                            pushCheck = new Timer(callback);
                            pushCheck.Change(TimeSpan.FromMilliseconds(PushInterval), TimeSpan.FromMilliseconds(PushInterval));
                        }


                        if (alloweOperation == AllowedOperation.PushStockLevel || alloweOperation == AllowedOperation.All)
                        {
                            //start pull process at regular interval
                            TimerCallback callback = PerformPushStockLevelOperation;
                            pushStockLevelCheck = new Timer(callback);
                            pushStockLevelCheck.Change(TimeSpan.FromMilliseconds(PushStockLevelInterval), TimeSpan.FromMilliseconds(PushStockLevelInterval));
                            //pushCheck = new Timer(new TimerCallback(PushData), null, System.Threading.Timeout.Infinite, PushInterval);
                            //pushCheck.Change(PushInterval, PushInterval);
                        }

                        if (alloweOperation == AllowedOperation.Pull || alloweOperation == AllowedOperation.All)
                        {
                            //start push process at regular interval
                            TimerCallback callback = PerformPullOperation;
                            pullCheck = new Timer(callback);
                            pullCheck.Change(TimeSpan.FromMilliseconds(PullInterval), TimeSpan.FromMilliseconds(PullInterval));
                            //pullCheck = new Timer(new TimerCallback(PullData), null, System.Threading.Timeout.Infinite, PullInterval);
                            //pullCheck.Change(PullInterval, PullInterval);
                        }

                        if (alloweOperation == AllowedOperation.ExportCSV || alloweOperation == AllowedOperation.All)
                        {
                            //start push process at regular interval
                            TimerCallback callback = PerformExportOperation;
                            exportCSVCheck = new Timer(callback);
                            exportCSVCheck.Change(TimeSpan.FromMilliseconds(ExportCSVInternal), TimeSpan.FromMilliseconds(ExportCSVInternal));
                            //exportCSVCheck = new Timer(new TimerCallback(GenerateCSV), null, System.Threading.Timeout.Infinite, ExportCSVInternal);
                            //exportCSVCheck.Change(ExportCSVInternal, ExportCSVInternal);
                        }
                    }
                }
                else
                {
                    LogHelper.Info(typeof(Stock), "woocommerce channel is not currently bind to service");
                    return;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "ScheddulePushCall issue", ex);
            }
        }

        private void PerformPushOperation(object state)
        {
            if (!PushinProcess)
                PushData(null);
            else
                LogHelper.Error(typeof(Stock), " Push already in process so aborting another process");
        }
        private void PerformPushStockLevelOperation(object state)
        {
            if (!PushStockLevelinProcess)
                PushStockLevel(null);
            else
                LogHelper.Error(typeof(Stock), "Push Stock Level already in process so aborting another  process");
        }
        private void PerformPullOperation(object state)
        {
            if (!PullinProcess)
                PullData(null);
            else
                LogHelper.Error(typeof(Stock), "Pull already in process so aborting another  process");
        }
        private void PerformExportOperation(object state)
        {
            if (CheckExportTime() && DateTime.Now.Date != Convert.ToDateTime(CSVLastExportRun).Date)
            {
                if (!ExportinProcess)
                    GenerateCSV(null);
                else
                    LogHelper.Error(typeof(Stock), "Export already in process so aborting another process");
            }
            else
            {
                //var src = DateTime.Now;
                //LogHelper.Error(typeof(Stock), "Export Time is Every Month " + CSVLastExportRun + " and current time is " + src.Hour + ":" + src.Minute + ":" + src.Second);
                LogHelper.Error(typeof(Stock), "Last CSV Export Run DateTime:" + CSVLastExportRun);
            }


        }
        private bool CheckExportTime()
        {

            string[] ExportDayTime = WooCommerceCSVExportDayTime.Split(':');

            DateTime OperationDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, Convert.ToInt32(ExportDayTime[0])); //Set Day of Current Month,Year

            TimeSpan OperationTime = new TimeSpan(Convert.ToInt32(ExportDayTime[1]), Convert.ToInt32(ExportDayTime[2]), 0);

            TimeSpan timeNow = DateTime.Now.TimeOfDay;
            TimeSpan currentTime = new TimeSpan(timeNow.Hours, timeNow.Minutes, timeNow.Seconds);

            if (DateTime.Now.Date == OperationDay && (currentTime >= OperationTime))
                return true;
            else
                return false;
        }


        #endregion

        #region "Push  Operation"

        public void GenerateCSV(object args)
        {
            try
            {
                LogHelper.Info(typeof(Stock), "GenerateCSV Called");
                ExportinProcess = true;

                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateWooCommerce"];
                string offset = System.Configuration.ConfigurationManager.AppSettings["WooCommerceOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["WooCommercePageSize"];

                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;
                LastSyncDate = "26-01-2014 14:21:21";
                var requestStockforWoocommerce = new RestRequest("api/pos/GetStockDetailforWooCommerce?offset=" + offset + "&pageSize=" + pagesize, Method.GET);
                requestStockforWoocommerce.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforWoocommerce.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemWooCommerce> stockInfoResponseforWooCommerce = goodSideClient.Execute<StockItemWooCommerce>(requestStockforWoocommerce);

                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                var stockItmsInfo = stockInfoResponseforWooCommerce.Data.stockItemInfoforWooCommerce;

                LogHelper.Info(typeof(Stock), "GenerateCSV Stock Count:" + stockItmsInfo.Count);

                List<CSVProduct> objListOfProduct = new List<CSVProduct>();
                CSVProduct csvProductInfo;
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string ftpImagePath = "/" + FtpImageFolderName;
                Dictionary<string, string> SkuImagePath =
                                        new Dictionary<string, string>();
                foreach (var ordoriteitem in stockItmsInfo)
                {
                    csvProductInfo = new CSVProduct();

                    //Set default values
                    string path = Path.Combine(rootPath, "DefaultProductImportValues_WooCommerce.xml");
                    XmlSerializer serializer = new XmlSerializer(typeof(CSVProduct));
                    StreamReader reader = new StreamReader(path);
                    csvProductInfo = (CSVProduct)serializer.Deserialize(reader);
                    reader.Close();

                    //Map Ordorite database information
                    csvProductInfo.Productcode = ordoriteitem.part_no;

                    csvProductInfo.ProdCat = ordoriteitem.categoryid;

                    csvProductInfo.Description = ordoriteitem.product_name;

                    csvProductInfo.ProdDesc = ordoriteitem.description;

                    csvProductInfo.BarCode = ordoriteitem.barcode_no;

                    csvProductInfo.SuppId = ordoriteitem.supplier;

                    csvProductInfo.RetailPrice = ordoriteitem.regularprice;

                    csvProductInfo.SalePrice = ordoriteitem.sellingprice;

                    csvProductInfo.StPhysical = (string.IsNullOrEmpty(ordoriteitem.unallocatedstocklevel) ? "0" : Convert.ToInt32(ordoriteitem.unallocatedstocklevel) > 0 ? ordoriteitem.unallocatedstocklevel : "0");

                    csvProductInfo.ImagesPaths = (ordoriteitem.imagepaths.Replace("productimages", ftpImagePath + "/" + ordoriteitem.part_no)).Replace(',', '|');


                    csvProductInfo.short_description = ordoriteitem.short_description;
                    //csvProductInfo.Weight = ordoriteitem.Weight;// To be read from database


                    csvProductInfo.special_price = ordoriteitem.special_price; // to be read from database. Needss to take based on applied promotion

                    if (!string.IsNullOrEmpty(ordoriteitem.special_price))
                        csvProductInfo.special_price_from_date = (ordoriteitem.special_price_from_date == DateTime.MinValue ? string.Empty : ordoriteitem.special_price_from_date.ToShortDateString()); // to be read from database. Needs to take based on applied promotion and promotion start date
                    if (!string.IsNullOrEmpty(ordoriteitem.special_price))
                        csvProductInfo.special_price_to_date = (ordoriteitem.special_price_to_date == DateTime.MinValue ? string.Empty : ordoriteitem.special_price_to_date.ToShortDateString()); // to be read from database. Needs to take based on applied promotion and promotion end date.

                    //csvProductInfo.Created_at = DateTime.Now.ToString(); // To be read fromd database

                    //csvProductInfo.Updated_at = DateTime.Now.ToString(); // To be read fromd database

                    //csvProductInfo.Msrp_price = ordoriteitem.spnow;                  

                    //csvProductInfo.ImagesPaths = ordoriteitem.imagepaths == null ? string.Empty : (ordoriteitem.imagepaths.Replace("productimages", ftpImagePath + "/" + ordoriteitem.part_no)).Replace(',', '|');

                    csvProductInfo.is_in_stock = ordoriteitem.is_in_stock;//  To be set as per unallocatedstocklevel data

                    csvProductInfo.allow_backorders = ordoriteitem.allow_backorders;

                    csvProductInfo.OnWeb = ordoriteitem.OnWeb;

                    if (ordoriteitem.created_at > DateTime.MinValue)
                        csvProductInfo.created_at = ordoriteitem.created_at.ToShortDateString(); //ordoriteitem.stockcreateddate;
                    if (ordoriteitem.updated_at > DateTime.MinValue)
                        csvProductInfo.updated_at = ordoriteitem.updated_at.ToShortDateString();//ordoriteitem.stockmodifieddate;
                    if (ordoriteitem.new_from_date > DateTime.MinValue)
                        csvProductInfo.new_from_date = ordoriteitem.new_from_date.ToShortDateString();
                    if (ordoriteitem.new_to_date > DateTime.MinValue)
                        csvProductInfo.new_to_date = ordoriteitem.new_to_date.ToShortDateString(); ;

                    csvProductInfo.tax_class_name = ordoriteitem.tax_class_name;

                    csvProductInfo.url_key = ordoriteitem.url_key;

                    csvProductInfo.meta_title = ordoriteitem.meta_title;

                    csvProductInfo.meta_keywords = ordoriteitem.meta_keywords;

                    csvProductInfo.meta_description = ordoriteitem.meta_description;


                    if (!string.IsNullOrEmpty(ordoriteitem.visibility))
                    {
                        switch (ordoriteitem.visibility.ToLower())
                        {
                            case "catalogue, search":
                                csvProductInfo.visibility = "visible";
                                break;
                            case "catalogue only":
                                csvProductInfo.visibility = "catalog";
                                break;
                            case "search only":
                                csvProductInfo.visibility = "search";
                                break;
                            case "nowhere":
                                csvProductInfo.visibility = "hidden";
                                break;
                            default:
                                break;
                        }
                    }

                    csvProductInfo.out_of_stock_qty = ordoriteitem.out_of_stock_qty;

                    objListOfProduct.Add(csvProductInfo);

                    if (!SkuImagePath.ContainsKey(ordoriteitem.part_no))
                        SkuImagePath.Add(ordoriteitem.part_no, ordoriteitem.imagepaths);
                    else
                    {
                        var currentimagepaths = SkuImagePath[ordoriteitem.part_no];
                        currentimagepaths += "|" + ordoriteitem.imagepaths;
                        SkuImagePath[ordoriteitem.part_no] = currentimagepaths;
                    }
                }

                csvProductInfo = new CSVProduct();
                var output = csvProductInfo.ToCsvHeader();
                output += Environment.NewLine;

                objListOfProduct.ForEach(csvProduct =>
                {
                    output += csvProduct.ToCsvRow();
                    output += Environment.NewLine;
                });


                var CSVFileWithData = Path.Combine(rootPath, "ordorite_generated_products.csv");

                if (objListOfProduct.Count >= 1)
                {

                    System.IO.File.WriteAllText(CSVFileWithData, output);

                    bool IsSuccess = ftpUpload(CSVFileWithData);

                    ftpimageUpload(SkuImagePath);

                    if (IsSuccess)
                    {

                        System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                        // Add an Application Setting.
                        //config.AppSettings.Settings["LastSyncDateWooCommerce"].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        config.AppSettings.Settings["WooCommerceCSVLastExportRun"].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        CSVLastExportRun = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        config.Save(ConfigurationSaveMode.Modified, true);
                        // Force a reload of a changed section.
                        ConfigurationManager.RefreshSection("appSettings");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Generate CSV issue", ex);
            }
            finally
            {
                ExportinProcess = false;
            }
        }

        public bool ftpUpload(string FiletoUpload)
        {
            try
            {
                byte[] fileBytes = null;

                string fileName = Path.GetFileName(FiletoUpload);

                string fileextention = System.IO.Path.GetExtension(FiletoUpload);

                CreateDirectory();

                using (StreamReader fileStream = new StreamReader(FiletoUpload))
                {

                    fileBytes = Encoding.UTF8.GetBytes(fileStream.ReadToEnd());
                    fileStream.Close();
                }

                //Create FTP Request.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/" + fileName); //add folder name aftre ftpurl
                request.Method = WebRequestMethods.Ftp.UploadFile;

                //Enter FTP Server credentials.
                request.Credentials = new NetworkCredential(Username, Password);
                request.ContentLength = fileBytes.Length;
                request.KeepAlive = false;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileBytes, 0, fileBytes.Length);
                    requestStream.Close();

                }
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                response.Close();

                return true;
            }
            catch (WebException ex)
            {
                LogHelper.Error(typeof(Stock), "FTP upload issue", ex);
                return false;
            }
        }

        public bool ftpimageUpload(Dictionary<string, string> SkuImagePaths)
        {
            try
            {
                CreateImageDirectory();
                foreach (KeyValuePair<string, string> SKU_Imagepath in SkuImagePaths)
                {

                    if (!string.IsNullOrEmpty(SKU_Imagepath.Value))
                    {
                        CreateSKUDirectory(SKU_Imagepath.Key);

                        string[] Images = SKU_Imagepath.Value.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (var image in Images)
                        {
                            string filepath = System.Configuration.ConfigurationManager.AppSettings["ImageRootPath"] + image;
                            byte[] fileBytes = null;

                            bool IsExists = true;

                            string fileName = Path.GetFileName(filepath);

                            string fileextention = System.IO.Path.GetExtension(filepath);

                            using (StreamReader fileStream = new StreamReader(filepath))
                            {

                                fileBytes = File.ReadAllBytes(filepath);
                                fileStream.Close();
                            }

                            //Create FTP Request.
                            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpImageFolderName + "/" + SKU_Imagepath.Key + "/" + fileName); //add folder name aftre ftpurl
                            request.Method = WebRequestMethods.Ftp.UploadFile;

                            //Enter FTP Server credentials.
                            request.Credentials = new NetworkCredential(Username, Password);
                            request.ContentLength = fileBytes.Length;
                            request.KeepAlive = false;

                            using (Stream requestStream = request.GetRequestStream())
                            {
                                requestStream.Write(fileBytes, 0, fileBytes.Length);
                                requestStream.Close();
                            }
                            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                            response.Close();
                        }
                    }
                }
                return true;
            }
            catch (WebException ex)
            {
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected void CreateDirectory()
        {
            if (!DirectoryExists(FtpFolderName))
            {
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpFolderName + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        protected void CreateImageDirectory()
        {
            if (!DirectoryExists(FtpImageFolderName))
            {
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + "/" + FtpImageFolderName + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        public bool DirectoryExists(string FTPDirectory)
        {
            bool directoryExists;

            var filePath = FtpURL + "/" + FTPDirectory + "/";
            var request = (FtpWebRequest)FtpWebRequest.Create(filePath);
            request.Method = WebRequestMethods.Ftp.ListDirectory;
            request.Credentials = new NetworkCredential(Username, Password);

            try
            {
                using (request.GetResponse())
                {
                    directoryExists = true;
                }
            }
            catch (WebException)
            {
                directoryExists = false;
            }

            return directoryExists;
        }

        protected void CreateSKUDirectory(string SKU)
        {
            if (!DirectoryExists((FtpImageFolderName + "/" + SKU)))
            {
                var ImageFolder = "/" + FtpImageFolderName + "/" + SKU;
                var request = (FtpWebRequest)WebRequest.Create(FtpURL + ImageFolder + "/");
                request.Method = WebRequestMethods.Ftp.MakeDirectory;
                request.Credentials = new NetworkCredential(Username, Password);
                using (var resp = (FtpWebResponse)request.GetResponse())
                {
                }
            }
        }

        public void PushData(object args)
        {
            try
            {
                LogHelper.Info(typeof(Stock), "Push Data Called");
                PushinProcess = true;
                string wooCommerceURL = System.Configuration.ConfigurationManager.AppSettings["WooCommerceAPIUrl"];
                string wooCommerceConsumerKey = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerKey"];
                string wooCommerceConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerSecret"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateWooCommerce"];
                bool IsSuccess = false;

                RestAPI rest;

                if (HTTPSEnable.ToLower() == "true") //For HTTS/ TSL Authentication
                    rest = new RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, false);
                else
                    rest = new RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, true);
                WCObject wc = new WCObject(rest);

                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                string offset = System.Configuration.ConfigurationManager.AppSettings["WooCommerceOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["WooCommercePageSize"];
                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;

                LogHelper.Info(typeof(Stock), "PushData stockItemInfoforWooCommerce Token:" + accessToken.ToString());
                var requestStockforMagento = new RestRequest("api/pos/GetStockDetailforWooCommerce?offset=" + offset + "&pageSize=" + pagesize, Method.GET);
                requestStockforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforMagento.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemWooCommerce> stockInfoResponseforMagento = goodSideClient.Execute<StockItemWooCommerce>(requestStockforMagento);
                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var stockItmsInfo = stockInfoResponseforMagento.Data.stockItemInfoforWooCommerce;


                LogHelper.Info(typeof(Stock), "PushData stockItemInfoforWooCommerce Count:" + stockItmsInfo.Count.ToString());
                if (stockItmsInfo.Any())
                {
                    IsSuccess = true;
                }

                StringBuilder lstOfFailedProductAdd = new StringBuilder();
                Dictionary<string, string> SkuImagePath =
                                         new Dictionary<string, string>();
                try
                {
                    foreach (var productInfo in stockItmsInfo)
                    {
                        try
                        {

                            Dictionary<string, string> lstParameters = new Dictionary<string, string>();

                            lstParameters.Add("sku", productInfo.part_no);

                            var resultExistingProduct = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.Product>>(() => wc.GetProducts(lstParameters));
                            LogHelper.Info(typeof(Stock), String.Format("Search with SKU:{0} and Result:{1}", productInfo.part_no, resultExistingProduct.Count));
                            List<WooCommerceNET.WooCommerce.Category> catList; //new List<WooCommerceNET.WooCommerce.Category>();

                            if (resultExistingProduct.Count > 0)
                            {
                                catList = resultExistingProduct[0].categories;
                            }
                            else
                            {
                                catList = new List<WooCommerceNET.WooCommerce.Category>();
                            }

                            if (!string.IsNullOrWhiteSpace(productInfo.categoryid))
                            {
                                //First Find Category if Exists then Set. 
                                Dictionary<string, string> lstParametersCat = new Dictionary<string, string>();
                                lstParametersCat.Add("search", productInfo.categoryid);
                                lstParametersCat.Add("per_page", "100");

                                var categoryList = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.ProductCategory>>(() => wc.GetProductCategories(lstParametersCat));
                                if (categoryList.Any())
                                {
                                    var resultcat = categoryList.Find(c => c.name.ToLower() == productInfo.categoryid.ToLower());
                                    if (resultcat != null)
                                    {
                                        LogHelper.Info(typeof(Stock), String.Format("Added with Category:ID{0} Name{1}", resultcat.id.Value.ToString(), resultcat.name));
                                        WooCommerceNET.WooCommerce.Category objCat = new WooCommerceNET.WooCommerce.Category();
                                        objCat.id = resultcat.id;

                                        if (catList.Count == 0 || (catList.Count > 0 && catList.Find(a => a.id == resultcat.id) == null))
                                            catList.Add(objCat);
                                    }
                                }
                            }

                            //Add new product
                            WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product();

                            p.name = productInfo.product_name;

                            if (!string.IsNullOrWhiteSpace(productInfo.description))
                            {
                                p.description = productInfo.description;
                            }

                            //Append Supplier and Dimention/Color Info
                            string dimentionInfo = Utility.GetDimentionString(productInfo.item_length, productInfo.item_width, productInfo.item_height);
                            StringBuilder footerInfo = new StringBuilder();
                            if (!string.IsNullOrEmpty(productInfo.supplier) || (!string.IsNullOrEmpty(dimentionInfo)) || (!string.IsNullOrEmpty(productInfo.product_colour)))
                            {

                                footerInfo = footerInfo.Append(@"< hr />");
                                footerInfo = footerInfo.Append(@"< p > ");

                                if (!string.IsNullOrEmpty(productInfo.supplier))
                                    footerInfo = footerInfo.AppendFormat(@"Info: {0}  |", productInfo.supplier);

                                if (!string.IsNullOrEmpty(dimentionInfo))
                                {
                                    footerInfo = footerInfo.AppendFormat(@" Dimensions: {0} ", dimentionInfo);
                                    p.dimensions = new WooCommerceNET.WooCommerce.Dimension() { height = productInfo.item_height, length = productInfo.item_length, width = productInfo.item_width };
                                }

                                if (!string.IsNullOrEmpty(productInfo.product_colour))
                                    footerInfo = footerInfo.AppendFormat(@"| Colour: {0} ", productInfo.product_colour);

                                footerInfo = footerInfo.Append("</ p >");
                            }

                            if (footerInfo.Length > 0 && !string.IsNullOrWhiteSpace(productInfo.description))
                            {
                                p.description = p.description + footerInfo.ToString();
                            }
                            //< p > Info: Newgate Clocks Ltd | Dimensions: 17 X 11.7 X 5.5 CM </ p >


                            if (!string.IsNullOrWhiteSpace(productInfo.short_description))
                                p.short_description = productInfo.short_description;


                            //Regular Price// Retail Price
                            if (string.IsNullOrWhiteSpace(productInfo.regularprice))
                            {
                                p.regular_price = null;
                            }
                            else
                            {
                                p.regular_price = Convert.ToDecimal(productInfo.regularprice);
                            }

                            //Selling Price// Sale Price
                            if (string.IsNullOrWhiteSpace(productInfo.sellingprice))
                            {
                                p.sale_price = null;
                            }
                            else
                            {
                                p.sale_price = Convert.ToDecimal(productInfo.sellingprice);
                            }

                            if (p.regular_price == p.sale_price)
                            {
                                p.regular_price = p.sale_price;
                            }

                            p.price = string.IsNullOrEmpty(productInfo.sellingprice) ? 0 : Convert.ToDecimal(productInfo.sellingprice);
                            p.sku = productInfo.part_no;
                            p.in_stock = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? false : ((Convert.ToInt32(productInfo.unallocatedstocklevel) > 0));
                            p.categories = catList;
                            p.slug = productInfo.product_name;
                            p.manage_stock = true;
                            p.stock_quantity = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? 0 : Convert.ToInt32((productInfo.unallocatedstocklevel));
                            p.weight = string.IsNullOrEmpty(productInfo.Weight) ? 0 : Convert.ToDecimal(productInfo.Weight);

                            //Product Color Section
                            if (!string.IsNullOrEmpty(productInfo.product_colour))
                            {
                                //Set Colour Attribute
                                if (resultExistingProduct.Any() && resultExistingProduct[0].attributes.Any() && resultExistingProduct[0].attributes.Find(a => a.name.ToLower() == "colour") != null)
                                {
                                    WooCommerceNET.WooCommerce.Attribute colorAtrribute = resultExistingProduct[0].attributes.Find(a => a.name.ToLower() == "colour");
                                    if (!colorAtrribute.options.Contains(productInfo.product_colour))
                                    {
                                        colorAtrribute.options.Add(productInfo.product_colour);
                                    }
                                    colorAtrribute.visible = true;
                                    p.attributes = resultExistingProduct[0].attributes;
                                }
                                else
                                {
                                    p.attributes = new List<WooCommerceNET.WooCommerce.Attribute>();
                                    p.attributes.Add(new WooCommerceNET.WooCommerce.Attribute() { name = "Colour", options = new List<string> { productInfo.product_colour }, visible = true });
                                }
                            }


                            if (!string.IsNullOrEmpty(productInfo.visibility))
                            {
                                switch (productInfo.visibility.ToLower())
                                {
                                    case "catalogue, search":
                                        p.catalog_visibility = "visible";
                                        break;
                                    case "catalogue only":
                                        p.catalog_visibility = "catalog";
                                        break;
                                    case "search only":
                                        p.catalog_visibility = "search";
                                        break;
                                    case "nowhere":
                                        p.catalog_visibility = "hidden";
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (!String.IsNullOrEmpty(productInfo.OnWeb))
                            {
                                switch (productInfo.OnWeb)
                                {

                                    case "Enabled":
                                        p.status = "publish";
                                        break;
                                    case "Disabled":
                                        p.status = "draft";
                                        break;
                                }
                            }

                            string productrestult = string.Empty;
                            if (resultExistingProduct.Count > 0)
                            {
                                var id = Convert.ToInt32(resultExistingProduct[0].id);
                                productrestult = AsyncHelper.RunSync<string>(() => wc.UpdateProduct(id, p));
                                LogHelper.Info(typeof(Stock), "Updated Product");
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(p.catalog_visibility))
                                    p.catalog_visibility = "visible";

                                p.type = "simple";
                                productrestult = AsyncHelper.RunSync<string>(() => wc.PostProduct(p));
                                LogHelper.Info(typeof(Stock), "Added Product");
                            }
                            if (string.IsNullOrEmpty(productrestult) || productrestult.ToLower().Contains("error"))
                            {
                                lstOfFailedProductAdd.Append(productInfo.id + "|");
                            }
                            SkuImagePath.Add(productInfo.part_no, productInfo.imagepaths);
                        }
                        catch (Exception ex)
                        {
                            LogHelper.Error(typeof(Stock), "Error Single Item", ex);
                            IsSuccess = false;
                        }
                    }
                    ftpimageUpload(SkuImagePath);
                }
                catch (Exception ex)
                {
                    LogHelper.Error(typeof(Stock), "Push Data issue For Products", ex);
                    IsSuccess = false;
                }

                if (stockItmsInfo.Count >= 1)
                {
                    if (IsSuccess)
                    {
                        System.Configuration.Configuration config =
                        ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                        // Add an Application Setting.
                        config.AppSettings.Settings["LastSyncDateWooCommerce"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                        config.Save(ConfigurationSaveMode.Modified, true);
                        // Force a reload of a changed section.
                        ConfigurationManager.RefreshSection("appSettings");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Push Data issue", ex);
            }
            finally
            {
                PushinProcess = false;
            }
        }

        public void PushStockLevel(object args)
        {
            try
            {
                LogHelper.Info(typeof(Stock), "PushStockLevel Called");
                PushStockLevelinProcess = true;
                string wooCommerceURL = System.Configuration.ConfigurationManager.AppSettings["WooCommerceAPIUrl"];
                string wooCommerceConsumerKey = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerKey"];
                string wooCommerceConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerSecret"];
                string LastSyncDate = System.Configuration.ConfigurationManager.AppSettings["LastSyncDateWooCommerce"];
                bool IsSuccess = false;

                RestAPI rest;

                if (HTTPSEnable.ToLower() == "true") //For HTTS/ TSL Authentication
                    rest = new RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, false);
                else
                    rest = new RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret, true);

                WCObject wc = new WCObject(rest);

                string apiURL = System.Configuration.ConfigurationManager.AppSettings["RESTAPIURL"];
                string apiCredentials = System.Configuration.ConfigurationManager.AppSettings["WooCommerceCompanyCredentials"];
                string offset = System.Configuration.ConfigurationManager.AppSettings["WooCommerceOffset"];
                string pagesize = System.Configuration.ConfigurationManager.AppSettings["WooCommercePageSize"];
                var goodSideClient = new RestClient(apiURL);

                //Authenticate
                var request = new RestRequest("api/authenticate/authenticate", Method.POST);
                var bytes = Encoding.UTF8.GetBytes(apiCredentials);
                request.AddHeader("Authorization", "basic " + Convert.ToBase64String(bytes));
                IRestResponse response = goodSideClient.Execute(request);
                var accessToken = response.Headers.Where(p => p.Name == "Token").First().Value;

                var requestStockforMagento = new RestRequest("api/pos/GetStockDetailforWooCommerce?offset=" + offset + "&pageSize=" + pagesize, Method.GET);
                LastSyncDate = "26-01-2014 14:21:21";
                requestStockforMagento.AddParameter("LastSyncDate", LastSyncDate, ParameterType.QueryString);
                requestStockforMagento.AddHeader("Authorization", accessToken.ToString());
                IRestResponse<StockItemWooCommerce> stockInfoResponseforMagento = goodSideClient.Execute<StockItemWooCommerce>(requestStockforMagento);
                var rootPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                var stockItmsInfo = stockInfoResponseforMagento.Data.stockItemInfoforWooCommerce;

                LogHelper.Info(typeof(Stock), "PushStockLevel Count:" + stockItmsInfo.Count);

                if (stockItmsInfo.Any())
                {
                    List<WooCommerceNET.WooCommerce.Product> allWooProducts = new List<WooCommerceNET.WooCommerce.Product>();

                    try
                    {
                        for (int i = 1; i < 12; i++)
                        {
                            try
                            {

                                Dictionary<string, string> lstParametersSKU = new Dictionary<string, string>();
                                lstParametersSKU.Add("page", i.ToString());   //page no
                                lstParametersSKU.Add("per_page", "100");   // page size

                                var resultExistingProduct = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.Product>>(() => wc.GetProducts(lstParametersSKU));
                                allWooProducts.AddRange(resultExistingProduct);
                                Thread.Sleep(3000);
                            }
                            catch (Exception ex)
                            {
                                LogHelper.Error(typeof(Stock), "Read All Products Error On Page:" + ex.ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.Error(typeof(Stock), "Exception On Get All Products:" + ex.ToString());
                    }

                    if (allWooProducts.Count > 0)
                    {
                        LogHelper.Info(typeof(Stock), "allWooProducts.Count:" + allWooProducts.Count);

                        try
                        {

                            List<WooCommerceNET.WooCommerce.Product> productList = new List<WooCommerceNET.WooCommerce.Product>();
                            string productTable = "";
                            foreach (var productInfo in stockItmsInfo)
                            {
                                try
                                {
                                    //Find ProductId from WooCommerce List
                                    WooCommerceNET.WooCommerce.Product wproduct = allWooProducts.Where(p => p.sku == productInfo.part_no).FirstOrDefault();

                                    if (wproduct != null)
                                    {
                                        productTable = productTable + productInfo.id.ToString() + "," + wproduct.id.ToString() + "|";
                                        //prepare batch update.
                                        WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product();
                                        p.id = wproduct.id;
                                        p.stock_quantity = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? 0 : Convert.ToInt32((productInfo.unallocatedstocklevel));

                                        //Regular Price// Retail Price
                                        if (string.IsNullOrWhiteSpace(productInfo.regularprice))
                                        {
                                            p.regular_price = null;
                                        }
                                        else
                                        {
                                            p.regular_price = Convert.ToDecimal(productInfo.regularprice);
                                        }

                                        //Selling Price// Sale Price
                                        if (string.IsNullOrWhiteSpace(productInfo.sellingprice))
                                        {
                                            p.sale_price = null;
                                        }
                                        else
                                        {
                                            p.sale_price = Convert.ToDecimal(productInfo.sellingprice);
                                        }

                                        if (p.regular_price == p.sale_price)
                                        {
                                            p.regular_price = p.sale_price;
                                        }
                                        productList.Add(p);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogHelper.Error(typeof(Stock), "PushStockLevel Stock Item Error:" + ex.ToString());
                                }
                            }

                            int numberOfObjectsPerPage = 100;
                            int TotalPages = productList.Count / numberOfObjectsPerPage;
                            if (productList.Count % numberOfObjectsPerPage != 0)
                            {
                                TotalPages = TotalPages + 1;
                            }
                            LogHelper.Info(typeof(Stock), "Batch Stock Total Counts:" + productList.Count);
                            LogHelper.Info(typeof(Stock), "Batch Stock Page Counts:" + TotalPages);

                            for (int i = 0; i < TotalPages; i++)
                            {
                                try
                                {
                                  LogHelper.Info(typeof(Stock), "Batch Stock Page Process:" + i);

                                    var queryResultPage = productList
                                  .Skip(numberOfObjectsPerPage * i)
                                  .Take(numberOfObjectsPerPage).ToList();

                                    WooCommerceNET.Base.BatchObject<WooCommerceNET.WooCommerce.Product> batchproducts = new WooCommerceNET.Base.BatchObject<WooCommerceNET.WooCommerce.Product>();
                                    //Start batch update
                                    batchproducts.update = queryResultPage;
                                    var resultAttributes = AsyncHelper.RunSync<String>(() => wc.UpdateProducts(batchproducts));
                                    LogHelper.Info(typeof(Stock), "Batch Stock Page Process Completed:" + i + " Records " + queryResultPage.Count);
                                }
                                catch (Exception ex)
                                {
                                    LogHelper.Error(typeof(Stock), "PushStockLevel Stock Page Process Error:"+ i.ToString() + "  " + ex.ToString());
                                }
                            }

                            //Update Stock_Main

                            var requestSetStockChannelDataId = new RestRequest("api/pos/SetStockChannelDataId", Method.POST);
                            requestStockforMagento.AddParameter("Ids", productTable, ParameterType.QueryString);
                            requestStockforMagento.AddParameter("source", "woocommerce", ParameterType.QueryString);

                            IRestResponse responseSetStockChannelDataId = goodSideClient.Execute(requestSetStockChannelDataId);
                        }
                        catch (Exception ex)
                        {
                            LogHelper.Error(typeof(Stock), "PushStockLevel Stock Batch Update Error:" + ex.ToString());
                        }

                        //try
                        //    {



                        //        foreach (var productInfo in stockItmsInfo)
                        //        {
                        //            try
                        //            {
                        //                Dictionary<string, string> lstParameters = new Dictionary<string, string>();

                        //                lstParameters.Add("sku", productInfo.part_no);

                        //                var resultExistingProduct = AsyncHelper.RunSync<List<WooCommerceNET.WooCommerce.Product>>(() => wc.GetProducts(lstParameters));

                        //                //Add new product
                        //                WooCommerceNET.WooCommerce.Product p = new WooCommerceNET.WooCommerce.Product();
                        //                p.stock_quantity = string.IsNullOrEmpty(productInfo.unallocatedstocklevel) ? 0 : Convert.ToInt32((productInfo.unallocatedstocklevel));

                        //                //Regular Price// Retail Price
                        //                if (string.IsNullOrWhiteSpace(productInfo.regularprice))
                        //                {
                        //                    p.regular_price = null;
                        //                }
                        //                else
                        //                {
                        //                    p.regular_price = Convert.ToDecimal(productInfo.regularprice);
                        //                }

                        //                //Selling Price// Sale Price
                        //                if (string.IsNullOrWhiteSpace(productInfo.sellingprice))
                        //                {
                        //                    p.sale_price = null;
                        //                }
                        //                else
                        //                {
                        //                    p.sale_price = Convert.ToDecimal(productInfo.sellingprice);
                        //                }

                        //                if (p.regular_price == p.sale_price)
                        //                {
                        //                    p.regular_price = p.sale_price;
                        //                }

                        //                string productrestult = string.Empty;
                        //                if (resultExistingProduct.Count > 0)
                        //                {
                        //                    var id = Convert.ToInt32(resultExistingProduct[0].id);
                        //                    productrestult = AsyncHelper.RunSync<string>(() => wc.UpdateProduct(id, p));
                        //                }
                        //                IsSuccess = true;
                        //            }
                        //            catch (Exception ex)
                        //            {
                        //                LogHelper.Error(typeof(Stock), "PushStockLevel Stock Item Error:" + ex.ToString());
                        //            }
                        //        }

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        LogHelper.Error(typeof(Stock), "PushStockLevel Error:" + ex.ToString());
                        //        IsSuccess = false;
                        //    }

                        //if (stockItmsInfo.Count >= 1)
                        //{
                        //    //if (IsSuccess)
                        //    //{
                        //    //    System.Configuration.Configuration config =
                        //    //    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                        //    //    // Add an Application Setting.
                        //    //    //config.AppSettings.Settings["LastSyncDateWooCommerce"].Value = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss");
                        //    //    //config.Save(ConfigurationSaveMode.Modified, true);
                        //    //    // Force a reload of a changed section.
                        //    //    ConfigurationManager.RefreshSection("appSettings");
                        //    //}
                        //}
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(typeof(Stock), "Push Stock Data issue", ex);
            }
            finally
            {
                PushStockLevelinProcess = false;
            }

        }

        public string CreateProducts(List<WooCommerceNET.WooCommerce.Product> lstOfProducts)
        {
            try
            {
                StringBuilder lstOfFailedProductAdd = new StringBuilder();
                string wooCommerceURL = System.Configuration.ConfigurationManager.AppSettings["WooCommerceAPIUrl"];
                string wooCommerceConsumerKey = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerKey"];
                string wooCommerceConsumerSecret = System.Configuration.ConfigurationManager.AppSettings["WooCommerceConsumerSecret"];

                RestAPI rest = new RestAPI(wooCommerceURL, wooCommerceConsumerKey, wooCommerceConsumerSecret);
                WCObject wc = new WCObject(rest);

                foreach (var item in lstOfProducts)
                {
                    var addProduct = wc.PostProduct(item);
                    addProduct.Wait();

                    if (addProduct.Exception != null)
                    {
                        lstOfFailedProductAdd.Append(item.id + "|");
                    }
                }

                return lstOfFailedProductAdd.ToString();
            }
            catch (Exception)
            {
                var result = string.Join("|", lstOfProducts
                                  .Select(p => p.id.ToString()));
                return result;
            }
        }

        #endregion

        #region "Pull Operation"

        public void PullData(object args)
        {
            PullinProcess = true;

            try
            {

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                PullinProcess = false;
            }
        }

        #endregion
    }

    internal static class AsyncHelper
    {
        private static readonly TaskFactory _myTaskFactory = new
          TaskFactory(CancellationToken.None,
                      TaskCreationOptions.None,
                      TaskContinuationOptions.None,
                      TaskScheduler.Default);

        public static TResult RunSync<TResult>(Func<Task<TResult>> func)
        {
            return AsyncHelper._myTaskFactory
              .StartNew<Task<TResult>>(func)
              .Unwrap<TResult>()
              .GetAwaiter()
              .GetResult();
        }

        public static void RunSync(Func<Task> func)
        {
            AsyncHelper._myTaskFactory
              .StartNew<Task>(func)
              .Unwrap()
              .GetAwaiter()
              .GetResult();
        }
    }

    public static class Utility
    {
        public static string GetDimentionString(string item_length, string item_width, string item_height)
        {
            //Format :- Length X Width X Hight CM
            if (!string.IsNullOrEmpty(item_length) || !string.IsNullOrEmpty(item_width) || !string.IsNullOrEmpty(item_height))
                return string.Format("W {0}cm  D {1}cm  H {2}cm", item_width, item_length, item_height);
            else
                return null;
        }
    }
}
