﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GoodSide.ExternalIntegration.WooCommerce
{
    [XmlRoot(ElementName = "CSVProduct")]
    public class CSVProduct
    {
        [XmlElement(ElementName = "Description")]
        public string Description { get; set; }
        [XmlElement(ElementName = "Image")]
        public string Image { get; set; }
        [XmlElement(ElementName = "ProdType")]
        public string ProdType { get; set; }
        [XmlElement(ElementName = "Productcode")]
        public string Productcode { get; set; }
        [XmlElement(ElementName = "BarCode")]
        public string BarCode { get; set; }
        [XmlElement(ElementName = "ProdDesc")]
        public string ProdDesc { get; set; }
        [XmlElement(ElementName = "AdditionalInfo")]
        public string AdditionalInfo { get; set; }
        [XmlElement(ElementName = "Colour")]
        public string Colour { get; set; }
        [XmlElement(ElementName = "Dimensions")]
        public string Dimensions { get; set; }
        [XmlElement(ElementName = "Range")]
        public string Range { get; set; }
        [XmlElement(ElementName = "Company")]
        public string Company { get; set; }
        [XmlElement(ElementName = "OnWeb")]
        public string OnWeb { get; set; }
        [XmlElement(ElementName = "SuppId")]
        public string SuppId { get; set; }
        [XmlElement(ElementName = "RetailPrice")]
        public string RetailPrice { get; set; }
        [XmlElement(ElementName = "WebPrice")]
        public string WebPrice { get; set; }
        [XmlElement(ElementName = "SalePrice")]
        public string SalePrice { get; set; }
        [XmlElement(ElementName = "OnSale")]
        public string OnSale { get; set; }
        [XmlElement(ElementName = "StPhysical")]
        public string StPhysical { get; set; }
        [XmlElement(ElementName = "StLoan")]
        public string StLoan { get; set; }
        [XmlElement(ElementName = "WorkshopRepair")]
        public string WorkshopRepair { get; set; }
        [XmlElement(ElementName = "StAllocated")]
        public string StAllocated { get; set; }
        [XmlElement(ElementName = "StFree")]
        public string StFree { get; set; }
        [XmlElement(ElementName = "StOnOrder")]
        public string StOnOrder { get; set; }
        [XmlElement(ElementName = "Room")]
        public string Room { get; set; }
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "ShippingClassName")]
        public string ShippingClassName { get; set; }
        [XmlElement(ElementName = "ProdCat")]
        public string ProdCat { get; set; }
        [XmlElement(ElementName = "ProdSub-Cat1")]
        public string ProdSubCat1 { get; set; }
        [XmlElement(ElementName = "ProdSub-Cat2")]
        public string ProdSubCat2 { get; set; }
        [XmlElement(ElementName = "ProdSub-Cat3")]
        public string ProdSubCat3 { get; set; }
        [XmlElement(ElementName = "ProdSub-Cat4")]
        public string ProdSubCat4 { get; set; }
        [XmlElement(ElementName = "ImageName")]
        public string ImageName { get; set; }
       

        [XmlElement(ElementName = "short_description")]
        public string short_description { get; set; }      
        [XmlElement(ElementName = "special_price")]
        public string special_price { get; set; }
        [XmlElement(ElementName = "special_price_from_date")]
        public string special_price_from_date { get; set; }
        [XmlElement(ElementName = "special_price_to_date")]
        public string special_price_to_date { get; set; }
        [XmlElement(ElementName = "tax_class_name")]
        public string tax_class_name { get; set; }
        [XmlElement(ElementName = "cost")]
        public string cost { get; set; }
        [XmlElement(ElementName = "url_key")]
        public string url_key { get; set; }
        [XmlElement(ElementName = "meta_title")]
        public string meta_title { get; set; }
        [XmlElement(ElementName = "meta_keywords")]
        public string meta_keywords { get; set; }
        [XmlElement(ElementName = "meta_description")]
        public string meta_description { get; set; }       
        [XmlElement(ElementName = "stock_status")]
        public string is_in_stock { get; set; }
        [XmlElement(ElementName = "out_of_stock_qty")]
        public string out_of_stock_qty { get; set; }
        [XmlElement(ElementName = "allow_backorders")]
        public string allow_backorders { get; set; }

        [XmlElement(ElementName = "new_from_date")]
        public string new_from_date { get; set; }
        [XmlElement(ElementName = "new_to_date")]
        public string new_to_date { get; set; }        
        [XmlElement(ElementName = "created_at")]
        public string created_at { get; set; }
        [XmlElement(ElementName = "updated_at")]
        public string updated_at { get; set; }
        [XmlElement(ElementName = "visibility")]
        public string visibility { get; set; }
        [XmlElement(ElementName = "ImagesPaths")]
        public string ImagesPaths { get; set; }
    }
}
