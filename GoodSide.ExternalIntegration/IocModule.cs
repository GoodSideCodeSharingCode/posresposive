﻿using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Conventions;
using GoodSide.Domain;
using GoodSide.Domain.Services;
using GoodSide.ExternalIntegration.Magento;
using GoodSide.Data.Repositories;
using GoodSide.Domain.Repositories;
namespace GoodSide.ExternalIntegration
{
    public class IocModule : NinjectModule
    {
        // Bind Interfaces to implementations for dependancy injection
        public override void Load()
        {
            //Kernel.Bind(x => x.FromAssembliesMatching("*").SelectAllClasses().BindDefaultInterface());
            Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            Bind<IStockItemService>().To<StockItemService>();            
            ///Bind<ist>().To<StockItemService>();
            Bind<Stock>().ToSelf().InSingletonScope();
            Bind<Order>().ToSelf().InSingletonScope();
        }
    }
}
